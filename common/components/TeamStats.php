<?php

namespace common\components;
use Yii;
use yii\base\Component;
use yii\base\Widget;
use yii\helpers\Html;
use  yii\helpers\Url;

use common\models\User;
use common\models\Team;
use common\models\Links;
use common\models\Letters;
use common\models\TeamMembers;
use yii\helpers\ArrayHelper;

class TeamStats  extends Component{
        public $teamId;
	    public $start_date;
	    public $end_date;
	    public $users;


	    public function init(){

		        parent::init();
                $this->start_date=strtotime("-7 day");
                $this->end_date=strtotime("Today");
		}
        public  function genTeamStat($teamId) {
		        $this->teamId=$teamId;		        $this->users=$this->getUsers();
                $stats['links_total_sent']=(int)Links::find()->andwhere(['letterId'=>0])->andwhere(['IN','userId',$this->users])
                ->andFilterWhere(['between','created_at',$this->start_date,$this->end_date])->count();
                $stats['links_total_opened']=(int)Links::find()->andwhere(['letterId'=>0,'isOpened'=>1])->andwhere(['IN','userId',$this->users])
                ->andFilterWhere(['between','created_at',$this->start_date,$this->end_date])->count();
                $stats['links_conversion']=$stats['links_total_opened']=='0'?'0':round((( $stats['links_total_sent'] / $stats['links_total_opened']) * 100),1);

                $stats['letters_total_sent']=(int)Letters::find()->joinWith('links',true,'RIGHT JOIN')->andwhere(['IN','letters.userId',$this->users])
                ->andFilterWhere(['between','letters.created_at',$this->start_date,$this->end_date])->groupby('letterId')->count();
                $stats['letters_total_opened']=(int)Letters::find()->joinWith('links',true,'RIGHT JOIN')->andwhere(['links.isOpened'=>1])->andwhere(['IN','letters.userId',$this->users])
                ->andFilterWhere(['between','letters.created_at',$this->start_date,$this->end_date])->groupby('letterId')->count();
                $stats['letters_conversion']=$stats['letters_total_opened']=='0'?'0':round((( $stats['letters_total_sent'] / $stats['letters_total_opened']) * 100),1);

                $stats['lettersnoatt_total_sent']=(int)Letters::find()->joinWith('links')->where('links.id is null')->andwhere(['IN','letters.userId',$this->users])
                ->andFilterWhere(['between','letters.created_at',$this->start_date,$this->end_date])->count();
                $stats['lettersnoatt_total_opened']=(int)Letters::find()->joinWith('links')->where('links.id is null')->andwhere(['letters.isOpened'=>1])->andwhere(['IN','letters.userId',$this->users])
                ->andFilterWhere(['between','letters.created_at',$this->start_date,$this->end_date])->groupby('letterId')->count();
                $stats['lettersnoatt_conversion']=$stats['lettersnoatt_total_opened']=='0'?'0':round((( $stats['lettersnoatt_total_sent'] / $stats['lettersnoatt_total_opened']) * 100),1);
                //print_r($stats);
                $this->sendMail($stats);
        }
        private function getUsers()
        {        	return ArrayHelper::map(User::find()->joinWith('teams',true,'RIGHT JOIN')->where(['teamId' => $this->teamId])->groupBy('user.id')->asArray()->all(),'','id');
        }
        private function sendMail($stats)
        {
           $stats['start_date']=$this->start_date;
           $stats['end_date']=$this->end_date;
           $team=Team::findOne($this->teamId);
           $subQuery=TeamMembers::find()->select('memberId')->where(['teamId' => $this->teamId,'member_status'=>1])->groupBy('memberId');
           $managers=User::find()->where(['in', 'id', $subQuery])->all();
           foreach($managers as $user)
           {           	      echo $user->email."\n";
                   $mail= Yii::$app->mailer->compose('team-stat',['team'=>$team,'stats'=>$stats])
                  ->setTo($user->email)
                  ->setFrom(Yii::$app->params['supportEmail'])
                  ->setSubject('Еженедельная статистика по команде '.$team->name)
		          ->send();
           }
        }
}
