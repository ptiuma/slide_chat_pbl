<?php

namespace common\components\payments;
use Yii;
use yii\base\Component;
use yii\base\Widget;
use yii\helpers\Html;
use  yii\helpers\Url;
use common\models\Payment;

class Payonline  extends Component{
	    public $MerchantId='73882';
		public $PrivateSecurityKey='dce66ef9-eaf0-4d76-b91d-3dfbd36c4873';
		public $Currency='RUB';
		public $Language='ru';
		public $OrderId;
		public $OrderDescription;
		public $ValidUntil;
		public $Amount;
		public $ReturnUrl;
		public $FailUrl;
		public $TransactionId;
		public $Ip;
		public $Email ;

		public $TermUrl;


		  public function init(){

		        parent::init();
                $this->FailUrl=Url::to(['/'],true);
                $this->ReturnUrl=Url::to(['/'],true);
		    }

	    public function makeUrl($params=[]) {	    	 $this->Amount=number_format($params['Amount'], 2, '.', '');
	    	 $this->OrderId=$params['OrderId'];
	    	 $this->OrderDescription=$params['OrderDescription'];

		    $params	 = 'MerchantId='. $this->MerchantId;
			$params .= '&OrderId='.$this->OrderId;
			$params .= '&Amount='. $this->Amount;
			$params .= '&Currency='. $this->Currency;
			if ($this->ValidUntil)
				{
				$params .= '&ValidUntil=' . $this->ValidUntil;
				}
			    if (strlen($this->OrderDescription))
				{
				$params .= '&OrderDescription=' . $this->OrderDescription;
				}
			$params .= '&PrivateSecurityKey=' . $this->PrivateSecurityKey;
			//echo $params;
			$SecurityKey=md5($params);
			//$this->saveSecurityKey($this->OrderId,$SecurityKey);
			$Paymenturl="https://secure.payonlinesystem.com/".$this->Language."/payment/";
			$url_query= "?MerchantId=".$this->MerchantId."&OrderId=".urlencode($this->OrderId)."&Amount=".$this->Amount."&Currency=".$this->Currency;
			if ($this->ValidUntil) {$url_query.= "&ValidUntil=".urlencode($this->ValidUntil);}
			if ($this->OrderDescription) {$url_query.= "&OrderDescription=".urlencode($this->OrderDescription);}
			if ($this->ReturnUrl) {$url_query.= "&ReturnUrl=".urlencode($this->ReturnUrl);}
			if ($this->FailUrl) {$url_query.= "&FailUrl=".urlencode($this->FailUrl);}
			$url_query.="&SecurityKey=".$SecurityKey;
			$url_full=$Paymenturl.$url_query;


	return $url_full;
    }
     protected static function saveSecurityKey($OrderId,$SecurityKey)
     {        $order=Payment::findOne($OrderId);
        $order->SecurityKey=$SecurityKey;
        $order->save();
     }

}
