<?php

namespace common\components\payments;
use Yii;
use yii\base\Component;
use yii\base\Widget;
use yii\helpers\Html;
use  yii\helpers\Url;
use common\models\Payment;

class PaymentHelpers  extends Component{

	    public static function calcPaidDate($orderId) {
		$order=Payment::findOne(['id'=>$orderId,'isPaid'=>1]);
		if($order->months>0)
		{
		        $paid_date=$order->user->paid_date>time()?strtotime( "+".$order->months." month"):strtotime( "+".$order->months." month",$order->user->paid_date);
		        $order->user->paid_date=$paid_date;
		        $order->user->save();
		        print_r($order->user->getErrors());
        }
    }
}
