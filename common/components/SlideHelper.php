<?php


namespace common\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;
use Yii;
use common\models\User;
use common\models\MailLog;

/**
 * Class SlideHelper
 * @package common\components\SlideHelper
 */
class SlideHelper extends Component{

    public function SendLetter($model){

        /*   Yii::$app->mailer->htmlLayout='layouts/html2';
         $mail= Yii::$app->mailer->compose('letter-body',['model'=>$model])
                ->setTo($this->RFCEmail($model->email_to))
               ->setFrom($model->user->email)
                ->setSubject($model->email_subject)
           ->setCc($this->RFCEmail($model->email_copy))
           ->send();
          */
           //echo 'sdf';
          if($model->user->smtp->id&&$model->user->smtp->isActive==1)
          {

          	  $provider='smtp';
          	  Yii::$app->swiftmailer->useFileTransport=false;
			  Yii::$app->swiftmailer->htmlLayout='layouts/html2';
              Yii::$app->swiftmailer->setTransport(['class' => 'Swift_SmtpTransport', 'host' => $model->user->smtp->smtp_server,
              'username' => $model->user->smtp->email_address,
              'password' => $model->user->smtp->email_password,
              'port' => $model->user->smtp->port,
              'encryption' => $model->user->smtp->encryption==1?'ssl':false]);
			     $mail= \Yii::$app->swiftmailer
			    ->compose('letter-body', ['model' => $model])
			     ->setFrom($model->user->smtp->email_address)
			     ->setSubject($model->email_subject)
			    ->setTo($this->RFCEmail($model->email_to))
			    ->setCC($this->RFCEmail($model->email_copy));
			    $response=$mail->send();

               // Yii::warning($mail);
          }
          else
          {
          $provider='mailgun';
           Yii::$app->mailer->htmlLayout='layouts/html2';
           $mail=Yii::$app->mailer->compose('letter-body',['model'=>$model])
                ->setTo($model->email_to)
                ->setFrom($model->user->email)
                ->setSubject($model->email_subject)
                ->setCc($model->email_copy)
                ->send();
         }
           $log=new MailLog;
           $log->userId=$model->userId;
           $log->letterId=$model->id;
           $log->messageId=$provider=='smtp'?'':$mail->http_response_body->id;
           $log->provider=$provider;
           $log->save();
         //  print_r($log->getErrors());

            return true;



    }
   public function SendMailingLetter($model){

      if($model->mailing_use_smtp==1)
      {
      	    $provider='mailgun';
          if($model->mailing_layout==1)
           Yii::$app->mailer->htmlLayout='layouts/html';
           else  Yii::$app->mailer->htmlLayout='layouts/html2';
           $mail=Yii::$app->mailer->compose('letter-body',['model'=>$model])
                ->setTo($model->email_to)
                ->setFrom(!$model->mailing_from?Yii::$app->params['supportEmail']:$model->mailing_from)
                ->setSubject($model->email_subject)
                ->send();
        }
        else if($model->mailing_use_smtp==2&&$model->user->smtp->id&&$model->user->smtp->isActive==1)
          {

          	  $provider='smtp';
          	  Yii::$app->swiftmailer->useFileTransport=false;
			  Yii::$app->swiftmailer->htmlLayout='layouts/html2';
              Yii::$app->swiftmailer->setTransport(['class' => 'Swift_SmtpTransport', 'host' => $model->user->smtp->smtp_server,
              'username' => $model->user->smtp->email_address,
              'password' => $model->user->smtp->email_password,
              'port' => $model->user->smtp->port,
              'encryption' => $model->user->smtp->encryption==1?'ssl':false]);
			     $mail= \Yii::$app->swiftmailer
			    ->compose('letter-body', ['model' => $model])
			     ->setFrom($model->user->smtp->email_address)
			     ->setSubject($model->email_subject)
			    ->setTo($this->RFCEmail($model->email_to));
			    $mail->send();
        }
            return true;



    }
   public function SendViewAlert($model,$LetterOpened=0){
             Yii::$app->mailer->compose('view-alert',['model'=>$model,'LetterOpened'=>$LetterOpened])
                ->setTo($model->user->email)
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setSubject($LetterOpened==1?'Извещение об открытии письма':'Извещение о просмотре документа')
                ->send();
           $this->SendTelegramAlert($model,$LetterOpened);
            return true;

    }
   public function SendEndViewAlert($model){
             Yii::$app->mailer->compose('end-view-alert',['model'=>$model])
                ->setTo($model->link->user->email)
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setSubject('Извещение об окончании просмотра документа')
                ->send();
            return true;

    }
    private  function SendTelegramAlert($model,$LetterOpened=0){
		     if($model->user->telegram->chat_id)
		     {
		    	 $msg=$LetterOpened==1?'Клиент открыл ваше письмо':($model->doc->noParsed==1?'Клиент скачал документ':'Клиент просматривает документ');
		      	 $msg.="\n".'Документ: '.$model->doc->name;
		      	 $msg.="\n".'Подробности в разделе "Активность"';
		        	try
		        	{
		        	\Yii::$app->bot->sendMessage($model->user->telegram->chat_id, $msg);
		        		}
						 catch (\TelegramBot\Api\HttpException $e) {

						}
		    }
    }


  private function RFCEmail($email)
    {
          if(!$email) return null;
           $emails=explode(',',$email);
            return $emails;
             return implode(", ", $emails);
            return "'" . implode("','", $emails) . "'";

    }
}