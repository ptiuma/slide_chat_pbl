<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
   'language' => 'ru_RU',
  'modules' => [


    'user' => [
        'class' => 'dektrium\user\Module',
        'enableConfirmation'=>true,
        'enableUnconfirmedLogin'=>true,
        'enableFlashMessages'=>false,
        'modelMap' => [
            'User' => [
                'class' => 'common\models\User',

            ],
            'Profile' => [
                'class' => 'common\models\Profile',

            ],
             'LoginForm' => [
                'class' => 'common\models\LoginForm',

            ],
             'RegistrationForm' => ['class' => 'frontend\models\RegistrationForm'],
               'SettingsForm' => ['class' => 'frontend\models\SettingsForm'],
        ],

    ],
],
    'components' => [
   'bot' => [
       'class' => 'common\components\TelegramBot',
       'apiToken' => '144198363:AAH9F9C0hCBqdJTICo1CL1gtyC-hdWD0EtI',
   ],
   'payonline' => [
       'class' => 'common\components\payments\Payonline',
   ],
       'slidehelper' => [

            'class' => 'common\components\SlideHelper',

            ],
          'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
                'keyStorage'=>[
            'class'=>'common\components\keyStorage\KeyStorage'
        ],
        'i18n' => [
            'translations' => [
              'daysago*' => [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@sfedosimov/daysago/messages',
            'sourceLanguage' => 'ru',
        ],
                'app'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@common/messages',
                ],
                '*'=> [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@common/messages',
                    'fileMap'=>[
                        'common'=>'common.php',
                        'backend'=>'backend.php',
                        'frontend'=>'frontend.php',
                    ]
                ],


            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ]
    ],
];
