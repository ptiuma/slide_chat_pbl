<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "team_members".
 *
 * @property integer $teamId
 * @property integer $memberId
 * @property integer $member_status
 */
class TeamMembers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team_members';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teamId', 'memberId', 'member_status'], 'required'],
            [['teamId', 'memberId', 'member_status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teamId' => Yii::t('app', 'Team ID'),
            'memberId' => Yii::t('app', 'Member ID'),
            'member_status' => Yii::t('app', 'Member Status'),
        ];
    }
}
