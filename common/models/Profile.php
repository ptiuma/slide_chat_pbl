<?php
namespace common\models;
use common\models\User;
use frontend\models\Settings;
use frontend\models\TelegramToken;
use common\models\Logger;
use common\models\Documents;
use common\models\Letters;
use common\models\Links;
use common\models\DocumentsPages;
use Yii;


use dektrium\user\models\Profile as BaseProfile;

class Profile extends BaseProfile
{

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'][] = 'phone';
        $scenarios['update'][] = 'phone';
        $scenarios['register'][] = 'phone';
        return $scenarios;
    }
    public function rules() {
        $rules = parent::rules();
        $rules['fieldRequired'] = [['phone'], 'required'];
        $rules['fieldLength']   = [['phone'], 'string', 'max' => 20];
        return $rules;
    }

}