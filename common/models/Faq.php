<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%faq}}".
 *
 * @property integer $faq_id
 * @property string $question
 * @property string $answer
 * @property integer $order_num
 * @property integer $status
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%faq}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question', 'answer'], 'required'],
            [['question', 'answer'], 'string'],
            [['order_num', 'status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'faq_id' => Yii::t('app', 'Faq ID'),
            'question' => Yii::t('app', 'Question'),
            'answer' => Yii::t('app', 'Answer'),
            'order_num' => Yii::t('app', 'Order Num'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
