<?php


namespace common\models;
use Yii;
use dektrium\user\models\LoginForm as BaseLoginForm;
class LoginForm extends BaseLoginForm
{
       public function attributeLabels()
    {
        return [
            'login'      => Yii::t('user', 'Email'),
            'password'   => Yii::t('user', 'Password'),
            'rememberMe' => Yii::t('user', 'Remember me next time'),
        ];
    }
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->user = $this->finder->findUserByEmail($this->login);

            return true;
        } else {
            return false;
        }
    }
        public function loginAdmin()
    {

        if ($this->validate()) {
        $u= Yii::$app->getUser()->login($this->user, $this->rememberMe ? $this->module->rememberFor : 0);

        if(Yii::$app->user->can('admin'))
        {
         // echo Yii::$app->user->id;
          //print_r($this->user);
          //exit;
         return Yii::$app->getUser()->login($this->user, $this->rememberMe ? $this->module->rememberFor : 0);
        } else{

         //	   Yii::$app->getUser()->logout();
         	       Yii::$app->user->logout();
         	     // return $this->goBack();
          return false;
          }
        } else {
            return false;
        }
    }
}
