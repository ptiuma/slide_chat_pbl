<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%payments_plans}}".
 *
 * @property integer $id
 * @property integer $months
 * @property integer $amount
 * @property integer $title
 * @property integer $comments
 */
class PaymentsPlans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payments_plans}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['months', 'amount'], 'integer'],
               [['top_title','title', 'comments'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'months' => Yii::t('app', 'Кол-во месяцев'),
            'amount' => Yii::t('app', 'Сумма'),
            'top_title' => Yii::t('app', 'Заголовок'),
            'title' => Yii::t('app', 'Описание'),
            'comments' => Yii::t('app', 'Комментарий'),
        ];
    }
    public function getTotal()
    {
        return $this->months*$this->amount;
    }
}
