<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%clients}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $clients_type
 * @property string $phone
 * @property string $workphone
 * @property string $cellphone
 * @property string $fax
 * @property string $email
 * @property string $postcode
 * @property string $state
 * @property string $country
 * @property string $comments
 * @property integer $created_at
 * @property integer $updated_at
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%clients}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clients_type', 'comments'], 'required'],
            [['clients_type', 'created_at', 'updated_at'], 'integer'],
            [['comments'], 'string'],
            [['name', 'phone', 'workphone'], 'string', 'max' => 255],
            [['cellphone', 'fax', 'state', 'country'], 'string', 'max' => 32],
            [['email'], 'string', 'max' => 96],
            [['postcode'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'clients_type' => 'Clients Type',
            'phone' => 'Phone',
            'workphone' => 'Workphone',
            'cellphone' => 'Cellphone',
            'fax' => 'Fax',
            'email' => 'Email',
            'postcode' => 'Postcode',
            'state' => 'State',
            'country' => 'Country',
            'comments' => 'Comments',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
