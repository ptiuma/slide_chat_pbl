<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use  yii\helpers\Url;
 use frontend\models\LinksViews;

/**
 * This is the model class for table "{{%file_storage_item}}".
 *
 * @property integer $id
 * @property string $component
 * @property string $base_url
 * @property string $path
 * @property string $type
 * @property integer $size
 * @property string $name
 * @property string $upload_ip
 * @property integer $created_at
 */
class Documents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%documents}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),

            ]
        ];
    }
     public $docFile;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path','userId', 'filename','source_name'], 'required'],
            [['size','userId'], 'integer'],
            [['docFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'ppt, pptx, pdf, xls, xlsx, doc, docx, pps, ppsx, jpg, gif, png','checkExtensionByMimeType'=>false,'maxSize' => 1024 * 1024 * 5],
            [['component', 'name','original', 'source_name'], 'string', 'max' => 255],
            [['path', 'base_url', 'filename'], 'string', 'max' => 1024],
            [['upload_ip'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'userId'=>Yii::t('common', 'User'),
            'filename' => Yii::t('common', 'FileName'),
            'source_name' => Yii::t('common', 'SourceName'),
            'component' => Yii::t('common', 'Component'),
            'base_url' => Yii::t('common', 'Base Url'),
            'path' => Yii::t('common', 'Path'),
            'type' => Yii::t('common', 'Type'),
            'size' => Yii::t('common', 'Size'),
            'name' => Yii::t('common', 'Name'),
            'original' => Yii::t('common', 'Original'),
            'isDeleted' => Yii::t('common', 'isDeleted'),
            'noParsed' => Yii::t('common', 'noParsed'),
            'upload_ip' => Yii::t('common', 'Upload Ip'),
            'created_at' => Yii::t('common', 'Created At')
        ];
    }
		public static function find()
		{
		    return new DocumentsQuery(get_called_class());
		}
      public function getVisits()
    {
          return $this->hasMany(LinksViews::className(), ['linkId' => 'id'])
           ->viaTable('links', ['docId' => 'id'])
          ->count();
    }
        public function getUser()
    {
          return $this->hasOne(User::className(), ['id' => 'userId']);
    }
        public function getPages()
    {
          return $this->hasMany(DocumentsPages::className(), ['docId' => 'id']);
    }
        public function getPreview()
    {
          return $this->dir.($this->noParsed==1?basename($this->source_name):$this->pages[0]->filename);
    }
        public function getThumb()
    {
          return $this->dir.'thumbs/'.($this->noParsed==1?basename($this->source_name):$this->pages[0]->filename);
    }

            public function getDir()
    {
          return Yii::$app->keyStorage->get('system.uploadDir').'/'.$this->repository.'/';
    }
            public function getFulldoc()
    {
          return \Yii::getAlias('@web').'/'.$this->dir.$this->filename;
    }
        public function getLinks()
    {
          return $this->hasMany(Links::className(), ['docId' => 'id']);
    }
        public function getTimeviews_sum()
    {
          //return LinksViews::find()->joinWith('link')->where(['docId'=>$this->id])->sum();
    }
         public static function Doc_count($uid,$today=0,$d1=0,$d2=0)
   {
     if($today){$m2=0;$m1=mktime(0,0,0,date('m'),date('d'),date('Y'));}
     $query=Documents::find()->where(['userId'=>$uid]);
     if($d1)$query->andwhere('created_at >=\''.(int)$d1.'\'');
     if($d2)$query->andwhere('created_at <=\''.(int)$d2.'\'');
     return $query->count();
    }
}
