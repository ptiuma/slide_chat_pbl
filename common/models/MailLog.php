<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%mail_log}}".
 *
 * @property integer $id
 * @property integer $letterId
 * @property integer $userId
 * @property string $messageId
 * @property integer $event_type
 * @property string $event
 * @property integer $isOpenMail
 * @property integer $isOpenAtt
 * @property integer $OpenMail_at
 * @property integer $OpenAtt_at
 * @property string $provider
 * @property integer $created_at
 * @property integer $updated_at
 */
class MailLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mail_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['letterId', 'userId'], 'required'],
            [['letterId', 'userId', 'event_type', 'isOpenMail', 'isOpenAtt', 'OpenMail_at', 'OpenAtt_at', 'created_at', 'updated_at'], 'integer'],
            [['messageId'], 'string', 'max' => 200],
            [['event'], 'string', 'max' => 20],
            [['provider'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'letterId' => Yii::t('app', 'Letter ID'),
            'userId' => Yii::t('app', 'User ID'),
            'messageId' => Yii::t('app', 'Message ID'),
            'event_type' => Yii::t('app', 'Event Type'),
            'event' => Yii::t('app', 'Event'),
            'isOpenMail' => Yii::t('app', 'Is Open Mail'),
            'isOpenAtt' => Yii::t('app', 'Is Open Att'),
            'OpenMail_at' => Yii::t('app', 'Open Mail At'),
            'OpenAtt_at' => Yii::t('app', 'Open Att At'),
            'provider' => Yii::t('app', 'Provider'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
