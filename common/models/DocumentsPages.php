<?php

namespace common\models;

use Yii;
 use  yii\behaviors\TimeStampBehavior;
use  yii\helpers\Url;
/**
 * This is the model class for table "{{%documents_pages}}".
 *
 * @property integer $id
 * @property integer $docId
 * @property string $url
 * @property string $path
 * @property string $mimeType
 * @property string $upload_ip
 * @property integer $size
 * @property integer $preview
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Documents $id0
 * @property DocumentsPagesViews $documentsPagesViews
 */
class DocumentsPages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%documents_pages}}';
    }
    public function behaviors()
    {
        return [
           [
             'class' => 'yii\behaviors\TimestampBehavior',

            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['docId', 'filename'], 'required'],
            [['docId', 'size','pageNum', 'preview', 'created_at', 'updated_at'], 'integer'],
            [['url', 'path','filename'], 'string', 'max' => 2048],
            [['mimeType'], 'string', 'max' => 128],
            [['upload_ip'], 'string', 'max' => 15],
           // [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Documents::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'docId' => 'Doc ID',
            'pageNum' => 'pageNum',
            'url' => 'Url',
            'path' => 'Path',
            'filename' => 'FileName',
            'mimeType' => 'Mime Type',
            'upload_ip' => 'Upload Ip',
            'size' => 'Size',
            'preview' => 'Preview',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoc()
    {
        return $this->hasOne(Documents::className(), ['id' => 'docId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentsPagesViews()
    {
        return $this->hasOne(DocumentsPagesViews::className(), ['id' => 'id']);
    }
       public function getImg()
    {
       return Url::to(['/'],true).Yii::$app->params['uploadPath'].'/'.$this->doc->repository.'/'.$this->filename;
    }
}
