<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "documents_to_team".
 *
 * @property integer $teamId
 * @property integer $docId
 */
class DocumentsToTeam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents_to_team';
    }
           public $teamList;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teamId', 'docId'], 'required'],
            [['teamId', 'docId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teamId' => Yii::t('app', 'Team ID'),
            'docId' => Yii::t('app', 'Doc ID'),
        ];
    }
}
