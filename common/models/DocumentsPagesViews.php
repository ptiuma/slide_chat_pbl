<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%documents_pages_views}}".
 *
 * @property integer $id
 * @property integer $pageId
 * @property string $client_ip
 * @property string $client_browser
 * @property string $client_os
 * @property string $client_os_type
 * @property integer $started_at
 * @property integer $ended_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property DocumentsPages $id0
 */
class DocumentsPagesViews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%documents_pages_views}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pageId', 'started_at', 'ended_at', 'created_at', 'updated_at'], 'required'],
            [['pageId', 'started_at', 'ended_at', 'created_at', 'updated_at'], 'integer'],
            [['client_ip'], 'string', 'max' => 15],
            [['client_browser', 'client_os', 'client_os_type'], 'string', 'max' => 255],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentsPages::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pageId' => 'Page ID',
            'client_ip' => 'Client Ip',
            'client_browser' => 'Client Browser',
            'client_os' => 'Client Os',
            'client_os_type' => 'Client Os Type',
            'started_at' => 'Started At',
            'ended_at' => 'Ended At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoc()
    {
        return $this->hasOne(DocumentsPages::className(), ['docId' => 'id']);
    }
}
