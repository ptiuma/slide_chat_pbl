<?php

namespace common\models;

class LinksQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[isArchive]]=0');
    }
    public function noletter()
    {
        return $this->andWhere('[[letterId]]=0');
    }

}
