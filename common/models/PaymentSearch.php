<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Payment;

/**
 * PaymentSearch represents the model behind the search form about `common\models\Payment`.
 */
class PaymentSearch extends Payment
{
    public function rules()
    {
        return [
            [['id', 'transactionId', 'userId', 'planId', 'months', 'isPaid', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number'],
            [['SecurityKey', 'currency', 'response'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Payment::find()->where(['isPaid'=>1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'transactionId' => $this->transactionId,
            'userId' => $this->userId,
            'planId' => $this->planId,
            'months' => $this->months,
            'amount' => $this->amount,
            'isPaid' => $this->isPaid,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'SecurityKey', $this->SecurityKey])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'response', $this->response]);

        return $dataProvider;
    }
}
