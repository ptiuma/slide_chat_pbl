<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
 use common\models\Documents;
 use common\models\Links;
/**
 * This is the model class for table "{{%letters}}".
 *
 * @property integer $id
 * @property integer $linkDocId
 * @property integer $templateId
 * @property string $email_to
 * @property string $email_copy
 * @property string $subject
 * @property string $email_body
 * @property string $client_name
 * @property string $client_phone
 * @property string $sheduller
 * @property integer $type
 * @property integer $isArchive
 * @property integer $created_at
 * @property integer $updated_at
 */
class Letters extends \yii\db\ActiveRecord
{
    public $attachments;
       public $maildb;
     //   public $isMailing;
      //  public $mailing_from;
       //  public $mailing_layout;
    public static $alert_opened_in_moment_arr=[
       '1'=>'Открытия письма',
      '2'=>'Открытия вложения',
    ];
    public static $mailing_use_layout_arr=[
       '1'=>'Без шаблона',
      '2'=>'Использовать шаблон',
    ];
    public static $alert_after_days_arr=[
       '1'=>' 1 дня',
      '2'=>'2 дней',
      '3'=>'3 дней',
    ];
   public static $mailing_use_smtp_arr=[
      '1'=>'Mailgun',
       '2'=>'SMTP,прописанный в профайле',
    ];
 /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%letters}}';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),

            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email_subject', 'email_body', 'userId','key'], 'required'],
            [['email_to'], 'required','except' => 'mailing'],
           //   [['mailing_from','mailing_use_smtp'], 'required','on' => 'mailing'],
     //     [[ 'isMailing'], 'integer'],
    //       [[ 'mailing_from'], 'email'],
          [[ 'type','askLinkInfo', 'isArchive','isMultiEmail','sheduller_use','templateId','alert_after_days','alert_opened_in_moment'], 'integer'],
            ['email_to', 'string'],
            [['email_copy', 'email_subject', 'client_name','client_company','sheduller', 'client_phone','key'], 'string', 'max' => 255],
            [['email_body'], 'string', 'max' => 12555],
             ['sheduller', 'string','skipOnEmpty' => true,'on' => 'mailing'],
        ];
    }
   public function scenarios()
 {
      $scenarios = parent::scenarios();
        $scenarios['mailing'] = ['email_to', 'email_subject','email_body','mailing_from','sheduller','isMailing','mailing_use_smtp'];

        return $scenarios;
 }
       public function checkDateInPast($attribute,$params)
{

    if($this->sheduller)
    {
			        if($this->hasErrors())
			    {
			        if(strtotime($this->sheduller)<time())
			        {
			             $this->addError($attribute,'Дата и время в планиировщике некорректны.');
			        }
			    }

    }
  }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userId' => Yii::t('app', 'User ID'),
            'linkDocId' => Yii::t('app', 'Link Doc ID'),
            'templateId' => Yii::t('app', 'Шаблон'),
            'email_to' => Yii::t('app', 'Кому'),
            'email_copy' => Yii::t('app', 'Копия'),
            'email_subject' => Yii::t('app', 'Тема'),
            'email_body' => Yii::t('app', 'Собщение'),
            'client_name' => Yii::t('app', 'Клиент'),
            'client_company' => Yii::t('app', 'Компания'),
            'client_phone' => Yii::t('app', 'Телефон Клиента'),
            'sheduller' => Yii::t('app', 'Расписание'),
            'sheduller_use' => Yii::t('app', 'Use Sheduller'),
            'isMultiEmail'=>'isMultiEmail',
            'askLinkInfo' => Yii::t('app', 'запросить контактные данные у посетителя перед открытием вложения'),
            'isMailing'=>'isMailing',
                'mailing_from'=>'От кого',
                'mailing_layout'=>'Использовать layout',
                'mailing_use_smtp'=>'Метод отправки',

            'type' => Yii::t('app', 'Type'),
            'isOpened' => Yii::t('app', 'isOpened'),
            'opened_at' => Yii::t('app', 'Opened At'),
            'key' => Yii::t('app', 'Key'),

            'alert_after_days' => Yii::t('app', 'или если не откроют в течение'),
            'alert_opened_in_moment' => Yii::t('app', 'Уведомить в момент'),
            'isArchive' => Yii::t('app', 'Is Archive'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
		public static function find()
		{
		    return new LettersQuery(get_called_class());
		}
       public function getUser()
    {
          return $this->hasOne(User::className(), ['id' => 'userId']);
    }
     public function getClient_name()
    {
           return !$this->client_name?'noname':$this->client_name;
    }
     public function getDocs()
    {
           return $this->hasMany(Documents::className(), ['id' => 'docId'])
        ->viaTable('letters_attached_docs', ['letterId' => 'id']);
    }
         public function getLinks()
    {
           return $this->hasMany(Links::className(), ['letterId' => 'id']);
    }
            public static function LetterLink($id,$docId)
    {
          return Links::find()
          ->andWhere(['letterId' => $id,'docId' => $docId])
          ->one();
    }
   public static function Letters_count($uid,$today=0,$d1=0,$d2=0)
   {
     if($today==1){$d1=mktime(0,0,0,date('m'),date('d'),date('Y'));}
     $query=Letters::find()->where(['userId'=>$uid]);
     if($d1)$query->andwhere(['>','created_at',(int)$d1]);
     if($d2)$query->andwhere(['<','created_at',(int)$d2]);
     return $query->count();
    }
}
