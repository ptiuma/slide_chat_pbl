<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Logger;

/**
 * LoggerSearch represents the model behind the search form about `app\models\Logger`.
 */
class LoggerSearch extends Logger
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'level',  ], 'integer'],
            [['category', 'prefix','log_time', 'message'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Logger::find()->orderBy('id desc');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
               'category' => $this->category,
            'level' => $this->level,
      //      'log_time' => $this->log_time,
            'objectType' => $this->objectType,
            'objectID' => $this->objectID,
            'userID' => $this->userID,
           // 'companyID' => $this->companyID,
        ]);

     		if(isset($this->log_time) && $this->log_time!=''){
		$date_explode = explode(" : ", $this->log_time);
		$date1 = trim($date_explode[0]);
		$date2= trim($date_explode[1]);
			$date1 = \DateTime::createFromFormat("Y-m-d", $date1);
	    	$date1= $date1->format('U');
			$date2 = \DateTime::createFromFormat("Y-m-d", $date2);
	    	$date2= $date2->format('U');
		$query->andFilterWhere(['between', 'log_time', $date1,$date2]);
		}
        $query->andFilterWhere(['like', 'prefix', $this->prefix])
            ->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
