<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%feedback}}".
 *
 * @property integer $feedback_id
 * @property integer $userId
 * @property integer $reply_to
 * @property integer $parent_id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $title
 * @property string $text
 * @property string $answer_subject
 * @property string $answer_text
 * @property integer $time
 * @property string $ip
 * @property integer $status
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'reply_to', 'parent_id', 'time', 'status'], 'integer'],
            [['email','name', 'text'], 'required'],
            [['text', 'answer_text'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['email', 'title', 'answer_subject'], 'string', 'max' => 128],
            [['email'], 'email'],
            [['phone'], 'string', 'max' => 64],
            [['ip'], 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'feedback_id' => Yii::t('app', 'Feedback ID'),
            'userId' => Yii::t('app', 'User ID'),
            'reply_to' => Yii::t('app', 'Reply To'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'name' => Yii::t('app', 'Имя'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Телефон'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Сообщение'),
            'answer_subject' => Yii::t('app', 'Answer Subject'),
            'answer_text' => Yii::t('app', 'Answer Text'),
            'time' => Yii::t('app', 'Time'),
            'ip' => Yii::t('app', 'Ip'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
