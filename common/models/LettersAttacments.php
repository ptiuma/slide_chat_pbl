<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%communications_attached_docs}}".
 *
 * @property integer $communicationId
 * @property integer $docId
 */
class LettersAttacments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%letters_attached_docs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['letterId', 'docId'], 'required'],
            [['letterId', 'docId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'letterId' => Yii::t('app', 'LetterID'),
            'docId' => Yii::t('app', 'Doc ID'),
        ];
    }
}
