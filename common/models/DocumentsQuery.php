<?php

namespace common\models;

class DocumentsQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[isDeleted]]< 1');
    }


}
