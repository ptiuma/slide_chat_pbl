<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_console".
 *
 * @property string $id
 * @property integer $level
 * @property string $category
 * @property integer $log_time
 * @property string $prefix
 * @property string $message
 */
class Logger extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_console';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level', 'log_time','userID','objectType','objectID',], 'integer'],
            [['prefix', 'message'], 'string'],
            [['category'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level' => 'Level',
            'category' => 'Категория',
            'log_time' => 'Дата',
            'prefix' => 'Prefix',
            'message' => 'Сообщение',
            'userID' => 'Юзер',
                        'objectType' => 'Тип записи',
                                    'objectID' => 'ID записи',
        ];
    }
       public static function addLog($category,$msg='',$oType=0,$oID=0,$compID=0)
    {
       	$log = new Logger();
		$log->category=$category;
		$log->message = $msg;
		$log->objectType = $oType;
		$log->objectID = $oID;
				$log->userID = \Yii::$app->getUser()->getId();
						$log->log_time =time();

		$log->save();  // equivalent to $customer->insert();
    }

        public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'userID']);
    }
        public static function getType($tp)
    {

    }
}
