<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%payment}}".
 *
 * @property string $id
 * @property string $transactionId
 * @property string $userId
 * @property string $planId
 * @property integer $months
 * @property string $amount
 * @property string $currency
 * @property integer $isPaid
 * @property string $response
 * @property integer $created_at
 * @property integer $updated_at
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment}}';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),

            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'planId', 'months', 'amount', 'currency'], 'required'],
            [['transactionId', 'userId', 'planId', 'months', 'isPaid'], 'integer'],
            [['amount'], 'number'],
            [['SecurityKey','response','payment_by'], 'string'],
            [['currency'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'transactionId' => Yii::t('app', 'Transaction ID'),
            'userId' => Yii::t('app', 'User ID'),
            'planId' => Yii::t('app', 'Plan ID'),
            'months' => Yii::t('app', 'Months'),
            'amount' => Yii::t('app', 'Amount'),
            'currency' => Yii::t('app', 'Currency'),
            'isPaid' => Yii::t('app', 'Is Paid'),
            'SecurityKey' => Yii::t('app', 'SecurityKey'),
            'response' => Yii::t('app', 'Response'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'payment_by' =>Yii::t('app', '�����')
        ];
    }
    public function getUser()
    {
          return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
