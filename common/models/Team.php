<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\TeamMembers;
use frontend\models\TeamInvites;
use frontend\models\UserTemplates;
use frontend\models\UserTemplatesToTeam;
/**
 * This is the model class for table "team".
 *
 * @property integer $id
 * @property string $name
 * @property integer $userId
 * @property string $comments
 * @property integer $created_at
 * @property integer $updated_at
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team';
    }
          public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),

            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'userId'], 'required'],
            [['userId'], 'integer'],
            [['comments'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Команда'),
            'userId' => Yii::t('app', 'User ID'),
            'comments' => Yii::t('app', 'Комментарии'),
            'created_at' => Yii::t('app', 'Создана'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    public function beforeDelete()
{
         TeamInvites::deleteAll(['teamId'=> $this->id]);
        TeamMembers::deleteAll(['teamId'=> $this->id]);
        return parent::beforeDelete();

}
        public function getUser()
    {
          return $this->hasOne(User::className(), ['id' => 'userId']);
    }
     public function getInvites()
{
    return $this->hasMany(TeamInvites::className(), ['teamId' => 'id']);
}
     public function getMembers()
{
    return $this->hasMany(TeamMembers::className(), ['teamId' => 'id']);
}
     public function getMembers_count()
{
    return TeamMembers::find()->where(['teamId' => $this->id])->groupBy('memberId')->count();
}
     public function getTemplates()
	{
	    return $this->hasMany(UserTemplates::className(), ['teamId' => 'id']);
	}
     public function getTemplates_count()
	{
	    return  $this->hasMany(UserTemplates::className(), ['id' => 'templateId'])
    ->viaTable('user_templates_to_team', ['teamId' => 'id'])->count();
	}
}
