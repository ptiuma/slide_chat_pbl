<?php

namespace common\models;

class LettersQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[isArchive]]=0');
    }
    public function opened2()
    {
        return $this->andWhere(['isOpened'=>1]);
    }

}
