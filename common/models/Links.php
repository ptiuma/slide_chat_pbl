<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use  yii\helpers\Url;
 use frontend\models\LinksViews;

/**
 * This is the model class for table "{{%links}}".
 *
 * @property integer $id
 * @property integer $docId
 * @property string $link_name
 * @property string $client_name
 * @property string $client_phone
 * @property string $expired_date
 * @property integer $isExpire
 * @property string $protect_code
 * @property integer $isProtect
 * @property integer $allowDownloading
 * @property integer $isArchive
 * @property integer $created_at
 * @property integer $updated_at
 */
class Links extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%links}}';
    }
        public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),

            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['docId','userId','link_name'], 'required'],
            [['docId', 'created_at', 'updated_at'], 'integer'],
	        [['askInfo','isProtect', 'denyDownloading', 'isExpire'],'integer'],
    	    [['link_name', 'client_name','client_company', 'client_phone', 'expired_date', 'protect_code'], 'string', 'max' => 255],
    	     ['client_name',  'required', 'on' => 'link'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'docId' => Yii::t('app', 'Doc ID'),
            'userId' => Yii::t('app', 'User'),
            'letterId' => Yii::t('app', 'letter Id'),

            'link_name' => Yii::t('app', 'Ссылка'),
            'client_name' => Yii::t('app', 'Клиент'),
                        'client_company' => Yii::t('app', 'Компания'),
            'client_phone' => Yii::t('app', 'Телефон'),
            'expired_date' => Yii::t('app', 'Дата истечения ссылки'),
            'isExpire' => Yii::t('app', 'Истекает'),
            'protect_code' => Yii::t('app', 'Защитный код'),
            'isProtect' => Yii::t('app', 'Защита'),
            'askInfo'=>'Запрашивать инфо перед просмотром документа',
            'denyDownloading' => Yii::t('app', 'Запретить скачивание'),
            'isOpened' => Yii::t('app', 'Opened'),
            'isBlocked' => Yii::t('app', 'Блокировано'),
            'isArchive' => Yii::t('app', 'Архив'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'updated_at' => Yii::t('app', 'Дата обновление'),
        ];
    }

	public static function find()
	{
	    return new LinksQuery(get_called_class());
	}
      public function getUser()
    {
          return $this->hasOne(User::className(), ['id' => 'userId']);
    }
   public function getDoc()
    {
          return $this->hasOne(Documents::className(), ['id' => 'docId']);
    }
   public function getLetter()
    {
          return $this->hasOne(Letters::className(), ['id' => 'letterId']);
    }
       public function getUrl()
    {         $p=Url::to(['/'],true);
          return $p.Yii::$app->params['slideShowPath'].$this->link_name;
    }
            public function getViews()
    {
          return $this->hasMany(LinksViews::className(), ['linkId' => 'id'])
          ->from(['views' => LinksViews::tableName()]);
    }

    public function getViews_count()
{
    return LinksViews::find()->where(['linkId' => $this->id])->count();
}

            public static function Viewed_count($uid,$today=0,$d1=0,$d2=0)
   {
     if($today){$d2=0;$d1=mktime(0,0,0,date('m'),date('d'),date('Y'));}
     $query=LinksViews::find()->joinWith('link')->where(['userId'=>$uid]);
     if($d1)$query->andwhere('links_views.created_at >=\''.(int)$d1.'\'');
     if($d2)$query->andwhere('links_views.created_at <=\''.(int)$d2.'\'');
     return $query->count();
    }
}
