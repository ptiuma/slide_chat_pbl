<?php
namespace common\models;
use common\models\User;
use frontend\models\Settings;
use frontend\models\UserMailSettings;

use frontend\models\TelegramToken;
use common\models\Logger;
use common\models\Documents;
use common\models\Letters;
use common\models\Links;
use common\models\Payment;
use common\models\DocumentsPages;
use Yii;


use dektrium\user\models\User as BaseUser;

class User extends BaseUser
{
        public static $acc_levels = [
        '1'   => 'Free',
        '2' => 'Premium',
    ];
	 public function init() {
        $this->on(self::BEFORE_REGISTER, function() {
            $this->username = $this->email;
        });
        $this->on(self::AFTER_REGISTER, function() {
            $this->demodata();

             Yii::$app->getUser()->login($this->finder->findUserByUsernameOrEmail($this->email), 0);
        });

        parent::init();
    }

    public function rules() {
        $rules = parent::rules();
        unset($rules['usernameRequired']);
     //   $rules['fieldRequired'] = [['phone'], 'required'];
        $rules['fieldLength']   = [['paid_date'], 'integer'];
        return $rules;
    }

    private function demodata()
    {

        //$this->profile->name=$_POST['register-form']['name'];;
        //$this->profile->user_id=$this->id;
          //  $this->profile->save();
        	 Logger::addLog('register','Новый пользователь # '.$this->id);
	         $settings=new Settings;
	         $settings->userId=$this->id;
	         $settings->save();

    	   $model=new Documents();
        		     $model->name = "slide_chat.pdf";
                     $model->userId = $this->id;
				$model->upload_ip = Yii::$app->getRequest()->getUserIP();
                $model->repository = 'demo';
                $model->original = "slide_chat.pdf";
                $model->source_name = "slide_chat.pdf";
                $model->filename = "slide_chat.pdf";
                $model->path = 'shared/demo/'.$model->name;
                $model->save();
				  $files=\yii\helpers\FileHelper::findFiles('shared/demo/thumbs');
				    if (isset($files[0])) {
				        foreach ($files as $index => $file) {
				       if(strpos($file,'jpg')!== false)
				       {
        				 $page=new DocumentsPages;
						$page->docId=$model->id;
						$page->filename=basename($file);
                        if ($page->validate()) {
                         $page->save();
                         }
				        }
				       }
				    }

    }

	public static function isUserAdmin($username)
	{

	    $user=static::findOne(['username' => $username]);
	//var_dump($user);
	echo   Yii::$app->user->can('admin');
	exit;
	    if ($user->id&&\Yii::$user->identity->group == 'admin')
	    {
	        return true;
	    } else {
	        return false;
	    }
	}
     public static function isDemo()
    {
         $diff=time()-Yii::$app->user->identity->created_at;
         return intval($diff / 60 / 60 / 24)<=10?true:false;
    }
     public static function AccountLevel()
    {
         return Yii::$app->user->identity->paid_date-time()>0?2:1;
    }
     public static function AccountLevelName()
    {
         return self::$acc_levels[Yii::$app->user->identity->paid_date-time()>0?2:1];
    }
   public  function getPayment_last()
    {

		return $this->hasOne(Payment::className(), ['userId' => 'id'])->orderby('payments.id');
    }
   public  function getSmtp()
    {

		return $this->hasOne(UserMailSettings::className(), ['userId' => 'id']);
    }
   public  function getSettings()
    {

		return $this->hasOne(Settings::className(), ['userId' => 'id']);
    }
   public  function getTelegram()
    {

		return $this->hasOne(TelegramToken::className(), ['userId' => 'id']);
    }
     public static function getTeamAsManager()
    {
         User::find()->where(['id'=>Yii::$app->user->id]);
         return true;
    }
   public  function getTeams()
    {

		return $this->hasMany(Team::className(), ['id' => 'teamId'])
		->viaTable(TeamMembers::tableName(), ['memberId' => 'id']);
    }
   public  function getLetters_count()
    {

		return Letters::find()->where(['id'=>Yii::$app->user->id])->count();
    }
   public  function getLinks_count()
    {

		return Links::find()->where(['id'=>Yii::$app->user->id])->count();
    }
}