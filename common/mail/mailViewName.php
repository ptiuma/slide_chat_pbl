<?php


use yii\helpers\Html;
use  yii\helpers\Url;


?>


							<?php if($LetterOpened==1):?>
							    Ваш клиент открыл письмо:<br />
							    Тема: <?=$model->email_subject?><br />
							    Кому: <?=$model->email_to?><br />
							    Отослано: <?=date('Y-m-d H:i',$model->created_at)?><br />

							<?php else:?>
								<?php if($model->doc->noParsed==1):?>
								Ваш клиент скачал документ "<?=$model->doc->name?>".
								<?php else:?>
								Ваш клиент просматривает документ "<?=$model->doc->name?>".
								<?php endif;?>

							</p>
							<div style="margin:10px;font-size:12px">Подробную статистику Вы можете просмотреть на сайте</div>
							 <a title="Карточка клиента" href="<?=Url::to(['activity/client?id='.$model->id], true);?>" style="font-weight:bold;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;word-wrap:break-word" target="_blank">Карточка клиента</a>
                             	<?php endif;?>
