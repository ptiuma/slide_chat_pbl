<?php
namespace console\controllers;
use Yii;
use yii\console\Controller;
use common\models\Documents;
use common\models\search\DocumentsSearch;
use common\models\DocumentsPages;
use common\models\Links;
use frontend\models\LinksViews;
use \common\models\DocumentsToTeam;
use \common\models\Letters;
use common\models\Team;
use common\models\TeamMembers;
use common\components\TeamStats;
use yii\db\Expression;

class SlideController extends Controller {


    public function actionShedullersLetters(){
       //$letters=Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['isAlertSended'=>0])->all();
       $letters=Letters::find()->where(['sheduller_use'=>1])
       ->andwhere(['>','sheduller',0])
       ->andwhere(['<','sheduller',date('Y-m-d H:i:s')])
       ->andwhere(['isMailing'=>null])
       ->all();
       /*
       $command = Letters::find()->where(['sheduller_use'=>1])
       ->andwhere(['>','sheduller',0])
       ->andwhere(['<','sheduller',date('Y-m-d h:i:s')])
       //->all()
       ->createCommand();
		echo  $command->getRawSql();
		*/
       foreach($letters as $letter)
       {
       //echo $letter->id."-";
       	   Yii::$app->slidehelper->sendletter($letter);
       	   $letter->sheduller_use=0;
       	   $letter->save();
       	 //  exit;
       }
    }
    public function actionMailingShedullersLetters(){
       $letters=Letters::find()->where(['sheduller_use'=>1])
       ->andwhere(['>','sheduller',0])
       ->andwhere(['isMailing'=>1])
       ->andwhere(['<','sheduller',date('Y-m-d H:i:s')])
       ->all();
       foreach($letters as $letter)
       {
       	   Yii::$app->slidehelper->sendmailingletter($letter);
       	   $letter->sheduller_use=0;
       	   $letter->save();

       }
    }
    public function actionAlertAfterDay(){
    $date1=mktime(date("H"), 0, 0, date("m"), date("d")-1, date("Y"));
    $date2=$date1+3600;

       $letters=Letters::find()->joinWith('links',true,'RIGHT JOIN')->where(['letters.isOpened'=>0])
       ->andwhere(['!=','sheduller_use',1])
       ->andwhere(['IS','sheduller',(new Expression('Null'))])
		//->andwhere(['between','letters.created_at',$date1,$date2])
       ->all();

       foreach($letters as $model)
       {
       if($model->user->settings->alert_after_day!=1)continue;
        $mail=Yii::$app->mailer->compose('afterday-alert',['model'=>$model])
                ->setTo($model->user->email)
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['name']])
                ->setSubject('Извещение о не открытых письмах')
                ->send();
              print_r($mail);
              echo $model->id;
       exit;
       }
    }
    public function actionAlertClosedViews(){

       $views=LinksViews::find()->where(['isAlertSended'=>0])
       ->andwhere(['>','ended_at',0])
       ->andwhere(['>','created_at',time()-(60*60*1)])
       ->all();

         foreach($views as $view)
       {
       //echo $view->id."-";
        if($view->link->doc->noParsed!=1 && $view->link->user->settings->send_telegram_alert!=2 && $view->link->user->telegram->chat_id )
        {
           $msg="Клиент закончил просмотр\n";
           $msg.='Документ: '.$view->link->doc->name."\n";
           $msg.='Клиент: '.$view->visitor_name."\n";
           $msg.='Время просмотра: '.$view->interval."\n";
           \Yii::$app->bot->sendMessage($view->link->user->telegram->chat_id, $msg);
       //exit;
       }
        if($view->link->doc->noParsed!=1 && $view->link->user->settings->send_email_alert!=2)
        {            Yii::$app->slidehelper->sendendviewalert($view);
        }
       	   $view->isAlertSended=1;
       	   $view->save();
         }
    }

    public function actionAlertTeamStat(){

       $teams=Team::find()->all();

         foreach($teams as $team)
       {
           echo $team->id."\n";
           $stat=new TeamStats;
           $stat->genTeamStat($team->id);
       }
    }

}