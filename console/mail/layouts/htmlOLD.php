<?php
use yii\helpers\Html;
use  yii\helpers\Url;
/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body bgcolor="#8d8e90">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#8d8e90">
  <tr>
    <td><table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" align="center">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="144"><div style="font-family:'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;line-height:1.1;font-weight:900;font-size:14px;text-transform:uppercase;color:#444;padding-top:0;padding-bottom:0;padding-right:0;padding-left:20px;margin-top:0 !important;margin-bottom:0 !important;margin-right:0 !important;margin-left:10px !important;" >&nbsp;&nbsp;Slide.Chat</div></td>
                <td width="393"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="46" align="right" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="67%" align="right"><font style="font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#68696a; font-size:8px; text-transform:uppercase"><a href= "<?php echo Url::to(['/'],true)?>" style="color:#68696a; text-decoration:none"><strong>ПОСЕТИТЬ САЙТ</strong></a></font></td>
                            <td width="4%">&nbsp;</td>
                          </tr>
                        </table></td>
                    </tr>

                  </table></td>
              </tr>

            </table></td>
        </tr>
           <tr>
                      <td style="height:6px;background:#32856C;width:100%"></td>
                    </tr>
        <tr>
          <td align="center">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10%">&nbsp;</td>
                <td width="80%" align="left" valign="top"><font style="font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:15px">
    <?php $this->beginBody() ?>
    <?= $content ?>
    <?php $this->endBody() ?>

 </td>
                <td width="10%">&nbsp;</td>
              </tr>

            </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><img src="<?php echo Url::to(['/'],true)?>img/mail/images/PROMO-GREEN2_07.jpg" width="598" height="7" style="display:block" border="0" alt=""/></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td align="center"><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td  align="center"><font style="font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#010203; font-size:9px; text-transform:uppercase"><a href= "<?php echo Url::to(['/'],true)?>" style="color:#010203; text-decoration:none"><strong>SLIDE.CHAT </strong></a></font></td>
                <td  align="center"><font style="font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#010203; font-size:9px; text-transform:uppercase"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></font></td>
                <td  align="center"><font style="font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#010203; font-size:9px; text-transform:uppercase"><a href= "<?php echo Url::to(['/'],true)?>" style="color:#010203; text-decoration:none"><strong>КОНТАКТЫ </strong></a></font></td>
			     <td align="right">&nbsp;&nbsp;<a href="https://www.facebook.com/" target="_blank"><img src="<?php echo Url::to(['/'],true)?>img/mail/images/PROMO-GREEN2_09_01.jpg" alt="facebook" width="23" height="19" border="0" /></a></td>
                <td  align="right">&nbsp;&nbsp;<a href="https://twitter.com/" target="_blank"><img src="<?php echo Url::to(['/'],true)?>img/mail/images/PROMO-GREEN2_09_02.jpg" alt="twitter" width="27" height="19" border="0" /></a></td>

              </tr>
            </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td align="center"><font style="font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#231f20; font-size:8px"><strong> Tel: 123 555 555 | <a href= "<?php echo Url::to(['/'],true)?>" style="color:#010203; text-decoration:none">info@slide.chat</a></strong></font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>

<?php $this->endPage() ?>
