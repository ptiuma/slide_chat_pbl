<?php

use yii\db\Migration;
use yii\db\Schema;

class m160303_040546_add_new_field_to_user extends Migration
{
    public function up()
    {
    $this->addColumn('{{%profile}}', 'phone', Schema::TYPE_STRING);
  //  $this->addColumn('{{%user}}', 'nickname', Schema::TYPE_STRING);
    }

    public function down()
    {
    $this->dropColumn('{{%user}}', 'phone');

    }
}
