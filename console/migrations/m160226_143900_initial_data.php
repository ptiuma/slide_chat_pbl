<?php

use yii\db\Migration;

class m160226_143900_initial_data extends Migration
{
    public function up()
    {
        $path = dirname(__FILE__);
      $sql = file_get_contents(str_replace('.php','.sql',$path.'/m160226_143900_initial_data.php'));
		  $this->execute($sql);
    }

    public function down()
    {
        echo "m160226_143900_initial_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
