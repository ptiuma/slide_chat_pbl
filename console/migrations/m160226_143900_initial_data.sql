SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '1', 1455036136);

DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, '', NULL, NULL, 1455035739, 1455038971),
('user', 1, '', NULL, NULL, 1455035869, 1455035869);

DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `clients_type` tinyint(4) NOT NULL,
  `phone` varchar(255) NOT NULL DEFAULT '',
  `workphone` varchar(255) NOT NULL DEFAULT '',
  `cellphone` varchar(32) NOT NULL DEFAULT '',
  `fax` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(96) NOT NULL DEFAULT '',
  `postcode` varchar(10) NOT NULL DEFAULT '',
  `state` varchar(32) DEFAULT NULL,
  `country` varchar(32) NOT NULL DEFAULT '',
  `comments` text NOT NULL,
  `created_at` int(14) DEFAULT NULL,
  `updated_at` int(14) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `documents`;
CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(9) NOT NULL,
  `name` varchar(255) NOT NULL,
  `original` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `source_name` varchar(255) NOT NULL,
  `repository` varchar(32) NOT NULL,
  `component` varchar(255) NOT NULL,
  `category` varchar(128) DEFAULT NULL,
  `base_url` varchar(2048) DEFAULT NULL,
  `path` varchar(2048) NOT NULL,
  `mimeType` varchar(128) NOT NULL,
  `upload_ip` varchar(15) DEFAULT NULL,
  `size` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL,
  `isParsed` tinyint(4) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

INSERT INTO `documents` (`id`, `userId`, `name`, `original`, `filename`, `source_name`, `repository`, `component`, `category`, `base_url`, `path`, `mimeType`, `upload_ip`, `size`, `type`, `isDeleted`, `isParsed`, `status`, `created_at`, `updated_at`) VALUES
(7, 12, 'slide_chat.pdf', 'slide_chat.pdf', 'slide_chat.pdf', 'slide_chat.pdf', 'demo', '', NULL, NULL, 'shared/demo/slide_chat.pdf', '', '127.0.0.1', 0, 0, 0, 0, 10, 1456319956, 1456319956),
(8, 12, '1.pdf', '1.pdf', 'Z9460_H.pdf', 'Z9460_H', '12_b-ac27I', '', NULL, NULL, 'shared/12_b-ac27I/Z9460_H.pdf', 'application/pdf', '127.0.0.1', 76638, 0, 0, 0, 10, 1456417489, 1456417489),
(9, 12, 'my Doc', 'wel_app.pdf', '9GeMPmO.pdf', '9GeMPmO', '12_LowhshL', '', NULL, NULL, 'shared/12_LowhshL/9GeMPmO.pdf', 'application/pdf', '127.0.0.1', 3341501, 0, 0, 0, 10, 1456417580, 1456459537),
(10, 1, '1.pdf', '1.pdf', 'LbaUMry.pdf', 'LbaUMry', '1_7fq_4-w', '', NULL, NULL, 'shared/1_7fq_4-w/LbaUMry.pdf', 'application/pdf', '127.0.0.1', 76638, 0, 0, 0, 10, 1456675538, 1456675538),
(11, 1, '1.pdf', '1.pdf', '9AjAI-W.pdf', '9AjAI-W', '1_RN1WTcV', '', NULL, NULL, 'shared/1_RN1WTcV/9AjAI-W.pdf', 'application/pdf', '127.0.0.1', 76638, 0, 0, 0, 10, 1456675584, 1456675584),
(12, 1, '1.pdf', '1.pdf', '7dJCo5D.pdf', '7dJCo5D', '1_mb14etQ', '', NULL, NULL, 'shared/1_mb14etQ/7dJCo5D.pdf', 'application/pdf', '127.0.0.1', 76638, 0, 0, 0, 10, 1456675954, 1456675954),
(13, 1, 'wel_app.pdf', 'wel_app.pdf', 'PaCzWZH.pdf', 'PaCzWZH', '1_IKU2xFl', '', NULL, NULL, 'shared/1_IKU2xFl/PaCzWZH.pdf', 'application/pdf', '127.0.0.1', 3341501, 0, 0, 0, 10, 1456762657, 1456762657),
(14, 13, 'slide_chat.pdf', 'slide_chat.pdf', 'slide_chat.pdf', 'slide_chat.pdf', 'demo', '', NULL, NULL, 'shared/demo/slide_chat.pdf', '', '127.0.0.1', 0, 0, 0, 0, 10, 1456811266, 1456811266),
(15, 13, '1.pdf', '1.pdf', 'sex2hnp.pdf', 'sex2hnp', '13_U5IcI7x', '', NULL, NULL, 'shared/13_U5IcI7x/sex2hnp.pdf', 'application/pdf', '127.0.0.1', 76638, 0, 0, 0, 10, 1456828987, 1456828987),
(16, 13, '1.pdf', '1.pdf', 'm90Iqm4.pdf', 'm90Iqm4', '13_Gy_8qYD', '', NULL, NULL, 'shared/13_Gy_8qYD/m90Iqm4.pdf', 'application/pdf', '127.0.0.1', 76638, 0, 0, 0, 10, 1456892969, 1456892969),
(17, 13, '1.pdf', '1.pdf', 'AgusbH4.pdf', 'AgusbH4', '13_jZTN4S0', '', NULL, NULL, 'shared/13_jZTN4S0/AgusbH4.pdf', 'application/pdf', '127.0.0.1', 76638, 0, 0, 0, 10, 1456893212, 1456893212);

DROP TABLE IF EXISTS `documents_pages`;
CREATE TABLE IF NOT EXISTS `documents_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `docId` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `url` varchar(2048) DEFAULT NULL,
  `path` varchar(2048) NOT NULL,
  `mimeType` varchar(128) NOT NULL,
  `upload_ip` varchar(15) DEFAULT NULL,
  `size` int(11) NOT NULL,
  `preview` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

INSERT INTO `documents_pages` (`id`, `docId`, `filename`, `url`, `path`, `mimeType`, `upload_ip`, `size`, `preview`, `created_at`, `updated_at`) VALUES
(9, 7, '1.jpg', NULL, '', '', NULL, 0, 0, 1456319956, 1456319956),
(10, 7, '2.jpg', NULL, '', '', NULL, 0, 0, 1456319956, 1456319956),
(11, 7, '3.jpg', NULL, '', '', NULL, 0, 0, 1456319956, 1456319956),
(12, 7, '4.jpg', NULL, '', '', NULL, 0, 0, 1456319956, 1456319956),
(13, 8, 't_p-V5DtIb.jpg', NULL, '', '', NULL, 0, 0, 1456417493, 1456417493),
(14, 9, 'U_JSY5X3Ty.jpg', NULL, '', '', NULL, 0, 0, 1456417585, 1456417585),
(15, 9, 'Uau-B3FWji.jpg', NULL, '', '', NULL, 0, 0, 1456417586, 1456417586),
(16, 9, 'UQBWXygtRg.jpg', NULL, '', '', NULL, 0, 0, 1456417587, 1456417587),
(17, 9, 'fL4CcWCZZF.jpg', NULL, '', '', NULL, 0, 0, 1456417589, 1456417589),
(18, 9, '09bjWGsSFf.jpg', NULL, '', '', NULL, 0, 0, 1456417590, 1456417590),
(19, 9, 'hhWnk5UUwo.jpg', NULL, '', '', NULL, 0, 0, 1456417592, 1456417592),
(20, 9, 'bHnLOU9DKk.jpg', NULL, '', '', NULL, 0, 0, 1456417593, 1456417593),
(21, 9, 'RM8STxgrNX.jpg', NULL, '', '', NULL, 0, 0, 1456417595, 1456417595),
(22, 9, 'qJo3gr9p-d.jpg', NULL, '', '', NULL, 0, 0, 1456417596, 1456417596),
(23, 9, '1cggIPH2w6.jpg', NULL, '', '', NULL, 0, 0, 1456417597, 1456417597),
(24, 9, 'sugcAfhe7l.jpg', NULL, '', '', NULL, 0, 0, 1456417599, 1456417599),
(25, 9, 'rxkSWQ2to_.jpg', NULL, '', '', NULL, 0, 0, 1456417600, 1456417600),
(26, 10, '3jK-UssXNn.jpg', NULL, '', '', NULL, 0, 0, 1456675541, 1456675541),
(27, 11, 'NtOXUbevSe.jpg', NULL, '', '', NULL, 0, 0, 1456675584, 1456675584),
(28, 12, 'A7QgZOyq4M.jpg', NULL, '', '', NULL, 0, 0, 1456675955, 1456675955),
(29, 13, 'vhbw8E7_Ui.jpg', NULL, '', '', NULL, 0, 0, 1456762665, 1456762665),
(30, 13, 'djpBQdnxzk.jpg', NULL, '', '', NULL, 0, 0, 1456762666, 1456762666),
(31, 13, 'd09y6Pni8i.jpg', NULL, '', '', NULL, 0, 0, 1456762668, 1456762668),
(32, 13, 'fRw4kDniXM.jpg', NULL, '', '', NULL, 0, 0, 1456762669, 1456762669),
(33, 13, 'KC5Fgw4ekV.jpg', NULL, '', '', NULL, 0, 0, 1456762671, 1456762671),
(34, 13, 'M38_b61hpB.jpg', NULL, '', '', NULL, 0, 0, 1456762672, 1456762672),
(35, 13, 'opmzigSj0v.jpg', NULL, '', '', NULL, 0, 0, 1456762673, 1456762673),
(36, 13, '8UbIzjW1VJ.jpg', NULL, '', '', NULL, 0, 0, 1456762675, 1456762675),
(37, 13, 'Nt_6xv5Y4P.jpg', NULL, '', '', NULL, 0, 0, 1456762676, 1456762676),
(38, 13, 'PKCE_6Op4d.jpg', NULL, '', '', NULL, 0, 0, 1456762678, 1456762678),
(39, 13, 'dBIy3uQyoY.jpg', NULL, '', '', NULL, 0, 0, 1456762679, 1456762679),
(40, 13, 'JfMOW8QzSe.jpg', NULL, '', '', NULL, 0, 0, 1456762680, 1456762680),
(41, 14, '1.jpg', NULL, '', '', NULL, 0, 0, 1456811266, 1456811266),
(42, 14, '2.jpg', NULL, '', '', NULL, 0, 0, 1456811266, 1456811266),
(43, 14, '3.jpg', NULL, '', '', NULL, 0, 0, 1456811266, 1456811266),
(44, 14, '4.jpg', NULL, '', '', NULL, 0, 0, 1456811266, 1456811266),
(45, 15, '1h_KIekIXq.jpg', NULL, '', '', NULL, 0, 0, 1456828992, 1456828992),
(46, 16, 'UeGaEwGTp4.jpg', NULL, '', '', NULL, 0, 0, 1456892973, 1456892973),
(47, 17, 'sCvG-6Teiq.jpg', NULL, '', '', NULL, 0, 0, 1456893212, 1456893212);

DROP TABLE IF EXISTS `documents_to_team`;
CREATE TABLE IF NOT EXISTS `documents_to_team` (
  `teamId` int(11) NOT NULL,
  `docId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `documents_to_team` (`teamId`, `docId`) VALUES
(8, 17),
(8, 17);

DROP TABLE IF EXISTS `fbstatus`;
CREATE TABLE IF NOT EXISTS `fbstatus` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_text` text,
  `t_status` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

INSERT INTO `fbstatus` (`status_id`, `s_text`, `t_status`) VALUES
(11, '555', '2016-02-18 09:45:53');

DROP TABLE IF EXISTS `key_storage_item`;
CREATE TABLE IF NOT EXISTS `key_storage_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_key_storage_item_key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

INSERT INTO `key_storage_item` (`id`, `key`, `value`, `comment`, `updated_at`, `created_at`) VALUES
(1, 'backend.theme-skin', 'skin-red-light', '', 1455097595, NULL),
(2, 'system.pdfconverter', 'C:\\Program Files\\OpenOffice 4\\program\\soffice.exe --headless --convert-to pdf {filename-in} -outdir {filename-out}', 'cmd converter to pdf', 1455097060, 1455096991),
(3, 'common.siteName', 'Slide.chat', '', 1455098319, 1455098319),
(4, 'system.uploadDir', 'shared', '', 1455467372, 1455099250),
(5, 'common.thumbs_width', '116', '', 1455457927, 1455457803),
(6, 'common.thumbs_height', '150', '', 1455457937, 1455457827);

DROP TABLE IF EXISTS `letters`;
CREATE TABLE IF NOT EXISTS `letters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `linkDocId` int(11) NOT NULL,
  `templateId` int(11) NOT NULL,
  `key` varchar(32) NOT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `email_copy` varchar(255) DEFAULT NULL,
  `email_subject` varchar(255) DEFAULT NULL,
  `email_body` varchar(255) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `client_phone` varchar(255) DEFAULT NULL,
  `sheduller` datetime DEFAULT NULL,
  `sheduller_use` tinyint(4) NOT NULL,
  `isOpened` tinyint(4) NOT NULL DEFAULT '0',
  `opened_at` int(11) NOT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `isArchive` tinyint(4) DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

INSERT INTO `letters` (`id`, `userId`, `linkDocId`, `templateId`, `key`, `email_to`, `email_copy`, `email_subject`, `email_body`, `client_name`, `client_phone`, `sheduller`, `sheduller_use`, `isOpened`, `opened_at`, `type`, `isArchive`, `created_at`, `updated_at`) VALUES
(1, 13, 0, 0, '2UCtXqLOz1in9lDBEpfdu7hynBoeS4hX', 'ptiuma@mail.ru', '', 'bhjnklm', '<p>vghbjkml</p>', '������ ����', '555-55-55', NULL, 0, 0, 0, NULL, 0, 1456845740, 1456845740),
(2, 13, 0, 0, '4_yQcaymiL3yRAE8mdY8_ES2wyo9Vv25', 'ptiuma@mail.ru', '', 'bhjnklm', '<p>vghbjkml</p>', '������ ����', '555-55-55', NULL, 0, 1, 1456846055, NULL, 0, 1456846044, 1456846055),
(3, 13, 0, 0, 'oLznpzwIEbe8ir8-szP4DyV1NJHwF-hl', 'ptiuma@mail.ru', '', 'bhjnklm', '<p>vghbjkml</p>', '������ ����', '', NULL, 0, 1, 1456846277, NULL, 0, 1456846269, 1456907637);

DROP TABLE IF EXISTS `letters_attached_docs`;
CREATE TABLE IF NOT EXISTS `letters_attached_docs` (
  `letterId` int(11) NOT NULL,
  `docId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `links`;
CREATE TABLE IF NOT EXISTS `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `docId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `letterId` int(11) NOT NULL DEFAULT '0',
  `link_name` varchar(255) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `client_phone` varchar(255) DEFAULT NULL,
  `expired_date` datetime DEFAULT NULL,
  `isExpire` tinyint(4) NOT NULL,
  `protect_code` varchar(255) DEFAULT NULL,
  `isProtect` tinyint(4) NOT NULL,
  `denyDownloading` tinyint(4) DEFAULT NULL,
  `isOpened` tinyint(4) NOT NULL DEFAULT '0',
  `isBlocked` tinyint(4) NOT NULL,
  `isArchive` tinyint(4) DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

INSERT INTO `links` (`id`, `docId`, `userId`, `letterId`, `link_name`, `client_name`, `client_phone`, `expired_date`, `isExpire`, `protect_code`, `isProtect`, `denyDownloading`, `isOpened`, `isBlocked`, `isArchive`, `created_at`, `updated_at`) VALUES
(1, 14, 13, 3, 'jwKLSuRxLP', '������ ����', '', NULL, 0, NULL, 0, NULL, 0, 0, 0, 1456846269, 1456846269),
(2, 15, 13, 3, '1E5NybtM-N', '������ ����', '', NULL, 0, '', 0, NULL, 0, 0, 0, 1456846269, 1456853594),
(3, 17, 13, 0, 'hlDDnc2ntJ', 'Pavel', '222-333-444', NULL, 0, '', 0, NULL, 0, 0, 0, 1456895918, 1456895918);

DROP TABLE IF EXISTS `links_views`;
CREATE TABLE IF NOT EXISTS `links_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `docId` int(11) NOT NULL,
  `linkId` int(11) NOT NULL,
  `socketId` varchar(255) NOT NULL,
  `client_session` varchar(255) DEFAULT NULL,
  `client_ip` varchar(15) DEFAULT NULL,
  `client_browser` varchar(255) DEFAULT NULL,
  `client_device` tinyint(4) DEFAULT '0',
  `client_os` varchar(255) DEFAULT NULL,
  `client_platorm` varchar(255) DEFAULT NULL,
  `client_country` varchar(150) NOT NULL,
  `client_city` varchar(150) NOT NULL,
  `isAlertSended` tinyint(4) NOT NULL DEFAULT '0',
  `downloads` int(3) NOT NULL,
  `print` int(3) NOT NULL,
  `started_at` int(11) NOT NULL,
  `ended_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `links_views` (`id`, `docId`, `linkId`, `socketId`, `client_session`, `client_ip`, `client_browser`, `client_device`, `client_os`, `client_platorm`, `client_country`, `client_city`, `isAlertSended`, `downloads`, `print`, `started_at`, `ended_at`, `created_at`, `updated_at`) VALUES
(1, 0, 1, '', 'mGWN7chG8u5otAL5AAAA', '127.0.0.1', 'Mozilla Firefox', 1, 'windows', NULL, '��������', '������', 0, 0, 0, 1456907641, 1456907663, 1456907641, 0);

DROP TABLE IF EXISTS `links_views_pages`;
CREATE TABLE IF NOT EXISTS `links_views_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageId` int(11) NOT NULL,
  `linkId` int(11) NOT NULL,
  `client_session` varchar(255) NOT NULL,
  `viewId` int(11) NOT NULL,
  `started_at` int(11) NOT NULL,
  `ended_at` int(11) NOT NULL,
  `total_time` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

INSERT INTO `links_views_pages` (`id`, `pageId`, `linkId`, `client_session`, `viewId`, `started_at`, `ended_at`, `total_time`, `created_at`, `updated_at`) VALUES
(1, 41, 1, 'mGWN7chG8u5otAL5AAAA', 0, 1456907641, 1456907642, 0, 1456907641, 0),
(2, 41, 1, 'mGWN7chG8u5otAL5AAAA', 0, 1456907642, 1456907646, 0, 1456907642, 0),
(3, 42, 1, 'mGWN7chG8u5otAL5AAAA', 0, 1456907646, 1456907659, 0, 1456907646, 0),
(4, 43, 1, 'mGWN7chG8u5otAL5AAAA', 0, 1456907659, 1456907663, 0, 1456907659, 0);

DROP TABLE IF EXISTS `log_console`;
CREATE TABLE IF NOT EXISTS `log_console` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `log_time` int(11) DEFAULT NULL,
  `prefix` text,
  `message` text,
  `objectType` int(11) NOT NULL,
  `objectID` int(11) NOT NULL,
  `userID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

INSERT INTO `log_console` (`id`, `level`, `category`, `log_time`, `prefix`, `message`, `objectType`, `objectID`, `userID`) VALUES
(4, NULL, 'track', 1456141942, NULL, 'open 4444=444444', 0, 0, 1),
(5, NULL, 'register', 1456318499, NULL, 'New user # ', 0, 0, NULL),
(6, NULL, 'register', 1456319138, NULL, 'New user # 6', 0, 0, NULL),
(7, NULL, 'register', 1456319369, NULL, 'New user # 7', 0, 0, NULL),
(8, NULL, 'register', 1456319454, NULL, 'New user # 8', 0, 0, NULL),
(9, NULL, 'register', 1456319519, NULL, 'New user # 9', 0, 0, NULL),
(10, NULL, 'register', 1456319623, NULL, 'New user # 10', 0, 0, NULL),
(11, NULL, 'register', 1456319686, NULL, 'New user # 11', 0, 0, NULL),
(12, NULL, 'register', 1456319956, NULL, 'New user # 12', 0, 0, NULL),
(13, NULL, 'register', 1456811266, NULL, 'New user # 13', 0, 0, NULL);

DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1455012042),
('m130524_201442_init', 1455012049),
('m140209_132017_init', 1455012226),
('m140403_174025_create_account_table', 1455012227),
('m140504_113157_update_tables', 1455012230),
('m140504_130429_create_token_table', 1455012230),
('m140506_102106_rbac_init', 1455035502),
('m140618_045255_create_settings', 1455036894),
('m140830_171933_fix_ip_field', 1455012230),
('m140830_172703_change_account_table_name', 1455012231),
('m141222_110026_update_ip_field', 1455012231),
('m141222_135246_alter_username_length', 1455012231),
('m150614_103145_update_social_account_table', 1455012232),
('m150623_212711_fix_username_notnull', 1455012233),
('m151126_091910_add_unique_index', 1455036523);

DROP TABLE IF EXISTS `presentations_run`;
CREATE TABLE IF NOT EXISTS `presentations_run` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `docId` int(11) DEFAULT '0',
  `pageId` int(11) DEFAULT '0',
  `isClosed` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

INSERT INTO `presentations_run` (`id`, `userId`, `docId`, `pageId`, `isClosed`, `created_at`, `updated_at`) VALUES
(20, 12, NULL, 12, 0, 1456395436, 1456397800),
(21, 1, NULL, 38, 0, 1456800122, 1456800143),
(22, 13, NULL, 41, 0, 1456811342, 1456811642);

DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `public_email` varchar(255) DEFAULT NULL,
  `gravatar_email` varchar(255) DEFAULT NULL,
  `gravatar_id` varchar(32) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `bio` text,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `section` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

INSERT INTO `settings` (`id`, `type`, `section`, `key`, `value`, `active`, `created`, `modified`) VALUES
(1, 'string', 'system', 'pdfconverter', 'C:\\Program Files\\OpenOffice 4\\program\\soffice.exe --headless --convert-to pdf {filename-in} -outdir {filename-out}', 1, '2016-02-10 15:04:27', NULL),
(2, 'integer', 'system', 'thumbs_width', '116', 1, '2016-02-14 18:53:03', '2016-02-14 18:54:00'),
(3, 'integer', 'system', 'thumbs_height', '150', 1, '2016-02-14 18:54:44', NULL);

DROP TABLE IF EXISTS `social_account`;
CREATE TABLE IF NOT EXISTS `social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `data` text,
  `code` varchar(32) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_unique` (`provider`,`client_id`),
  UNIQUE KEY `account_unique_code` (`code`),
  KEY `fk_user_account` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `system_log`;
CREATE TABLE IF NOT EXISTS `system_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_time` int(11) NOT NULL,
  `prefix` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `idx_log_level` (`level`),
  KEY `idx_log_category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

INSERT INTO `system_log` (`id`, `level`, `category`, `log_time`, `prefix`, `message`) VALUES
(1, 1, 'yii\\base\\ErrorException:1', 1422259067, '[frontend][/yii2-starter-kit/frontend/web/]', 'exception ''yii\\base\\ErrorException'' with message ''Call to undefined function yii\\web\\symlink()'' in E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\AssetManager.php:449\nStack trace:\n#0 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\AssetBundle.php(162): yii\\web\\AssetManager->publish()\n#1 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\AssetManager.php(221): yii\\web\\AssetBundle->publish()\n#2 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\AssetManager.php(192): yii\\web\\AssetManager->loadBundle()\n#3 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\View.php(284): yii\\web\\AssetManager->getBundle()\n#4 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\AssetBundle.php(118): yii\\web\\View->registerAssetBundle()\n#5 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2-bootstrap\\Widget.php(64): yii\\web\\AssetBundle::register()\n#6 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2-bootstrap\\Carousel.php(89): yii\\bootstrap\\Widget->registerPlugin()\n#7 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\Widget.php(96): yii\\bootstrap\\Carousel->run()\n#8 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\frontend\\views\\site\\index.php(9): yii\\base\\Widget::widget()\n#9 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\View.php(325): ::unknown()\n#10 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile()\n#11 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile()\n#12 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\Controller.php(367): yii\\base\\View->render()\n#13 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\frontend\\controllers\\SiteController.php(35): yii\\base\\Controller->render()\n#14 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): frontend\\controllers\\SiteController->actionIndex()\n#15 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): ::call_user_func_array:{E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\InlineAction.php:55}()\n#16 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\Controller.php(151): yii\\base\\InlineAction->runWithParams()\n#17 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\Module.php(455): yii\\base\\Controller->runAction()\n#18 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\Application.php(83): yii\\base\\Module->runAction()\n#19 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest()\n#20 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\frontend\\web\\index.php(32): yii\\base\\Application->run()\n#21 {main}'),
(2, 1, 'yii\\base\\ErrorException:1', 1422259089, '[backend][/yii2-starter-kit/backend/web/sign-in/login]', 'exception ''yii\\base\\ErrorException'' with message ''Call to undefined function yii\\web\\symlink()'' in E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\AssetManager.php:449\nStack trace:\n#0 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\AssetBundle.php(162): yii\\web\\AssetManager->publish()\n#1 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\AssetManager.php(221): yii\\web\\AssetBundle->publish()\n#2 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\AssetManager.php(192): yii\\web\\AssetManager->loadBundle()\n#3 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\View.php(284): yii\\web\\AssetManager->getBundle()\n#4 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\AssetBundle.php(118): yii\\web\\View->registerAssetBundle()\n#5 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\validators\\RequiredValidator.php(108): yii\\web\\AssetBundle::register()\n#6 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\widgets\\ActiveField.php(751): yii\\validators\\RequiredValidator->clientValidateAttribute()\n#7 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\widgets\\ActiveField.php(210): yii\\widgets\\ActiveField->getClientOptions()\n#8 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\widgets\\ActiveField.php(200): yii\\widgets\\ActiveField->begin()\n#9 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2-bootstrap\\ActiveField.php(188): yii\\widgets\\ActiveField->render()\n#10 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\widgets\\ActiveField.php(156): yii\\bootstrap\\ActiveField->render()\n#11 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\View.php(18): yii\\widgets\\ActiveField->__toString()\n#12 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\View.php(325): ::unknown()\n#13 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\View.php(247): yii\\base\\View->renderPhpFile()\n#14 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\View.php(149): yii\\base\\View->renderFile()\n#15 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\Controller.php(367): yii\\base\\View->render()\n#16 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\backend\\controllers\\SignInController.php(48): yii\\base\\Controller->render()\n#17 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): backend\\controllers\\SignInController->actionLogin()\n#18 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\InlineAction.php(55): ::call_user_func_array:{E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\InlineAction.php:55}()\n#19 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\Controller.php(151): yii\\base\\InlineAction->runWithParams()\n#20 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\Module.php(455): yii\\base\\Controller->runAction()\n#21 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\web\\Application.php(83): yii\\base\\Module->runAction()\n#22 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\vendor\\yiisoft\\yii2\\base\\Application.php(375): yii\\web\\Application->handleRequest()\n#23 E:\\openserver\\domains\\yii2-test\\www\\yii2-starter-kit\\backend\\web\\index.php(32): yii\\base\\Application->run()\n#24 {main}');

DROP TABLE IF EXISTS `team`;
CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `userId` int(11) NOT NULL,
  `comments` text NOT NULL,
  `created_at` int(14) DEFAULT NULL,
  `updated_at` int(14) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

INSERT INTO `team` (`id`, `name`, `userId`, `comments`, `created_at`, `updated_at`) VALUES
(7, 'First Team', 1, '', 1456117700, 1456117700),
(8, 'Second team', 13, '', 1456930553, 1456930553);

DROP TABLE IF EXISTS `team_invites`;
CREATE TABLE IF NOT EXISTS `team_invites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `teamId` int(11) NOT NULL,
  `invite_role` int(11) NOT NULL,
  `invite_email` varchar(255) NOT NULL DEFAULT '',
  `invite_code` varchar(255) NOT NULL DEFAULT '',
  `isParsed` tinyint(4) NOT NULL DEFAULT '0',
  `comments` text NOT NULL,
  `created_at` int(14) DEFAULT NULL,
  `updated_at` int(14) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

INSERT INTO `team_invites` (`id`, `userId`, `teamId`, `invite_role`, `invite_email`, `invite_code`, `isParsed`, `comments`, `created_at`, `updated_at`) VALUES
(11, 13, 8, 0, 'ptiuma@mail.ru', 'nUIj343KN4-GXsEJbWI8', 0, '', 1456976101, 1456976101);

DROP TABLE IF EXISTS `team_members`;
CREATE TABLE IF NOT EXISTS `team_members` (
  `teamId` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `member_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `team_members` (`teamId`, `memberId`, `member_status`) VALUES
(7, 1, 1),
(7, 1, 2),
(8, 13, 1),
(8, 13, 2);

DROP TABLE IF EXISTS `token`;
CREATE TABLE IF NOT EXISTS `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_email` (`email`),
  UNIQUE KEY `user_unique_username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`) VALUES
(1, 'admin', 'ptiuma@mail.ru', '$2y$10$eIpHwu1wgXsqfi0IVyRgqOavwO3AtUbPed4VXHuO9tgrmIGLEbl52', 'NuV_kGY0BeIC3WnWwCeBULYpaO5_-bbK', 1455018449, NULL, NULL, '127.0.0.1', 1455018371, 1455035313, 0),
(12, 'ptiuma', 'ptiuma@list.ru', '$2y$10$G2NtKPN3.s/6Nn8GKzQ8m.s9E/l9hReJt8UlYnVQ8sQifjf1g4/Cu', 'OVbk1zR6I0VlQ3FVquieGNq8HfGb9f1c', 1456319956, NULL, NULL, '127.0.0.1', 1456319956, 1456319956, 0),
(13, 'ptiuma2', 'ptiuma@yandex.ru', '$2y$10$MT4JTkGCZI5AfHsZnmzwW.FbpewBqmSaIbe5V1gwnjLrjlrddgkMC', '1ku1lNEbjYrbQGXs8O4K0ShEUWx-8ySQ', 1456811264, NULL, NULL, '127.0.0.1', 1456811264, 1456811264, 0);

DROP TABLE IF EXISTS `user_settings`;
CREATE TABLE IF NOT EXISTS `user_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `balance` double(10,2) NOT NULL,
  `send_sms` tinyint(4) DEFAULT NULL,
  `send_email_alert` tinyint(4) DEFAULT NULL,
  `email_copy` varchar(255) DEFAULT NULL,
  `email_crm` varchar(255) DEFAULT NULL,
  `alert_no_open_att` tinyint(4) NOT NULL DEFAULT '1',
  `alert_after_day` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_user_settinngs` (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `user_settings` (`id`, `userId`, `balance`, `send_sms`, `send_email_alert`, `email_copy`, `email_crm`, `alert_no_open_att`, `alert_after_day`) VALUES
(1, 1, 0.00, NULL, 3, '', '45', 1, 1);

DROP TABLE IF EXISTS `user_templates`;
CREATE TABLE IF NOT EXISTS `user_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email_subject` varchar(255) NOT NULL,
  `email_body` varchar(255) DEFAULT NULL,
  `AttachmentCollection` text NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

INSERT INTO `user_templates` (`id`, `userId`, `name`, `email_subject`, `email_body`, `AttachmentCollection`, `created_at`, `updated_at`) VALUES
(5, 12, 'dfgh', 'xdfg', 'xcvbn', '', 1456670187, 1456670187),
(18, 1, 'fgh', 'bhjnklm', '<p>rt yg</p>', '7', 1456758197, 1456758197),
(19, 13, 'rtfygh', 'dfghjm', '<p>fghjk\r\n</p>', '15', 1456842759, 1456842759),
(20, 13, 'fghjk', 'bhjnklm', '<p>vghbjkml</p>', '15,14', 1456844583, 1456844583);
