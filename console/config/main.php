<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components' => [
	     'urlManager' => [
	    //s  'class' => 'yii\web\UrlManager',
	        'enablePrettyUrl' => true,
        'showScriptName' => false,
	       'scriptUrl' => 'http://slide.chat/',
 	    	 'hostInfo' => 'http://slide.chat/',
	         'baseUrl' => 'http://slide.chat/'
		],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];
