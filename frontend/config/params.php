<?php
return [
    'adminEmail' => 'admin@slide.chat',
     'icon-framework' => 'fa',
     'GOOGLE_API_KEY' => '',
     'facebook_link'=>'https://www.facebook.com/',
     'twitter_link'=>'https://www.twitter.com/',
     'publicPhone'=>'222-222-222',
     'publicEmail'=>'info@slide.chat',
];
