<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-practical-frontend',
    'name'=>'Slide.Chat',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log','devicedetect'],
    'controllerNamespace' => 'frontend\controllers',
'modules' => [
  /*  'poprigun_chat' => [
         'class' => \poprigun\chat\PChatModule::className(),
         'params' => [
             'pchat-settings' => [
                 'userModel' => \common\models\User::className(),


             ],
         ],
     ],
    */
],
  'components' => [
  /*
     'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
  */
   'view' => [
        'theme' => [
            'pathMap' => [
                '@dektrium/user/views' => '@app/views/user'
            ],
        ],
    ],
      'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ]
    ],
    'params' => $params,
];
