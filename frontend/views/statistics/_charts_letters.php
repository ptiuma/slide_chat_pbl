<?php
use kartik\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

use common\models\Documents;
use common\models\Letters;
use common\models\Links;
use common\models\LinksViews;
use frontend\models\UserTemplates;
$total=$type==3?Links::find()->where(['userId' => Yii::$app->user->id])->count():Letters::find()->where(['userId' => Yii::$app->user->id])->count();

?>
        &nbsp;
        <label class="control-label"><?=$type==3?'Ссылки':($type==2?'Письма без вложений':'Письма с вложениями')?></label>
<?php if($manager_data==1):?>
            <div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="row">
     <div class="col-md-12">
			<div class="box box-info">
               	<div class="box-header with-border">
              					<h4 class="box-title">Кол-во отправленных</h4>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">

 <?= GridView::widget([
        'dataProvider' =>$teamProvider,
           'pjax' => true,
           'bordered' => false,
            'layout' => '{items}',
            'showPageSummary' => false,
               // 'showCaption' => false,
           'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
        'email',
        [
		    'attribute' => 'visits',
		    'format' => 'raw',
		      'contentOptions'=>['style'=>'width: 75%;'],
		    'value' => function($data) use ($stats,$type) {
		    $tot=$type==1?Letters::find()->where(['userId'=>$data->id])->count():Links::find()->where(['userId'=>$data->id,'letterId'=>0])->count();
		    $percent=!$stats['total_sent']?0:intval($tot*100/$stats['total_sent']);
		    		   return '<div class="progress">
  <div class="progress-bar progress-bar-green progress-bar-striped" role="progressbar" aria-valuenow="'.$percent.'" aria-valuemin="0" aria-valuemax="100" style="width:'.$percent.'%;min-width: 2em;">
    '.$percent.' %
  </div>
</div>'; },
		],



],


    ]); ?>

		            </div>
        		  </div>
            </div>

            </div>

        <div class="col-md-12">
			<div class="box box-info">
               	<div class="box-header with-border">
              					<h4 class="box-title">Качество отправленных</h4>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">
<?php
if($type==3)
{
  $columns=[
        'email',
        [
		    'attribute' => 'visits',
		    'label'=>'Создано ссылок',
		    'format' => 'raw',
			    'value' => function($data) use ($stats) {
		    return Links::find()->where(['userId'=>$data->id,'letterId'=>0])->count();
		    },
		],
		    [
		    'attribute' => 'visits',
		    'label'=>'Открыто ссылок',
		    'format' => 'raw',
			    'value' => function($data) use ($stats) {
		    return Links::find()->where(['userId'=>$data->id,'letterId'=>0,'isOpened'=>1])->count();
		    },
		],
];
}
else
{
  $columns=[
        'email',
        [
		    'attribute' => 'visits',
		    'label'=>'Отправлено',
		    'format' => 'raw',
			    'value' => function($data) use ($stats) {
		    return Letters::find()->where(['userId'=>$data->id])->count();
		    },
		],
		    [
		    'attribute' => 'visits',
		    'label'=>'Открыто писем',
		    'format' => 'raw',
			    'value' => function($data) use ($stats) {
		    return Letters::find()->where(['userId'=>$data->id,'isOpened'=>1])->count();
		    },
		],
		    [
		    'attribute' => 'visits',
		    'label'=>'Открыто вложений',
		    'format' => 'raw',
			    'value' => function($data) use ($stats) {
		    return Letters::find()->joinWith('links',true,'RIGHT JOIN')->where(['letters.userId'=>$data->id,'links.isOpened'=>1])->count();
		    },
		],
];
}
?>
 <?= GridView::widget([
        'dataProvider' =>$teamProvider,
           'pjax' => true,
           'bordered' => false,
            'layout' => '{items}',
            'showPageSummary' => false,
          // 'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' => $columns,


    ]); ?>

		            </div>
        		  </div>
            </div>

            </div>

</div>
</div>
<? else:
//print_r($stats['pyramid']);
?>

		<div id="pyramidchartdiv<?=$type?>" style="width: 95%; background-color: #FFFFFF;" >
		<?=$this->render('_funnel', ['model' => $model,'stats'=>$stats,'type'=>$type])?>
		</div>
		<p>&nbsp;</p>
		<div id="chartdiv<?=$type?>" style="width: 100%; height: 360px; background-color: #FFFFFF;" ></div>
		<p>&nbsp;</p>
		<div id="chartdivhour<?=$type?>" style="width: 100%; height: 360px; background-color: #FFFFFF;" ></div>
		<p>&nbsp;</p>
<script type="text/javascript">
var total_sent='<?=$stats['total_sent']?>';
displayCharts('<?=$type?>',<?=json_encode($stats)?>);

		</script>
<?php endif;?>
<p>&nbsp;</p>
  <?=
            Html::a('Cтатистику по каждому документу Вы можете просмотреть в разделе "Мои документы"',['documents/'],['class'=>'btn btn-sm btn-default']);
            ?>