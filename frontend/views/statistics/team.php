<?php
use kartik\tabs\TabsX;
use  yii\helpers\Url;
use kartik\grid\GridView;
use kartik\daterange\DateRangePicker;
use yii\widgets\Pjax;
use kartik\icons\Icon;
Icon::map($this);
use kartik\helpers\Html;
use kartik\helpers\Enum;
use kartik\popover\PopoverX;
           use kartik\form\ActiveForm;

use common\models\Documents;
use common\models\Letters;
use common\models\Links;
use common\models\LinksViews;
$this->title = $team->name;
$this->params['breadcrumbs'][] = 'Командная Статистика';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/amcharts.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/serial.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/themes/light.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/themes/patterns.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/funnel.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/pie.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/personal.js',['depends' => 'yii\web\JqueryAsset']);

$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/funnel.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile(Yii::$app->request->baseUrl.'/frontend/assets/css/stats.css', ['depends' => ['frontend\assets\AppAsset']]);
?>
<?php
 Pjax::begin(['id' => 'team_stats']);?>
<div class="row">
<div class="col-md-5 col-md-offset-1">
<?php

$form = ActiveForm::begin(['id'=>'params']);
$data = [0 => 'Воронка продаж', 1 => 'Рейтинг Менеджеров'];
$layout3 = <<< HTML
    <span class="input-group-addon">от</span>
    {input1}
    <span class="input-group-addon">до</span>
    {input2}
HTML;
use kartik\date\DatePicker;
   echo '<label class="control-label">Диапозон дат</label>';
echo DatePicker::widget([
    'model' => $params_model,
    'attribute' => 'from_date',
    'attribute2' => 'to_date',
    'options' => ['placeholder' => 'Start date'],
    'options2' => ['placeholder' => 'End date'],
    'type' => DatePicker::TYPE_RANGE,
    'form' => $form,
           'layout' => $layout3,
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
    ],
      'pluginEvents' => [
    "changeDate" => "function(e) {  runTab() }",
]
]);
?><p>&nbsp;</p>

<?
echo $form->field($params_model, 'teamid')->hiddenInput()->label('');
echo $form->field($params_model, 'manager_data')->radioButtonGroup($data, [
    'class' => 'btn-group-sm',
    'itemOptions' => ['labelOptions' => ['class' => 'btn btn-primary']]
])->label('');?>

<?php
ActiveForm::end();

         ?>
</div>
</div>
<p></p>

<div class="row">
<div class="col-md-10 col-md-offset-1">
  <?

$items = [
    [
       'id'=>'tabLetters',
       'label'=>'<i class="fa fa-envelope-o"></i> Письма',
        'content'=>'',
        'active'=>true
    ],
    [
        'label'=>'<i class="fa fa-external-link"></i> Сcылки',
        'content'=>'',
        // 'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/statistics/tabs-data?tab=2'])]
    ],
       [
        'label'=>'<i class="fa fa-file-code-o"></i> Шаблоны',
        'content'=>'',
      //   'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/statistics/tabs-data?tab=3'])]
    ],
  ];

echo TabsX::widget([
    'items'=>$items,
    'id'=>'statTbs',
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false,
    'bordered'=>true,
     'pluginOptions' =>  ['enableCache'=>false],
]);

?>

</div>
</div>
	<?php
	  $js = <<< EOT
     runTab();

EOT;

$this->registerJs($js, yii\web\View::POS_READY);
	?>