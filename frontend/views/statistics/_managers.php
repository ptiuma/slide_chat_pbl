<?php
use kartik\tabs\TabsX;
use  yii\helpers\Url;
use kartik\grid\GridView;
use kartik\daterange\DateRangePicker;
use yii\widgets\Pjax;
use kartik\icons\Icon;
Icon::map($this);
use kartik\helpers\Html;
use kartik\helpers\Enum;
use kartik\popover\PopoverX;
           use kartik\form\ActiveForm;

use common\models\Documents;
use common\models\Letters;
use common\models\Links;
use common\models\LinksViews;
$this->title = $team->name;
$this->params['breadcrumbs'][] = 'Командная Статистика';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/amcharts.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/serial.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/themes/patterns
.js',['depends' => 'yii\web\JqueryAsset']);
?>
<?php
 Pjax::begin(['id' => 'team_stats']);?>
<div class="row">
<div class="col-md-4 col-md-offset-1">
<script>
   function reloadStats() {
	$.pjax({container: '#team_stats',
	 url: "?id=<?=$team->id?>&date_range=" + $('#date_range').val(),
	 });
}
</script>

   <?php
   echo '<label class="control-label">Диапозон дат</label>';
echo '<div class="input-group drp-container" style="width:100%"> ';
    echo DateRangePicker::widget([
    'id'=>'date_range',
    'name'=>'date_range',
      'value'=>$date_range,
    'useWithAddon'=>true,
      'convertFormat'=>true,
    'language'=>'ru',             // from demo config
     'hideInput'=>false,           // from demo config
    'presetDropdown'=>true,
 'pluginOptions'=>[
        'locale'=>[
          'format'=>'Y-m-d',
            'separator'=>' - ',
        ],
        'opens'=>'left'
    ],
    'pluginEvents' => [

"apply.daterangepicker" => "function() {reloadStats();}",

]
]);
echo '</div>';
?>
</div>
<div class="col-md-5">
         <?php

$form = ActiveForm::begin(['id'=>'params']);
$data = [0 => 'Sun', 1 => 'Mon', 2 => 'Tue', 3 => 'Wed', 4 => 'Thu', 5 => 'Fri', 6 => 'Sat'];

// Simple basic usage
echo $form->field($params_model, 'date1')->textinput();?>

<?php
ActiveForm::end();

         ?>
</div>
</div>
<p></p>
<div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="row">
     <div class="col-md-12">
			<div class="box box-info">
            			<div class="box-header with-border">
              					<h4 class="box-title">Статистика визитов</h4>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">

<?php $this->registerJs('
		AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "category",
					"autoMarginOffset": 40,
					"marginRight": 60,
					"marginTop": 60,
					"startDuration": 1,
					"fontSize": 13,
					"theme": "patterns",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"bullet": "round",
							"bulletSize": 10,
							"id": "AmGraph-1",
							"lineAlpha": 1,
							"lineThickness": 3,
							"title": "graph 1",
							"type": "smoothedLine",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": ""
						}
					],
					"allLabels": [],
					"balloon": {},
					"titles": [],
					"dataProvider": '.$stats.'
				}
			);
	   ');
	?>
		<div id="chartdiv" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>

		            </div>
        		  </div>
            </div>

            </div>


</div>
</div>
<?php
 Pjax::end();?>
<p>&nbsp;</p>
<div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="row">
     <div class="col-md-12">
			<div class="box box-info">
            			<div class="box-header with-border">
              					<h4 class="box-title">Команда (участников: <?= $teamProvider->getTotalCount() ?>)</h4>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">

 <?= GridView::widget([
        'dataProvider' =>$teamProvider,
           'pjax' => true,
           'bordered' => false,
            'layout' => '{items}',
            'showPageSummary' => false,
               // 'showCaption' => false,
         //  'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
        'email',
        [
		    'attribute' => 'letters',
		    'label'=>'Писем',
		    'format' => 'raw',
		    'value' => function($data) use ($stats) {
		    return Letters::find()->where(['userId'=>$data->id])->count(); },
		],
        [
		    'attribute' => 'links',
		    'label'=>'Ссылки',
		    'format' => 'raw',
		    'value' => function($data) use ($stats) {
		    return Links::find()->where(['userId'=>$data->id])->count(); },
		],
        [
		    'attribute' => 'visits',
		    'label'=>'Визиты',
		    'format' => 'raw',
		    'value' => function($data) use ($stats) {
		    		    return Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['userId'=>$data->id])->count(); },
		],



],


    ]); ?>

		            </div>
        		  </div>
            </div>

            </div>


</div>
</div>
   <p>&nbsp;</p>
<div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="row">
     <div class="col-md-12">
		<div class="box box-info">
            			<div class="box-header with-border">
              					<h3 class="box-title">Наиболее активные ссылки</h3>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">

 <?= GridView::widget([
        'dataProvider' =>$mostviewProvider,
           'pjax' => true,
           'bordered' => false,
            'layout' => '{items}',
            'showPageSummary' => false,
               // 'showCaption' => false,
         //  'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
        'link_name',

              	[
    'attribute' => 'doc',
    'label'=>'Документ',
    'format' => 'raw',
    'value' => function($data) { return $data->doc->name; },
],
        'client_name',
              	[
    'attribute' => 'doc',
    'label'=>'Создал',
    'format' => 'raw',
    'value' => function($data) { return $data->user->profile->name; },
],
             'created_at:date',

],


    ]); ?>

		            </div>
        		  </div>
            </div>

            </div>


</div>
</div>