<?php
use  yii\helpers\Url;
use kartik\icons\Icon;
Icon::map($this);
use kartik\helpers\Html;
use kartik\helpers\Enum;

?>
<div class="row statistic-box-content">
<?php if($type==3):?>
<div class="col-lg-12">
                        <h2>Движения клиента по воронке</h2>
                        <div class="row fbox">
                            <div class="col-lg-8 link-funnel-box cross-box">
                                <div class="row text-center">
                                    <span class=" small">
                                        Кликните на этап воронки для получения полезной информации.
                                    </span>
                                </div>
                                <div class="funnel-main-box">
                                    <div class="funnel-box box-5" data-funnel="target" data-funnel-target-id="l1">
                                        <div class="funnel-main">
                                            <span><?=$stats['total_sent']?></span>
                                        </div>
                                        <div class="funnel-add">
                                            <span class="text-box">создано</span>
                                            <span class="value-box">100%</span>

                                        </div>
                                    </div>
                                    <div class="funnel-box box-6" data-funnel="target" data-funnel-target-id="l2">
                                        <div class="funnel-main">
                                            <span><?=$stats['total_opened']?></span>
                                        </div>
                                        <div class="funnel-add">
                                            <span class="text-box">открыто</span>
                                            <span class="value-box"><?=$stats['total_sent']==0?'100':round((( $stats['total_opened'] / $stats['total_sent']) * 100),1)?>%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="info-icon">
                                    <span>i</span>
                                </div>
                                <div class="row text-center title">
                                    <span>Полезные советы</span>
                                </div>
                                <div class="info-box">
                                    <div style="display: none;" class="help-box" data-funnel="show" data-funnel-id="def">
                                        А как вы отправляете ссылку? Точнее как ваши клиенты привыкли их получать.
                                        Попробуйте ответить на данный вопрос, ведь ссылка дает возможность передавать
                                        документ не только через стандартное email письмо, но и через социальные
                                        сети, sms сообщения и месенджеры. Вы можете тестировать и выбрать лучший
                                        канал доставки вашего КП до вашей ЦА. Также вы можете публиковать ваши
                                        докуметы или презентации в соц сетях, получать уведомления о действиях
                                        читателей, общаться с ними с помощью чата или генерировать заявки с
                                        помощью встроеных кнопок обратной связи.
                                    </div>
                                    <div style="display: none;" class="help-box" data-funnel="show" data-funnel-id="l1">
                                        А как вы отправляете ссылку? Точнее как ваши клиенты привыкли их получать.
                                        Попробуйте ответить на данный вопрос, ведь ссылка дает возможность передавать
                                        документ не только через стандартное email письмо, но и через социальные
                                        сети, sms сообщения и месенджеры. Вы можете тестировать и выбрать лучший
                                        канал доставки вашего КП до вашей ЦА. Также вы можете публиковать ваши
                                        докуметы или презентации в соц сетях, получать уведомления о действиях
                                        читателей, общаться с ними с помощью чата или генерировать заявки с
                                        помощью встроеных кнопок обратной связи.
                                    </div>
                                    <div style="display: none;" class="help-box" data-funnel="show" data-funnel-id="l2">
                                        Открытие ссылок напрямую зависит от качества первичных переговоров.
                                        Клиент должен быть заинтересован и ждать вашего КП. Обратите внимание на
                                        способ доставки ссылки - возможно вашей ЦА удобно будет просматривать
                                        документ на мобильном устройстве и получать ее через «WhatsApp».
                                        Или же она очень консервативна и желает получать письмо с вложением.
                                        Обращайте внимание на график «Статистика по времени открытия ссылок»,
                                        он подскажет когда чаще всего просматривают ваши документы, отправляйте
                                        их незадолго до пика открываемости.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


  <?php elseif($type==2):?>
   <div class="col-lg-12">
                        <div class="h4 text-center">Движения клиента по воронке2.</div>

                        <div class="row fbox">
                            <div class="col-lg-8 message-funnel-box cross-box">
                                <div class="row text-center">
                                    <span class="small">
                                        Кликните на этап воронки для получения полезной информации.
                                    </span>
                                </div>
                                <div class="funnel-main-box">



                                    <div class="">
                                        <div class="funnel-box box-1" data-funnel="target" data-funnel-target-id="f1">
                                            <div class="funnel-main">
                                                <span><?=$stats['total_sent']?></span>
                                            </div>
                                            <div class="funnel-add">
                                                <span class="text-box">отправлено</span>
                                                <span class="value-box">100%</span>

                                            </div>
                                        </div>
                                        <div class="funnel-box box-2" data-funnel="target" data-funnel-target-id="f2">
                                            <div class="funnel-main">
                                                <span><?=$stats['total_sent']?></span>
                                            </div>
                                            <div class="funnel-add">
                                                <span class="text-box">доставлено</span>
                                                <span class="value-box">100%</span>
                                            </div>
                                        </div>
                                        <div class="funnel-box box-3" data-funnel="target" data-funnel-target-id="f3">
                                            <div class="funnel-main">
                                                <span><?=$stats['total_opened']?></span>
                                            </div>
                                            <div class="funnel-add">
                                                <span class="text-box">открыто</span>
                                                <span class="value-box"><?=$stats['total_sent']==0?'100':round((( $stats['total_opened'] / $stats['total_sent']) * 100),1)?>%</span>
                                                <span class="text-box">писем</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="info-icon">
                                    <span>i</span>
                                </div>
                                <div class="row text-center title">
                                    <span>Полезные советы</span>
                                </div>
                                <div class="info-box">
                                    <div style="display: block;" class="help-box" data-funnel="show" data-funnel-id="def">
                                        <div class="situation-box select-help" data-situation="0">
                                            <div class="row text-center">
                                                <h3>Вы отправили</h3>
                                                <h3>
                                                    <span id="count-mess"><?=$stats['total_sent']?></span>
                                                </h3>
                                            </div>
                                            <p style="">
                                                Для получения достоверной статистики необходимо отправить
                                                50 писем или выбрать больший период.
                                                <br>
                                                Если вы отправляете похожие письма клиентам,
                                                меняя только имя или, например, цену, сохраните его
                                                как шаблон и экономьте 1 час рабочего времени в день!
                                                <br>
                                                Желаем успеха, у вас все получится!
                                            </p>
                                        </div>
                                        <!--DelToOpenMess > 70 && OpenMessToOpenAttach > 65-->
                                        <div class="situation-box" data-situation="1">
                                            <p>
                                                <strong>Ваши показатели в норме, поздравляем!</strong><br>
                                                Теперь обратите внимание на детали:
                                            </p>
                                            <p>
                                                1. Вносите <strong>изменения в скрипт переговоров</strong> с клиентом,
                                                убедительно доносите ваши ценности.
                                            </p>
                                            <p>
                                                2. Обратите внимание на график «Зависимость просмотра вложений
                                                относительно времени открытия письма», он подскажет
                                                <strong>оптимальное время для отправки письма</strong>.
                                            </p>
                                            <p>
                                                <strong>Важно! Проводите А/Б тестирование</strong>,
                                                где А – исходная версия письма, Б - новая. Рекомендуем вносить
                                                изменения в версию Б, которые могут повлиять только на одну метрику,
                                                чтобы вы четко понимали на какой этап воронки влияете.
                                            </p>
                                        </div>
                                        <!--DelToOpenMess > 50 && OpenMessToOpenAttach > 65-->
                                        <div class="situation-box" data-situation="2">
                                            <p>
                                                У вас средние показатели открываемости писем и вложений. Для их улучшения работайте
                                                со статистикой, которая у Вас уже есть. Выделите отправки, когда
                                                конверсия была максимальной и проанализируйте их. Что привело к
                                                таким высоким показателям? Внедрите в работу свои лучшие практики.
                                            </p>
                                        </div>
                                        <!--DelToOpenMess < 50 && OpenMessToOpenAttach > 65-->
                                        <div class="situation-box" data-situation="3">
                                            <p>
                                                Ваши письма открывают всего <span class="to-DelToOpenMess"></span>%.<br>
                                                Чтобы <strong>увеличить открываемость писем</strong>, поработайте над:

                                            </p>
                                            <p>
                                                <strong>1. Качеством первичных переговоров</strong><br>
                                                Важно заинтересовать клиента, а не получить согласие на отправку КП.
                                            </p>
                                            <p>
                                                <strong>2. Темой письма и прехедером</strong><br>
                                                Обращайтесь к получателю по имени, укажите основной тезис вашего
                                                письма, и особое внимание уделите «прехедеру»! (первые фразы
                                                после темы письма, которые видны в общем списке писем в почте)
                                            </p>
                                            <p>
                                                <br>
                                                Ваши вложения открывают <span class="to-OpenMessToOpenAttach"></span>%.
                                                Это хороший показатель.
                                                <br>Чтобы достигнуть более высоких показателей работайте над деталими,
                                                создавайте у клиента ощущение единого диалога сквозь все виды коммуникаций:
                                                звонок (и/или встреча)-письмо-текст письма-предложение.

                                            </p>
                                        </div>
                                        <!--DelToOpenMess > 70 && OpenMessToOpenAttach > 50-->
                                        <div class="situation-box" data-situation="4">
                                            <p>
                                                Ваши клиенты «отваливаются» после открытия письма, вложения открывают
                                                <span class="to-OpenMessToOpenAttach"></span>%.<br>
                                                <strong>Чтобы достичь более высоких показателей, используйте в тексте письма:</strong>
                                            </p>
                                            <p>
                                                <strong>1. Персонализацию</strong>, избавьтесь от шаблонных штампов:
                                                «Как и обещал, высылаю вам КП», «По всем вопросам свяжитесь со мной
                                                по телефону» - они не работают.
                                            </p>
                                            <p>
                                                <strong>2. Ключевые выдержки из разговора</strong>, тезисно обозначьте
                                                интересные темы из КП, чтобы завлечь читателя
                                            </p>
                                            <p>
                                                <strong>3. Призыв к действию</strong>. Например «Николай, особое внимание
                                                прошу уделить 5-му слайду, на нем график по вашему запросу!».
                                            </p>
                                        </div>
                                        <!--DelToOpenMess > 50 && OpenMessToOpenAttach > 50-->
                                        <div class="situation-box" data-situation="5">
                                            <p>
                                                Ваши письма открывают <span class="to-DelToOpenMess"></span>%,
                                                а вложения - <span class="to-OpenMessToOpenAttach"></span>%.
                                                <strong>Чтобы достичь более высоких результатов, используйте:</strong>
                                            </p>
                                            <p>
                                                <strong>1. Персонализацию</strong>, избавьтесь от шаблонных штампов:
                                                «Как и обещал, высылаю вам КП», «По всем вопросам свяжитесь со мной
                                                по телефону» - они не работают.
                                            </p>
                                            <p>
                                                <strong>2. Ключевые выдержки из разговора</strong>, тезисно
                                                обозначьте интересные темы из КП, чтобы завлечь читателя.
                                            </p>
                                            <p>
                                                <strong>3. Призыв к действию</strong>. Например «Николай, особое
                                                внимание прошу уделить 5-му слайду, на нем график по вашему запросу!».
                                            </p>
                                        </div>
                                        <!--DelToOpenMess < 50 && OpenMessToOpenAttach > 50-->
                                        <div class="situation-box" data-situation="6">
                                            <p>
                                                Ваши письма открывают всего <span class="to-DelToOpenMess"></span>%.<br>
                                                Чтобы <strong>увеличить открываемость писем</strong>, поработайте над:
                                            </p>
                                            <p>
                                                <strong>1. Качеством первичных переговоров</strong><br>
                                                Важно заинтересовать клиента, а не получить согласие на отправку КП.
                                            </p>
                                            <p>
                                                <strong>2. Темой письма и прехедером</strong><br>
                                                Обращайтесь к получателю по имени, укажите основной тезис вашего письма, и особое внимание уделите «прехедеру»! (первые фразы после темы письма, которые видны в общем списке писем в почте)
                                            </p>

                                            <p>
                                                <br>
                                                Ваши вложения открывают <span class="to-OpenMessToOpenAttach"></span>%. Это средний показатель по рынку.<br>
                                                <strong>Чтобы достичь более высоких показателей, используйте:</strong>
                                            </p>
                                            <p>
                                                <strong>1. Персонализацию</strong>, избавьтесь от шаблонных штампов:
                                                «Как и обещал, высылаю вам КП», «По всем вопросам свяжитесь со мной
                                                по телефону» - они не работают.
                                            </p>
                                            <p>
                                                <strong>2. Ключевые выдержки из разговора</strong>, тезисно
                                                обозначьте интересные темы из КП, чтобы завлечь читателя.
                                            </p>
                                            <p>
                                                <strong>3. Призыв к действию.</strong> Например «Николай, особое
                                                внимание прошу уделить 5-му слайду, на нем график по вашему запросу!».
                                            </p>
                                        </div>
                                        <!--DelToOpenMess > 70 && OpenMessToOpenAttach < 50-->
                                        <div class="situation-box" data-situation="7">
                                            <p>
                                                Вы теряете клиентов после открытия письма. Ваши вложения открывают
                                                <span class="to-OpenMessToOpenAttach"></span>%. Клиенты не готовы к вашему предложению.
                                                Рекомендуем поработать над качеством первичных переговоров.
                                                Важно не получить согласие об отправке КП, а заинтересовать клиента
                                                тем, что будет в этом КП. Убедитесь, что предложение ему сейчас
                                                нужно и он его ждет.
                                            </p>
                                        </div>
                                        <!--DelToOpenMess > 50 && OpenMessToOpenAttach < 50-->
                                        <div class="situation-box" data-situation="8">
                                            <p>
                                                Ваши вложения открывают только <span class="to-OpenMessToOpenAttach"></span>%.
                                                Клиенты не готовы к вашему предложению. Рекомендуем поработать над
                                                качеством первичных переговоров. Важно не получить согласие об
                                                отправке КП, а заинтересовать клиента тем, что будет в этом КП.
                                            </p>
                                            <p>
                                                Обратите внимание на те ситуации, когда вы отправляете предложение
                                                клиенту. Вы уверены, что предложение ему сейчас нужно и он его ждет?
                                            </p>
                                        </div>
                                        <!--DelToOpenMess < 50 && OpenMessToOpenAttach < 50-->
                                        <div class="situation-box" data-situation="9">
                                            <p>
                                                <strong>Вы теряете половину своих клиентов!</strong>
                                                <br>Обратите внимание на:
                                            </p>
                                            <p>
                                                <strong>1. Качество первичных переговоров</strong><br>
                                                Важно заинтересовать клиента, а не получить согласие на отправку КП.
                                            </p>
                                            <p>
                                                <strong>2. Тема письма и прехедер</strong><br>
                                                Письмо с темой «Высылаю вам КП» открывают мало. Обращайтесь к получателю по имени, укажите основной тезис вашего письма, заинтересуйте клиента!
                                            </p>
                                            <p>
                                                <strong>3. Содержание письма</strong><br>
                                                Пишите персонализировано, чтобы ваше письмо не казалось «спамом», упоминайте контекст разговора. Обозначьте  преимущества вашего КП, используйте СТА (призыв к действию)
                                            </p>
                                            <p>
                                                У вас все получится!
                                            </p>
                                        </div>
                                    </div>
                                    <!--Отправленные письма-->
                                    <div style="display: none;" class="help-box" data-funnel="show" data-funnel-id="f1">
                                        <h4>Отправленные письма</h4>
                                        <p>
                                            Наш сервис предназначен для адресной отправки коммерческих предложений по запросу или предварительному согласию получателя.
                                        </p>
                                        <p>
                                            Не допускается использование сервиса для массовой рассылки писем.
                                        </p>
                                        <p>
                                            <strong>Запрещается</strong> отправка коммерческих предложений по следующим спискам адресов: купленные списки, арендованные списки, списки, полученные автоматически с помощью программ-экстракторов.
                                        </p>
                                        <p>
                                            В противном случае ваш аккаунт будет заблокирован на неопределённый срок без возврата денежных средств.
                                        </p>
                                    </div>
                                    <!--Доставленные письма-->
                                    <div style="display: none;" class="help-box" data-funnel="show" data-funnel-id="f2">
                                        <h4>Доставленные письма</h4>
                                        <p>
                                            Отправлено - не всегда доставлено.
                                            Отправляя письма на некорректные адреса или используя шаблонные сообщения,
                                            после которых получатели добавляют вас в спам, вы теряете рейтинг,
                                            установленный почтовыми службами. Чем ниже рейтинг, тем больше
                                            вероятность, что почтовый фильтр клиента отправит ваше письмо в спам.
                                            Процент доставляемости будет уменьшаться. По статистике, хороший процент
                                            доставляемости <strong>97%-100%</strong>.
                                        </p>
                                    </div>
                                    <!--Открыто писем-->
                                    <div style="display: none;" class="help-box" data-funnel="show" data-funnel-id="f3">
                                        <h4>Открыто писем</h4>
                                        <p>
                                            Открытие писем напрямую зависит от качества первичных переговоров.
                                            Клиент должен быть заинтересован и ждать вашего письма.
                                        </p>
                                        <p>
                                            Также обратите внимание на тему письма и прехедер
                                            (первые фразы после темы письма, которые видны в общем списке писем в
                                            почте)<br>
                                            Не используйте стоп слова(<i>«бесплатно», «купите сейчас»</i>),
                                            указывайте ценность для получателя (<i>«Имя, смета и договор для вас готовы!»</i>)
                                        </p>
                                        <p>
                                            Создавайте и тестируйте шаблоны писем. Тема, прехедер, обращение (текст).
                                        </p>
                                    </div>
                                    <!--Открыто вложений-->
                                    <div style="display: none;" class="help-box" data-funnel="show" data-funnel-id="f4">
                                        <h4>Открыто вложений</h4>
                                        <p>
                                            Открытие вложений зависит от:
                                        </p>
                                        <p>
                                            </p><dl>
                                                <dt>
                                                    - Текст сообщения.
                                                </dt>
                                                <dd>
                                                    Не используйте шаблонных обращений, пишите личное сообщение,
                                                    с упоминаем ключевых выдержек из разговора. Сформулируйте призыв
                                                    к действию. Например:
                                                    <i>
                                                        «Николай, особое внимание прошу уделить 5-му слайду -
                                                        на нем график по вашему запросу!».
                                                    </i>
                                                </dd>
                                                <dt>
                                                    - Время отправки.
                                                </dt>
                                                <dd>
                                                    Обращайте внимание на график «Зависимость просмотра вложений
                                                    относительно времени открытия письма», он подскажет оптимальное
                                                    время для отправки письма.
                                                </dd>
                                            </dl>
                                        <p></p>
                                        <p>
                                            Не забывайте создавать и тестировать шаблоны писем.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

   <?php else:?>
   <div class="col-lg-12">
                        <div class="h4 text-center">Движения клиента по воронке.</div>

                        <div class="row fbox">
                            <div class="col-lg-8 message-funnel-box cross-box">
                                <div class="row text-center">
                                    <span class="small">
                                        Кликните на этап воронки для получения полезной информации.
                                    </span>
                                </div>
                                <div class="funnel-main-box">

                                    <div class="convert-box-1" data-funnel="target" data-funnel-target-id="f3">
                                        <div class="title-box">
                                            <span>Конверсия с доставленных в открытые письма</span>
                                        </div>
                                        <span class="arrow-box">

                                        </span>
                                        <div class="pc-main-box">
                                            <div class="b-pc-box">
                                                <span>
                                                    <span class="pc-val">100</span>
                                                    <span class="pc-span">%</span>
                                                </span>
                                            </div>
                                            <div class="l-pc-box">-<?=$stats['total_sent']==0?'100':round((( $stats['total_opened'] / $stats['total_sent']) * 100),1)?>%</div>

                                        </div>
                                    </div>
                                    <div class="convert-box-2" data-funnel="target" data-funnel-target-id="f4">
                                        <div class="title-box">
                                            <span>Конверсия с отк. писем в отк. вложения</span>
                                        </div>
                                        <span class="arrow-box">

                                        </span>
                                        <div class="pc-main-box">
                                            <div class="b-pc-box">
                                                <span>
                                                    <span class="pc-val">100</span>
                                                    <span class="pc-span">%</span>
                                                </span>
                                            </div>
                                            <div class="l-pc-box">-<?=$stats['total_opened']=='0'?'0':round((( $stats['total_opened_att'] / $stats['total_opened']) * 100),1)?>%</div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="funnel-box box-1" data-funnel="target" data-funnel-target-id="f1">
                                            <div class="funnel-main">
                                                <span><?=$stats['total_sent']?></span>
                                            </div>
                                            <div class="funnel-add">
                                                <span class="text-box">отправлено</span>
                                                <span class="value-box">100%</span>

                                            </div>
                                        </div>
                                        <div class="funnel-box box-2" data-funnel="target" data-funnel-target-id="f2">
                                            <div class="funnel-main">
                                                <span><?=$stats['total_sent']?></span>
                                            </div>
                                            <div class="funnel-add">
                                                <span class="text-box">доставлено</span>
                                                <span class="value-box">100%</span>
                                            </div>
                                        </div>
                                        <div class="funnel-box box-3" data-funnel="target" data-funnel-target-id="f3">
                                            <div class="funnel-main">
                                                <span><?=$stats['total_opened']?></span>
                                            </div>
                                            <div class="funnel-add">
                                                <span class="text-box">открыто</span>
                                                <span class="value-box"><?=$stats['total_sent']==0?'100':round((( $stats['total_opened'] / $stats['total_sent']) * 100),1)?>%</span>
                                                <span class="text-box">писем</span>
                                            </div>
                                        </div>
                                        <div class="funnel-box box-4" data-funnel="target" data-funnel-target-id="f4">
                                            <div class="funnel-main">
                                                <span><?=$stats['total_opened_att']?></span>
                                            </div>
                                            <div class="funnel-add">
                                                <span class="text-box">открыто</span>
                                                <span class="value-box"><?=$stats['total_sent']==0?'100':round((( $stats['total_opened_att'] / $stats['total_sent']) * 100),1)?>%</span>
                                                <span class="text-box">вложений</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="info-icon">
                                    <span>i</span>
                                </div>
                                <div class="row text-center title">
                                    <span>Полезные советы</span>
                                </div>
                                <div class="info-box">
                                    <div style="display: block;" class="help-box" data-funnel="show" data-funnel-id="def">
                                        <div class="situation-box select-help" data-situation="0">
                                            <div class="row text-center">
                                                <h3>Вы отправили</h3>
                                                <h3>
                                                    <span id="count-mess"><?=$stats['total_sent']?></span>
                                                </h3>
                                            </div>
                                            <p style="">
                                                Для получения достоверной статистики необходимо отправить
                                                50 писем или выбрать больший период.
                                                <br>
                                                Если вы отправляете похожие письма клиентам,
                                                меняя только имя или, например, цену, сохраните его
                                                как шаблон и экономьте 1 час рабочего времени в день!
                                                <br>
                                                Желаем успеха, у вас все получится!
                                            </p>
                                        </div>
                                        <!--DelToOpenMess > 70 && OpenMessToOpenAttach > 65-->
                                        <div class="situation-box" data-situation="1">
                                            <p>
                                                <strong>Ваши показатели в норме, поздравляем!</strong><br>
                                                Теперь обратите внимание на детали:
                                            </p>
                                            <p>
                                                1. Вносите <strong>изменения в скрипт переговоров</strong> с клиентом,
                                                убедительно доносите ваши ценности.
                                            </p>
                                            <p>
                                                2. Обратите внимание на график «Зависимость просмотра вложений
                                                относительно времени открытия письма», он подскажет
                                                <strong>оптимальное время для отправки письма</strong>.
                                            </p>
                                            <p>
                                                <strong>Важно! Проводите А/Б тестирование</strong>,
                                                где А – исходная версия письма, Б - новая. Рекомендуем вносить
                                                изменения в версию Б, которые могут повлиять только на одну метрику,
                                                чтобы вы четко понимали на какой этап воронки влияете.
                                            </p>
                                        </div>
                                        <!--DelToOpenMess > 50 && OpenMessToOpenAttach > 65-->
                                        <div class="situation-box" data-situation="2">
                                            <p>
                                                У вас средние показатели открываемости писем и вложений. Для их улучшения работайте
                                                со статистикой, которая у Вас уже есть. Выделите отправки, когда
                                                конверсия была максимальной и проанализируйте их. Что привело к
                                                таким высоким показателям? Внедрите в работу свои лучшие практики.
                                            </p>
                                        </div>
                                        <!--DelToOpenMess < 50 && OpenMessToOpenAttach > 65-->
                                        <div class="situation-box" data-situation="3">
                                            <p>
                                                Ваши письма открывают всего <span class="to-DelToOpenMess"></span>%.<br>
                                                Чтобы <strong>увеличить открываемость писем</strong>, поработайте над:

                                            </p>
                                            <p>
                                                <strong>1. Качеством первичных переговоров</strong><br>
                                                Важно заинтересовать клиента, а не получить согласие на отправку КП.
                                            </p>
                                            <p>
                                                <strong>2. Темой письма и прехедером</strong><br>
                                                Обращайтесь к получателю по имени, укажите основной тезис вашего
                                                письма, и особое внимание уделите «прехедеру»! (первые фразы
                                                после темы письма, которые видны в общем списке писем в почте)
                                            </p>
                                            <p>
                                                <br>
                                                Ваши вложения открывают <span class="to-OpenMessToOpenAttach"></span>%.
                                                Это хороший показатель.
                                                <br>Чтобы достигнуть более высоких показателей работайте над деталими,
                                                создавайте у клиента ощущение единого диалога сквозь все виды коммуникаций:
                                                звонок (и/или встреча)-письмо-текст письма-предложение.

                                            </p>
                                        </div>
                                        <!--DelToOpenMess > 70 && OpenMessToOpenAttach > 50-->
                                        <div class="situation-box" data-situation="4">
                                            <p>
                                                Ваши клиенты «отваливаются» после открытия письма, вложения открывают
                                                <span class="to-OpenMessToOpenAttach"></span>%.<br>
                                                <strong>Чтобы достичь более высоких показателей, используйте в тексте письма:</strong>
                                            </p>
                                            <p>
                                                <strong>1. Персонализацию</strong>, избавьтесь от шаблонных штампов:
                                                «Как и обещал, высылаю вам КП», «По всем вопросам свяжитесь со мной
                                                по телефону» - они не работают.
                                            </p>
                                            <p>
                                                <strong>2. Ключевые выдержки из разговора</strong>, тезисно обозначьте
                                                интересные темы из КП, чтобы завлечь читателя
                                            </p>
                                            <p>
                                                <strong>3. Призыв к действию</strong>. Например «Николай, особое внимание
                                                прошу уделить 5-му слайду, на нем график по вашему запросу!».
                                            </p>
                                        </div>
                                        <!--DelToOpenMess > 50 && OpenMessToOpenAttach > 50-->
                                        <div class="situation-box" data-situation="5">
                                            <p>
                                                Ваши письма открывают <span class="to-DelToOpenMess"></span>%,
                                                а вложения - <span class="to-OpenMessToOpenAttach"></span>%.
                                                <strong>Чтобы достичь более высоких результатов, используйте:</strong>
                                            </p>
                                            <p>
                                                <strong>1. Персонализацию</strong>, избавьтесь от шаблонных штампов:
                                                «Как и обещал, высылаю вам КП», «По всем вопросам свяжитесь со мной
                                                по телефону» - они не работают.
                                            </p>
                                            <p>
                                                <strong>2. Ключевые выдержки из разговора</strong>, тезисно
                                                обозначьте интересные темы из КП, чтобы завлечь читателя.
                                            </p>
                                            <p>
                                                <strong>3. Призыв к действию</strong>. Например «Николай, особое
                                                внимание прошу уделить 5-му слайду, на нем график по вашему запросу!».
                                            </p>
                                        </div>
                                        <!--DelToOpenMess < 50 && OpenMessToOpenAttach > 50-->
                                        <div class="situation-box" data-situation="6">
                                            <p>
                                                Ваши письма открывают всего <span class="to-DelToOpenMess"></span>%.<br>
                                                Чтобы <strong>увеличить открываемость писем</strong>, поработайте над:
                                            </p>
                                            <p>
                                                <strong>1. Качеством первичных переговоров</strong><br>
                                                Важно заинтересовать клиента, а не получить согласие на отправку КП.
                                            </p>
                                            <p>
                                                <strong>2. Темой письма и прехедером</strong><br>
                                                Обращайтесь к получателю по имени, укажите основной тезис вашего письма, и особое внимание уделите «прехедеру»! (первые фразы после темы письма, которые видны в общем списке писем в почте)
                                            </p>

                                            <p>
                                                <br>
                                                Ваши вложения открывают <span class="to-OpenMessToOpenAttach"></span>%. Это средний показатель по рынку.<br>
                                                <strong>Чтобы достичь более высоких показателей, используйте:</strong>
                                            </p>
                                            <p>
                                                <strong>1. Персонализацию</strong>, избавьтесь от шаблонных штампов:
                                                «Как и обещал, высылаю вам КП», «По всем вопросам свяжитесь со мной
                                                по телефону» - они не работают.
                                            </p>
                                            <p>
                                                <strong>2. Ключевые выдержки из разговора</strong>, тезисно
                                                обозначьте интересные темы из КП, чтобы завлечь читателя.
                                            </p>
                                            <p>
                                                <strong>3. Призыв к действию.</strong> Например «Николай, особое
                                                внимание прошу уделить 5-му слайду, на нем график по вашему запросу!».
                                            </p>
                                        </div>
                                        <!--DelToOpenMess > 70 && OpenMessToOpenAttach < 50-->
                                        <div class="situation-box" data-situation="7">
                                            <p>
                                                Вы теряете клиентов после открытия письма. Ваши вложения открывают
                                                <span class="to-OpenMessToOpenAttach"></span>%. Клиенты не готовы к вашему предложению.
                                                Рекомендуем поработать над качеством первичных переговоров.
                                                Важно не получить согласие об отправке КП, а заинтересовать клиента
                                                тем, что будет в этом КП. Убедитесь, что предложение ему сейчас
                                                нужно и он его ждет.
                                            </p>
                                        </div>
                                        <!--DelToOpenMess > 50 && OpenMessToOpenAttach < 50-->
                                        <div class="situation-box" data-situation="8">
                                            <p>
                                                Ваши вложения открывают только <span class="to-OpenMessToOpenAttach"></span>%.
                                                Клиенты не готовы к вашему предложению. Рекомендуем поработать над
                                                качеством первичных переговоров. Важно не получить согласие об
                                                отправке КП, а заинтересовать клиента тем, что будет в этом КП.
                                            </p>
                                            <p>
                                                Обратите внимание на те ситуации, когда вы отправляете предложение
                                                клиенту. Вы уверены, что предложение ему сейчас нужно и он его ждет?
                                            </p>
                                        </div>
                                        <!--DelToOpenMess < 50 && OpenMessToOpenAttach < 50-->
                                        <div class="situation-box" data-situation="9">
                                            <p>
                                                <strong>Вы теряете половину своих клиентов!</strong>
                                                <br>Обратите внимание на:
                                            </p>
                                            <p>
                                                <strong>1. Качество первичных переговоров</strong><br>
                                                Важно заинтересовать клиента, а не получить согласие на отправку КП.
                                            </p>
                                            <p>
                                                <strong>2. Тема письма и прехедер</strong><br>
                                                Письмо с темой «Высылаю вам КП» открывают мало. Обращайтесь к получателю по имени, укажите основной тезис вашего письма, заинтересуйте клиента!
                                            </p>
                                            <p>
                                                <strong>3. Содержание письма</strong><br>
                                                Пишите персонализировано, чтобы ваше письмо не казалось «спамом», упоминайте контекст разговора. Обозначьте  преимущества вашего КП, используйте СТА (призыв к действию)
                                            </p>
                                            <p>
                                                У вас все получится!
                                            </p>
                                        </div>
                                    </div>
                                    <!--Отправленные письма-->
                                    <div style="display: none;" class="help-box" data-funnel="show" data-funnel-id="f1">
                                        <h4>Отправленные письма</h4>
                                        <p>
                                            Наш сервис предназначен для адресной отправки коммерческих предложений по запросу или предварительному согласию получателя.
                                        </p>
                                        <p>
                                            Не допускается использование сервиса для массовой рассылки писем.
                                        </p>
                                        <p>
                                            <strong>Запрещается</strong> отправка коммерческих предложений по следующим спискам адресов: купленные списки, арендованные списки, списки, полученные автоматически с помощью программ-экстракторов.
                                        </p>
                                        <p>
                                            В противном случае ваш аккаунт будет заблокирован на неопределённый срок без возврата денежных средств.
                                        </p>
                                    </div>
                                    <!--Доставленные письма-->
                                    <div style="display: none;" class="help-box" data-funnel="show" data-funnel-id="f2">
                                        <h4>Доставленные письма</h4>
                                        <p>
                                            Отправлено - не всегда доставлено.
                                            Отправляя письма на некорректные адреса или используя шаблонные сообщения,
                                            после которых получатели добавляют вас в спам, вы теряете рейтинг,
                                            установленный почтовыми службами. Чем ниже рейтинг, тем больше
                                            вероятность, что почтовый фильтр клиента отправит ваше письмо в спам.
                                            Процент доставляемости будет уменьшаться. По статистике, хороший процент
                                            доставляемости <strong>97%-100%</strong>.
                                        </p>
                                    </div>
                                    <!--Открыто писем-->
                                    <div style="display: none;" class="help-box" data-funnel="show" data-funnel-id="f3">
                                        <h4>Открыто писем</h4>
                                        <p>
                                            Открытие писем напрямую зависит от качества первичных переговоров.
                                            Клиент должен быть заинтересован и ждать вашего письма.
                                        </p>
                                        <p>
                                            Также обратите внимание на тему письма и прехедер
                                            (первые фразы после темы письма, которые видны в общем списке писем в
                                            почте)<br>
                                            Не используйте стоп слова(<i>«бесплатно», «купите сейчас»</i>),
                                            указывайте ценность для получателя (<i>«Имя, смета и договор для вас готовы!»</i>)
                                        </p>
                                        <p>
                                            Создавайте и тестируйте шаблоны писем. Тема, прехедер, обращение (текст).
                                        </p>
                                    </div>
                                    <!--Открыто вложений-->
                                    <div style="display: none;" class="help-box" data-funnel="show" data-funnel-id="f4">
                                        <h4>Открыто вложений</h4>
                                        <p>
                                            Открытие вложений зависит от:
                                        </p>
                                        <p>
                                            </p><dl>
                                                <dt>
                                                    - Текст сообщения.
                                                </dt>
                                                <dd>
                                                    Не используйте шаблонных обращений, пишите личное сообщение,
                                                    с упоминаем ключевых выдержек из разговора. Сформулируйте призыв
                                                    к действию. Например:
                                                    <i>
                                                        «Николай, особое внимание прошу уделить 5-му слайду -
                                                        на нем график по вашему запросу!».
                                                    </i>
                                                </dd>
                                                <dt>
                                                    - Время отправки.
                                                </dt>
                                                <dd>
                                                    Обращайте внимание на график «Зависимость просмотра вложений
                                                    относительно времени открытия письма», он подскажет оптимальное
                                                    время для отправки письма.
                                                </dd>
                                            </dl>
                                        <p></p>
                                        <p>
                                            Не забывайте создавать и тестировать шаблоны писем.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
   <?php endif;?>
         </div>