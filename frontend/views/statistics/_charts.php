<?php
use kartik\helpers\Html;
use yii\helpers\Url;
use common\models\Links;
use common\models\Letters;
$total=$type==1?Letters::find()->where(['userId' => Yii::$app->user->id])->count():Links::find()->where(['userId' => Yii::$app->user->id])->count();

?>
        &nbsp;

<label class="control-label"><?=$type==1?'Письма':'Ссылки'?></label>
<?php if($total=='0'):?>
   <?php
echo Html::jumbotron([
    'heading' => '',
    'body' => 'В настоящее время ещё нет созданных Вами '.($type==1?'писем':'ссылок').'.<br>Создайте ссылки, или отправьте письмо для доступа к статистике',

]);
?>
<?php else:?>
		<div id="pyramidchartdiv<?=$type?>" style="width: 60%; height: 300px; background-color: #FFFFFF;" ></div>
		<p>&nbsp;</p>
		<div id="chartdiv<?=$type?>" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
		<p>&nbsp;</p>
		<div id="chartdivhour<?=$type?>" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
	<?php
	  $js = <<< EOT
     runCharts($type);

EOT;

$this->registerJs($js, yii\web\View::POS_READY);
	?>
	<?php endif;?>

