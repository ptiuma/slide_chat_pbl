<?php
use kartik\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

use common\models\Documents;
use common\models\Letters;
use common\models\Links;
use common\models\LinksViews;
use frontend\models\UserTemplates;
$total=$type==1?Letters::find()->where(['userId' => Yii::$app->user->id])->count():Links::find()->where(['userId' => Yii::$app->user->id])->count();

?>
        &nbsp;

<label class="control-label">Шаблоны</label>

<?php if($manager_data==1):?>
            <div class="row">
<div class="col-md-10 col-md-offset-1">
<div class="row">
     <div class="col-md-12">
			<div class="box box-info">
               	<div class="box-header with-border">
              					<h4 class="box-title">Кол-во писем с шаблонами</h4>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">

 <?= GridView::widget([
        'dataProvider' =>$teamProvider,
           'pjax' => true,
           'bordered' => false,
            'layout' => '{items}',
            'showPageSummary' => false,
               // 'showCaption' => false,
         //  'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
        'email',
        [
		    'attribute' => 'letters',
		    'label'=>'Шаблоны',
		    'format' => 'raw',
		    'value' => function($data) use ($stats) {
		    return UserTemplates::find()->where(['userId'=>$data->id])->count(); },
		],
        [
		    'attribute' => 'links',
		    'label'=>'Отослано с шаблоном',
		    'format' => 'raw',
		    'value' => function($data) use ($stats) {
		    return Letters::find()->where(['userId'=>$data->id])->andFilterWhere(['!=','templateId',0])->count(); },
		],
        [
		    'attribute' => 'visits',
		    'label'=>'Открыто писем',
		    'format' => 'raw',
		    'value' => function($data) use ($stats) {
		    		   return Letters::find()->where(['userId'=>$data->id,'isOpened'=>1])->andFilterWhere(['!=','templateId',0])->count(); },
		],



],


    ]); ?>

		            </div>
        		  </div>
            </div>

            </div>


</div>
</div>
<? else:
//print_r($stats);
?>
<?php if(!$stats['total_templates']):?>
                  <?php
echo Html::jumbotron([
    'heading' => '',
    'body' => 'В настоящее время ещё нет созданных шаблонов.<br>при создании письма сохраните его шаблон, чтобы видеть по статистике какие шаблоны у вас более успешные',

]);
?>
<?php else:?>

	<div id="chartdivpie<?=$type?>" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
                               <p>&nbsp;</p>
	<div id="chartdivcolumn<?=$type?>" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
	<script type="text/javascript">
displayCharts('<?=$type?>',<?=json_encode($stats)?>);

		</script>
<?php endif;?>
<?php endif;?>