<?php
use kartik\tabs\TabsX;
use yii\helpers\Url;
use yii\widgets\Pjax;
           use kartik\form\ActiveForm;
$this->title = 'Статистика';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/amcharts.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/serial.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/funnel.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/pie.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/themes/light.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/personal.js',['depends' => 'yii\web\JqueryAsset']);

$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/funnel.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerCssFile(Yii::$app->request->baseUrl.'/frontend/assets/css/stats.css', ['depends' => ['frontend\assets\AppAsset']]);
?>
<div class="row">
<div class="col-md-3 col-md-offset-1">
&nbsp;
</div>
</div>
<div class="row">
<div class="col-md-5 col-md-offset-1">
<script>
   function reloadStats() {
runTab()

}
</script>
<?php

$form = ActiveForm::begin(['id'=>'params']);
$data = [0 => 'Воронка продаж', 1 => 'Менеджеры'];
$layout3 = <<< HTML
    <span class="input-group-addon">от</span>
    {input1}
    <span class="input-group-addon">до</span>
    {input2}
HTML;
use kartik\date\DatePicker;
   echo '<label class="control-label">Выберите период</label>';
echo DatePicker::widget([
    'model' => $params_model,
    'attribute' => 'from_date',
    'attribute2' => 'to_date',
    'options' => ['placeholder' => 'Start date'],
    'options2' => ['placeholder' => 'End date'],
    'type' => DatePicker::TYPE_RANGE,
    'form' => $form,
           'layout' => $layout3,
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
          'startDate' => date('Y-m-d',strtotime("-2 year")),
         'endDate' => date('Y-m-d'),
    ],
      'pluginEvents' => [
    "changeDate" => "function(e) {  runTab() }",
]
]);
?><p>&nbsp;</p>

<?
echo $form->field($params_model, 'teamid')->hiddenInput()->label('');

ActiveForm::end();

         ?>
&nbsp;
</div>

</div>
&nbsp;
<div class="row">
<div class="col-md-10 col-md-offset-1">
  <?

$items = [
    [
       'id'=>'tabLetters',
       'label'=>'<i class="fa fa-envelope"></i> Письма c вложениями',
        'content'=>'',
        'active'=>true
    ],
    [
        'label'=>'<i class="fa fa-envelope-o"></i> Письма без вложений',
        'content'=>'',
        // 'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/statistics/tabs-data?tab=2'])]
    ],
    [
        'label'=>'<i class="fa fa-external-link"></i> Сcылки',
        'content'=>'',
        // 'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/statistics/tabs-data?tab=2'])]
    ],
       [
        'label'=>'<i class="fa fa-file-code-o"></i> Шаблоны',
        'content'=>'',
      //   'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/statistics/tabs-data?tab=3'])]
    ],
  ];

echo TabsX::widget([
    'items'=>$items,
    'id'=>'statTbs',
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false,
    'bordered'=>true,
     'pluginOptions' =>  ['enableCache'=>false],
]);

?>

</div>
</div>
	<?php
	  $js = <<< EOT
     runTab();

EOT;

$this->registerJs($js, yii\web\View::POS_READY);
	?>