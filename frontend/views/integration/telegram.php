<?php
use kartik\tabs\TabsX;
use kartik\helpers\Html;
use  yii\helpers\Url;
use kartik\editable\Editable;
use kartik\popover\PopoverX;
$this->title = 'Telegram';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
<div class="col-md-9 col-md-offset-1" style="border:4px solid #EEE" >
<div class="col-md-12">
 <?=Html::img('../img/telegram1.png')?>
</div>
<div class="col-md-8">
Для интеграции с Telegram, Вам предстоит совершить следующие действия
<br />
1.Сгенерировать и скопировать код активации (в правой колонке)
<br />
2.Найти в Telegram наш бот <b>SlideChat_bot</b>
<br />
3.Ввести полученный код активации в следующем формате:<br />
<b>/sendactivationcode [your_code]</b>   <br />
К примеру, полный код активации будет выглядеть примерно так:  <b>/sendactivationcode <?=!$model->activation_code?'1:xnd2dffgi':$model->activation_code?></b>

</div>
<div class="col-md-4">
    <b><u>Код активации</u></b><br />
    <?php
      if($model->activation_code)   echo $model->activation_code;
      else echo Html::a('Сгенерировать код активации',['telegram?code=1'],['class'=>'btn btn-danger']);?>
</div>
</div>
</div>