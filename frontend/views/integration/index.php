<?php
use kartik\tabs\TabsX;
use kartik\helpers\Html;
use  yii\helpers\Url;
$this->title = 'Интеграция';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
<div class="col-md-6 col-md-offset-1" style="border:4px solid #EEE" >
 <h1>Telegram</h1>
<div class="col-md-6">
 <?=Html::img('img/telegram.png')?>
</div>
<div class="col-md-6">
<p>&nbsp;</p>
<p>&nbsp;</p>
  <?=Html::a(Yii::t('app', 'Интегрировать Telegram'),['telegram'],[
			'class'=>'btn btn-warning btn-main',
   			])?>
</div>
</div>
</div>