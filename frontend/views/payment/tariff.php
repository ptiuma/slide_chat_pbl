<?php

use kartik\icons\Icon;
Icon::map($this);

use yii\helpers\Html;
 use common\components\payments\Payonline;
$this->title = 'Выбор тарифа';


?>

<div class="tariffs">
    <div class="row">

    <?php if($selected_plan->id&&$order->id):?>
           <div class="col-md-4">
            <div class="panel panel-info">
      <div class="panel-heading">
                    <h4 class="text-center">
                        Суммарная информация</h4>
                </div>
               <div class="panel-body">
                   <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  Кол-во месяцев <a class="pull-right"><?=$selected_plan->months?></a>
                </li>
                <li class="list-group-item">
                  Оплата за месяц <a class="pull-right"><?=$selected_plan->amount?> руб.</a>
                </li>
                <li class="list-group-item">
                  <b>Итого</b> <a class="pull-right"><?=$selected_plan->total?> руб.</a>
                </li>
              </ul>
              	 <?php echo Html::a('<i class="fa fa-money"></i>&nbsp;&nbsp; Оплатить через PayOnline',Yii::$app->payonline->makeUrl([
	              	 'Amount' => $order->amount,
	              	  'OrderId' => $order->id,
	              	 //  'OrderDescription' => 'Order payment # '.$order->id,

	              	 ]), ['id'=>'PayOnlineBtn','class' => 'btn btn-sm btn-success'])?>


                </div>
             </div>
            </div>
    <?php else:?>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="text-center">
                        <?=$plan1->top_title?></h4>
                </div>
                <div class="panel-body text-center">
            <p>&nbsp;</p>

                 <p class="lead">
                        <strong><?=$plan1->amount*$plan1->months?> руб</strong></p>
                </div>
                <ul class="list-group list-group-flush text-center">
                    <li class="list-group-item"><b><?=$plan1->title?></b></li>
                </ul>
              <p>&nbsp;</p>
                      <p>&nbsp;</p>
               <div class="panel-footer">
                    <a class="btn btn-lg btn-block btn-info" href="?plan=1">Оплатить</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h4 class="text-center">
                        <?=$plan2->top_title?></h4>
                </div>
                <div class="panel-body text-center">
            <p>&nbsp;</p>

                 <p class="lead">
                        <strong><?=$plan2->amount*$plan2->months?> руб</strong></p>
                </div>
                <ul class="list-group list-group-flush text-center ">
                    <li class="list-group-item text-info" style="background:#CDD1CE"><b><?=$plan2->title?></b></li>
                </ul>
              <p>&nbsp;</p>
                      <p>&nbsp;</p>
               <div class="panel-footer">
                    <a class="btn btn-lg btn-block btn-danger" href="?plan=2">Оплатить</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-info">
                    <div class="panel-heading">
                    <h4 class="text-center">
                        <?=$plan3->top_title?></h4>
                </div>
                <div class="panel-body text-center">
            <p>&nbsp;</p>

                 <p class="lead">
                        <strong><?=$plan3->amount*$plan3->months?> руб</strong></p>
                </div>
                <ul class="list-group list-group-flush text-center">
                    <li class="list-group-item"><b><?=$plan3->title?></b></li>
                </ul>
              <p>&nbsp;</p>
                      <p>&nbsp;</p>
               <div class="panel-footer">
                    <a class="btn btn-lg btn-block btn-info" href="?plan=3">Оплатить</a>
                </div>
            </div>
        </div>

    </div>
</div>
          <?php endif;?>

      </div>
    </div>

