<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use  yii\helpers\Url;
use kartik\popover\PopoverX;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мои команды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index" >
<div class="text-right" >
    <p>
                   <?php
 $form = ActiveForm::begin(['id' => 'create-tm','method'=>'post','action'=>''.Url::to(['team/create']).'']);
PopoverX::begin([
'type'=>'success',
    'header' => '<i class="fa fa-group "></i> Создать команду',
    'placement' => PopoverX::ALIGN_LEFT,
    'toggleButton' => ['label'=>'<i class="fa fa-group "></i> Создать команду', 'class'=>'btn btn-xs btn-danger'],

       'size'=>'md',
    'footer'=>Html::submitButton('Создать команду', ['class'=>'btn btn-sm btn-success'])
]);
echo "<b>Название команды</b><br>";
echo Html::textInput('name');

PopoverX::end();
ActiveForm::end();
?>
    </p>
</div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             	[
	 		'attribute' => 'name',
	 		'format'=>'html',
		       'value'=>function ($data) {
		        return Html::a($data->name,'team/'.$data->id);
		    },
      ],
            'created_at:date',
     	[
	 		'attribute' => 'numof',
	 		  'label'=>'Кол-во участников',
	 		'format'=>'raw',
		       'value'=>function ($data) {
		        return $data->Members_count;
		    },
      ],
     [
            'class' => '\kartik\grid\ActionColumn',
            'template' => '{view} {delete}',


        ],

   ]
  ]); ?>
</div>
