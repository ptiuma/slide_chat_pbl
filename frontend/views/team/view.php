<?php

use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\icons\Icon;
Icon::map($this);
use kartik\helpers\Html;
use kartik\helpers\Enum;
use kartik\popover\PopoverX;
use kartik\widgets\ActiveForm;
use  yii\helpers\Url;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $model common\models\Team */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->title = false;
$this->params['breadcrumbs']=false;
$team=$model;
?>
<div class="team-view">

    <h2><?php
         echo Editable::widget([
    'name'=>'name',
        'format' => Editable::FORMAT_BUTTON,
    'asPopover' => true,
    'value' => $model->name,
    'header' => 'Название команды',
    'size'=>'md',
        'resetButton'=>['icon'=>'Отменить','label'=>'Отменить'],
    'submitButton'=>['icon'=>'Применить','label'=>'Применить'],
    'options' => ['class'=>'form-control', 'placeholder'=>'Enter  name...']
]);?></h2>
      <div class="row">
            <div class="col-md-6">
        <div class="text-right">
        <?php
         $form = ActiveForm::begin(['action'=>'view?id='.$model->id,'fieldConfig'=>['showLabels'=>false]]);
PopoverX::begin([
    'header' => 'Добавить руководителя',
    'placement' => PopoverX::ALIGN_LEFT,
    'size' => 'lg',
    'footer' => Html::submitButton('Отправить', ['class'=>'btn btn-sm btn-primary']),
    'toggleButton' => ['label'=>'Добавить руководителя', 'class'=>'btn btn-success'],
]);
$members->invite_role=1;
?>

<b>Введите Email нового руководителя</b>
<br />

На указанный email будет отправлено приглашение стать частью команды. О подтверждении приглашения мы уведомим через email и отобразим в данном разделе
<? echo $form->field($members, 'invite_role')->hiddenInput()?>
<? echo $form->field($members, 'invite_email')->textInput(['placeholder'=>'Введите email...'])?>
<?php
PopoverX::end();
ActiveForm::end();
        ?>
        </div>
	&nbsp;
	<div class="box box-success box-solid">
            			<div class="box-header with-border">
              					<h3 class="box-title">Руководители</h3>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">

 <?= GridView::widget([
        'dataProvider' =>$leaderProvider,
           'pjax' => true,
           'bordered' => false,
            'layout' => '{items}',
            'showPageSummary' => false,
               // 'showCaption' => false,
         //  'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
         'id',
         'username',
         'email',
        /*	[
    'attribute' => 'phone',
    'format' => 'html',
    'value' => function($data) { return $data->profile->phone; },
],
         */
 [
        'class' => '\kartik\grid\ActionColumn',
        'header'=>'Статус',
         'template' => '{my_button}',
         'buttons' => [
    'my_button' => function ($url, $model, $key)  use ($team) {
        return $model->id==Yii::$app->user->id?'<small class="label label-info">Основатель</small>':Html::a('Удалить', ['delete-member','teamId'=>$team->id,'type'=>1, 'id'=>$model->id],['class'=>'btn btn-danger']);
    },
]

    ]
],


    ]); ?>

		            </div>
        		  </div>
            </div>

        <div class="col-md-6">
        <div class="text-right">
                    <?php
         $form = ActiveForm::begin(['action'=>'view?id='.$model->id,'fieldConfig'=>['showLabels'=>false]]);
PopoverX::begin([
    'header' => 'Добавить менеджера',
    'placement' => PopoverX::ALIGN_LEFT,
    'size' => 'lg',
    'footer' => Html::submitButton('Отправить', ['class'=>'btn btn-sm btn-primary']),
    'toggleButton' => ['label'=>'Добавить менеджера', 'class'=>'btn btn-info'],
]);
$members->invite_role=2;
?>

<b>Введите Email нового менеджера</b>
<br />

На указанный email будет отправлено приглашение стать частью команды. О подтверждении приглашения мы уведомим через email и отобразим в данном разделе
<? echo $form->field($members, 'invite_role')->hiddenInput()?>
<? echo $form->field($members, 'invite_email')->textInput(['placeholder'=>'Введите email...'])?>
<?php
PopoverX::end();
ActiveForm::end();
        ?>
        </div>
&nbsp;
		<div class="box box-info box-solid">
            			<div class="box-header with-border">
              					<h3 class="box-title">Менеджеры</h3>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">

 <?= GridView::widget([
        'dataProvider' =>$managerProvider,
           'pjax' => true,
           'bordered' => false,
            'layout' => '{items}',
            'showPageSummary' => false,
               // 'showCaption' => false,
         //  'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
        'id',
         'username',
         'email',
        /*	[
    'attribute' => 'phone',
    'format' => 'html',
    'value' => function($data) { return $data->profile->phone; },
],
         */
 [
        'class' => '\kartik\grid\ActionColumn',
         'header'=>'Статус',
         'template' => '{my_button}',
         'buttons' => [
    'my_button' => function ($url, $model, $key) {
        return $model->id==Yii::$app->user->id?'<small class="label label-info">Основатель</small>':Html::a('Удалить', ['delete-member', 'id'=>$model->id],['class'=>'btn btn-danger']);
    },
]

    ]
],


    ]); ?>

		            </div>
        		  </div>
            </div>

            </div>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div class="row">
     <div class="col-md-12">
		<div class="box box-default box-solid">
            			<div class="box-header with-border">
              					<h3 class="box-title">Ожидают приглашения</h3>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">

 <?= GridView::widget([
        'dataProvider' =>$invitesProvider,
           'pjax' => true,
           'bordered' => false,
            'layout' => '{items}',
            'showPageSummary' => false,
               // 'showCaption' => false,
         //  'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
        'id',

         'invite_email',

              	[
    'attribute' => 'invite_role',
    'format' => 'html',
    'value' => function($data) { return $data->invite_role==1?'Руководитель':'Менеджер'; },
],
             'created_at:date',
 [
        'class' => '\kartik\grid\ActionColumn',
         'template' => '{my_button}{delete}',
         'buttons' => [
    'my_button' => function ($url, $model, $key) {
        return '';
    },
],
      'urlCreator' => function ($action, $model, $key, $index) {
            if ($action == 'delete') {
             return Url::toRoute(['team/delete-invite', 'id' => $key]);
        }
      }

    ]
],


    ]); ?>

		            </div>
        		  </div>
            </div>

            </div>

