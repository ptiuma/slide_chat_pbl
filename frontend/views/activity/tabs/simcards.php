<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="company-index">

    <h3><?= Html::encode($lable) ?></h3>

  <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn'],

            'id',
                       	[
	 		'attribute' => 'simPhoneNumber',
	 		'format'=>'raw',
		       'value'=>function ($data) {
		        return Html::a($data->simPhoneNumber, ['equipment/simcards/update', 'id' => $data->id]);
		    },
    	],

       	[
	 		'attribute' => 'companyID',
	 		'label'=>'Компания',
	 		'format'=>'raw',
		       'value'=>function ($data) {
		        return Html::a($data->company->groupID, ['/company/update', 'id' => $data->companyID]);
		    },
    	],
    	 	[
	 		'attribute' => 'auto.autoNumber',
	 		'label'=>'АТС',
	 		'format'=>'raw',
		       'value'=>function ($data) {
		        return Html::a($data->auto->autoNumber, ['/auto/update', 'id' => $data->autoID]);
		    },
    	],
    	 	[
	 		'attribute' => 'equipment.imeiNumber',
	 		'label'=>'Трекер',
	 		'format'=>'raw',
		       'value'=>function ($data) {
		        return Html::a($data->equipment->imeiNumber, ['/equipment/default/update', 'id' => $data->equipmentID]);
		    },
    	],
    //        'imeiNumber',
            'operatorCode',

        ],
    ]); ?>

</div>
