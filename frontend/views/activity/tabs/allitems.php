<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="company-index">

    <h3><?= Html::encode($label) ?></h3>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn'],

            'id',
                	[
	 		'attribute' => 'imeiNumber',
	 		'format'=>'raw',
		       'value'=>function ($data) {
		        return Html::a($data->imeiNumber, ['/equipment/default/update', 'id' => $data->id]);
		    },
            ],
            [
	 		'attribute' => 'companyID',
	 		'label'=>'Компания',
	 		'format'=>'raw',
		       'value'=>function ($data) {
		        return Html::a($data->company->groupID, ['/company/update', 'id' => $data->companyID]);
		    },
    	],
    	 	[
	 		'attribute' => 'auto.autoNumber',
	 		'label'=>'АТС',
	 		'format'=>'raw',
		       'value'=>function ($data) {
		        return Html::a($data->auto->autoNumber, ['/auto/update', 'id' => $data->autoID]);
		    },
    	],
            // 'serialNumber',
     	[
	 		'attribute' => 'simPhoneNumber',
	 		'label'=>'Симкарта',
	 		'format'=>'raw',
		       'value'=>function ($data) {
		        return $data->simcards->id."-".$data->simcards->uniKey."<br>".$data->simcards->simPhoneNumber;
		    },
      ],
     [
	 'attribute' => 'status',
	 'value' => 'status.sName',
	 	 	 'label' => 'Статус',
	 'filter' => yii\helpers\ArrayHelper::map(app\modules\equipment\models\EquipmentStatuses::find()->asArray()->all(),'id','sName')
	 ],
        ],
    ]); ?>
</div>
