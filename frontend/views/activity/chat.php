<?php
use common\models\Links;
use common\models\Letters;
 use common\models\Documents;
 use kartik\icons\Icon;
Icon::map($this);
use kartik\helpers\Html;
use kartik\helpers\Enum;
/* @var $this yii\web\View */


$this->title = 'Общение через чат';


?>
<div class="site-chat">
        <div class="row">
     <?php if($model->isNode==1&&!$model->ended_at):?>
             <?php
echo Html::jumbotron([
    'heading' => '',
    'body' => 'В настоящее время клиент находится онлайн, вы можете отправить ему сообщение по ссылке<br>'.
    Html::a(Icon::show('commenting-o', ['class'=>'fa-1x']).'Перейти в Чат',['view/preview-doc','id'=>$model->link->doc->id,'viewid'=>$model->id],['class'=>'btn btn-sm btn-primary']),

]);
?>
     <?php else:?>
         <?php if(!$model->chat->id):?>
          <?php
echo Html::jumbotron([
    'heading' => '',
    'body' => 'Общение с этим посетителем не производилось<br>',

]);
?>
        <?php else:
        $data=$model->chat->data();
        ?>
        <div class="col-md-8">
          <!-- DIRECT CHAT SUCCESS -->
          <div class="box box-success direct-chat direct-chat-success">
            <div class="box-header with-border">
              <h3 class="box-title">История чата</h3>

              <div class="box-tools pull-right">
                                     </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- Conversations are loaded here -->
              <div class="direct-chat-messages">
              <?=$data?>
              </div>
              <!--/.direct-chat-messages-->

              <!-- /.direct-chat-pane -->
            </div>
            <!-- /.box-body -->

          </div>
          <!--/.direct-chat -->
        </div>
     <?php endif;?>
     <?php endif;?>
     </div>


</div>

