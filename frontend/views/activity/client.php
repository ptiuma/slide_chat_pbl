<?php
use common\models\Links;
use common\models\Letters;
 use common\models\Documents;
 use kartik\grid\GridView;
 use yii\widgets\Pjax;
use kartik\icons\Icon;
Icon::map($this);
use sfedosimov\daysago\DaysAgo;
use kartik\helpers\Html;
use kartik\helpers\Enum;
/* @var $this yii\web\View */


$this->title = 'Карточка клиента';

$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/amcharts.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/serial.js',['depends' => 'yii\web\JqueryAsset']);

?>
<div class="site-index">
        <div class="row">
            <div class="col-md-6">
<div class="row">
<div class="col-md-6">
 <h3><?php echo $model->client_name?></h3>
 <?php if($model->client_phone):?>
  <div class=""><?=Icon::show('phone', ['class'=>'fa-1x'])?>&nbsp;&nbsp;<?=$model->client_phone?></div>
  <?php endif;?>
   <div class="">Документ: &nbsp;&nbsp;<?=Icon::show('file-pdf-o', ['class'=>'fa-1x'])?>&nbsp;&nbsp; <?=$model->doc->name?></div>
   <p>&nbsp;</p>
 <?php
 $html='';
 foreach($model->views as $v)$html.=Html::button(Icon::show('bar-chart', ['class'=>'fa-1x']).'Статистика просмотра',['class'=>'btn btn-default btn-xs view-letter-stat','data-viewid'=>$v->id]).'&nbsp;&nbsp;
 Дата: '.date('Y-m-d H:i:s',$v->created_at).'<br>';
 echo $html;
 ?>
    </div>

</div>
      </div>
</div>


<?php
yii\bootstrap\Modal::begin([
    'id' => 'letterStat',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Статистика визита',
    'size' => 'modal-lg',

]);
yii\bootstrap\Modal::end();
?>

