<?php
use common\models\Links;
use common\models\Letters;
 use common\models\Documents;
 use kartik\grid\GridView;
 use yii\widgets\Pjax;
use kartik\icons\Icon;
Icon::map($this);
use sfedosimov\daysago\DaysAgo;
use kartik\helpers\Html;
use kartik\helpers\Enum;
/* @var $this yii\web\View */


$this->title = 'Письма по расписанию';

?>
<p>&nbsp;</p>
 <div class="row">
            <div class="col-md-12">


  <?php echo GridView::widget([
        'dataProvider' =>$dataProvider,
            //    'filterModel' => $searchModel,
              'id'=>'letters',
           'pjax' => true,
            'pjaxSettings' =>[
        'neverTimeout'=>true,
        'options'=>[
                'id'=>'w0',
            ]
        ],
           'bordered' => false,
      'layout' => '{items}{pager}',
            'showFooter'=>true,
               // 'showCaption' => false,
         //  'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
         ['class' => 'yii\grid\SerialColumn'],
      [
    'class'=>'kartik\grid\CheckboxColumn',
    'headerOptions'=>['class'=>'kartik-sheet-style'],
],
'email_to',
//'client_company',
'email_subject',
'email_body:html',
'sheduller',
 	[
    'attribute' => 'isMailing',
      'label'=>'Рассылка',
    'format' => 'html',
    'value' => function($data) { return $data->isMailing==1?'Да':'Нет';},
],

],
     'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'success',
            'after'=>"<div class='text-left'>".
            Html::button('<i class="glyphicon glyphicon-trash"></i> Удалить выбранные письма',  ['id'=>'DelButton','class' => 'btn btn-danger']).'</div>',

           // 'showFooter'=>false
        ],

    ]);  ?>

       </div>
                        <!-- /.end visits -->



       </div>

        <?php

    $this->registerJs('

    $(document).ready(function(){
	$(\'body\').on(\'click\',\'#DelButton\', function(e) {

        var HotId = $(\'#letters\').yiiGridView(\'getSelectedRows\');
       if(HotId.length==0) alert(\'Не выбрано ни одной записи\')
       else
       {
          $.ajax({
            type: \'POST\',
            url : BASE+"email/multiple-email-delete",
            data : {row_id: HotId},
            success : function(data) {
                $.pjax.reload({container:\'#w0\'});
            }
        });
       }
    });

    });', \yii\web\View::POS_READY);

?>



