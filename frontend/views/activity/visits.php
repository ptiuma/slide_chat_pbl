<?php
use common\models\Links;
use common\models\Letters;
 use common\models\Documents;
 use kartik\grid\GridView;
 use yii\widgets\Pjax;
use kartik\icons\Icon;
Icon::map($this);
use sfedosimov\daysago\DaysAgo;
use kartik\helpers\Html;
use kartik\helpers\Enum;
/* @var $this yii\web\View */


$this->title = 'История визитов';
$types=[1=>'Письмо',2=>'Ссылка'];

$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/amcharts.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/serial.js',['depends' => 'yii\web\JqueryAsset']);

?>
&nbsp;
         <div class="row">
          <div class="col-md-11 text-right">
         <form  class="form-inline" method="get">
           <div class="form-group">
           <input class="form-control input-lg" name="keyword" value="<?php echo Html::encode($keyword)?>" size="48" placeholder="Введите имя, email или телефон" type="search">
           </div>
          <input type="submit" class="btn btn-primary btn-search" value="Поиск">

          </form>
           </div>
         </div>
<p>&nbsp;</p>
 <div class="row">
            <div class="col-md-12">
				<div class="box box-success box-solid">
            			<div class="box-header with-border">
              					<h3 class="box-title">Визиты</h3>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">


    <?= GridView::widget([
        'dataProvider' =>$dataProvider,
            //    'filterModel' => $searchModel,
           'pjax' => true,
           'bordered' => false,
      'layout' => '{items}{pager}',
            'showFooter'=>true,
               // 'showCaption' => false,
         //  'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
         ['class' => 'yii\grid\SerialColumn'],
       	[
    'attribute' => 'client_name',
      'label'=>'Имя клиента',
    'format' => 'html',
    'value' => function($data) { return '<div class="link-title">'.$data->visitor_name.'</div>';},
],
       	[
    'attribute' => 'client_email',
      'label'=>'Email',
    'format' => 'html',
    'value' => function($data) { return $data->visitor_email;},
],
//'client_email',
'client_phone',
//'client_company',
'client_ip',
'client_country',
  	[
    'attribute' => 'type',
      'label'=>'Тип',
    'format' => 'html',
    'value' => function($data) { return !$data->link->letterId?'Ссылка':'Письмо';},

             'filterType'=>GridView::FILTER_SELECT2,
   'filter' => $types,
    'filterWidgetOptions'=>[
        'pluginOptions' => [ 'minimumResultsForSearch'=> '-1','allowClear' => true,'width'=>120],
    ],
    'filterInputOptions' => ['placeholder' => 'Тип'],
    	],
  	[
    'attribute' => 'docId',
      'label'=>'Документ',
    'format' => 'html',
    'value' => function($data) { return '<div class="link-title">'.$data->link->doc->name.'</div>';},
],
  [
    	 'attribute' => 'created_at',

	 	'format'=>'raw',
 'value'=>function ($data) {
		        return date('d.m.Y H:i',$data->created_at);
		    },
		    	     'filterType'=> \kartik\grid\GridView::FILTER_DATE_RANGE,
         'filterWidgetOptions' => [
					//'presetDropdown' => true,
					'pluginOptions' => [
					'format' => 'YYYY-MM-DD',
					'locale'=>[
					'separator' => ' : ',
					            'format'=>'YYYY-MM-DD'
					        ],

					'opens'=>'left',
					] ,
					'pluginEvents' => [
					"apply.daterangepicker" => "function() { apply_filter('date') }",
					]
					],
					],
[
    'attribute' => '',
        'label'=>'Статистика',
    'format' => 'html',
    'value' => function($data) { return Html::a(Icon::show('bar-chart', ['class'=>'fa-1x']),$data->id,['class'=>'btn btn-xs view-letter-stat']).'&nbsp;&nbsp;&nbsp;'.
    ($data->chat_messages_count>0?Html::a(Icon::show('commenting-o', ['class'=>'fa-1x']),['chat-archive','id'=>$data->id],['title'=>'Чат','class'=>'btn btn-xs showModalButton']):'');
    },
],
],


    ]); ?>
  		            </div>
        		  </div>
       </div>
                        <!-- /.end visits -->



       </div>



<?php
yii\bootstrap\Modal::begin([
    'id' => 'letterStat',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Статистика визита',
    'size' => 'modal-lg',

]);
yii\bootstrap\Modal::end();
?>

