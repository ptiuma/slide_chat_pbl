<?php

use yii\helpers\Html;
use kartik\grid\GridView;

use common\models\Links;
use common\models\Letters;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="company-index">

       <h3><?= Html::encode($label) ?></h3>
 <?= GridView::widget([
    'id'=>'tasksListGrid',
        'dataProvider' =>$dataProvider,
           'pjax' => false,
          'showHeader'=>'',
         'columns' =>
         [
         [
		    'attribute' => 'visits',
		    'label'=>'',
		    'format' => 'raw',
		      'contentOptions'=>['style'=>'width: 75%;'],
		    'value' => function ($data) use ($isArchive) {

      $model=$data['type']=='1'?Letters::findOne($data['id']):Links::findOne($data['id']);
      $item=$data['type']=='1'?'_item_letters':'_item_links';
      return $this->render($item, ['model' => $model,    'isArchive'=>$isArchive]);
    },
		],



],


    ]); ?>

<script>
     var isHistory='1';
</script>

</div>
