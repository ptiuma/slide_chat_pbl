<?php
use kartik\helpers\Html;
use  yii\helpers\Url;
use common\models\Links;
use kartik\popover\PopoverX;
use kartik\icons\Icon;
Icon::map($this);
?>
<div class="row">
   <div class="col-md-1" style="margin-top:15px">
  	<?=Icon::show('envelope-o', ['class'=>'fa-2x'])?>

        </div>
   <div class="col-md-4">
    <div class="link-title"><?=
            Html::encode($model->client_name);
            ?></div>
     <div class="link-title">
     <?=
            Html::encode($model->email_to);
            ?>
     </div>
    <div class=""><?=Icon::show('phone', ['class'=>'fa-1x'])?>&nbsp;&nbsp;<?=$model->client_phone?></div>
       &nbsp;
       <div>
              <?=
            Html::a('Повторить',['email/send', 'letterId'=>$model->id,'repeat' => 1] ,['class'=>'no-pjax btn btn-sm btn-default']);
            ?>
            <?php if($isArchive!=1):?>
              <?=
            Html::a('В архив',['activity/task-archive', 'type'=>1,'id' => $model->id] ,['class'=>'btn btn-sm btn-default task-archive']);
            ?>
             <?php endif;?>
       </div>

        </div>
   <div class="col-md-4">
<div class="process">
    <div class="process-row">
        <div class="process-step">
            <button type="button" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-envelope-o fa-2x"></i></button>
            <p>Создано <br><?=date('d.m.Y h:i',$model->created_at)?></p>
        </div>
        <div class="process-step">
            <button type="button" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-folder-open-o fa-2x"></i></button>
              <?php if($model->isOpened>0):?>
            <p>Открыто <br><?=date('d.m.Y h:i',$model->opened_at)?></p>
         <?php else:?>
			<p>не открыто</p>
          <?php endif;?>
        </div>
                <div class="process-step">
            <button type="button" class="btn btn-default btn-circle" disabled="disabled">
            <?=Links::find()->where(['letterId' => $model->id])->count()?>
            /
             <?=Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['letterId' => $model->id])->count()?></button>

        </div>
    </div>
</div>
       <?php foreach($model->links as $link):
        $cnt++;
        $views=$link->views_count;
        $html='<div class="collapse-group">'.$cnt.'.&nbsp;&nbsp;<span class="text">'.$link->doc->name.'</span>
        '.Html::button('<small class="label label-info"><b>'.$views.'</b> просмотров</small>',['class'=>'btn-link','data-toggle'=>'collapse','data-target'=>'#viewed_clients'.$link->id]).'';
        if($views>0)
        {
        	$html.='<div id="viewed_clients'.$link->id.'" class="collapse">';
        	foreach($link->views as $v)$html.=Html::button(Icon::show('bar-chart', ['class'=>'fa-1x']).'Статистика просмотра',['class'=>'btn btn-default btn-xs view-letter-stat','data-viewid'=>$v->id]).'<br>';
        	$html.='</div>';
        }
        $html.='</div>';
        echo $html;
        endforeach;

       ?>

        </div>

   <div class="col-md-3 text-cente">
          <div class="">Тема: </div>
   		 <div class=""><?=$model->email_subject?></div>
   </div>

</div>
&nbsp;