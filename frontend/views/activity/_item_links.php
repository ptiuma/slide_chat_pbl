<?php
use kartik\helpers\Html;
use  yii\helpers\Url;
use common\models\Links;
use kartik\popover\PopoverX;
use kartik\icons\Icon;
Icon::map($this);

?>
<div class="row">
   <div class="col-md-1" style="margin-top:15px">
  	<?=Icon::show('link', ['class'=>'fa-2x'])?>

        </div>
   <div class="col-md-4">
    <div class="link-title">
      <?=
            Html::encode($model->client_name);
            ?></div>
    <div class=""><?=Icon::show('phone', ['class'=>'fa-1x'])?>&nbsp;&nbsp;<?=$model->client_phone?></div>
       &nbsp;
       <div>
            <?php
            $content = '<p class="text-justify">' .
    ' <div><input type="text" class="form-control" value="'.$model->url.'" id="copy-link-'.$model->id.'" readonly="readonly">
     <button class="btn btn-sm btn-copy btn-default" data-clipboard-target="#copy-link-'.$model->id.'" data-clipboard-action="copy" type="button" id="copy-btnlink-'.$model->id.'" data-toggle="tooltip" data-placement="button" title="Копировать">Копировать</button></div>' .
    '</p>';
             echo PopoverX::widget([
    'header' => 'Адрес ссылки',
    'id'=>'links_'. $model->id,
    'size'=>'md',
    'placement' => PopoverX::ALIGN_TOP_LEFT,
    'content' => $content,
    'toggleButton' => ['label'=>'Ссылка', 'class'=>'btn btn-sm  btn-default'],
]);
?>
            &nbsp;
  <?php if($isArchive!=1):?>
      <?=
            Html::a('В архив',['/activity/task-archive', 'type'=>2,'id' =>  $model->id] ,['class'=>' btn btn-sm btn-default task-archive']);
            ?>
      <?php endif;?>
    </div>

        </div>
   <div class="col-md-4">
<div class="process">
    <div class="process-row">
        <div class="process-step">
            <button type="button" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-link fa-2x"></i></button>
            <p>Создано <br><?=date('d.m.Y h:i',$model->created_at)?></p>
        </div>
        <div class="process-step">
            <button type="button" class="btn btn-default btn-circle" disabled="disabled"><i class="fa fa-hand-pointer-o fa-2x"></i></button>
        <?php if($model->views_count>0):?>
            <p>Открыто <br><?=date('d.m.Y h:i',$model->views[0]->created_at)?></p>
         <?php else:?>
			<p>не открыто</p>
          <?php endif;?>
        </div>

    </div>
</div>


        </div>

   <div class="col-md-3 text-cente">
          <div class="round round-slg hollow green document-views">
                  <em><?=$model->views_count?></em><p>Визиты</p>
          </div>
   </div>

</div>
&nbsp;