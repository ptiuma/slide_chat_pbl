<?php
use common\models\Links;
use common\models\Letters;
 use common\models\Documents;
 use yii\bootstrap\Tabs;
 use kartik\popover\PopoverX;
use kartik\helpers\Html;
$this->title = 'Отправленные письма и ссылки';



?>
<div class="site-index">
  <div class="row">
            <div class="col-md-12 text-right">
                  <?php echo Html::a('<i class="glyphicon glyphicon-list"></i> Экспорт писем','export-letters', ['id'=>'ViewVisits','class' => 'btn btn-sm btn-primary'])?>
            </div>
 </div>
        <div class="row">
            <div class="col-md-12">
<?php
$items = [
    [
        'label'=>'Неоткрытые <span class="badge">'.(int)$counters['total_noactive'].'</span>',
        'content'=>'noactive'==$tab?$this->render('_tabs_content', ['label' =>$label,'dataProvider' => $dataProvider,]):'',
        'active'=>'noactive'==$tab,
         'url'=>\yii\helpers\Url::to(['activity/history?tab=noactive'])
    ],
    [
        'label'=>'Открытые  <span class="badge">'.(int)$counters['total_active'].'</span>',
        'content'=>'active'==$tab?$this->render('_tabs_content', ['label' =>$label,'dataProvider' => $dataProvider,]):'',
        'active'=>'active'==$tab,
         'url'=>\yii\helpers\Url::to(['activity/history?tab=active'])
    ],
    [
        'label'=>'Все  <span class="badge">'.(int)$counters['total_all'].'</span>',
        'content'=>'all'==$tab?$this->render('_tabs_content', ['label' =>$label,'dataProvider' => $dataProvider,]):'',
        'active'=>'all'==$tab,
         'url'=>\yii\helpers\Url::to(['activity/history?tab=all'])
    ],
    [
        'label'=>'Архив  <span class="badge">'.(int)$counters['total_archive'].'</span>',
        'content'=>'archive'==$tab?$this->render('_tabs_content', ['label' =>$label,'dataProvider' => $dataProvider,]):'',
        'active'=>'archive'==$tab,
         'url'=>\yii\helpers\Url::to(['activity/history?tab=archive'])
    ],
    [
        'label'=>'Отложенные письма  <span class="badge">'.(int)$counters['total_shed'].'</span>',
         'headerOptions' => ['class'=>'pull-right'],
         'url'=>\yii\helpers\Url::to(['activity/sheduller-letters'])
    ],
];
echo Tabs::widget([
    'items'=>$items,
   // 'position'=>TabsX::POS_ABOVE,
   // 'bordered'=>true,
    'encodeLabels'=>false
]);
?>

  </div>


</div>

<?php
yii\bootstrap\Modal::begin([
    'id' => 'letterStat',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Прикрепить файл из облака',
    'size' => 'modal-lg',

]);
yii\bootstrap\Modal::end();
?>
