<?php
use common\models\Links;
use common\models\Letters;
 use common\models\Documents;
 use kartik\grid\GridView;
 use yii\widgets\Pjax;
use kartik\icons\Icon;
Icon::map($this);
use sfedosimov\daysago\DaysAgo;
use kartik\helpers\Html;
use kartik\helpers\Enum;
/* @var $this yii\web\View */


$this->title = 'Активность';

$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/amcharts.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/serial.js',['depends' => 'yii\web\JqueryAsset']);

?>
<div class="site-index">
        <div class="row">
<div class="col-md-2 col-sm-12 col-xs-12">
               <?=Html::a(Yii::t('app', 'Поиск по имени, email или телефону'),'activity/visits',[
			'class'=>'btn btn-primary  btn-to-visits',])?>
</div>
            <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12 pull-right">
<div class="row">

<div class="col-md-6 col-sm-12 col-xs-12">
 <?php echo \ptiuma\widgets\InfoBox::widget([
                     'boxBg'=>\ptiuma\widgets\InfoBox::TYPE_AQUA,
                     'iconBg'=>\ptiuma\widgets\InfoBox::TYPE_BLUE,
                     'number'=>Links::Viewed_count(Yii::$app->user->id),
                     'text'=>'Визиты',
                     'icon'=>'fa fa-eye',
                     'progress'=>100,
                     'progressText'=>'Сегодня: '.Links::Viewed_count(Yii::$app->user->id,1)
                 ])?>
    </div>

<div class="col-md-6 col-sm-12 col-xs-12">
 <?php echo \ptiuma\widgets\InfoBox::widget([
                     'boxBg'=>\ptiuma\widgets\InfoBox::TYPE_GRAY,
                     'iconBg'=>\ptiuma\widgets\InfoBox::TYPE_BLACK,
                     'number'=>Letters::Letters_count(Yii::$app->user->id),
                     'text'=>'Письма',
                     'icon'=>'fa fa-envelope',
                     'progress'=>100,
                     'progressText'=>'Сегодня: '.Letters::Letters_count(Yii::$app->user->id,1)
                 ])?>
    </div>
</div>
      </div>
</div>

&nbsp;
        <div class="row">
            <div class="col-md-7 col-sm-12 col-xs-12">
				<div class="box box-success box-solid">
            			<div class="box-header with-border">
              					<h3 class="box-title">Визиты</h3>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">


    <?= GridView::widget([
        'dataProvider' =>$viewsProvider,
           'pjax' => true,
           'bordered' => false,
            'layout' => '{items}',
            'showPageSummary' => false,
               // 'showCaption' => false,
         //  'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
         	[
    'attribute' => 'client',
      'label'=>'Клиент',
    'format' => 'html',
    'value' => function($data) { return '<div class="link-title">'.$data->visitor_name.'</div><div class="">'.(Icon::show('phone', ['class'=>'fa-1x'])).'&nbsp;&nbsp;'.$data->visitor_phone.'</div>
    <div class="link-created">Документ:&nbsp;&nbsp;<b>'.$data->link->doc->name.'</b><br />Дата:&nbsp;&nbsp;<b>'.date('d.m.Y h:i',$data->created_at).'</b></div>';},
],

[
     'attribute' => '',
     'label'=>'Статус',
    'format' => 'html',
    'value' => function($data) { return $data->status_label."<br /><br />".
    ($data->isNode==1&&!$data->ended_at?Html::a(Icon::show('commenting-o', ['class'=>'fa-1x']).' Подключиться',['view/preview-doc','id'=>$data->link->docId,'viewid'=>$data->id],['title'=>'Чат','class'=>'btn btn-xs btn-primary']):'');},

],
/*
[
    'attribute' => '',
    'format' => 'html',
    'value' => function($data) { return "<div>".($data->downloads>0?Icon::show('download', ['class'=>'fa-1x']):'')."&nbsp;&nbsp;".($data->print>0?Icon::show('print', ['class'=>'fa-1x']):'')."";},
],
*/
[
    'attribute' => '',
        'label'=>'Статистика',
    'format' => 'html',
    'value' => function($data) { return Html::a(Icon::show('bar-chart', ['class'=>'fa-1x']),$data->id,['class'=>'btn btn-xs view-letter-stat']).'&nbsp;&nbsp;'.
    (($data->isNode==1&&!$data->ended_at)||$data->chat_messages_count>0?Html::a(Icon::show('commenting-o', ['class'=>'fa-1x']),['chat-archive','id'=>$data->id],['title'=>'Чат','class'=>'btn btn-xs showModalButton']):'');},
],
],


    ]); ?>
  		            </div>
        		  </div>
  <?php echo Html::a('<i class="glyphicon glyphicon-list"></i> История визитов','activity/visits', ['id'=>'ViewVisits','class' => 'btn btn-sm btn-success'])?>
          </div>
                        <!-- /.end visits -->

            <div class="col-md-5 col-sm-12 col-xs-12">
				<div class="box box-warning box-solid">
            			<div class="box-header with-border">
              					<h3 class="box-title">Активные письма и ссылки</h3>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">
    <?php Pjax::begin([
'id' => 'tasksList',
]); ?>

    <?= \yii\widgets\ListView::widget([
  'dataProvider' => $lettersProvider,
  'id'=>'tasksListGrid',
  'itemOptions' => ['class' => 'item'],
  'summary'=>'',
    'separator'=>'<hr  width="85%">',
  'itemView' => function ($data) {

      $model=$data['type']=='1'?Letters::findOne($data['id']):Links::findOne($data['id']);
      $item=$data['type']=='1'?'_item_letters':'_item_links';
      return $this->render($item, ['model' => $model]);
    },
]) ?>


<?php Pjax::end(); ?>
		            </div>
        		  </div>
            </div>
                        <!-- /.end visits -->

       </div>


<p>&nbsp;</p>

        <div class="row">
            <div class="col-md-12">
            <div class="panel panel-default">
  <div class="panel-heading">Статистика визитов</div>
  <div class="panel-body">

<?php
for($z=30;$z>=0;$z--)
{
        $mk=mktime(0,0,0,date('m'),date('d')-$z,date('Y'));
        $dates[]= date("M d", $mk);
        $stat_data[]=intval(Links::Viewed_count(Yii::$app->user->id,0,$mk,$mk+86400));
}

echo \dosamigos\highcharts\HighCharts::widget([
    'clientOptions' => [
        'chart' => [
                'type' => 'line'
        ],
        'title' => [
             'text' => 'Визиты за месяц'
             ],
        'xAxis' => [
            'categories' =>$dates

        ],
        'yAxis' => [
            'title' => [
                'text' => 'Число визитов'
            ]
        ],
        'series' => [
            ['name' => 'Визиты', 'data' => $stat_data ]
        ]
    ]
]);
?>

  </div>

    </div>

     </div>


</div>

<?php
yii\bootstrap\Modal::begin([
    'id' => 'letterStat',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Статистика визита',
    'size' => 'modal-lg',

]);
yii\bootstrap\Modal::end();
?>

