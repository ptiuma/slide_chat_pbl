<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use common\models\User;
 use common\models\Links;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">
       <?= Html::a('<span class="logo-mini"><span class="fa fa-bar-chart-o"></span></span><span class="logo-lg"><span class="fa fa-bar-chart-o"></span>' . Yii::$app->keyStorage->get('common.siteName') . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar  navbar-inverse navbar-static-top navbar-custom" role="navigation">
       <?php if (!Yii::$app->user->isGuest):?>
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
       <?php endif;?>
        <div class="navbar-custom-menu">

     <?php
   if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Регистрация', 'url' => ['/user/register']];
        $menuItems[] = ['label' => 'Войти', 'url' => ['/user/login']];
    } else {
if(!Yii::$app->user->identity->confirmed_at)$warnings[]='<li class="divider"></li><li class="header"><i class="fa fa-warning text-yellow"></i>&nbsp;&nbsp;<span>Ваше регистрационное письмо не активированно</span></li>';

    if(User::isDemo())
    {
    	   $menuItems[] = ['label' => 'Демо режим <i class="fa fa-shopping-cart"></i> Оплатить', 'url' => ['/payment/tariff'],'class' => 'dropdown notifications-menu hidden-md-down'];
    }
    else
    {
    	   $menuItems[] = ['label' => User::AccountLevelName().' доступ '.(User::AccountLevel()==1?'&nbsp;(ограниченное использование)&nbsp;&nbsp;':'').' <i class="fa fa-shopping-cart"></i> Оплатить', 'url' => ['/payment/tariff'],'options'=>['class'=>'dropdown notifications-menu hidden-xs hidden-sm']];
    }
     //$menuItems[] = ['label' => 'Баланс: 0 руб'];
    // $menuItems[] = ['label' => '<i class="fa fa-bell-o"></i><span class="label label-warning">10</span>', 'url' => ['/user/login'],['class' => 'dropdown notifications-menu']];

    $hot=Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['userId' => Yii::$app->user->id])->andwhere(['<','UNIX_TIMESTAMP()-views.created_at',60*5])->count();
    if($hot>0)
    {
    	$menuItems[] = ['label' => '<i class="fa fa-flag-o"></i><span class="label label-danger">'.$hot.'</span>', 'options' => ['class' => 'dropdown tasks-menu'],
	    	'items'=>['<li class="header"><i class="fa fa-warning text-yellow"></i>&nbsp;&nbsp;<span>Вы имеете "горячих" клиентов</span></li>'],
	    		];
    }
    if(count($warnings))
    {
	    	$menuItems[] = ['label' => '<i class="fa fa-bell-o"></i><span class="label label-warning">'.count($warnings).'</span>', 'options' => ['class' => 'dropdown notifications-menu'],
	    	'items'=>$warnings
	    		];
    }
   $menuItems[] = [
            'label' => 'Logout (' . Yii::$app->user->identity->email . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['style'=>'margin-right:40px','data-method' => 'post'],
            'options'=>['class'=>'hidden-xs hidden-sm']
        ];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
        'encodeLabels' => false
    ]);
    ?>

           </div>
    </nav>
</header>
