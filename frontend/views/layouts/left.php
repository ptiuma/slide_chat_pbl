<?php use common\models\Team;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->


        <?php
        $teams=Team::find()->joinWith('members')->where(['memberId'=>Yii::$app->user->id])->groupBy('id')->all();
        $teamsmenu[]=['label' => 'Персональная', 'icon' => 'fa fa-file-code-o', 'url' => ['/statistics/']];
        foreach($teams as $team)$teamsmenu[]=['label' => $team->name, 'icon' => 'fa fa-meh-o', 'url' => ['/statistics/team?id='.$team->id]];

        dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree','data-widget'=>"tree"],
                'items' => [
               ['label' => 'Основное меню','options' => ['class' => 'header']],
           ['label' => 'Создать письмо', 'icon' => 'fa fa-envelope-o', 'url' => ['/email/send'],],
        ['label' => 'Начать презентацию',  'icon' => 'fa fa-video-camera', 'url' => ['#'],'options'=>['class'=>'create-presentation-link']],
        ]  ] ) ?>
          <hr>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree','data-widget'=>"tree"],
                'items' => [
     /*
         [
                        'label' => 'Быстрый доступ',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Загрузить', 'icon' => 'fa fa-upload', 'url' => ['/documents/'],],
                            ['label' => 'Создать письмо', 'icon' => 'fa fa-envelope-o', 'url' => ['/email/send'],],

                        ],
                    ],
         */
             ['label' => 'Активность', 'icon' => 'fa fa-dashboard', 'url' => ['/']],
                 ['label' => 'Отправленные', 'icon' => 'fa fa-arrow-down', 'url' => ['/activity/history']],
                    ['label' => 'Мои документы', 'icon' => 'fa fa-file-code-o', 'url' => ['/documents/'],],
                    ['label' => 'Адресная книга', 'icon' => 'fa fa-file-excel-o', 'url' => ['/abook/'],],
      [
                        'label' => 'Статистика',
                        'icon' => 'fa fa-line-chart',
          'options' => ['class' => 'treeview'],
                        'url' =>count($teams)==0?['/statistics/']:['#'],
                        'items' =>count($teams)==0?[]:$teamsmenu,
                    ],
                  ['label' => 'Интеграция', 'icon' => 'fa fa-sitemap', 'url' => ['/integration']],
                ],
            ]
        ) ?>
           <p>&nbsp;</p>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree','data-widget'=>"tree"],
                'items' => [
                   ['label' => 'Дополнительные опции','options' => ['class' => 'header']],
                 ['label' => 'Команда', 'icon' => 'fa fa-users', 'url' => ['/team']],
                     ['label' => 'Шаблоны', 'icon' => 'fa fa-copy', 'url' => ['/templates']],
                 ['label' => 'Настройки', 'icon' => 'fa fa-cogs', 'url' => ['/slide-settings']],
                  ['label' => 'Профиль', 'icon' => 'fa fa-user', 'url' => ['/user/settings/profile']],
                  ['label' => 'Рассылка', 'icon' => 'fa fa-envelope', 'url' => ['/mailing/send'], 'visible' => Yii::$app->user->can('admin')||Yii::$app->user->can('manager')],
                    ['label' => 'Помощь', 'icon' => 'fa fa-commenting-o', 'url' => ['/pages/help']],





                ],
            ]
        ) ?>
    </section>

</aside>
