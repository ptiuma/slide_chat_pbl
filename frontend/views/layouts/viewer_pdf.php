<?php

/* @var $this \yii\web\View */
/* @var $content string */
use  yii\helpers\Url;
use yii\helpers\Html;
use frontend\assets\AppAssetViewPdf;
use common\widgets\Alert;



 $asset=AppAssetViewPdf::register($this);
   $baseUrl=$asset->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

         <link rel="stylesheet" href="<?=$baseUrl?>/js/pdfviewer/web/viewer.css"/>

    <script src="<?=$baseUrl?>/js/pdfviewer/web/compatibility.js"></script>



<!-- This snippet is used in production (included from viewer.html) -->
<link rel="resource" type="application/l10n" href="<?=$baseUrl?>/js/pdfviewer/web/locale/locale.properties"/>
<script src="<?=$baseUrl?>/js/pdfviewer/web/l10n.js"></script>
<script src="<?=$baseUrl?>/js/pdfviewer/build/pdf.js"></script>



    <script src="<?=$baseUrl?>/js/pdfviewer/web/debugger.js"></script>
    <script src="<?=$baseUrl?>/js/pdfviewer/web/viewer.js"></script>

<script>
var BASE='<?=Url::to(['/'],true)?>';
  PDFJS.workerSrc = '<?=$baseUrl?>/js/pdfviewer/build/pdf.worker.js';
 DEFAULT_URL='';
</script>
</head>
 <body tabindex="1" class="loadingInProgress">
<?php $this->beginBody() ?>


         <?= $content ?>

<?php $this->endBody() ?>


</body>
</html>
<?php $this->endPage() ?>
