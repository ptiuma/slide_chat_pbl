<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;
use yii\helpers\Html;
use kartik\icons\Icon;
Icon::map($this);


?>
<div class="content-wrapper <?=Yii::$app->user->isGuest?'content-wrapper-full':''?>">

    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
            <h1>
                <?php
                if ($this->title !== null) {
                    echo \yii\helpers\Html::encode($this->title);
                } else {
                    echo \yii\helpers\Inflector::camel2words(
                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                } ?>
            </h1>
        <?php } ?>

        <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer <?=Yii::$app->user->isGuest?'content-wrapper-full':''?>">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Copyright &copy; 2016 <a href="#">Slide.chat</a>.</strong>
</footer>
<?php
yii\bootstrap\Modal::begin([
    'id' => 'createPresentation',
     'headerOptions' => ['class'=>'modal-header pres-modal-header'],
    'header'=>'Начать презентацию',
      'size' => 'modal-md',
]);
?>
<p>&nbsp;</p>
<b>Адрес вашей презентации</b>
<div>
<input type="text" name="presname" id="presname" class="form-control" readonly="readonly" value="">
 <button class="btn btn-sm btn-copy btn-default" data-clipboard-target="#presname" data-clipboard-action="copy" type="button" id="copy-button-pres"     data-toggle="tooltip" data-placement="button"  title="Копировать">
        Копировать
      </button></div>
    <p>  Отправьте ее Вашему потенциальному клиенту любым привычным способом (Email, Skype, WhatsApp и т.п.)
</p>

<p>&nbsp;</p>
        <?= Html::a(Icon::show('video-camera', ['class'=>'fa-1x']).' Начать презентацию', ['p/'.Yii::$app->user->identity->username], [
    	'class'=>'btn btn-info btn-sm',
    	'id'=>'preslink',
    	'target'=>'_blank',
    	'data'=>[
        'method' => 'post',
        'params'=>['docId'=>$data->id],
    		]
		]) ?>

<?php
yii\bootstrap\Modal::end();
?>

<?php
yii\bootstrap\Modal::begin([
    'id' => 'modal',
     'headerOptions' => ['id' => 'modalHeader','class'=>'modal-header pres-modal-header'],
    'header'=>'',
      'size' => 'modal-md',
]);
echo "<div id='modalContent'></div>";
?>

<?php
yii\bootstrap\Modal::end();
?>