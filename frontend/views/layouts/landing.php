<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use  yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */



        $asset=frontend\assets\AppAssetLanding::register($this);
       $baseUrl=$asset->baseUrl;
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>


	<!--pageMeta-->

	<!-- Lib CSS -->
	<link href="//fonts.googleapis.com/css?family=Rancho|Open+Sans:400,300,300italic,400italic,600,600italic,700,800italic,700italic,800" rel="stylesheet">
	<link href="<?=$baseUrl?>/minify/rgen_min.css" rel="stylesheet">
	<link href="<?=$baseUrl?>/css/custom.css" rel="stylesheet">

	<!-- Favicons -->
	<link rel="icon" href="<?=$baseUrl?>/images/favicons/favicon.ico">
	<link rel="apple-touch-icon" href="<?=$baseUrl?>/images/favicons/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?=$baseUrl?>/images/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?=$baseUrl?>/images/favicons/apple-touch-icon-114x114.png">
	<script src="<?=$baseUrl?>/minify/rgen_min.js"></script>
<script async="" src="<?=$baseUrl?>/js/rgen.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
	<script src="<?=$baseUrl?>js/html5shiv.js"></script>
	<script src="<?=$baseUrl?>js/respond.min.js"></script>
	<![endif]-->
	<!--[if IE 9 ]><script src="<?=$baseUrl?>js/ie-matchmedia.js"></script><![endif]-->
<script>
var BASE='<?=Url::to(['/'],true)?>';
</script>
</head>
<body>
<div id="page">
<nav style="outline-offset: -3px;" class="nav-wrp nav-3">
	<div class="container">

		<div class="nav-header">
			<a style="" class="navbar-brand" href="<?=Yii::getAlias('@web')?>/"><strong>SLIDE.CHAT</strong></a>
			<a class="nav-handle fs-touch-element" data-nav=".nav"><i class="fa fa-bars"></i></a>
		</div>

		<div class="nav vm-item m-nav">
			<ul class="nav-links">
				<li><a class="" href="<?=Yii::getAlias('@web')?>/#aboutus">О нас</a></li>
				<li><a class="" href="<?=Yii::getAlias('@web')?>/#feature">Возможности</a></li>
				<li><a class="" href="<?=Yii::getAlias('@web')?>/#price">Цены</a></li>

				<li><a class="" href="<?=Yii::getAlias('@web')?>/#contact">Контакты</a></li>
			</ul>
			<div class="nav-other">
				<span style="margin-right: 15px;"><i class="fa fa-phone"></i> <?=Yii::$app->params['contactPhone']?></span>
				<a href="<?=Yii::getAlias('@web')?>/user/login" class="btn btn-default btn-sm"><i class="fa fa-lock"></i> Войти</a>
				<a href="<?=Yii::getAlias('@web')?>/user/register" class="btn btn-primary btn-sm"><i class="fa fa-user"></i> Регистрация</a>
			</div>
		</div><!-- /.nav -->

	</div><!-- /.container -->
</nav><!-- /.nav-wrp --><!-- Intro -->
<?php $this->beginBody() ?>
<?php

/* @var $this yii\web\View */

$this->title = 'Home';
?>

 <?= $content ?>


<?php $this->endBody() ?>
<section style="outline-offset: -3px;" class="footer-section footer-section-4">
	<div class="container">
		<div style="" class="row eqh fs-equalize-element">
			<div style="height: 150px;" class="col-md-3 l">
				<h6 >SLIDE.CHAT</h6>
                   <p> <b>Телефон:</b> 84992845011<br />
<b>Email:</b> info@slide.chat<br /></p>
			</div><!-- /Logo and address -->


			<div style="height: 150px;" class="col-md-3 c">
			<h6>Быстрая навигация</h6>
				<a style="" href="<?=Yii::getAlias('@web')?>/pages/help">Помощь</a>
				<a style="" href="<?=Yii::getAlias('@web')?>/pages/license">Публичная оферта</a>
				<a style="" href="<?=Yii::getAlias('@web')?>/pages/payment-and-return-policy">Оплата и возврат средств</a>
				<a style="" href="<?=Yii::getAlias('@web')?>/pages/privacy">Политика конфиденциальности</a>
			</div><!-- Quick links -->


			<div style="height: 150px;" class="col-md-3 c">
				<h6>Адреса</h6>
				<p>
<b>Юридический адрес</b><br />
Suite 1, 44 Main street, Douglas, ML11 0 QW, Scotland<br />
<b>Фактический адрес</b><br />
Россия, Москва, Очаковское шоссе, корпус 1, к.55<br />

									</p>
			</div><!-- Contactsl  -->
			<div class="col-md-3 r">
			<div style="padding:10px; background:#FFF;border:3px solid #cacaca;min-width:230px;">
                <img src="<?=Yii::getAlias('@web')?>/img/payment/01-payonline-w300.png" width="70" border="0">
                <img src="<?=Yii::getAlias('@web')?>/img/payment/02-visa-w300.png"  width="70" border="0">
                 <img src="<?=Yii::getAlias('@web')?>/img/payment/03-mastercard-w300.png"  width="70" border="0">
                 </div>
			</div><!-- Payment  -->
		</div>
		<hr>
		<p class="copyright">Slide.Chat © 2016</p>
	</div>
</section><!-- ./Footer section --></div>

<script type="text/javascript">
    var reformalOptions = {
        project_id: 973336,
        project_host: "slidechat.reformal.ru",
        tab_orientation: "right",
        tab_indent: "50%",
        tab_bg_color: "#F05A00",
        tab_border_color: "#FFFFFF",
        tab_image_url: "http://tab.reformal.ru/T9GC0LfRi9Cy0Ysg0Lgg0L%252FRgNC10LTQu9C%252B0LbQtdC90LjRjw==/FFFFFF/a08a7c60392f68cb33f77d4f56cf8c6f/right/1/tab.png",
        tab_border_width: 2
    };

    (function() {
        var script = document.createElement('script');
        script.type = 'text/javascript'; script.async = true;
        script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'media.reformal.ru/widgets/v3/reformal.js';
        document.getElementsByTagName('head')[0].appendChild(script);
    })();
</script><noscript><a href="http://reformal.ru"><img src="http://media.reformal.ru/reformal.png" /></a><a href="http://slidechat.reformal.ru">Oтзывы и предложения для slide.chat</a></noscript>



</body></html>
    <?php $this->endPage() ?>

