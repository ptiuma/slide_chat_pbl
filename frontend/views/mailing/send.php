<?php
use yii\helpers\Html;
use  yii\helpers\Url;
use dosamigos\switchinput\SwitchBox;
use \vova07\imperavi\Widget;
use dosamigos\tinymce\TinyMce;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use frontend\models\UserTemplates;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use kartik\widgets\FileInput;
use kartik\icons\Icon;
Icon::map($this);
use kartik\growl\GrowlAsset;
use kartik\base\AnimateAsset;
GrowlAsset::register($this);
AnimateAsset::register($this);
use yii\web\JsExpression;
use kartik\widgets\Spinner;

use common\models\Letters;

$templates=ArrayHelper::map(UserTemplates::find()->where(['userId'=> Yii::$app->user->id])->orderBy('id')->all(), 'id', 'name');
$this->title = '';
$this->params['breadcrumbs'] = null;

$this->registerJsFile(
    '@web/frontend/assets/js/email.js',  ['depends' => [yii\web\JqueryAsset::className()]]);
?>
 <style>
  .file-maildb
  {

  	 background-color: #4E9CBE !important;
  }
  div#spinner
{
    display: none;
    width:350px;
    height: 100px;
    position: fixed;
    top: 50%;
    left: 50%;
    background:#F4F4F4;
    text-align:center;
    padding:10px;
    font:normal 16px Tahoma, Geneva, sans-serif;
    border:4px solid #B6B6B6;
    margin-left: -50px;
    margin-top: -50px;
    z-index:2;
    overflow: auto;
}
 </style>
    <div id="spinner">
    <?
    echo Spinner::widget(['preset' => 'medium', 'align' => 'center', 'color' => 'blue']);
	echo '<div> Подождите, идёт загрузка рассылки...</div>';
	echo '<div class="clearfix"></div>';?>

    </div>
<div class="row email-form" style="margin-left:20px;margin-right:20px;">
       <div class="col-md-12 col-lg-12 col-centered text-center2 bordered2">


<?php
 Pjax::begin(['id' => 'new_letter']);
   if($model->id&&$isSend==1)
    {
    	$this->registerJs("
         //   $('#success').modal('show')
            ");
      }
   if($model->id)
    {
    	foreach($model->links as $link)
    	{
	    	$this->registerJs("
	            	insertAttachment('".$link->docId."')
	            ");
        }
      }
    elseif($doc->id)
    {
	    	$this->registerJs("
	            	insertAttachment('".$doc->id."')
	            ");
      }
  if(!$model->id)
  {
  	$model->email_copy=Yii::$app->user->identity->settings->email_copy;
  		$model->email_body=Yii::$app->user->identity->profile->bio;
  }
  $form = ActiveForm::begin(['id'=>'mailingForm','type'=>ActiveForm::TYPE_HORIZONTAL,'options' => ['enctype' => 'multipart/form-data','linkSelector'=>false,'data-pjax' => false]]);
  ?>

<div class="row">
<div class="col-md-12">
  <div class="row">
   <?= Html::activeLabel($model, 'templateId', [
        'label'=>'Шаблон',
        'class'=>'col-md-1 control-label'
    ]); ?>

     <div class="col-md-9">
        <?= $form->field($model, 'templateId',[
            'showLabels'=>false,
        ])->dropDownList($templates,['prompt'=>'Без шаблона','class'=>'text-left']); ?>
        </div>
    <div class="col-md-2" style="padding:0;padding-top:0px;margin:0 !important;">
     <?=Html::button(Icon::show('plus', ['class'=>'btn fa-2x']),['id'=>'tplC','class'=>'btn-link create-template','data-original-title' => 'Создать шаблон',
     'data' => [
    'id'=>'-1',
       'toggle' => 'modal',
        'target' => '#createTemplate',
   ]])?>
   <?=Html::button(Icon::show('trash', ['class'=>'btn fa-2x']),['id'=>'tplR','class'=>'collapse btn-link  delete-template','data-original-title' => 'Удалить шаблон','data-toggle' => 'tooltip','data-placement' => 'bottom'])?>
      </div>
        </div>
      </div>

</div>

<div class="row ">
     <div class="col-md-6">
        <?= $form->field($model, 'email_to')->label('*Кому')->textArea(['placeholder' =>'Введите список emails через запятую или загрузите файл']) ?>
      </div>

     <div class="col-md-6 text-right" style="display: inline-block;vertical-align: top !important;">
                 	<?php

    echo $form->field($model, 'maildb')->widget(FileInput::classname(), [
    'options' => ['multiple' => false],
      'pluginOptions' => [
        'showPreview' => false,
        'showCaption' => true,
        'showRemove' => true,
        'showUpload' => false,
                'browseClass' => 'btn btn-primary btn-block file-maildb',
        'browseIcon' => '<i class="glyphicon glyphicon-file"></i> ',
        'browseLabel' =>  'Выбрать emails',
           'allowedExtensions' => ['csv','xls(x)'],
    ]
])->label("");
echo '<hint>Загрузите файл (Форматы: csv, xls(x))<br />Для замены имени в теле письма, используйте [NAME]</hint>';
         ?>
      </div>
</div>
<div class="row ">
     <div class="col-md-6">
        <?= $form->field($model, 'email_subject')->label('*Тема')->textInput(['placeholder' =>'Коммерческое предложение']) ?>
      </div>
           <div class="col-md-6">
        <?= $form->field($model, 'mailing_from')->label('*От кого')->textInput(['placeholder' =>'Введите email, f.e. support@slide.chat']) ?>
      </div>

</div>
<div class="row">
 <div class="col-md-12">
    <?= $form->field($model, 'attachments')->hiddenInput()->label(''); ?>
 <div id='attaches' class='text-center' style='margin-left:5%'></div>
</div>
</div>
<div class="row">
 <div class="col-md-4 col-lg-3">
        <?=Html::button(Yii::t('app', 'Прикрепить файл из облака'),[
			'class'=>'btn btn-warning select-cloud btn-main',
   			'data' => [
   			    'isTempl'=>'0',
        		'toggle' => 'modal',
        		'target' => '#cloud',
   ]])?>
 </div>
      <div class="col-md-4 col-lg-3">
        <? echo FileInput::widget(['name'=>'doc',
        'id'=>'doc',
        'language' => 'ru',

	    'options' => ['multiple' => false],
		    'pluginOptions' => [ 'showPreview' => false,
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => false,
        'autoUpload'=>true,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Загрузить с диска',
     //   'showUpload'=>false,
        'showCancel'=>false,
        'showClose'=>false,
         'uploadUrl' => Url::to(['/documents/file-upload-send']),
    ]
		]);?>
      </div>

</div>
<div class="row">
        <div class="col-md-12 text-right">
      <?=Html::button(Yii::t('app', Icon::show('save', ['class'=>'fa-1x']).' Сохранить шаблон'),[
'class'=>'btn-link create-template',
    'data' => [
    'id'=>'-1',
       'toggle' => 'modal',
        'target' => '#createTemplate',
   ]])?>
      </div>
</div>
<div class="row">
      <div class="col-md-12">
<?= $form->field($model, 'email_body',['showLabels'=>false])->widget(TinyMce::className(), [

    'options' => ['rows' => 6],
    'language' => 'ru',
    'clientOptions' => [
    'setup'=>new JsExpression('function(editor){
            console.log(\'rf\')
            editor.on(\'focus\', function(e) {
             $("#letters-email_body_ifr").css("height","350px");
                console.log("focus");
            });
             editor.on(\'blur\', function(e) {
             $("#letters-email_body_ifr").css("height","130px");
                console.log("focus");
            });
     }'),
       'menubar'=>'false',
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ]
])->label(false);?>

      </div>
      </div>

<div class="row">
      <div class="col-md-6 text-left">
        <?= Html::activeLabel($model, 'mailing_use_smtp', [
        'class'=>'control-label col-md-6 text-right'
    ]) ?>
     <div class="col-md-6 text-left">
     <?= $form->field($model, 'mailing_use_smtp',[
            'showLabels'=>false,

        ])->dropDownList(Letters::$mailing_use_smtp_arr,['class'=>'text-left']); ?>
      </div>
    </div>
   <div class="col-md-4 text-left">
             <?php
            echo \kartik\widgets\DateTimePicker::widget([
	'model' => $model,
	'attribute' => 'sheduller',

	'options' => [  'hint'=>'','placeholder' => 'Запланировать отправку ...'],
	'language' => 'ru',
	'pluginOptions' => [

	'minuteStep' => 60,
	'pickerPosition' => 'top-right',

		'autoclose' => true
	]
]);
?>      </div>
      </div>
       &nbsp;
<div class="row text-center">
        <div class="form-group">
            <?= Html::submitButton('Отправить письмо', ['class' => 'btn btn-primary btn-main']) ?>
        </div>
  </div>



<?
ActiveForm::end();
  Pjax::end();
?>
<div class="clear"></div>
    </div>
</div>
<div class="clear"></div>
<?php

yii\bootstrap\Modal::begin([
    'id' => 'createTemplate',
      'header' => 'Создание шаблона',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
      'size' => 'modal-lg',
    'clientOptions' => ['backdrop'=>true,'keyboard' => FALSE]
]);
yii\bootstrap\Modal::end();
?>

<?php

Modal::begin([
    'id' => 'cloud',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Прикрепить файл из облака',
    'size' => 'modal-lg',
 //   'url' => Url::to(['/email/select']), // Ajax view with form to load
 //   'ajaxSubmit' => true,
]);
Modal::end();
?>
<?php

Modal::begin([
    'id' => 'success',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Письмо отправлено',
    'size' => 'modal-lg',
]);
?>
  Рассылка успешно загружена.<br>
  Можете сохранить рассылаемое письмо как шаблон, или создать новую рассылку<br>
  <?=Html::button('Сохранить как шаблон',['class'=>'btn-link create-template', 'data' => ['id'=>'-1','toggle' => 'modal','target' => '#createTemplate']])?>
 &nbsp;&nbsp;<?=Html::a( Icon::show('envelope', ['class'=>'fa-1x']).' Создать новую рассылку',"send",['class'=>'btn btn-default ',])?>
<?
Modal::end();
?>
