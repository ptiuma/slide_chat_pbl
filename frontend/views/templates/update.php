<?php
use yii\helpers\Html;
use  yii\helpers\Url;

use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use frontend\models\UserTemplates;
use kartik\widgets\FileInput;
use yii\widgets\Pjax;
use dosamigos\tinymce\TinyMce;
use yii\web\JsExpression;

use kartik\icons\Icon;
Icon::map($this);
/* @var $this yii\web\View */
/* @var $model frontend\models\UserTemplates */

$this->title = Yii::t('app', 'Редатировать шаблон: ', [
    'modelClass' => 'User Templates',
]) . $model->name;
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/templates.js',['depends' => 'yii\web\JqueryAsset']);


?>
<div class="row tpl-form">
       <div class="col-md-10 col-md-offset-1">
              <div class="panel2 panel-default2">
                                               <div class="panel-body">
<?php
 Pjax::begin(['id' => 'new_template']);
  $form = ActiveForm::begin(['id'=>'editTemplateForm','type'=>ActiveForm::TYPE_HORIZONTAL,'options' => ['data-pjax' => true]]);
     if($model->id)
    {
     $files=explode(',',$model->AttachmentCollection);
    	foreach($files as $v)
    	{
	    	$this->registerJs("
	            	insertAttachment('".$v."')
	            ");
        }
      }
  ?>


<div class="row">
 <div class="col-md-12">
    <?= $form->field($model, 'name')->textInput(); ?>
</div>
</div>
<div class="row">
 <div class="col-md-12">
    <?= $form->field($model, 'email_subject')->textInput(); ?>
</div>
</div>

<div class="row">
 <div class="col-md-12">
    <?= $form->field($model, 'AttachmentCollection[]')->hiddenInput()->label(''); ?>

 <div id='attachesT' class='text-center' style='margin-left:5%'></div>
</div>
</div>

<div class="row">
 <div class="col-md-5 col-lg-5">
        <?=Html::button(Yii::t('app', 'Прикрепить файл из облака'),[
			'class'=>'btn btn-warning select-cloud btn-main',
   			'data' => [
   			    'isTempl'=>'1',
        		'toggle' => 'modal',
        		'target' => '#cloud',
   ]])?>
 </div>
      <div class="col-md-5 col-lg-5">
        <? echo FileInput::widget(['name'=>'docT',
        'id'=>'docT',
        'language' => 'ru',

	    'options' => ['multiple' => false],
		    'pluginOptions' => [ 'showPreview' => false,
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => false,
        'autoUpload'=>true,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Загрузить с диска',
     //   'showUpload'=>false,
        'showCancel'=>false,
        'showClose'=>false,
         'uploadUrl' => Url::to(['/documents/file-upload-send']),
    ]
		]);?>
      </div>
</div>

<div class="row">
      <div class="col-md-12">
<?= $form->field($model, 'email_body',['showLabels'=>false])->widget(TinyMce::className(), [

  'options' => ['rows' => 6],
    'language' => 'ru',
    'clientOptions' => [
    'setup'=>new JsExpression('function(editor){
            console.log(\'rf\')
            editor.on(\'focus\', function(e) {
             $("#usertemplates-email_body_ifr").css("height","350px");
                console.log("focus");
            });
             editor.on(\'blur\', function(e) {
             $("#usertemplates-email_body_ifr").css("height","130px");
                console.log("focus");
            });
     }'),
       'menubar'=>'false',
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ]
])->label(false);?>

      </div>
      </div>
            <div class="col-md-12">
        <div class="form-group text-right">
            <?= Html::submitButton('Сохранить шаблон', ['class' => 'btn btn-primary btn-main']) ?>
        </div>
  </div>
  <?
ActiveForm::end();
   Pjax::end();

?>
                </div>
                  </div>
    </div>
</div>
<div class="clear"></div>
<?php

yii\bootstrap\Modal::begin([
    'id' => 'cloud',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Прикрепить файл из облака',
    'size' => 'modal-lg',
 //   'url' => Url::to(['/email/select']), // Ajax view with form to load
 //   'ajaxSubmit' => true,
]);
yii\bootstrap\Modal::end();
?>

