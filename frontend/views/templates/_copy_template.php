<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\UserTemplates */
/* @var $form yii\widgets\ActiveForm */
 $files=explode(',',$template->AttachmentCollection);
?>

<div class="user-templates-cnt">
<h3 class="text-left">Выберите команду</h3>

<p>&nbsp;</p>
<?php foreach($teams as $team):?>
   <div class="row text-center">

   <div class="col-md-12">
           <?= Html::a($team->name, ['/templates/copy-template','teamId'=>$team->id,'tplId'=>$template->id], ['class' => 'btn btn-block btn-default text-left']) ?>
   </div>
<p>   &nbsp;</p>

 </div>
<?php endforeach;?>
</div>
