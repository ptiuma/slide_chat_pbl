<?php

use yii\helpers\Html;
use yii\grid\GridView;
    use kartik\widgets\SideNav;
use yii\helpers\Url;
use common\models\Team;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Templates');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile(Yii::$app->request->baseUrl.'/frontend/assets/plugins/datatables/media/css/dataTables.bootstrap.css', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
$this->registerCssFile(Yii::$app->request->baseUrl.'/frontend/assets/plugins/datatables/extensions/Select/css/select.dataTables.min.css', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
 $this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/plugins/datatables/media/js/jquery.dataTables.min.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/plugins/datatables/media/js/dataTables.bootstrap.min.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/plugins/datatables/extensions/Select/js/dataTables.select.min.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/templates.js',['depends' => 'yii\web\JqueryAsset']);
?>
<style>
    .table thead{
    display:none;
}
</style>
<div class="template-left-col">
	<div class="text-center">
      <?= Html::a(Yii::t('app', 'Новый шаблон'), ['create'], ['class' => 'btn btn-block btn-info']) ?>
      &nbsp;
      <div class="form-search">
            <input id="search-field" class="form-control" placeholder="Поиск"  data-help-box-text="Поиск по названию шаблона">
        </div>
    </div>
    <p>&nbsp;</p>
<?php
$menuItems=[
   ['label' => '<span class="pull-right badge">'.$total_templates.'</span> Все шаблоны', 'icon' => 'file', 'url' => Url::to(['#']),'options'=>['data-id'=>'all']],
           ['label' => '<span class="pull-right badge">'.count($personal_templates).'</span> Персональные', 'icon' => 'user', 'url' => Url::to(['#']),'options'=>['data-id'=>'0']],
];

        foreach($teams as $team)
        {            $total=count($team->templates);
           // if($total>0)
             $menuItems[]=['label' => '<span class="pull-right badge">'.$total.'</span> '.$team->name, 'icon' => 'th', 'url' => Url::to(['#']),'options'=>['data-id'=>$team->id]];
        }
echo SideNav::widget([
    'type' => 'primary',
    'encodeLabels' => false,
    //'heading' => $heading,
    'items' => $menuItems,
]);
?>
</div>
<div class="user-templates-center">
     <div id="table-tpl-0" class="row tpl-table">
     	<div class="col-sm-12">
           <b>Персональные</b>
     <table  class="display table table-bordered  dataTable" role="grid" aria-describedby="example1_info">
           <thead>
        <tr>
            <th>Column 1</th>
            <th>Column 2</th>
            <th>Column 2</th>
            <th>Column 2</th>
        </tr>
    </thead>
         <tbody>
     <?php foreach($personal_templates as $template):?>
                <tr role="row" class="odd" id="<?=$template->id?>">
                  <td ><?=$template->name?></td>
                  <td width="15%"><img src="<?=Url::to(['/'])?>img/icons/envelope.png" border="0"> <?=$template->letters_total?></td>
                  <?php if(!$template->AttachmentCollection):?>
                  <td width="15%"><img src="<?=Url::to(['/'])?>img/icons/3_v.png" border="0"> <?=$template->letters_opened?></td>
                  <td width="15%"><img src="<?=Url::to(['/'])?>img/icons/no-attach.png" border="0"></td>
                   <?php else:?>
                  <td width="15%"><img src="<?=Url::to(['/'])?>img/icons/1_f.png" border="0"> <?=$template->letters_opened?></td>
                  <td width="15%"><img src="<?=Url::to(['/'])?>img/icons/2_f.png" border="0"> <?=$template->letters_opened_att?></td>
                   <?php endif;?>
                </tr>
                 <?php endforeach;?>
                </tbody>
              </table></div></div>
                         <p>&nbsp;</p>

<?php foreach($teams as $team):
 $total=count($team->templates);
 if($total>0):
?>
    <div id="table-tpl-<?=$team->id?>" class="row tpl-table">
     	<div class="col-sm-12">
           <b><?=$team->name?></b>
     <table id="table-tpl-0" class="display table table-bordered  dataTable" role="grid" aria-describedby="example1_info">
           <thead>
        <tr>
            <th>Column 1</th>
            <th>Column 2</th>
            <th>Column 2</th>
            <th>Column 2</th>
        </tr>
    </thead>
         <tbody>
     <?php foreach($team->templates as $template):?>
                <tr role="row" class="odd" id="<?=$template->id?>">
                  <td ><?=$template->name?></td>
                  <td width="15%"><img src="<?=Url::to(['/'])?>img/icons/envelope.png" border="0"> <?=$template->letters_total?></td>
                  <?php if(!$template->AttachmentCollection):?>
                  <td width="15%"><img src="<?=Url::to(['/'])?>img/icons/3_v.png" border="0"> <?=$template->letters_opened?></td>
                  <td width="15%"><img src="<?=Url::to(['/'])?>img/icons/no-attach.png" border="0"></td>
                   <?php else:?>
                  <td width="15%"><img src="<?=Url::to(['/'])?>img/icons/1_f.png" border="0"> <?=$template->letters_opened?></td>
                  <td width="15%"><img src="<?=Url::to(['/'])?>img/icons/2_f.png" border="0"> <?=$template->letters_opened_att?></td>
                   <?php endif;?>
                </tr>
                 <?php endforeach;?>
                </tbody>
              </table></div></div>
              <p>&nbsp;</p>
<?php endif;?>
<?php endforeach;?>
<?php

    	  $js = <<< EOT
      $('table.display').DataTable({
  'bPaginate':false,
  'select': true,
"sDom": 'top rt'

    });
EOT;

$this->registerJs($js, yii\web\View::POS_READY);
?>


</div>
<div class="template-right-col">
   <div  class="template-edit text-center">
        <div id="tpl-preview">
            <img src="<?=Url::to(['/'])?>img/icons/no-letter.png">
            <p>Выберите шаблон, что бы посмотреть его целиком</p>
        </div>
         <div id="tpl-content"></div>
        </div>
</div>