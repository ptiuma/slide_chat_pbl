<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\UserTemplates */
/* @var $form yii\widgets\ActiveForm */
 $files=explode(',',$template->AttachmentCollection);
?>

<div class="user-templates-cnt">
<?php if(!$template->id):?>
         Вы не имеете права редактировать этот шаблон
<?php else:?>

<h3 class="text-left"><?=$template->name?></h3>
<p>&nbsp;</p>
   <div class="row text-center">
   <div class="col-md-6">
           <?= Html::a(Yii::t('app', 'Изменить'), ['/templates/update','id'=>$template->id], ['class' => 'btn btn-block btn-main']) ?>
   </div>
   <div class="col-md-6">
           <?= Html::a(Yii::t('app', 'Копировать'), ['/templates/copy-template','tplId'=>$template->id], ['title'=>'Копировать шаблон','class' => 'showModalButton btn btn-block btn-default',  'data' => [
        'toggle' => 'modal',
        'target' => '#modal',
   ]]) ?>
   </div> &nbsp;
   <div class="col-md-12">
           <?= Html::a(Yii::t('app', 'Удалить'), ['/templates/delete','id'=>$template->id], ['class' => 'btn btn-block btn-default','data' => [
                'confirm' => "Вы уверены, что хотите удалить шаблон?",
                'method' => 'post',
            ]]) ?>
   </div>
<p>   &nbsp;</p>
   <div class="text-left">
   <div class="col-md-12">
           Тема письма: <?=$template->email_subject?>
   </div>
   <div class="col-md-12">
           Прикреплённые файлы: <?=!$template->AttachmentCollection?0:count($files)?>
   </div>
   <div class="col-md-12">
           Сообщение:<br /> <?=strip_tags($template->email_body)?>
   </div>
  </div>
 </div>
<?php endif;?>
</div>
