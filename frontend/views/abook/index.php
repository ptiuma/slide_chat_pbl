<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var frontend\models\AddressBookSearch $searchModel
 */

$this->title = 'Адресная книга';
//$this->params['breadcrumbs'][] = $this->title;


?>
<div class="address-book-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
         <div class="row">
          <div class="col-md-11 text-right">
         <form  class="form-inline" method="get">
           <div class="form-group">
           <input class="form-control input-lg" name="keyword" value="<?php echo Html::encode($keyword)?>" size="48" placeholder="Поиск по имени, email или телефону" type="search">
           </div>
          <input type="submit" class="btn btn-primary btn-search" value="Поиск">

          </form>
           </div>
         </div>
<p>&nbsp;</p>

      <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
    //    'filterModel' => $searchModel,
        'id'=>'abook',
        'columns' => [
            [
    'class'=>'kartik\grid\CheckboxColumn',
    'headerOptions'=>['class'=>'kartik-sheet-style'],
],
            ['class' => 'yii\grid\SerialColumn'],
                'email:ntext',
            'name',

                  'company',

            'phone',
     'notes:ntext',
             [
	 'attribute' => 'letters_count',
	 	'format'=>'raw',
	 	'label'=>'# писем',
 'value'=>function ($data) {
		        return $data->letters_count;
		    },
	 ],
             [
	 'attribute' => 'visits_count',
	 	'format'=>'raw',
	 	'label'=>'# визитов',
 'value'=>function ($data) {
		        return $data->visits_count;
		    },
	 ],
           'created_at:date',
//            'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                 'template' => '{visits} {update} {delete}',
                'buttons' => [
                  'visits' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-tasks"></span>', Yii::$app->urlManager->createUrl(['activity/visits','email' => $model->email,'check'=>'1']), [
                                                    'title' => Yii::t('yii', 'Visits'),
                                                  ]);},

                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['abook/update','id' => $model->id,'edit'=>'t']), [
                                                    'title' => Yii::t('yii', 'Edit'),
                                                  ]);}

                ],
            ],

        ],
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,


          'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
       'pjax' => true, // pjax is set to always true for this demo
    'pjaxSettings' =>[
        'neverTimeout'=>true,
        'options'=>[
                'id'=>'kv-unique-id-1',
            ]
        ],

        'export'=>['label'=>'Экспорт'],

        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'primary',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Новая запись', ['create'], ['class' => 'btn btn-success']).'&nbsp;&nbsp;'.
 			(!$keyword?'':Html::a('<i class="glyphicon glyphicon-list"></i> Вернуться к общему списку',['index'], ['id'=>'ViewVisits','class' => 'btn  btn-default'])).'',
            'after'=>"<div class='text-left'>".
            Html::button('<i class="glyphicon glyphicon-trash"></i> Удалить выбранные',  ['id'=>'DelButton','class' => 'btn btn-danger']).'&nbsp;&nbsp; '.
            Html::button('<i class="glyphicon glyphicon-plus"></i> Отправить письмо',  ['id'=>'EmailButton','class' => 'btn btn-default']).'</div>',

           // 'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>
 <?php

    $this->registerJs('

    $(document).ready(function(){
	$(\'#DelButton\').click(function(){

        var HotId = $(\'#abook\').yiiGridView(\'getSelectedRows\');
       if(HotId.length==0) alert(\'Не выбрано ни одной записи\')
       else
       {
          $.ajax({
            type: \'POST\',
            url : BASE+"abook/multiple-email-delete",
            data : {row_id: HotId},
            success : function(data) {
                $.pjax.reload({container:\'#w0\'});
            }
        });
       }
    });
    $(\'#EmailButton\').click(function(){

        var HotId = $(\'#abook\').yiiGridView(\'getSelectedRows\');
       if(HotId.length==0) alert(\'Не выбрано ни одной записи\')
       else
       {
          $.ajax({
            type: \'POST\',
            url : BASE+"/abook/multiple-email",
            data : {row_id: HotId},
            success : function(data) {
               location.href=BASE+"email/send?send_to="+data
            }
        });
       }
    });
    });', \yii\web\View::POS_READY);

?>
<script type="text/javascript">
function apply_filter() {

$('.grid-view').yiiGridView('applyFilter');

}
</script>