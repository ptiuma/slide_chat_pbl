<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var frontend\models\AddressBook $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="address-book-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'email'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Введите Email...','rows'=> 6]],
			 'name'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Введите Имя...', 'maxlength'=>255]],

           // 'nickname'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Введите Nickname...', 'maxlength'=>255]],

            'company'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Введите Компанию...', 'maxlength'=>255]],

                  'phone'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Введите телефон....','rows'=> 6]],



            'notes'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Введите Комментарий...','rows'=> 6]],


        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Редактировать'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
