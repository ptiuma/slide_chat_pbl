<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var frontend\models\AddressBook $model
 */

$this->title = 'Добавить запись';
$this->params['breadcrumbs'][] = ['label' => 'Address Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-book-create">
    <div class="page-header">
       &nbsp;
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
