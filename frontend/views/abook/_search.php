<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var frontend\models\AddressBookSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="row">
    <div class="col-md-8 bordered">
    <form>
      <input class="input-lg" size="32" placeholder="Поиск по имени, email или телефону" type="search"><button type="submit" class="btn btn-primary ">Поиск</button>

    </form>
    </div

</div>
<div class="clear"></div>
&nbsp;
