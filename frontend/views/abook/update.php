<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var frontend\models\AddressBook $model
 */

$this->title = 'Обновить данные: ' . ' ' . $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Address Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->id]];
?>
<div class="address-book-update">

    <div class="page-header">
       &nbsp;
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
