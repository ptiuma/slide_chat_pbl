<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var frontend\models\AddressBook $model
 */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Address Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-book-view">
    <div class="page-header">
       &nbsp;
    </div>


    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>$this->title,
            'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'id',
            'userId',
            'firstname',
            'middlename',
            'lastname',
            'nickname',
            'company',
            'title',
            'address:ntext',
            'addr_long:ntext',
            'addr_lat:ntext',
            'addr_status:ntext',
            'home:ntext',
            'mobile:ntext',
            'work:ntext',
            'fax:ntext',
            'email:ntext',
            'homepage:ntext',
            'address2:ntext',
            'phone2:ntext',
            'notes:ntext',
            'isArchive',
            'created_at',
            'updated_at',
        ],
        'deleteOptions'=>[
            'url'=>['delete', 'id' => $model->id],
            'data'=>[
                'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
                'method'=>'post',
            ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
