<?php
use kartik\grid\GridView;
use yii\helpers\Html;
use kartik\icons\Icon;
Icon::map($this);
   use kartik\export\ExportMenu;
use kartik\widgets\FileInput;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\growl\GrowlAsset;
use kartik\base\AnimateAsset;
GrowlAsset::register($this);
AnimateAsset::register($this);
$this->title = 'Документы';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile(
    '@web/frontend/assets/js/docs.js',  ['depends' => [yii\web\JqueryAsset::className()]]);

?>
<div class="row">

		<div class="col-md-4 pull-right">
		<?php
		   //	foreach (Yii::$app->session->getAllFlashes() as $key => $message)echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
?>
		<?php
		echo '<br><label>Загрузите файлы (Форматы: ppt(x), xls(x),doc, pdf)</label>';
		$form1 = ActiveForm::begin([
		'id'=>'docForm',
    'options'=>['data-pjax' => false] // important
]);
echo FileInput::widget([
'name'=>'doc',
'id'=>'doc',
'language' => 'ru',
		    'options' => ['multiple' => false],
		    'pluginOptions' => [ 'showPreview' => false,
    //    'showCaption' => false,
        'showRemove' => false,
        'showUpload' => false,
        'autoUpload'=>true,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Загрузить с диска',
     //   'showUpload'=>false,
        'showCancel'=>false,
        'showClose'=>false,
      //   'allowedExtensions' => ['ppt','xls','pdf'],
       'uploadUrl' => Url::to(['/documents/file-upload-send']),
    ],


		]);
ActiveForm::end();

		?>
		</div>
</div>
<?php
$gridColumns = [
    // ['attribute'=>'id','width'=>'80px'],
     	[
    'attribute' => 'Превью',
    'format' => 'html',
    'value' => function($data) { return '<div class="thumbnail thumbnail-blue docthumbnail">'.Html::a(Html::img(Url::to(['/']).$data->preview, ['width'=>'60']),Url::to(['/']).'documents/'.$data->id).'</div>'; },
],
     	[
    'attribute' => 'name',
    'format' => 'raw',
    'value' => function($data) { return '<div class="document-name">'.
    	Html::a($data->name,Url::to(['/']).'documents/'.$data->id)
    	.'</div><div class="document-qstats">'
    	.Html::a(Icon::show('eye', ['class'=>'fa-1x']).' Открыть документ',  ['/view/preview-doc?id='.$data->id], ['target'=>'_blank','class'=>'btn btn-default btn-xs create-view-link'])
    	.'&nbsp;&nbsp;'
    	.Html::a(Icon::show('download', ['class'=>'fa-1x']).' Скачать документ', ['download-doc?id='.$data->id], ['class'=>'btn btn-default btn-xs create-view-link'])
    	.'<br><br>'
    	.Html::a(Icon::show('link', ['class'=>'fa-1x']).' Создать ссылку', '#', ['class'=>'btn btn-default btn-xs create-view-link','data' => [
    'docId'=>$data->id,'dismiss'=>'modal','toggle' => 'modal','target' => '#createLink']])
    	.'&nbsp;&nbsp;'.Html::a(Icon::show('video-camera', ['class'=>'fa-1x']).' Начать презентацию', '#', [
    	'class'=>'btn btn-default btn-xs create-presentation-link',
    	'data'=>[
        'docId'=>$data->id,
    		]
		])
		.'&nbsp;'.Html::a(Icon::show('pie-chart', ['class'=>'fa-1x']).' Статистика', ['documents/'.$data->id], ['class'=>'btn btn-default btn-xs',])
		.'</div>'; },
],
     	[
    'attribute' => 'Превью',
    'format' => 'html',
    'value' => function($data) { return '<div class="document-actions-block text-center"><div class="round round-slg hollow green document-views">
               <em>'.$data->visits.'</em><p>Визитов</p>
             </div></div>'; },
],
    [
        'class' => '\kartik\grid\ActionColumn',
         'template' => '{delete}',
        'deleteOptions' => ['label' => ''.Icon::show('remove', ['class'=>'fa-1x']).'']
    ]
];



?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
       'columns' => $gridColumns,
           'pjax' => true,

           'bordered' => true,
           'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
          'pjaxSettings'=>[
          'options'=>[
        'linkSelector'=>false,
         'id'=>'docL',
            ]
    ]

    ]); ?>


    <?php

yii\bootstrap\Modal::begin([
    'id' => 'createLink',
    'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
    'header'=>'Создать ссылку',
      'size' => 'modal-md',
  //  'url' => Url::to(['/documents/create-link?docId='.$model->id]), // Ajax view with form to load
      'clientOptions' => ['backdrop'=>true,'keyboard' => FALSE]
]);
?>
<div id="createLinkContent"></div>
<?php
yii\bootstrap\Modal::end();
?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
    'header'=>'Ссылка создана',
    'id' => 'successLink',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
?>
     <div><input type="text" class="form-control" id="copy-input" readonly="readonly" size="32">
     <button class="btn btn-sm btn-copy btn-default" data-clipboard-target="#copy-input" data-clipboard-action="copy" type="button" id="copy-button"     data-toggle="tooltip" data-placement="button"
          title="Копировать">
        Копировать
      </button></div>
    <p>  Отправьте ее любым привычным способом (Email, Skype, WhatsApp и т.п.)
     </br>
Наш сервис уведомит, когда клиент просмотрел документ и Вы можете сразу связаться с ним
</p>

<?php
yii\bootstrap\Modal::end();
?>

