<?php
use kartik\helpers\Html;
use  yii\helpers\Url;
use frontend\models\LinksViewsPages;
foreach($model->link->doc->pages as $page)
{
       $num++;
       // echo $page->docId;
         //     echo $page->pageNum;
           // $query =LinksViewsPages::find()->where(['pageNum'=>$page->pageNum,'client_session'=>$model->client_session]);
    //echo $query->createCommand()->getRawSql();
       $stat=LinksViewsPages::find()->where(['pageNum'=>$num,'client_session'=>$model->client_session])->sum('ended_at-created_at');
       $stats[]=['category'=>'Страница '.$num,'column-1'=>(int)$stat];
       $pages[]='№'.$num;
}

?>
<div class="row" style="border:0px solid" >
  <div class="col-md-12 text-left" style="background:#FFF;border:1px dashed #86CCF7;padding:10px">
   <div class="col-md-6">
     Дата визита: <?=date('Y-m-d h:i',$model->created_at);?><br>
     Общее время просмотра: <?=$model->interval;?> (мин:сек)<br>
     IP: <?=$model->client_ip;?><br>
     Скачивание: <?=$model->downloads;?><br>
     Печать: <?=$model->print;?><br>
  </div>
   <div class="col-md-6">
     Город: <?=$model->client_city;?><br>
     Страна: <?=$model->client_country;?><br>
          Платформа: <?=$model->client_os;?><br>
               Броузер: <?=$model->client_browser;?><br>
                    Тип устройства: <?=$model->device;?>
  </div>
  </div>
  </div>
<div class="row">
  <div class="col-md-12">
  <?php if($model->link->doc->noParsed==1):?>
            <?php
echo Html::jumbotron([
    'heading' => '',
    'body' => 'Структура документа не позволяет разложить его по слайдам,<br /> и отобразить постраничную статистику<br>Документ был скачан напрямую',

]);
?>
 <?php else:?>
	<!-- amCharts javascript code -->
		<script type="text/javascript">
		 var chart = AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start",
						"dateFormats":[{period:'ss',format:'JJ:NN:SS'}]

					},
					"chartCursor": {
						"enabled": true
					},
					"chartScrollbar": {
						"enabled": true
					},
					"trendLines": [],
					"graphs": [
						{
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "graph 1",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Время, сек"
						}
					],
					"allLabels": [],
					"balloon": {},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Статистика по страницам"
						}
					],
					"dataProvider": <?=json_encode($stats)?>
				}
			);

		</script>

		<div id="chartdiv" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
            <?php endif;?>
  </div>

</div>
