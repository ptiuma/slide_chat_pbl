<?php
use kartik\helpers\Html;
use  yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\icons\Icon;
Icon::map($this);
use kartik\form\ActiveForm;
use common\models\Team;
use common\models\DocumentsToTeam;
$teams=yii\helpers\ArrayHelper::map(Team::find()->joinWith('members',true,'RIGHT JOIN')->where(['memberId' => Yii::$app->user->id])->asArray()->all(),'id','name');
$teams_selected=ArrayHelper::map(DocumentsToTeam::find()->where(['docId' => $doc->id])->asArray()->all(),'','teamId');

?>
             <div class="row link-block">
                   <div class="col-md-12">
                   <?php if(count($teams)==0):?>
                   <p>Вы не приписаны ни к одной команде</p>
                     <?php else:?>
  <?php
  $form = ActiveForm::begin([
    'id' => 'docTeamForm',
    'type' => ActiveForm::TYPE_VERTICAL
]);
?>
  <?php
  $model->docId=$doc->id;
echo $form->field($model, 'docId')->hiddenInput()->label(false); ?>
<?php
echo $form->field($model, 'teamList')->checkboxList($teams,
    ['item' =>
        function($index, $label, $name, $checked, $value) use ($teams_selected) {
            $checked=ArrayHelper::IsIn($value,$teams_selected)?'checked=checked':'';
             return "<label class='ckbox ckbox-primary col-md-4'><input type='checkbox' {$checked} name='{$name}' value='{$value}' tabindex='3'>&nbsp;&nbsp;{$label}</label>";

        }
    ]); ?>
     <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <?= Html::submitButton('Обновить', ['id'=>'tmBtn','class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?
ActiveForm::end();
?>
 <?php endif;?>
     </div>

             </div>
