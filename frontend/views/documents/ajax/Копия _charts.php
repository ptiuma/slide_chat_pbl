<?php

use yii\helpers\Url;
use tugmaks\GoogleMaps\Map;
use frontend\models\LinksViews;
$data=LinksViews::find()->joinwith('link')->where(['links.docId'=>$model->id])->all();
$locs=[];
    foreach($data as $v)$locs[]=['position'=>$v->client_country.' '.$v->client_city];
 ?>
 <div class="row">
            <div class="col-md-12">
            <h4>Расположение на карте</h4>
 <?php
echo Map::widget([
    'width' => 1100,
    'height' => 600,
    'mapType' => Map::MAP_TYPE_HYBRID,
    'markers' => $locs,
    'markerFitBounds'=>true
]);
?>
</div>
</div>