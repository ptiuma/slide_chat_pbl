<?php
use kartik\helpers\Html;
use  yii\helpers\Url;
use frontend\models\LinksViewsPages;

foreach($model->link->doc->pages as $page)
{
       $num++;
        $stat=LinksViewsPages::find()->where(['pageNum'=>$page->pageNum,'client_session'=>$model->client_session])->sum('ended_at-created_at');
       $stats[]=intval($stat)*1000;
       $pages[]='№'.$num;
}

?>
<div class="row-fluid" style="width:100%;border:0px solid" >
  <div class="col-md-8">

<?=
\dosamigos\highcharts\HighCharts::widget([
    'clientOptions' => [
       'chart' => [
               'width'=>'800',
            'type' => 'column',
            'margin' => 75,
            'options3d' => [
                'enabled' => true,
                'alpha' => 15,
                'beta' => 15,
                'depth' => 70,
              //  'viewDistance' => 25
            ],
        ],
      'title' => ['text' => 'Статистика по страницам'],
        'plotOptions' => ['column' => ['depth' => 25]],
        'xAxis' => [
          //  'min'=>1,
            'categories' =>$pages

        ],
         'tooltip'=>[

        ],
        'yAxis' => [
             'type'=>'datetime',
             'dateTimeLabelFormats'=>[ //force all formats to be hour:minute:second
                        'second'=>'%H:%M:%S',
                        'minute'=>'%H:%M:%S',
                        'hour'=>'%H:%M:%S',
                        'day'=>'%H:%M:%S',
                    ],
            'opposite' => false,
             'title'=>[
                'text'=>'Время (сек)'
            ]
        ],
        'series' => [
            ['name' => 'Page #', 'data' => $stats],
        ]
    ]
]);
?>
  </div>
  <div class="col-md-3 text-right">
  <div class="text-left" style="background:#FFF;border:3px solid #86CCF7;padding:10px">
     Город: <?=$model->client_city;?><br>
     Страна: <?=$model->client_country;?><br>
          Платформа: <?=$model->client_os;?><br>
               Броузер: <?=$model->client_browser;?><br>
                    Тип устройства: <?=$model->device;?>
  </div>
  </div>
</div>
