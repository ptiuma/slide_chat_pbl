<?php

use yii\helpers\Url;
use tugmaks\GoogleMaps\Map;
use common\models\Links;
use common\models\Letters;
use frontend\models\LinksViews;
use kartik\helpers\Enum;
use kartik\helpers\Html;
$data=LinksViews::find()->joinwith('link')->where(['links.docId'=>$model->id])->all();
$locs=[];
    foreach($data as $v)if($v->client_country)$locs[]=['position'=>$v->client_country.' '.$v->client_city];

$stat[]=['Число визитов',5];
$stat[]=['Общее время просмотра',7];
$stat[]=['Количество скачиваний',4];
$stat[]=['Всего писем',5];
 ?>
 <div class="row">
 <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Наиболее просматриваемая страница</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-condensed">
                <tbody>
                <tr>
                  <td><div class="thumbnail thumbnail-blue docthumbnail"><?=Html::img(Url::to(['/']).$model->preview, ['width'=>'60'])?></div></td>
                  <td></td>
                </tr>

              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
<div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Общая статистика по документу</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-condensed">
                <tbody>
                <tr>
                  <td>1.</td>
                  <td>Число визитов</td>
                  <td><span class="badge bg-red"><?=Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['links.docId' => $model->id])->count()?></span></td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>Общее время просмотра</td>
                  <td><span class="badge bg-yellow"><?= gmdate("H:i:s", (int)Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['links.docId' => $model->id])->sum('views.ended_at-views.created_at'))?></span></td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>Всего писем</td>
                  <td><span class="badge bg-light-blue"><?=Letters::find()->joinWith('links',true,'RIGHT JOIN')->where(['docId' => $model->id])->count()?></span></td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td>Количество скачиваний</td>
                  <td><span class="badge bg-green"><?=Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['links.docId' => $model->id,'downloads'=>1])->count()?></span></td>
                </tr>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
 </div>
 <div class="row">
            <div class="col-md-12">

</div>
</div>