<?php
use kartik\helpers\Html;
use  yii\helpers\Url;
use kartik\grid\GridView;
use kartik\icons\Icon;
Icon::map($this);
use kartik\widgets\SwitchInput;


?>
             <div class="row link-block">
                   <div class="col-md-12">
                      <?php foreach($data as $doc):?>
                          <div style="margin:8px"><?php echo Icon::show('chevron-circle-down', ['class'=>'fa-1x'])." &nbsp;".Html::a($doc->name,'#',['class'=>'','onClick'=>'SelectDoc('.$doc->id.')']);?></div>
                      <?php endforeach?>
                   </div>

             </div>
