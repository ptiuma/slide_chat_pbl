<?php
use yii\helpers\Html;
use  yii\helpers\Url;

use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;

use kartik\icons\Icon;
Icon::map($this);
$this->title = 'Создать ссылку';
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="row email-form">
       <div class="col-md-10 col-md-offset-1">
              <div class="panel panel-default">
                                                <div class="panel-body">
<?php
  $form = ActiveForm::begin(['id'=>'createLinkForm','type'=>ActiveForm::TYPE_HORIZONTAL]);
echo FormGrid::widget([
    'model'=>$model,
    'form'=>$form,
    'autoGenerateColumns'=>true,
    'rows'=>[
        [
      'contentBefore'=>'<legend class="text-info"><small>Карточка клиента</small></legend>',
            'attributes' => [
                                'client_name'=>['type'=>Form::INPUT_TEXT,'options'=>[],
                                'hint'=>'укажите любые данные о клиенте, которые получите в уведомлении'
                                ],

            ],
        ],
           [
            'attributes' => [
                                'docId'=>['type'=>Form::INPUT_HIDDEN,'label'=>'',],

            ],
        ],
            [
            'attributes' => [
                                'client_company'=>['type'=>Form::INPUT_TEXT,'options'=>[],
                                ],
            ],
        ],
        [
            'attributes' => [
                                'client_phone'=>['type'=>Form::INPUT_TEXT,'options'=>[],
                                'hint'=>'укажите номер клиента, чтобы иметь возможность быстро перезвонить ему после получения уведомления'
                                ],
            ],
        ],
        [
      'contentBefore'=>'<legend class="text-info"><small>Расширенные опции</small></legend>',
      'attributes' => [
            'denyDownloading'=>[       'type'=>Form::INPUT_WIDGET,
            'widgetClass'=>'kartik\checkbox\CheckboxX',
            'label'=>'',

			             'options' => [
                                   'autoLabel'=>true,
                                    'pluginOptions'=>['threeState'=>false],
			                       'labelSettings' => ['position' => kartik\checkbox\CheckboxX::LABEL_RIGHT]
						 ]
	           ],
            ],
        ],
        [
      'attributes' => [
            'isProtect'=>[       'type'=>Form::INPUT_WIDGET,
            'widgetClass'=>'kartik\checkbox\CheckboxX',
            'label'=>'',

			             'options' => [
                                   'autoLabel'=>true,
                                    'pluginOptions'=>['threeState'=>false],
			                       'labelSettings' => ['position' => kartik\checkbox\CheckboxX::LABEL_RIGHT],
			                        'pluginEvents' => [
			                         'change'=>'function(e) {var el = $(e.target);
			                         if(el.val()==1)$("#protect_code_el").removeClass("hidden");
			                         else $("#protect_code_el").addClass("hidden");  }',
			                        ],
						 ]
	           ],
            ],
        ],
         [
      'contentBefore'=>'<div id="protect_code_el" class="hidden">',
      'contentAfter'=>'</div>',
            'attributes' => [
                                'protect_code'=>['type'=>Form::INPUT_TEXT,'options'=>[]],

            ],
        ],
        [
      'attributes' => [
            'isExpire'=>[       'type'=>Form::INPUT_WIDGET,
            'widgetClass'=>'kartik\checkbox\CheckboxX',
            'label'=>'',

			             'options' => [
                                   'autoLabel'=>true,
                                    'pluginOptions'=>['threeState'=>false],
			                       'labelSettings' => ['position' => kartik\checkbox\CheckboxX::LABEL_RIGHT],
			                        'pluginEvents' => [
			                         'change'=>'function(e) {var el = $(e.target);
			                         if(el.val()==1)$("#expired_date_el").removeClass("hidden");
			                         else $("#expired_date_el").addClass("hidden");  }',
			                        ],
						 ]
	           ],
            ],
        ],
        [
         'attributes' => [
                'expired_date'=>['type'=>Form::INPUT_WIDGET,
          'label'=>'',
                  'widgetClass'=>'\kartik\widgets\DateTimePicker',
                     'hint'=>'Время московское',
                'options'=>[
                   'options'=>['placeholder'=>'Дата истечения ..', 'pluginOptions' => ['autoclose' => true]],
                ]],
            ],
        ],

                [
            'attributes'=>[
                'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW,
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .
                           Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) .
                        '</div>'
                ],
            ],
        ],
    ]
]);
ActiveForm::end();

?>
                </div>
                  </div>
    </div>
</div>
<div class="clear"></div>

