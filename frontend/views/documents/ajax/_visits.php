<?php
use kartik\helpers\Html;
use  yii\helpers\Url;
use kartik\grid\GridView;
use kartik\icons\Icon;
Icon::map($this);
use kartik\widgets\SwitchInput;
use sfedosimov\daysago\DaysAgo;



?>
<?php if($visits==0&&count($model->links)==0):?>
<?php
echo Html::jumbotron([
    'heading' => 'Создать ссылку',
    'body' => 'В настоящее время нет активности по этому документу.',
    'buttons' => [
        [
            'label' => 'Создать ссылку',

            'icon' => 'link',
            'url' => '#',
            'options'=>[
          'class'=>'create-view-link btn-link',
          'data' => [
          'docId'=>$model->id,
        'toggle' => 'modal',
        'target' => '#createLink',
   ]],
            'type' => Html::TYPE_PRIMARY,
            'size' => Html::SIZE_LARGE
        ],
        [
             'label' => 'Отправить  по почте',
             'icon' => 'envelope',
            'url' => Url::to(['/email/send','docId'=>$model->id]),
                   'class'=>'btn-link',
             'type' => Html::TYPE_DANGER,
             'size' => Html::SIZE_LARGE
        ]
    ]
]);
?>
<?php else:?>
             <div class="row link-block">
                   <div class="col-md-12">
                   <h3>Все визиты</h3>
<?php if($viewsProvider->getTotalCount()==0):?>
       <?php
echo Html::jumbotron([
    'heading' => '',
    'body' => 'В настоящее время ещё нет визитов по вашим данным.<br>Подожите, или создайте другие ссылки, или отправьте письмо',

]);
?>
<?php else:?>

  <?= GridView::widget([
        'dataProvider' =>$viewsProvider,
           'pjax' => false,
           'bordered' => false,
            'layout' => '{items}{pager}',
            'showPageSummary' => false,
               // 'showCaption' => false,
           'showHeader'=>true,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
         'columns' =>
         [
         	[
    'attribute' => 'client',
      'label'=>'Клиент',
    'format' => 'html',
    'value' => function($data) { return '<div class="link-title">'.$data->link->client_name.'</div>
    <div class="">'.(Icon::show('phone', ['class'=>'fa-1x'])).'&nbsp;&nbsp;'.$data->link->client_phone.'</div>
    <div class="link-created">'.date('d.m.Y h:i',$data->created_at).'</div>';},
],
     	[
    'attribute' => 'geo',
      'label'=>'География',
    'format' => 'html',
    'value' => function($data) { return '<div class="">'.$data->client_country.' '.$data->client_city.'</div>';},
],
/*
[
    'attribute' => '',
    'format' => 'html',
    'value' => function($data) { return "<div>".($data->downloads>0?Icon::show('download', ['class'=>'fa-1x']):'')."&nbsp;&nbsp;".($data->print>0?Icon::show('print', ['class'=>'fa-1x']):'')."";},
],
*/
[
    'attribute' => '',
    'format' => 'raw',
   'value' => function($data) {
    		return Html::button(Yii::t('app', $data->interval.'<br>мин:сек'),['class'=>'btn btn-default  btn-circle btn-xl']);
    		},

],
[
    'attribute' => 'stats',
      'label'=>'Статистика по страницам',
    'format' => 'html',
    'value' => function($data) { return Html::a(Icon::show('bar-chart', ['class'=>'fa-1x']),$data->id,['class'=>'btn btn-xs view-letter-stat']);},
],
  /*
[
    'attribute' => '',
    'format' => 'raw',
                'class' => 'kartik\grid\ExpandRowColumn',
            'value' => function ($model, $key, $index, $column) {

                return GridView::ROW_COLLAPSED;
            },

            'allowBatchToggle'=>true,
           'detail'=>function ($model) {
                return Yii::$app->controller->renderPartial('ajax/_visits_stats', ['model'=>$model]);
            },
            'detailOptions'=>[
                'class'=> 'kv-state-enable',
            ],
   'value' => function($data) {
    		return Html::button(Yii::t('app', '00:56<br>mm:ss'),['class'=>'btn btn-default  btn-circle btn-xl']).'&nbsp;&nbsp;&nbsp;'.
    		Html::button(Yii::t('app', ''.Icon::show('line-chart', ['class'=>'fa-2x']).''),['class'=>'btn btn-info  btn-circle btn-xl', 'data' => [
        'toggle' => 'modal',
        'target' => '#statview',
   ]]);
    		},

],
*/
],


    ]); ?>
    <?php endif;?>
        </div>
     </div>
    <div class="clear"></div>
             <div class="row link-block">
                   <div class="col-md-12">
                   <h3>Все ссылки</h3>
			     <?php foreach ($model->links as $link): ?>
		  <div class="row">
		<div class="col-md-3 link-title">
	   <?php echo Icon::show('link', ['class'=>'fa-1x'])?>
	   <?=$link->client_name?>
	   <div class="link-created">Создана: <?php echo (new DaysAgo())->make(date('d.m.Y',$link->created_at));?></div>
        </div>

        <div class="col-md-4">
	     <?=
            Html::input('txt','',$link->url,['id'=>'copyLink-'.$link->id,'size'=>42,'class'=>'btn   btn-default','readonly'=>'readonly' ]);
            ?>
	    <?=
            Html::button(Icon::show('tags', ['class'=>'fa-1x']).' Копировать',['class'=>'btn  btn-sm btn-copy btn-default','data-clipboard-action'=>'copy','data-clipboard-target'=>'#copyLink-'.$link->id.'']);
            ?>
            <?php if($link->isExpire&&(strtotime($link->expired_date)<time())):?>
            <small class="label label-warning"><i class="fa fa-clock-o"></i> истекшая</small>
               <?php endif;?>
        </div>
        <div class="col-md-2 text-right">
	 <?php
	   echo SwitchInput::widget([
    'name' => 'status_10',
    'class'=>'switch',
    'value'=>$link->isBlocked==1?0:1,
    'inlineLabel' => false,
     'pluginOptions' => [
        'size' => 'medium',
        'onColor' => 'success',
        'offColor' => 'danger',
        'onText'=>'Вкл.',
        'offText'=>'Выкл.'

   ],
  'pluginEvents' => [
  "switchChange.bootstrapSwitch" => "function(event,state) {blockLink('".$link->id."',state) }",
],

]);?>
</div>
        <div class="col-md-1">
         <?=
            Html::button(Yii::t('app', Icon::show('gear', ['class'=>'fa-1x']).'&nbsp;Опции'),['class'=>'btn btn-primary activity-view-link','data'=>['id'=>$link->id,
         'dismiss'=>'modal',
           'toggle' => 'modal',
        'target' => '#editLink',
            ]]);
            ?>
         </div>
            <div class="col-md-2 text-right">
         <?=
            Html::button(Yii::t('app', ' <b>'.$link->views_count.'</b><br>визит(ов)'),['class'=>'btn  btn-md  btn-default']);
            ?>

        </div>
        </div>
       <?php endforeach; ?>
                   </div>

             </div>
<?php endif;?>
