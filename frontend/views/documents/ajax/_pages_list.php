<?php
use kartik\helpers\Html;
use  yii\helpers\Url;
use kartik\grid\GridView;
use kartik\icons\Icon;
Icon::map($this);
use kartik\widgets\SwitchInput;


?>
             <div class="row link-block">
             <p>&nbsp;</p>
             <div style="color:#cacaca"><b><?=$data->name?><u></u></b></div>
             &nbsp;
                      <?php foreach($data->pages as $page):
                      $cnt++;
                      ?>
                          <div class="page-item" data-pageid="<?=$page->id?>" onClick="changePage('<?=$page->id?>','')" style="margin:10px;width:130px;color:#FFF">Страница № <?php echo $cnt;?>
                          <div class="thumbnail thumbnail-blue"><?php echo Html::a(Html::img(Url::to(['/'],true).$data->dir.'/thumbs/'.$page->filename, ['id'=>'page'.$page->id,
                          'width'=>'116','data-imgsrc'=>Url::to(['/'],true).$data->dir.'/'.$page->filename]),'#');?></div>
                          </div>
                      <?php endforeach?>
             </div>
