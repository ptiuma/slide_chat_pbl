<?php
use kartik\helpers\Html;
use  yii\helpers\Url;
use kartik\grid\GridView;
use kartik\icons\Icon;
Icon::map($this);
use kartik\widgets\SwitchInput;



?>
<?php if($visits==0&&count($model->links)==0):?>
<?php
echo Html::jumbotron([
    'heading' => 'Создать ссылку',
    'body' => 'В настоящее время нет активности по этому документу.',
    'buttons' => [
        [
            'label' => 'Создать ссылку',
            'icon' => 'link',
            'url' => '#',
            'options'=>[
            'data' => [
        'toggle' => 'modal',
        'target' => '#createLink',
   ]],
            'type' => Html::TYPE_PRIMARY,
            'size' => Html::SIZE_LARGE
        ],
        [
             'label' => 'Отправить  по почте',
             'icon' => 'envelope',
            'url' => Url::to(['/email/send']),
             'type' => Html::TYPE_DANGER,
             'size' => Html::SIZE_LARGE
        ]
    ]
]);
?>
<?php else:?>
             <div class="row">
                   <div class="col-md-12">
                   <h3>Все ссылки</h3>
                   <?php
$gridColumns = [
    // ['attribute'=>'id','width'=>'80px'],
     	[
    'attribute' => 'Превью',
    'format' => 'html',
    'value' => function($data) { return Icon::show('link', ['class'=>'fa-2x']).'
    &nbsp;&nbsp;
    '.$data->client_name; },
],
     	[
    'attribute' => 'name',
    'format' => 'html',
    'value' => function($data) { return $data->link_name;},
],
     	[
    'attribute' => 'Превью',
    'format' => 'html',
    'value' => function($data) { return '<div class="document-actions-block text-center"><div class="round round-lg hollow green document-views">'.
              \dmstr\widgets\SmallBox::widget([
                    // 'boxBg'=>\dmstr\widgets\InfoBox::TYPE_AQUA,
                    // 'iconBg'=>\dmstr\widgets\InfoBox::TYPE_GREEN,
                     'number'=>100500,
                     'text'=>'Визиты',
                     'icon'=>'fa fa-bolt',
                     'progress'=>66,
                     'progressText'=>'Сегодня: 5'
                 ]).'<em>8</em><p>Визитов</p>
             </div>&nbsp;&nbsp;&nbsp;</div>'; },
],
];



?>
    <?= GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getLinks()]),
      //  'filterModel' => $searchModel,
       'columns' => $gridColumns,
           'pjax' => true,
           'bordered' => false,
           'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,


    ]); ?>
                   </div>

             </div>
                          <div class="row">
                   <div class="col-md-12">
                   <h3>Все ссылки</h3>
			<div class="col-md-3">

			</div>
                   </div>

             </div>
<?php endif;?>