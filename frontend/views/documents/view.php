<?php
use kartik\grid\GridView;
use kartik\dropdown\DropdownX;
use yii\helpers\Html;
use kartik\tabs\TabsX;
use kartik\editable\Editable;
use kartik\icons\Icon;
Icon::map($this);
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
$this->title = $model->name;
$this->params['breadcrumbs'][] = "Документы" ;
$this->params['breadcrumbs'][] = $this->title;
$this->title = false;
$this->params['breadcrumbs']=false;
$this->registerJsFile(
    '@web/frontend/assets/js/docs.js',
    ['depends'=>'frontend\assets\AppAsset']
);

$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/amcharts.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/charts/amcharts/serial.js',['depends' => 'yii\web\JqueryAsset']);
?>
<div class="row">
        <div class="col-md-6  col-md-offset-1">
              <div><h3><?php
         echo Editable::widget([
    'name'=>'file_name',
        'format' => Editable::FORMAT_BUTTON,
    'asPopover' => true,
    'value' =>  $model->name,
    'header' => 'Имя файла',
    'size'=>'md',
      'inlineSettings' => [
      'closeButton'=>'Закрыть',
        'templateBefore' => Editable::INLINE_BEFORE_2,
        'templateAfter' =>  Editable::INLINE_AFTER_2
    ],
  //  'footer'=>'{buttons}{close}',
    'resetButton'=>false,
    'buttonsTemplate'=>'{submit}',
    'submitButton'=>['icon'=>'Применить','label'=>'Применить'],
    'options' => ['class'=>'form-control', 'placeholder'=>'Enter  name...']
]);?></h3></div>
         <?php echo Html::a(Icon::show('eye', ['class'=>'fa-1x']).' Просмотреть документ',['view/preview-doc?id='.$model->id],[
'class'=>'btn btn-xs btn-default','target'=>'_blank'])?>
                        &nbsp;
 <?php echo Html::a(Icon::show('envelope', ['class'=>'fa-1x']).' Написать письмо',['email/send?docId='.$model->id],[
'class'=>'btn btn-xs btn-default'])?>
                     &nbsp;
 <?php echo Html::a(Icon::show('video-camera', ['class'=>'fa-1x']).' Начать презентацию', '#', [
'class'=>'btn btn-xs btn-default create-presentation-link','data'=>[
        'docId'=>$model->id,
    		]])?>
        </div>
        <div class="col-md-5  text-right">
                  <?php echo Html::button(Yii::t('app', Icon::show('link', ['class'=>'fa-1x']).' Создать линк'),[
'class'=>'btn btn-sm  btn-primary create-view-link',
    'data' => [
    'docId'=>$model->id,
    	 'dismiss'=>'modal',
        'toggle' => 'modal',
        'target' => '#createLink',
   ]])?>
&nbsp;
    <?php echo Html::a(Icon::show('download', ['class'=>'fa-1x']).' Скачать документ','download-doc?id='.$model->id,[
'class'=>'btn btn-sm btn-primary'])?>


        </div>
</div>
       <p>&nbsp;</p>

<div class="row">
        <div class="col-md-10 col-md-offset-1">
           <?php   Pjax::begin(['id' => 'docPanel','linkSelector'=>false]);?>
  <?

$items = [
    [
        'label'=>'<i class="fa fa-user-plus"></i> Последние Визиты и Ссылки',
        'content'=>$this->render('ajax/_visits', [
        'model' => $model,
        'viewsProvider'=>$viewsProvider
    ]),
        'active'=>true
    ],
    [
     'id'=>'mapTab',
        'label'=>'<i class="fa fa-pie-chart"></i> Общая статистика по документу',
            'content'=>$this->render('ajax/_charts', [
        'model' => $model,
        'viewsProvider'=>$viewsProvider
    ]),
     //  'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['documents/doc-stats?id='.$model->id])
    ],
  ];

echo TabsX::widget([
    'items'=>$items,
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false,
    'bordered'=>true
]);
       Pjax::end();
?>
        </div>
</div>

<?php

yii\bootstrap\Modal::begin([
    'id' => 'createLink',
    'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
    'header'=>'Создать ссылку',
      'size' => 'modal-md',
  //  'url' => Url::to(['/documents/create-link?docId='.$model->id]), // Ajax view with form to load
      'clientOptions' => ['backdrop'=>true,'keyboard' => FALSE]
]);
?>
<div id="createLinkContent"></div>
<?php
yii\bootstrap\Modal::end();
?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
    'header'=>'Ссылка создана',
    'id' => 'successLink',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
?>
     <div><input type="text" class="form-control" id="copy-input" readonly="readonly" size="32">
     <button class="btn btn-sm btn-copy btn-default" data-clipboard-target="#copy-input" data-clipboard-action="copy" type="button" id="copy-button"     data-toggle="tooltip" data-placement="button"
          title="Копировать">
        Копировать
      </button></div>
    <p>  Отправьте ее любым привычным способом (Email, Skype, WhatsApp и т.п.)
     </br>
Наш сервис уведомит, когда клиент просмотрел документ и Вы можете сразу связаться с ним
</p>

<?php
yii\bootstrap\Modal::end();
?>


<?php
yii\bootstrap\Modal::begin([
    'id' => 'editLink',
     'headerOptions' => ['class'=>'modal-header pres-modal-header'],
    'header'=>'Редактировать опции',
      'size' => 'modal-lg',
      'clientOptions' => ['backdrop'=>true,'keyboard' => FALSE]
]);
?>
<div id="editLinkContent"></div>
<?php
yii\bootstrap\Modal::end();
?>


<?php
Modal::begin([
    'id' => 'docTeam',
         'headerOptions' => ['class'=>'modal-header pres-modal-header'],
    'header'=>'Командные опции документа',
   // 'url' => Url::to(['documents/doc-team?id='.$model->id]),
   // 'ajaxSubmit' => true,
]);
Modal::end();

?>
<?php
yii\bootstrap\Modal::begin([
    'id' => 'letterStat',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Статистика визита',
    'size' => 'modal-lg',

]);
yii\bootstrap\Modal::end();
?>

