<?php
use  yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\popover\PopoverX;
use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Carousel;
/* @var $this yii\web\View */
use kartik\icons\Icon;
Icon::map($this);
$this->title = 'Презентация '.$model->doc->name;
$items=array();
foreach($model->doc->pages as $k=>$page)
{

	$items[]=[
	'content' => Html::img($page->img,['id'=>$page->id,'data-interval' => 'false']),
	'options' => ['interval'=>false,],
	];
}
$runSocket=1;
$this->registerJsFile(Yii::$app->request->baseUrl.'/frontend/assets/js/presentation.js',['depends' => 'yii\web\JqueryAsset']);
?>
   <style>

          #wrapper {
          padding-left:0 !important;
          }
  </style>
<?php if($model->isArchive==1||$model->isBlocked==1||($model->isExpire==1&&(strtotime($model->expire_date)<time()))):
echo $this->render('_blocked');
$runSocket=0;
 ?>


<?php else:?>
     <?php
    NavBar::begin([
        'brandLabel' =>Icon::show('bar-chart-o', ['class'=>'fa-1x']).' '.strtoupper(Yii::$app->keyStorage->get('common.siteName')),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems[] = ['label' =>Icon::show('plus', ['class'=>'zoom-in fa-2x']), 'url' =>'#','linkOptions' => ['id'=>'zoom-in-btn','data-original-title' => 'Zoom In','data-toggle' => 'tooltip','data-placement' => 'bottom']];
    $menuItems[] = ['label' =>Icon::show('minus', ['class'=>'zoom-out fa-2x']), 'url' =>'#','linkOptions' => ['data-original-title' => 'Zoom Out','data-toggle' => 'tooltip','data-placement' => 'bottom']];
    if($model->denyDownloading!=1)$menuItems[] = ['label' =>Icon::show('download', ['class'=>'downloads fa-2x']), 'url' =>Url::to(['view/downloads?key='.$model->link_name]),'linkOptions' => ['data-original-title' => 'Download','data-toggle' => 'tooltip','data-placement' => 'bottom']];
    $menuItems[] = ['label' =>Icon::show('print', ['class'=>'print fa-2x']), 'url' =>'#','linkOptions' => ['data-original-title' => 'Print','data-toggle' => 'tooltip','data-placement' => 'bottom']];


    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
                'encodeLabels' => false
    ]);
    NavBar::end();
    ?>
<div class="call-btn-cont">

          <?php echo Html::button(Yii::t('app', Icon::show('phone', ['class'=>'fa-3x'])),[
'class'=>'btn btn-default blue-circle-button',
    'data' => [
        'toggle' => 'modal',
        'target' => '#phone',
   ]])?>
           &nbsp;&nbsp;
                     <?php echo Html::button(Yii::t('app', Icon::show('envelope-o', ['class'=>'fa-3x'])),[
'class'=>'btn btn-default blue-circle-button',
    'data' => [
        'toggle' => 'modal',
        'target' => '#message',
   ]])?>
   <?php
     PopoverX::begin([
    'placement' => PopoverX::ALIGN_BOTTOM_RIGHT,
    'toggleButton' => ['label'=>Yii::t('app', Icon::show('comments-o', ['class'=>'fa-3x'])), 'class'=>'btn btn-default blue-circle-button'],
    'header' => '<i class="glyphicon glyphicon-lock"></i> Telegram - Chat',
    'size'=>'md',
    'footer'=>false
]);
     ?>
 <?= \poprigun\chat\widgets\StaticChat::widget([
    'template'  =>  'path',
    'node' => true,
    'options' => [
        'dialogTime' => 0,
        'messageTime' => 0,
        'form' => '#poprigun-chat-send-form',
    ],
]);?>
     <?
PopoverX::end();

   ?>
</div>
        <p>&nbsp;</p>
<div  class="row main_content">
    <div class="container">
        <div class="col-md-112">
            <?php echo Carousel::widget(
            [
            'id'=>"viewer",
           // 'pause'=>true,

            'controls' => [
            		'<span class="glyphicon glyphicon-chevron-left"></span>',
            		 '<span class="glyphicon glyphicon-chevron-right"></span>'
             ],
             'showIndicators'=>false,
            'items' => $items,
            'options' => [
       'data'=>['keyboard'=>'false','interval'=>'false','wrap'=>'false',],

    ]
            ]); ?>
       </div>
   </div>
</div>
<script src="//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script>
    var RunPresentation='<?=$runSocket;?>';
    var SOCKET_SERVER='<?=Yii::$app->params['socketServer']?>';
    var presentation_config={
    linkid : '<?=$model->id;?>',
    viewid : '<?=$view->id;?>',
    'skey' : '<?=Yii::$app->session->id;?>',
    'pageid': '',
    };
</script>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader','class'=>'modal-header pres-modal-header'],
    'header' => '<h4>Связь по телефону</h4>',
    'id' => 'phone',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['keyboard' => FALSE]
]);
?>

     <div class="col-md-12">
     <div>Введите имя и номер телефона, чтобы наш менеджер оперативно связался с Вами</div>
 <?php $form = ActiveForm::begin([
    'method' => 'post',
    'id'=>'phoneform'
]); ?>

<?= $form->field($phone_model, 'phone')->textinput(['prompt'=>'+7xxx xxx xx','size' => 32])->label(false) ?>

<div class="form-group">
    <?= Html::submitButton('ЖДУ ЗВОНКА', ['class' => 'btn btn-primary','style'=>'width:100%']) ?>
</div>
<?php ActiveForm::end(); ?>
</div>
<p>&nbsp;</p>
<?php
yii\bootstrap\Modal::end();
?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader','class'=>'modal-header pres-modal-header'],
    'header' => '<h4>Отправить сообщение</h4>',
    'id' => 'message',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['keyboard' => FALSE]
]);
?>

     <div class="col-md-12">
     <div>Напишите вопрос нашему менеджеру, мы сделаем всё, чтобы Вы быстро получили ответ!</div>
 <?php $form = ActiveForm::begin([
    'method' => 'post',
    'id'=>'messageform'
]); ?>

<?= $form->field($message_model, 'email')->textinput(['prompt'=>'+7xxx xxx xx','size' => 32])->label('Контакты') ?>
<?= $form->field($message_model, 'message')->textarea(['prompt'=>'+7xxx xxx xx','rows' => 6])->label('Сообщение') ?>
<div class="form-group">
    <?= Html::submitButton('ОТПРАВИТЬ', ['class' => 'btn btn-primary','style'=>'width:100%']) ?>
</div>
<?php ActiveForm::end(); ?>
</div>
<p>&nbsp;</p>
<?php
yii\bootstrap\Modal::end();
?>
<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader','class'=>'modal-header pres-modal-header'],
    'header' => '<h4>Спасибо</h4>',
    'id' => 'successmessage',
    'size' => 'modal-md',
    'clientOptions' => ['keyboard' => FALSE]
]);
?>

     <div class="col-md-12">
     <div><b>Спасибо за обращение! Мы скоро свяжемся с Вами!</b></div>
 </div>
<p>&nbsp;</p>
<?php
yii\bootstrap\Modal::end();
?>
<?php endif;?>