<?php
use  yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


?>
   <style>

          #wrapper {
          padding-left:0 !important;
          }
  </style>
<div  class="row">
    <div class="container">
        <div class="col-md-4 col-md-offset-4" style="background:#FFF;padding:40px;margin-top:20%;border:5px solid #cacaca">
                        <p>
                         <?php $form = ActiveForm::begin([
    'method' => 'post',
    'id'=>'protectform'
]); ?>

<?= $form->field($protect_model, 'protect_code')->textinput(['size' => 16])->label('Введите защитный код') ?>
<div class="form-group">
    <?= Html::submitButton('ОТПРАВИТЬ', ['class' => 'btn btn-primary','style'=>'width:100%']) ?>
</div>
<?php ActiveForm::end(); ?>
       </div>
   </div>
</div>

