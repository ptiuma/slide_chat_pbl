<?php
use  yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


?>
   <style>

          #wrapper {
          padding-left:0 !important;
          }
  </style>
<div  class="row">
    <div class="container">
        <div class="col-md-5 col-md-offset-3" style="background:#FFF;padding:40px;margin-top:20%;border:5px solid #cacaca">
                        <p>
                         <?php $form = ActiveForm::begin([
    'method' => 'post',
]); ?>

<?= $form->field($visitor_info_model, 'name')->textinput(['size' => 16])->label('Введите Имя') ?>
<?= $form->field($visitor_info_model, 'email')->textinput(['size' => 16])->label('Введите Email') ?>
<?= $form->field($visitor_info_model, 'phone')->textinput(['size' => 16])->label('Введите Телефон') ?>
<div class="form-group">
    <?= Html::submitButton('ОТПРАВИТЬ', ['class' => 'btn btn-primary','style'=>'width:100%']) ?>
</div>
<small>Вся информация конфиденциальна и будет доступна только обладателю документа</small>
<?php ActiveForm::end(); ?>
       </div>
   </div>
</div>

