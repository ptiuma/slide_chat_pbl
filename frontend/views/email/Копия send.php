<?php
use yii\helpers\Html;
use  yii\helpers\Url;

use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use frontend\models\UserTemplates;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use kartik\widgets\FileInput;
use kartik\icons\Icon;
Icon::map($this);
use kartik\growl\GrowlAsset;
use kartik\base\AnimateAsset;
GrowlAsset::register($this);
AnimateAsset::register($this);

$templates=ArrayHelper::map(UserTemplates::find()->where(['userId'=> Yii::$app->user->id])->orderBy('id')->all(), 'id', 'name');
$this->title = '';
$this->params['breadcrumbs'] = null;

$this->registerJsFile(
    '@web/frontend/assets/js/email.js',
    ['depends'=>'frontend\assets\AppAsset']
);

?>

<div class="row email-form">
       <div class="col-md-10 col-md-offset-1">
<?php
    $this->registerJs(
        '$("document").ready(function(){
            $("#new_letter").on("pjax:end", function() {
            console.log("dd");  //Reload GridView
        });
    });'
    );
?>

<?php
 Pjax::begin(['id' => 'new_letter']);
   if($model->id&&$isSend==1)
    {
    	$this->registerJs("
            $('#success').modal('show')
            ");
      }
   if($model->id)
    {
    	foreach($model->links as $link)
    	{
	    	$this->registerJs("
	            	insertAttachment('".$link->docId."')
	            ");
        }
      }
  if(!$model->id)
  {
  	$model->email_copy=Yii::$app->user->identity->settings->email_copy;
  		$model->email_body=Yii::$app->user->identity->profile->bio;
  }
  $form = ActiveForm::begin(['id'=>'emailForm','type'=>ActiveForm::TYPE_HORIZONTAL,'options' => ['linkSelector'=>false,'data-pjax' => true]]);
echo FormGrid::widget([
    'model'=>$model,
    'form'=>$form,
    'autoGenerateColumns'=>true,
    'rows'=>[
        [
       // 'contentBefore'=>'<legend class="text-info"><small>Сообщение</small></legend>',
            'attributes' => [
                                'email_to'=>['type'=>Form::INPUT_TEXT,'options'=>[]],
                                  'client_name'=>['type'=>Form::INPUT_TEXT,'options'=>['placeholder'=>'Иванов Иван']],

            ],
        ],
        [
            'attributes' => [
                                'email_copy'=>['type'=>Form::INPUT_TEXT,'options'=>[]],
                                   'client_phone'=>['type'=>Form::INPUT_TEXT,'options'=>['placeholder'=>'+79XX XXX XX XX']],
            ],
        ],
        [

          'contentAfter'=>'<div id=\'tplBtns\' class=\'text-right\' style=\'display:none\'>'.
          Html::button(Icon::show('trash', ['class'=>'btn fa-1x']),['class'=>'btn-link delete-template','data-original-title' => 'Удалить шаблон','data-toggle' => 'tooltip','data-placement' => 'bottom']).'</div>',
          'attributes' => [
        'templateId'=>['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>$templates,'options'=>['prompt'=>'Без шаблона']],

            ],
        ],
        [
            'attributes' => [
                                'email_subject'=>['type'=>Form::INPUT_TEXT],
            ],
        ],
        [
          'contentAfter'=>'<div id=\'attaches\' class=\'text-center\' style=\'margin-left:40%\'></div><div class="row" style=\'margin:20px\'><div class=\'col-md-4 col-md-offset-3\'>
          '.Html::button(Yii::t('app', 'Прикрепить файл из облака'),[
			'class'=>'btn btn-default select-cloud',
   			'data' => [
   			    'isTempl'=>'0',
        		'toggle' => 'modal',
        		'target' => '#cloud',
   ]]).'&nbsp;&nbsp;</div><div class="col-md-4">'.FileInput::widget([
    'name' => 'doc',
    'id'=>'doc',
     'pluginOptions' => [
         'language' => 'ru',
        'browseLabel'=>'Загрузить новый файл',
         'showPreview' => false,
       'showCaption' => false,
        'showRemove' => false,
        'elCaptionText' => '#customCaption',
         'uploadUrl' => Url::to(['/documents/file-upload-send']),
    ] ,
    'pluginEvents' => [

]
]).'</div></div>',
     'attributes' => [
                'attachments' => ['type'=>Form::INPUT_HIDDEN,
                'label'=>''
                ]
            ],

        ],
       [
        'contentAfter'=>'<div class=\'text-right\'>'.Html::button(Yii::t('app', Icon::show('save', ['class'=>'fa-1x']).' Сохранить шаблон'),[
'class'=>'btn-link create-template',
    'data' => [
    'id'=>'-1',
       'toggle' => 'modal',
        'target' => '#createTemplate',
   ]]).'</div>',
         'attributes' => [
                'email_body' => [
                  'type'=>Form::INPUT_WIDGET,
            'label'=>Html::label('Сообщение'),
            'widgetClass'=>'vova07\imperavi\Widget',
            'options'=>[
                'settings'=>[
                	'minHeight' => 280,
                ]
            ]
            ],
            ],
        ],


     [
         'attributes' => [
                'sheduller_use'=>[
                 'type'=>Form::INPUT_WIDGET,
            'widgetClass'=>'dosamigos\switchinput\SwitchBox',
                'label'=>'Запланировать отправку',
                'options'=>[
     'options' => [
        'label' => false
    ],
             'clientOptions' => [
        'size' => 'small',
        'onColor' => 'warning',
           ]
                ]],
            ],
        ],
     [
      'contentBefore'=>'<div id="sheduller" class="hidden">',
      'contentAfter'=>'</div>',
         'attributes' => [
                'sheduller'=>['type'=>Form::INPUT_WIDGET,
          'label'=>'',
                  'widgetClass'=>'\kartik\widgets\DateTimePicker',
                     'hint'=>'Время московское',
                'options'=>[
                   'options'=>['placeholder'=>'Время отправки ..', 'class'=>'col-md-9'],
                   'class'=>'col-md-9 hidden'
                ]],
            ],
        ],
                [
            'attributes'=>[
                'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW,
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .
                           Html::submitButton('Отправить', ['class'=>'btn btn-primary']) .
                        '</div>'
                ],
            ],
        ],
    ]
]);
ActiveForm::end();
    Pjax::end();

?>

    </div>
</div>
<div class="clear"></div>
<?php

yii\bootstrap\Modal::begin([
    'id' => 'createTemplate',
      'header' => 'Создание шаблона',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
      'size' => 'modal-lg',
    'clientOptions' => ['backdrop'=>true,'keyboard' => FALSE]
]);
yii\bootstrap\Modal::end();
?>

<?php

Modal::begin([
    'id' => 'cloud',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Прикрепить файл из облака',
    'size' => 'modal-lg',
 //   'url' => Url::to(['/email/select']), // Ajax view with form to load
 //   'ajaxSubmit' => true,
]);
Modal::end();
?>
<?php

Modal::begin([
    'id' => 'success',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Письмо отправлено',
    'size' => 'modal-md',
]);
?>
  Письмо успешно отправлено.<br>
  Можете сохранить его как шаблон, или отправить новому адресату <br>
 <?=Html::a( Icon::show('envelope', ['class'=>'fa-1x']).' Создать новое письмо',"send",['class'=>'btn btn-default ',])?>
<?
Modal::end();
?>
<?php

Modal::begin([
    'id' => 'successRegister',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Благодарим за регистрацию',
    'size' => 'modal-md',
     'clientOptions' => ['backdrop'=>true,'header'=>false,'keyboard' => FALSE]
]);
?>
<?
Modal::end();
if($_GET['register']=='1')
	$this->registerJs("$('#successRegister').modal('show')");
?>