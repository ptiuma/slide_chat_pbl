<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;
use kartik\icons\Icon;
use yii\widgets\Pjax;
Icon::map($this);
?>
<h4>Мои документы</h1>
 <?php Pjax::begin([
'id' => 'docList',
]); ?>


 <div class="row">
	    <?= \yii\widgets\ListView::widget([
  'dataProvider' => $dataProvider,
  'id'=>'docListGrid',
  'itemOptions' => ['class' => 'item'],
  'summary'=>'',
  'itemView' => function ($data) use ($isTempl) {

      return '<div class="col-md-3" style="margin-bottom:40px"  onclick="insertAttachment('.$data->id.','.(int)$isTempl.');$(\'#cloud\').modal(\'hide\')">
      <div class="thumbnail thumbnail-blue docthumbnail" >'.Html::img('@web/'.$data->preview, ['width'=>'190','height'=>'200px','class'=>'add-attach','data-id'=>$data->id]).'</div>
      '.$data->name.'</div>';
    },
]) ?>

          </div>
<?php Pjax::end(); ?>