<?php
use yii\helpers\Html;
use  yii\helpers\Url;
use dosamigos\switchinput\SwitchBox;
use \vova07\imperavi\Widget;
use dosamigos\tinymce\TinyMce;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use frontend\models\UserTemplates;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use kartik\widgets\FileInput;
use kartik\icons\Icon;
Icon::map($this);
use kartik\growl\GrowlAsset;
use kartik\base\AnimateAsset;
GrowlAsset::register($this);
AnimateAsset::register($this);
use yii\web\JsExpression;

use common\models\Letters;

$templates=ArrayHelper::map(UserTemplates::find()->where(['userId'=> Yii::$app->user->id])->orderBy('id')->all(), 'id', 'name');
$this->title = '';
$this->params['breadcrumbs'] = null;

$this->registerJsFile(
    '@web/frontend/assets/js/email.js',  ['depends' => [yii\web\JqueryAsset::className()]]);
?>

<div class="row email-form" style="margin-left:20px;margin-right:20px;">
       <div class="col-md-12 col-lg-12 col-centered text-center2 bordered2">


<?php
 Pjax::begin(['id' => 'new_letter']);
   if($model->id&&$isSend==1)
    {
    	$this->registerJs("
         //   $('#success').modal('show')
            ");
      }
   if($model->id)
    {
    	foreach($model->links as $link)
    	{
	    	$this->registerJs("
	            	insertAttachment('".$link->docId."')
	            ");
        }
      }
    elseif($doc->id)
    {
	    	$this->registerJs("
	            	insertAttachment('".$doc->id."')
	            ");
      }
  if(!$model->id)
  {
  	$model->email_copy=Yii::$app->user->identity->settings->email_copy;
  		$model->email_body=Yii::$app->user->identity->profile->bio;
  }
  $form = ActiveForm::begin(['id'=>'emailForm','type'=>ActiveForm::TYPE_HORIZONTAL,'options' => ['linkSelector'=>false,'data-pjax' => false]]);
  ?>

<div class="row">
<div class="col-md-12">
  <div class="row">
   <?= Html::activeLabel($model, 'templateId', [
        'label'=>'Шаблон',
        'class'=>'col-md-1 control-label'
    ]); ?>

     <div class="col-md-9">
        <?= $form->field($model, 'templateId',[
            'showLabels'=>false,
        ])->dropDownList($templates,['prompt'=>'Без шаблона','class'=>'text-left']); ?>
        </div>
    <div class="col-md-2" style="padding:0;padding-top:0px;margin:0 !important;">
     <?=Html::button(Icon::show('plus', ['class'=>'btn fa-2x']),['id'=>'tplC','class'=>'btn-link create-template','data-original-title' => 'Создать шаблон',
     'data' => [
    'id'=>'-1',
       'toggle' => 'modal',
        'target' => '#createTemplate',
   ]])?>
   <?=Html::button(Icon::show('trash', ['class'=>'btn fa-2x']),['id'=>'tplR','class'=>'collapse btn-link  delete-template','data-original-title' => 'Удалить шаблон','data-toggle' => 'tooltip','data-placement' => 'bottom'])?>
      </div>
        </div>
      </div>

</div>

<div class="row ">
     <div class="col-md-6">
        <?= $form->field($model, 'email_to')->label('*Кому')->textInput(['placeholder' =>'Введите email']) ?>
      </div>

      <div class="col-md-6">
         <?= $form->field($model, 'client_name')->label('Клиент')->textInput(['placeholder' =>'Иван Петров']) ?>
      </div>
</div>
<div class="row">
      <div class="col-md-6">
        <?= $form->field($model, 'email_copy') ?>
      </div>
         <div class="col-md-6">
         <?= $form->field($model, 'client_company')->label('')->textInput(['placeholder' =>'ООО "Дружная компания"']) ?>
      </div>

</div>
<div class="row">
      <div class="col-md-6">
        <?= $form->field($model, 'email_subject')->label('*Тема')->textInput(['placeholder' =>'Коммерческое предложение']) ?>
      </div>
       <div class="col-md-6">
        <?= $form->field($model, 'client_phone')->label('Тел')->textInput(['placeholder' =>'+79XX XXX XX XX']) ?>
      </div>
</div>
<div class="row">
 <div class="col-md-12">
    <?= $form->field($model, 'attachments')->hiddenInput()->label(''); ?>
 <div id='attaches' class='text-center' style='margin-left:5%'></div>
</div>
</div>
<div class="row">
 <div class="col-md-5 col-lg-3">
        <?=Html::button(Yii::t('app', 'Прикрепить файл из облака'),[
			'class'=>'btn btn-warning select-cloud btn-main',
   			'data' => [
   			    'isTempl'=>'0',
        		'toggle' => 'modal',
        		'target' => '#cloud',
   ]])?>
 </div>
      <div class="col-md-5 col-lg-3">
        <? echo FileInput::widget(['name'=>'doc',
        'id'=>'doc',
        'language' => 'ru',

	    'options' => ['multiple' => false],
		    'pluginOptions' => [ 'showPreview' => false,
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => false,
        'autoUpload'=>true,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Загрузить с диска',
     //   'showUpload'=>false,
        'showCancel'=>false,
        'showClose'=>false,
         'uploadUrl' => Url::to(['/documents/file-upload-send']),
    ]
		]);?>
      </div>
</div>
<div class="row">
        <div class="col-md-12 text-right">
      <?=Html::button(Yii::t('app', Icon::show('save', ['class'=>'fa-1x']).' Сохранить шаблон'),[
'class'=>'btn-link create-template',
    'data' => [
    'id'=>'-1',
       'toggle' => 'modal',
        'target' => '#createTemplate',
   ]])?>
      </div>
</div>
<div class="row">
      <div class="col-md-12">
<?= $form->field($model, 'email_body',['showLabels'=>false])->widget(TinyMce::className(), [

    'options' => ['rows' => 6],
    'language' => 'ru',
    'clientOptions' => [
    'setup'=>new JsExpression('function(editor){
            console.log(\'rf\')
            editor.on(\'focus\', function(e) {
             $("#letters-email_body_ifr").css("height","350px");
                console.log("focus");
            });
             editor.on(\'blur\', function(e) {
             $("#letters-email_body_ifr").css("height","130px");
                console.log("focus");
            });
     }'),
       'menubar'=>'false',
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ]
])->label(false);?>

      </div>
      </div>
<div class="row">
      <div class="col-md-12 text-left">
      <?php
     echo $form->field($model, 'askLinkInfo')->widget(CheckboxX::classname(), [
    'autoLabel'=>true,
     'pluginOptions'=>['threeState'=>false],
])->label(false);
?>
      </div>
      </div>
<div class="row">
      <div class="col-md-6 text-left">
        <?= Html::activeLabel($model, 'alert_opened_in_moment', [
        'class'=>'control-label col-md-6 text-right'
    ]) ?>
     <div class="col-md-6 text-left">
     <?= $form->field($model, 'alert_opened_in_moment',[
            'showLabels'=>false,

        ])->dropDownList(Letters::$alert_opened_in_moment_arr,['class'=>'text-left']); ?>
      </div>
    </div>
      <div class="col-md-6 text-left">
      <?= Html::activeLabel($model, 'alert_after_days', [
        'class'=>'control-label  col-md-7 text-right'
    ]) ?>
     <div class="col-md-5 text-left">
       <?= $form->field($model, 'alert_after_days',[
            'showLabels'=>false,
        ])->dropDownList(Letters::$alert_after_days_arr,['class'=>'text-left']); ?>
      </div> </div>
      </div>
<div class="row text-center">
   <div class="col-md-6 text-right">
     <div class="col-md-12 col-lg-7 pull-right">
             <?php
            echo \kartik\widgets\DateTimePicker::widget([
	'model' => $model,
	'attribute' => 'sheduller',

	'options' => [  'hint'=>'','placeholder' => 'Запланировать отправку ...'],
	'language' => 'ru',
	'pluginOptions' => [
	'pickerPosition' => 'top-right',

		'autoclose' => true
	]
]);
?>      </div>
      </div>
      <div class="col-md-6 text-left">
        <div class="form-group">
            <?= Html::submitButton('Отправить письмо', ['class' => 'btn btn-primary btn-main']) ?>
        </div>
  </div>
      </div>

<?
ActiveForm::end();
  Pjax::end();
?>
<div class="clear"></div>
    </div>
</div>
<div class="clear"></div>
<?php

yii\bootstrap\Modal::begin([
    'id' => 'createTemplate',
      'header' => 'Создание шаблона',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
      'size' => 'modal-lg',
    'clientOptions' => ['backdrop'=>true,'keyboard' => FALSE]
]);
yii\bootstrap\Modal::end();
?>

<?php

Modal::begin([
    'id' => 'cloud',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Прикрепить файл из облака',
    'size' => 'modal-lg',
 //   'url' => Url::to(['/email/select']), // Ajax view with form to load
 //   'ajaxSubmit' => true,
]);
Modal::end();
?>
<?php

Modal::begin([
    'id' => 'success',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Письмо отправлено',
    'size' => 'modal-lg',
]);
?>
  Письмо успешно отправлено.<br>
  Можете сохранить его как шаблон, или отправить новому адресату <br>
  <?=Html::button('Сохранить как шаблон',['class'=>'btn-link create-template', 'data' => ['id'=>'-1','toggle' => 'modal','target' => '#createTemplate']])?>
 &nbsp;<?=Html::button( Icon::show('envelope', ['class'=>'fa-1x']).' Отправить другому клиенту',['class'=>'btn btn-primary ','onClick'=>'$("#email_to").val("");$("#success").modal("hide")'])?>
 &nbsp;&nbsp;<?=Html::a( Icon::show('envelope', ['class'=>'fa-1x']).' Создать новое письмо',"send",['class'=>'btn btn-default ',])?>
<?
Modal::end();
?>
<?php

Modal::begin([
    'id' => 'successRegister',
        'headerOptions' => ['class' => 'modalHeader pres-modal-header'],
              'header' => 'Благодарим за регистрацию',
    'size' => 'modal-md',
     'clientOptions' => ['backdrop'=>true,'header'=>false,'keyboard' => FALSE]
]);
?>
<?
Modal::end();
if($_GET['register']=='1')
	$this->registerJs("$('#successRegister').modal('show')");
?>