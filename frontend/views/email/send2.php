<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Letters */
/* @var $form ActiveForm */
?>
<div class="send2">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'email_to') ?>
        <?= $form->field($model, 'email_subject') ?>
        <?= $form->field($model, 'email_body') ?>
        <?= $form->field($model, 'client_name') ?>
        <?= $form->field($model, 'userId') ?>
        <?= $form->field($model, 'key') ?>
        <?= $form->field($model, 'type') ?>
        <?= $form->field($model, 'isArchive') ?>
        <?= $form->field($model, 'sheduller_use') ?>
        <?= $form->field($model, 'email_copy') ?>
        <?= $form->field($model, 'sheduller') ?>
        <?= $form->field($model, 'client_phone') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- send2 -->
