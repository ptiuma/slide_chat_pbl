<?php
use yii\helpers\Html;
use  yii\helpers\Url;

use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use frontend\models\UserTemplates;
use karnbrockgmbh\modal\Modal;

use kartik\icons\Icon;
Icon::map($this);
$this->title = 'Создать шаблон';
$this->params['breadcrumbs'][] = $this->title;



?>
<script src="//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU" type="text/javascript"></script>

<script>
ymaps.ready(init);
function init() {
    var geolocation = ymaps.geolocation;
	if (geolocation) {
		alert(geolocation.country + ', ' + geolocation.region + ', ' + geolocation.city);
	} else {
		console.log('Не удалось установить местоположение');
	}
}

</script>
