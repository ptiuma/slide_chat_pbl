<?php
use yii\helpers\Html;
use  yii\helpers\Url;

use yii\helpers\ArrayHelper;

?>

<div class="col-md-3" id="attach<?=$isTempl==1?'T':''?>_<?=$model->id?>" style="background:#FFF;" onClick="removeAttachment('<?=$model->id?>','<?=$isTempl?>')">
        <div class="thumbnail thumbnail-blue docthumbnail" >
             <?=Html::img('@web/'.$model->preview, ['width'=>'200','style'=>"border:1 px solid #fff"])?>
       </div>
        <div class="text-left"><?=$model->name?></div>
</div>
