<?php
  use kartik\icons\Icon;
Icon::map($this);
?>
<div class="row">
    <div class="col-md-7 left-part">
        <div>
            <h2>Здравствуйте!</h2>

            <p>Рады знакомству, благодарим вас за регистрацию!</p>
            <h2>C чего начать?</h2>

            <p>Отправьте пробное письмо и получите уведомления.</p>

            <p>
                Мы уже подготовили ваше первое письмо и предлагаем<br>
                отправить его на почту.
            </p>
        </div>
        <div  onclick=" $('#successRegister').modal('hide');" class="btn btn-danger">
            Я готов(-а)
        </div>
    </div>
    <div class="col-md-5 right-part">
        <h2>
             <?=Yii::$app->keyStorage->get('common.demoPeriod')?> дней
        </h2>
        <h3>
            без оплаты
        </h3>
        <h5>
            все возможности
        </h5>
        <ul>
            <li>
                <div>
                    <?=Icon::show('upload', ['class'=>'fa-1x'])?>
                    <span>
                        Загрузка и отправка документов
                        без ограничений*.
                    </span>
                </div>
            </li>
            <li>
                <div>
                   <?=Icon::show('envelope', ['class'=>'fa-1x'])?>
                    <span>
                        Email и sms уведомления
                        по каждому письму.
                    </span>
                </div>
            </li>
            <li>
                <div>
                  <?=Icon::show('pie-chart', ['class'=>'fa-1x'])?>
                    <span>
                        Статистика и аналитика
                        отправленных сообщений.
                    </span>
                </div>
            </li>
        </ul>
        <small>
            *ВАЖНО! <?=Yii::$app->keyStorage->get('common.siteName')?> предназначен для адресной
            отправки коммерческих предложений по запросу или
            предварительному согласию получателя. Не
            допускается использование сервиса для массовой
            рассылки писем.
        </small>
    </div>
</div>

<div class="clear"></div>