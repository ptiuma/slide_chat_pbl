<?php

/* @var $this yii\web\View */

$this->title ='Slide.Chat - система управления и контроля взаимодействия с клиентами::';
        $asset=frontend\assets\AppAssetLanding::register($this);
       $baseUrl=$asset->baseUrl;
?>
<section style="outline-offset: -3px;" class="intro-section intro-section-7">
	<div src="<?=$baseUrl?>/images/slidechat-pro1.png" data-bg-align="center-center-cover" style="outline-offset: -3px; background-image: url(&quot;<?=$baseUrl?>/images/bg9.jpg&quot;); background-color: rgb(139, 206, 123);" class="info-wrp bg-tc bg-cover" data-bg="<?=$baseUrl?>/images/bg9.jpg">
		<div class="content">
			<img src="<?=$baseUrl?>/images/slidechat-logo.png" class="logo" alt="Slide.chat">
			<h2 class="main-text">Полный контроль над рассылаемыми документами.</h2>
			<p class="sub-text">Лучшие Возможности Для Коммуникации С Вашими Клиентами</p>
			<a href="<?=Yii::getAlias('@web')?>/user/register" class="btn btn-default btn1">Демо доступ</a>
		</div><!-- /.container -->
	</div>
	<img src="<?=$baseUrl?>/images/slidechat-pro1.png" class="intro-img img-responsive" alt="Overview">
</section><!-- /.intro-section -->
<!-- Features -->
<section id="aboutus" class="feature-section feature-section-7">
	<div class="container-fluid">
		<div style="" class="row eqh fs-equalize-element">
			<div style="background-image: url(&quot;<?=$baseUrl?>/images/bg-img1.jpg&quot;); height: 1062px;" class="col-md-6 l bg-section bg-cover col-md-push-6" data-bg="<?=$baseUrl?>/images/bg-img1.jpg"></div>
			<div style="height: 1062px;" class="col-md-6 r col-md-pull-6">
				<div class="content vm-item">
					<h6 class="title">Отправка, трекинг и аналитика коммерческих предложений</h6>
					<p class="title-sub">Когда Вы или Ваш коллега отправляет коммерческое предложение (КП),
прайс-лист или презентацию клиенту - часто ли приходится перезванивать
клиенту, чтобы выяснить ознакомился ли он с письмом?
<br>
Если да, то советуем ознакомиться с сервисом Slide.chat
<br></p>

					<div class="panel-group" id="accordion" role="tablist">

						<div class="panel">
							<div class="panel-hd">
								<a style="" role="tab" data-toggle="collapse" data-parent="#accordion" href="#collapse-1" aria-expanded="true">Free доступ
<i class="panel-open fa fa-plus-circle"></i><i class="panel-close fa fa-minus-circle"></i></a>
							</div>
							<div id="collapse-1" class="panel-collapse collapse in" role="tabpanel">
								<div class="panel-body">С нашим free-сервисом* сможете отслеживать:<br>
- было ли доставлено письмо, было ли оно открыто получателем после
доставки, не попало ли оно с папку нежелательных писем "на другом
конце".
<br>
- ^открыл^ ли получатель вложение (КП, презентацию, прайс-лист).
<br>
- сколько времени он провел за изучением каждой страницы вложения.
<br>
-- Если клиент в командировке, можете ему отправить мобильную версию
вашего КП через смс, мессенджеры или социальные сети, используя
ссылку-линк (пример kp.doc.gl)
<br></div>
							</div>
						</div><!-- /.panel -->

						<div class="panel">
							<div class="panel-hd">
								<a style="" role="tab" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse-2" aria-expanded="true">
									Premium доступ
									<i class="panel-open fa fa-plus-circle"></i><i class="panel-close fa fa-minus-circle"></i>
								</a>
							</div>
							<div id="collapse-2" class="panel-collapse collapse in" role="tabpanel">
								<div class="panel-body">
							В premium-сервисе Вы сможете:<br />
- узнать имя непосредственного человека (ЛПР), который открыл сообщение (минуя секретаршу).   <br />
- начать "ненавязчивое" общение в тот момент, когда клиент посвятил свое время изучению Вашего КП. <br />
- провести online-презентацию.    <br />
- общаться с клиентом через Telegram даже если Вы на обеде или у Вас "перекур".          <br />
- настроить чат-бот для начала общения пока клиент "горячий" во время просмотра Вашего КП.   <br />
-- Клиент, нажав кнопку обратного звонка, может инициировать телефонный разговор с Вами.
								</div>
							</div>
						</div><!-- /.panel -->

						<div class="panel">
							<div class="panel-hd">
								<a style="" role="tab" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse-3" aria-expanded="true">
									Интеграция
									<i class="panel-open fa fa-plus-circle"></i><i class="panel-close fa fa-minus-circle"></i>
								</a>
							</div>
							<div id="collapse-3" class="panel-collapse collapse" role="tabpanel">
								<div class="panel-body">
								Всем клиентам доступна наглядная статистика эффективности отправки КП по почте.
Скоро доступна интеграция с более чем 500 сервисами, такими как Zapier, Gmail, Google таблицы, VK, Facebook, Twitter, Linkedin и др.
								</div>
							</div>
						</div><!-- /.panel -->



					</div><!-- /.panel-group -->

				</div><!-- /.content -->
			</div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.feature-section -->
<!-- Price section -->
<section id="price" class="price-section price-section-2">
	<div class="container">

		<h2 class="title">Выберите Free или Premium доступ</h2>
		<p class="title-sub text-left">
		Наша технология позволяет отслеживать действия клиентов при открытии писем и вложений.<br />
		 Определять места, где вы теряете до 80% клиентов, за привлечение которых вы уже заплатили. <br />
		Моментально взаимодействовать с заинтересованным клиентом.<br />
		Мгновенно предоставлять клиенту дополнительные материалы и проводить онлайн презентации. <br />
		Снимать возражения во время просмотра вложения или сразу после, пока клиент "горячий" и ему удобно говорить.

</p>

		<div class="price-table-1">
			<div class="free">
				<h3 class="hd">Free</h3>
				<div class="price"></div>
				<ul>
					<li>отправка писем</li>
					<li>контроль открытия письма</li>
					<li>контроль просмотра вложений</li>
					<li>статистика активности клиента</li>
				</ul>
				<a href="#" class="btn btn-default btn-sm">Регистрация</a>
			</div><!-- /.free -->

			<div class="pro1">
				<h3 class="hd">Premium</h3>
				<div class="price"></div>
				<ul>
					<li>доступ ко всем опциям</li>
					<li>контроль писем и ссылок</li>
					<li>мгновенное извещение</li>
					<li>детальная статистика</li>
				</ul>
				<a href="#" class="btn btn-primary btn-sm">Регистрация</a>
			</div><!-- /.pro1 -->
		</div><!-- /.price-table-1 -->

	</div><!-- /.container -->
</section><!-- /.price-section --><!-- Content -->

<!-- Price section -->
<section data-sandboxid="tmp-40" class="price-section price-section-2 dark bg-cover bg-cc" data-bg="images/bg1.jpg"  style="background-image: url('<?=$baseUrl?>/images/bg1.jpg');">
	<div class="container">

		<h2 data-sandboxid="tmp-170" class="title">Премиум доступ</h2>
		<p data-sandboxid="tmp-180" class="title-sub">
			Доступные тарифы для премиум доступа.
		</p>

	<div class="row">
        <div class="col-md-4">
            <div class="panel panel-info" style="background:#FFF;">
                <div class="panel-heading">
                    <h4 class="text-center" style="color:#2245EA">
                        на 1 месяц</h4>
                </div>
                <div class="panel-body text-center">
            <p>&nbsp;</p>

                 <p class="lead" style="background:#FFF;color:#000">
                        <strong>900 руб</strong></p>
                </div>
                <ul class="list-group list-group-flush text-center">
                    <li class="list-group-item"><b>если Вам удобно</b></li>
                </ul>
              <p>&nbsp;</p>
                      <p>&nbsp;</p>
               <div class="panel-footer">
                    <a class="btn btn-lg btn-block btn-info" href="<?=Yii::getAlias('@web')?>/payment/tariff?plan=1">Оплатить</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-danger" style="background:#FFF">
                <div class="panel-heading">
                    <h4 class="text-center" style="color:#2245EA">
                        на 6 месяцев</h4>
                </div>
                <div class="panel-body text-center">
            <p>&nbsp;</p>

                 <p class="lead" style="background:#FFF;color:#000">
                        <strong>4500 руб</strong></p>
                </div>
                <ul class="list-group list-group-flush text-center ">
                    <li class="list-group-item text-info" style="background:#CDD1CE"><b>5 + 1 месяц бесплатно</b></li>
                </ul>
              <p>&nbsp;</p>
                      <p>&nbsp;</p>
               <div class="panel-footer">
                    <a class="btn btn-lg btn-block btn-danger" href="<?=Yii::getAlias('@web')?>/payment/tariff?plan=2">Оплатить</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-info" style="background:#FFF">
                    <div class="panel-heading">
                    <h4 class="text-center" style="color:#2245EA">
                        на 12 месяцев</h4>
                </div>
                <div class="panel-body text-center">
            <p>&nbsp;</p>

                 <p class="lead" style="background:#FFF;color:#000">
                        <strong>8100 руб</strong></p>
                </div>
                <ul class="list-group list-group-flush text-center">
                    <li class="list-group-item"><b>9 + 3 месяца бесплатно</b></li>
                </ul>
              <p>&nbsp;</p>
                      <p>&nbsp;</p>
               <div class="panel-footer">
                    <a class="btn btn-lg btn-block btn-info" href="<?=Yii::getAlias('@web')?>/payment/tariff?plan=3">Оплатить</a>
                </div>
            </div>
        </div>



		<div class="row eqh gt50 fs-equalize-element">

			<div class="col-lg-4 col-sm-6" style="height: 124px;">
				<div class="info-box info-box4">
					<div class="img"><i class="fa fa-television"></i></div>
					<div class="info" style="height: 89px;">
						<h2 class="hd">Защита паролем</h2>
						<p class="sub-txt">Установка окнчательной даты просмотра документов, защита паролем, отключение доступа в любой момент </p>
					</div>
				</div><!-- /.info-box -->
			</div>

			<div class="col-lg-4 col-sm-6" style="height: 124px;">
				<div class="info-box info-box4">
					<div class="img"><i class="fa fa-object-group"></i></div>
					<div class="info" style="height: 89px;">
						<h2 class="hd">Информация о клиенте</h2>
						<p class="sub-txt">Захват доступной информации о клиенте, отслеживание кому был переслан документ</p>
					</div>
				</div><!-- /.info-box -->
			</div>

			<div class="col-lg-4 col-sm-6" style="height: 124px;">
				<div class="info-box info-box4">
					<div class="img"><i class="fa fa-map-o"></i></div>
					<div class="info" style="height: 89px;">
						<h2 class="hd">Мгновенное извещение</h2>
						<p class="sub-txt">Получение в режиме онлайн информации об активности клиента </p>
					</div>
				</div><!-- /.info-box -->
			</div>

			<div class="col-lg-4 col-sm-6" style="height: 124px;">
				<div class="info-box info-box4">
					<div class="img"><i class="fa fa-diamond"></i></div>
					<div class="info" style="height: 89px;">
						<h2 class="hd">SMTP опции</h2>
						<p class="sub-txt">Возможность отправулять вложения со своего собственного аккоунта, где бы он не находился(Gmail, mail.ru и т.д ..)</p>
					</div>
				</div><!-- /.info-box -->
			</div>

			<div class="col-lg-4 col-sm-6" style="height: 124px;">
				<div class="info-box info-box4">
					<div class="img"><i class="fa fa-code"></i></div>
					<div class="info" style="height: 89px;">
						<h2 class="hd">Шаблонизация</h2>
						<p class="sub-txt">Сравнение успешности различных шаблонов и их конверсии </p>
					</div>
				</div><!-- /.info-box -->
			</div>

			<div class="col-lg-4 col-sm-6" style="height: 124px;">
				<div class="info-box info-box4">
					<div class="img"><i class="fa fa-paperclip"></i></div>
					<div class="info" style="height: 89px;">
						<h2 class="hd">Контроль документа</h2>
						<p class="sub-txt">Даже после отсылки документа вы сохраняете полный контроль над активностью клиентов и содержимым документа.</p>
					</div>
				</div><!-- /.info-box -->
			</div>

		</div>


    </div>
	</div><!-- /.container -->
</section><!-- /.price-section -->

<section id="feature" style="outline-offset: -3px;" class="content-section align-c content-section-9">
	<div class="container">
		<h2 class="title">Возможности нашего сервиса</h2>
		<p class="title-sub">Полный спектр возможностей вы можете оценить, получив демо доступ</p>
		<hr class="mr-b-40">
		<div style="" class="row eqh gt40 fs-equalize-element">

			<div style="height: 275px;" class="col-md-3">
				<div class="info-box info-box7">
					<div class="img"><i class="fa fa-television"></i></div>
					<div class="info">
						<h2 class="hd">Целевая Заинтересованность</h2>
						<p class="sub-txt">Знать кто открыл документ и проявил заинтересованность, а кто не вникал в детали</p>
					</div>
				</div>
			</div>

			<div style="height: 275px;" class="col-md-3">
				<div class="info-box info-box7">
					<div class="img"><i class="fa fa-object-group"></i></div>
					<div class="info">
						<h2 class="hd">Статистика</h2>
						<p class="sub-txt">Статистика просмотров, посещений ваших документов, моментальное извещение о посещении или открытии письма</p>
					</div>
				</div>
			</div>

			<div style="height: 275px;" class="col-md-3">
				<div class="info-box info-box7">
					<div class="img"><i class="fa fa-map-o"></i></div>
					<div class="info">
						<h2 class="hd">Командная Работа</h2>
						<p class="sub-txt">Возможность создавать команды и работать группами</p>
					</div>
				</div>
			</div>

			<div style="height: 275px;" class="col-md-3">
				<div class="info-box info-box7">
					<div class="img"><i class="fa fa-diamond"></i></div>
					<div class="info">
						<h2 class="hd">Интеграция</h2>
						<p class="sub-txt">Интеграция с Telegram мессенджером и Gmail</p>
					</div>
				</div>
			</div>



		</div>
	</div><!-- /.container -->
</section><!-- /.content-section --><!-- Counter section -->
<section class="counter-section counter-section-2">
	<div class="container z1">
		<div class="rw eq5 gt30 dark">

			<div class="cl">
				<div class="count-box count-box2">
					<div class="icon"><i class="pe-7s-like2"></i></div>
					<div class="count-wrp"><b class="count">76</b>%</div>
					<h3>Качество</h3>
				</div><!-- /.count-box -->
			</div>

			<div class="cl">
				<div class="count-box count-box2">
					<div class="icon"><i class="pe-7s-diamond"></i></div>
					<div class="count-wrp"><b class="count">5</b></div>
					<h3>Опыт и квалификация</h3>
				</div><!-- /.count-box -->
			</div>

			<div class="cl">
				<div class="count-box count-box2">
					<div class="icon"><i class="pe-7s-clock"></i></div>
					<div class="count-wrp"><b class="count">24</b>/7</div>
					<h3>Поддержка клиентов</h3>
				</div><!-- /.count-box -->
			</div>

			<div class="cl">
				<div class="count-box count-box2">
					<div class="icon"><i class="pe-7s-user"></i></div>
					<div class="count-wrp"><b class="count">400</b>+</div>
					<h3>Участников</h3>
				</div><!-- /.count-box -->
			</div>

			<div class="cl">
				<div class="count-box count-box2">
					<div class="icon"><i class="pe-7s-id"></i></div>
					<div class="count-wrp"><b class="count">25</b>+</div>
					<h3>Команд</h3>
				</div><!-- /.count-box -->
			</div>

		</div><!-- /.row -->
	</div><!-- /.container -->
	<div style="background-image: url(&quot;<?=$baseUrl?>/images/bg11.jpg&quot;); outline-offset: -3px;" class="full-wh bg-cover bg-cc" data-bg="<?=$baseUrl?>/images/bg11.jpg"><b class="full-wh overlay"></b></div>
</section><!-- /.counter-section --><!-- / Contact section -->
<section id="contact" style="outline-offset: -3px;" class="contact-section contact-section-2">
	<div class="container">
		<div class="row">
			<div class="col-md-5 l">
				<!-- Popup contact form - 1 -->
				<div id="contact-form" class="form-block">
					<h2 class="title small">Напишите нам</h2>

					<form novalidate="novalidate" action="<?=Yii::getAlias('@web')?>/site/form-data" class="form-widget">
						<input name="to" value="info@slide.chat" type="hidden">
					<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
						<input aria-required="true" class="form-control" data-label="Name" required="" data-msg="Введите Имя" name="name" placeholder="Имя" type="text">
						<input aria-required="true" class="form-control" data-label="Email" required="" data-msg="Введите Email" name="email" placeholder="Email" type="email">
						<textarea aria-required="true" class="form-control" data-label="Message" required="" data-msg="Введите Сообщение" name="message" placeholder="Сообщение" cols="30" rows="10"></textarea>
						<button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Отправить</button>
					</form><!-- / form -->
					<div class="msg-block"></div>
				</div><!-- /#popup-contact-form -->

			</div><!-- /.col-md-6 -->
			<div style="" class="col-md-6 col-md-offset-1 r"><h2 class="title">Контакты</h2>
<h3 class="hd">Advantex Inter LP</h3>
<p class="sub-txt">
<b>Юридический адрес</b><br />
Suite 1, 44 Main street, Douglas, ML11 0 QW, Scotland<br />
<b>Фактический адрес</b><br />
Россия, Москва, Очаковское шоссе, корпус 1, к.55<br />
Reg. number SL16564, registered on 25 April, 2014<br /><br />
<b>Телефон:</b> 84992845011<br />
<b>Email:</b> info@slide.chat<br />
</p>
<!-- Call us -->

<!-- Email us -->
<!-- /.content --></div><!-- /.col-md-6 -->
		</div><!-- / row -->
	</div><!-- / container-fluid -->
</section><!-- /contact-section -->