<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */

$this->title ='Slide.Chat - '.$model->title;
        $asset=frontend\assets\AppAssetLanding::register($this);
       $baseUrl=$asset->baseUrl;
?>
<!-- Content -->

<section class="content-section align-c content-section-9" style="margin:0;padding:15px;min-height:200px" >

	<div class="container text-left">
		<h5 class="title2"><b><?=$model->title?></b></h5>

		<hr class="mr-b-40">
		<div style="" class="row eqh gt40 fs-equalize-element">

<?=$model->body?>


		</div>
	</div><!-- /.container -->
</section><!-- /.content-section -->

<?php if($page=='help'):?>
<!-- / Contact section -->
<section id="contact" style="outline-offset: -3px;" class="contact-section contact-section-2">
	<div class="container">
		<div class="row">
			<div class="col-md-5 l">
				<!-- Popup contact form - 1 -->
				<div id="contact-form" class="form-block">
					<h2 class="title small">Напишите нам</h2>
				<div id="feedback">
				 <?php $form = ActiveForm::begin(['id'=>'feedbackForm']); ?>

        <?= $form->field($feedback, 'name')->textInput(['placeholder' =>'Имя'])->label(false) ?>
              <?= $form->field($feedback, 'email')->textInput(['placeholder' =>'Email'])->label(false) ?>
           <?= $form->field($feedback, 'phone')->textInput(['placeholder' =>'Телефон'])->label(false) ?>
        <?= $form->field($feedback, 'text')->textArea(['placeholder' =>'Сообщение'])->label(false) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
                    </div>
					<div id="msg-block" class="msg-block" ></div>
				</div><!-- /#popup-contact-form -->

			</div><!-- /.col-md-6 -->
			<div style="" class="col-md-6 col-md-offset-1 r"><h2 class="title">Контакты</h2>
<h3 class="hd">Телефон<br>
</h3>
<p class="sub-txt">
		<?=Yii::$app->params['contactPhone']?>
</p>
<!-- Call us -->
<h3 class="hd">Email</h3>
<p class="sub-txt">
	<a href="mailto:info@slide.chat">info@slide.chat</a>
</p>
<!-- Email us -->
<!-- /.content --></div><!-- /.col-md-6 -->
		</div><!-- / row -->
	</div><!-- / container-fluid -->
</section><!-- /contact-section -->
<?php endif;?>