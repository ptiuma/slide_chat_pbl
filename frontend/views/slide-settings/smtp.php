<?php
use yii\helpers\Html;
use  yii\helpers\Url;
use yii\bootstrap\Collapse;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use frontend\models\Settings;
use karnbrockgmbh\modal\Modal;

use kartik\icons\Icon;
Icon::map($this);
$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;



?>
<div>
   <b>Укажите имя ящика и пароль от него, чтобы отправлять письма и получать уведомления</b>
      <br />
Интегрируйте почту с B2B Family и ведите переписку по привычному сценарию. Теперь все отправленные письма и уведомления о действиях клиента будут храниться в вашей почте.
</div><p>&nbsp;</p>
<div class="row settings-form">
       <div class="col-md-8">
              <div class="panel panel-default">

                                     <div class="panel-body">
<?php
  $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]);
echo FormGrid::widget([
    'model'=>$model,
    'form'=>$form,
    'autoGenerateColumns'=>true,
    'rows'=>[
    /*
       [
      'attributes' => [
                                'isActive'=>[       'type'=>Form::INPUT_WIDGET,
            'widgetClass'=>'kartik\checkbox\CheckboxX',
            'label'=>'',
             'options' => [
              'pluginOptions'=>['threeState'=>false],
                       'labelSettings' => [
        'label' => 'Использовать данные SMTP настройки для отправки моей почты',
        'position' => kartik\checkbox\CheckboxX::LABEL_RIGHT
    ]
    ]


            ],
            ],
        ],
    */
         [
      'attributes' => [
                                'email_address'=>[
                                'label'=>'Email'],

            ],
        ],
            [
      'attributes' => [
                                'email_password'=>[
                                'type'=>Form::INPUT_PASSWORD,
                                'label'=>'Пароль'],

            ],
        ],
            [
      'attributes' => [
                                'smtp_server'=>[],

            ],
        ],
            [
      'attributes' => [
                                'port'=>[],

            ],
        ],
     [
      'attributes' => [
                                'encryption'=>[       'type'=>Form::INPUT_WIDGET,
            'widgetClass'=>'kartik\checkbox\CheckboxX',
           'label'=>'',

             'options' => [
                 'pluginOptions'=>['threeState'=>false],
                       'labelSettings' => [
        'label' => 'SSL/TLS',
        'position' => kartik\checkbox\CheckboxX::LABEL_RIGHT
    ]
    ]


            ],
            ],
        ],


                [
            'attributes'=>[
                'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW,
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .
                           Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) .
                        '</div>'
                ],
            ],
        ],
    ]
]);
ActiveForm::end();

?>
                </div>
                  </div>
    </div>
    <div class="col-md-4">
         <div class="panel panel-default">

                                     <div class="panel-body">
                                     <div class="text-center">
                                     SMTP настройки ниболее популярных почтовых сервисов
                                       </div><br />
          <?php

 echo Collapse::widget([
    'items' => [
        [
            'label' => 'Почта на Yandex',
            'content' => [
                'Сервер: <b>smtp.yandex.ru</b>',
                'Порт: <b>465</b>',
                'Защита соединения: <b>SSL</b>'
            ],
                        'contentOptions' => ['class' => 'in']
        ],
                [
            'label' => 'Почта на Mail.ru',
            'content' => [
                'Сервер: <b>smtp.mail.ru</b>',
                'Порт: <b>465</b>',
                'Защита соединения: <b>SSL</b>'
            ],
        ],
                [
            'label' => 'Почта на Gmail',
            'content' => [
                'Сервер: <b>smtp.gmail.com</b>',
                'Порт: <b>465</b>',
                'Защита соединения: <b>SSL</b><br><a href=\'https://support.google.com/mail/troubleshooter/1668960?rd=1\'>Подробнее</a>'
            ],
        ],
                [
            'label' => 'Почта на Outlook.com, Hotmail.com ',
            'content' => [
                'Сервер: <b>smtp-mail.outlook.com</b>',
                'Порт: <b>587</b>',
                'Защита соединения: <b>Отключена</b>'
            ],
        ],
                [
            'label' => 'Почта на Office 365',
            'content' => [
                'Сервер: <b>smtp.office365.com</b>',
                'Порт: <b>587</b>',
                'Защита соединения: <b>Отключена</b>'
            ],
        ],
                [
            'label' => 'Другая почта',
            'content' => [
                'Пожалуйста, свяжитесь с нашим техническим специалистом или письмом на адрес support@slide.chat'
            ],
        ],
    ]
]);
?>
         </div>
                  </div>

    </div>
</div>
<div class="clear"></div>
