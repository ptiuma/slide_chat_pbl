<?php
use yii\helpers\Html;
use  yii\helpers\Url;

use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use frontend\models\Settings;
use karnbrockgmbh\modal\Modal;

use kartik\icons\Icon;
Icon::map($this);
$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="row settings-form">
       <div class="col-md-10 col-md-offset-1">
              <div class="panel panel-default">
              <div class="panel-heading">
               <div class="row">
             <div class="col-md-7"><?=$this->title?> </div>
              <div class="col-md-5 text-right"><?= Html::a(Icon::show('user', ['class'=>'fa-1x']).'Редактировать профиль', ['/user/settings/profile'], [
    	'class'=>'btn btn-default btn-sm',

		]) ?>
 </div>
              </div>
                    </div>
                                     <div class="panel-body">
<?php
  $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]);
echo FormGrid::widget([
    'model'=>$model,
    'form'=>$form,
    'autoGenerateColumns'=>true,
    'rows'=>[
        [
      'attributes' => [
                                'alert_no_open_att'=>[       'type'=>Form::INPUT_WIDGET,
            'widgetClass'=>'kartik\checkbox\CheckboxX',
            'label'=>'',
             'options' => [
              'pluginOptions'=>['threeState'=>false],
                       'labelSettings' => [
        'label' => 'Уведомлять, если открыли письмо, но не открыли вложение (письмо с вложением)',
        'position' => kartik\checkbox\CheckboxX::LABEL_RIGHT
    ]
    ]


            ],
            ],
        ],
        [
      'attributes' => [
                                'alert_after_day'=>[       'type'=>Form::INPUT_WIDGET,
            'widgetClass'=>'kartik\checkbox\CheckboxX',
            'label'=>'',

             'options' => [
                 'pluginOptions'=>['threeState'=>false],
                       'labelSettings' => [
        'label' => 'Уведомлять через сутки, если отправленное письмо не открывали',
        'position' => kartik\checkbox\CheckboxX::LABEL_RIGHT
    ]
    ]


            ],
            ],
        ],
            [
      'attributes' => [
                                'send_email_alert'=>[
                                'label'=>'Присылать Email уведомления',
                                'type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>Settings::$email_alert_types,'options'=>[]],

            ],
        ],
            [
      'attributes' => [
                                'send_telegram_alert'=>[
                                'label'=>'Присылать Telegram уведомления',
                                'type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>Settings::$email_alert_types,'options'=>[]],

            ],
        ],
      [
       'contentBefore'=>'<div class="">',
       'contentAfter'=>'</div>',
     'attributes' => [
                                'email_copy'=>[
                                  'label'=>'Email-адреса для копий по умолчанию',
                                'type'=>Form::INPUT_WIDGET,'widgetClass'=>'\kartik\widgets\Select2','options'=>[  'pluginOptions' => [
        'tags' => true,
         'tokenSeparators' => [',', ' '],
    ], 'options' => ['data'=>[],'hideSearch'=>true,'multiple' => true]]],

            ],
        ],
           [
      'attributes' => [
                                'email_crm'=>[
                                   'label'=>'Email-адреса СRM-систем',
                                'type'=>Form::INPUT_WIDGET,'widgetClass'=>'\kartik\widgets\Select2','options'=>[  'pluginOptions' => [
        'tags' => true,
         'tokenSeparators' => [',', ' '],
    ], 'options' => ['data'=>false,'showToggleAll'=>false,'toggleSettings'=>[],'hideSearch'=>true,'multiple' => true]]],

            ],
        ],
                [
            'attributes'=>[
                'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW,
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .
                           Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) .
                        '</div>'
                ],
            ],
        ],
    ]
]);
ActiveForm::end();

?>
                </div>
                  </div>
    </div>
</div>
<div class="clear"></div>

