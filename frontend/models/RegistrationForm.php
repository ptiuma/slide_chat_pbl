<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use dektrium\user\models\User;
use dektrium\user\models\Profile;
use dektrium\user\models\RegistrationForm as BaseRegistrationForm;

class RegistrationForm extends BaseRegistrationForm
{
      public $phone;
      public $name;
      public $oferta;
    public function rules() {
        $rules = parent::rules();
        unset($rules['usernameRequired']);

        $rules[] = [['phone','name'], 'required'];
         $rules[] = [['oferta'], 'required', 'requiredValue' => 1, 'message' => ' Вы должны ознакомиться и принять условия публичной оферты'];
        $rules[]   = [['phone','name'], 'string', 'max' => 30];
        return $rules;
    }
        public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['name'] = \Yii::t('user', 'Имя');
        $labels['oferta'] = \Yii::t('user', 'Оферта');
        return $labels;
    }
  public function loadAttributes(User $user)
    {
     $user->setAttributes($this->attributes);
            /** @var Profile $profile */
        $profile = \Yii::createObject(Profile::className());
        $profile->setAttributes([
            'name' => $this->name,
            'phone' => $this->phone,
        ]);
        $user->setProfile($profile);
    }


}