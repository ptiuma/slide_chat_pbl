<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "visitors_info".
 *
 * @property integer $id
 * @property integer $viewId
 * @property string $name
 * @property string $email
 * @property string $company
 */
class VisitorsInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visitors_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email' ], 'required'],
            [['viewId','linkId'], 'integer'],
            [['name', 'email', 'company','phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'viewId' => Yii::t('app', 'View ID'),
            'linkId' => Yii::t('app', 'link ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'phone'),
            'company' => Yii::t('app', 'Company'),
        ];
    }
}
