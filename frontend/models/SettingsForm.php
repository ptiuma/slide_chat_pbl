<?php
namespace frontend\models;
use dektrium\user\helpers\Password;
use dektrium\user\Mailer;
use dektrium\user\Module;
use dektrium\user\traits\ModuleTrait;
use Yii;
use yii\base\Model;
use dektrium\user\models\User;
use dektrium\user\models\Profile;
use dektrium\user\models\SettingsForm as BaseSettingsForm;

class SettingsForm extends BaseSettingsForm
{
       public $phone;
      public $nickname;
    public function rules() {
        $rules = parent::rules();
        unset($rules['usernameRequired']);
       // $rules['fieldRequired'] = [['phone','nickname'], 'required'];
        $rules['fieldLength']   = [['phone','nickname'], 'string', 'max' => 30];
        return $rules;
    }
        public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['nickname'] = \Yii::t('user', '���');
        return $labels;
    }
}
