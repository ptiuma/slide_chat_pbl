<?php

namespace frontend\models;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "{{%telegram_token}}".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $chat_id
 * @property string $activation_code
 * @property string $isActive
 * @property integer $created_at
 * @property integer $updated_at
 */
class TelegramToken extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%telegram_token}}';
    }
     public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),

            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId',  'activation_code'], 'required'],
            [['userId', 'chat_id', 'created_at', 'updated_at','isActive'], 'integer'],
            [['activation_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userId' => Yii::t('app', 'User ID'),
            'chat_id' => Yii::t('app', 'Chat ID'),
            'activation_code' => Yii::t('app', 'Activation Code'),
            'isActive' => Yii::t('app', 'Is Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
