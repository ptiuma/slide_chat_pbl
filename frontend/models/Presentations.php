<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
 use common\models\User;
   use \ptiuma\chat\models\Chat;
/**
 * This is the model class for table "{{%presentations_run}}".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $docId
 * @property integer $pageId
 * @property integer $isClosed
 * @property integer $created_at
 * @property integer $updated_at
 */
class Presentations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%presentations_run}}';
    }
     public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),

            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId'], 'required'],
            [['userId', 'isClosed', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userId' => Yii::t('app', 'User ID'),
            'docId' => Yii::t('app', 'Doc ID'),
            'pageId' => Yii::t('app', 'Page ID'),
            'isClosed' => Yii::t('app', 'Is Closed'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    public function getUser()
    {
          return $this->hasOne(User::className(), ['id' => 'userId']);
    }
                    public function getChat()
    {
          return $this->hasOne(Chat::className(), ['presentationId' => 'id']);
    }
}
