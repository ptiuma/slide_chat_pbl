<?php

namespace frontend\models;

use Yii;
 use common\models\Links;
  use \ptiuma\chat\models\Chat;
/**
 * This is the model class for table "{{%links_views}}".
 *
 * @property integer $id
 * @property integer $docId
 * @property integer $linkId
 * @property string $socketId
 * @property string $client_session
 * @property string $client_ip
 * @property string $client_browser
 * @property string $client_device
 * @property string $client_os
 * @property string $client_os_type
 * @property integer $downloads
 * @property integer $print
 * @property integer $started_at
 * @property integer $ended_at
 * @property integer $created_at
 * @property integer $updated_at
 */
class LinksViews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%links_views}}';
    }
        public static $device_types = [  "1"=>"Десктоп",
   "2"=>"Мобильник",
    "3"=>"Планшет",
    ];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'linkId', 'created_at'], 'required'],
            [['linkId', 'downloads','isNode'], 'integer'],
            [['socketId', 'client_session','client_country','client_city', 'client_browser','client_os', 'client_platorm'], 'string', 'max' => 255],
            [['client_name','client_phone','client_email'], 'string', 'max' => 25],
            [['client_ip'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'docId' => Yii::t('app', 'Doc ID'),
            'linkId' => Yii::t('app', 'Link ID'),
            'socketId' => Yii::t('app', 'Socket ID'),
            'client_session' => Yii::t('app', 'Client Session'),
            'client_ip' => Yii::t('app', 'IP'),
            'client_browser' => Yii::t('app', 'Броузер'),
            'client_device' => Yii::t('app', 'Client Device'),
            'client_os' => Yii::t('app', 'Client Os'),
            'client_platorm' => Yii::t('app', 'client_platorm'),
            'client_country' => Yii::t('app', 'Страна'),
            'client_city' => Yii::t('app', 'Город'),
            'client_name' => Yii::t('app', 'Имя'),
            'client_phone' => Yii::t('app', 'Телефон'),
            'client_email' => Yii::t('app', 'Email'),
            'isNode' => Yii::t('app', 'isNode'),
            'isAlertSended' => Yii::t('app', 'isAlertSended'),
            'downloads' => Yii::t('app', 'Downloads'),
            'print' => Yii::t('app', 'Print'),
            'started_at' => Yii::t('app', 'Started At'),
            'ended_at' => Yii::t('app', 'Ended At'),
            'created_at' => Yii::t('app', 'Дата визита'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
                public function getLink()
    {
          return $this->hasOne(Links::className(), ['id' => 'linkId'])
          ->from(['links' => Links::tableName()]);
    }
                public function getChat()
    {
          return $this->hasOne(Chat::className(), ['viewId' => 'id']);
    }
                public function getChat_messages_count()
    {
          return Chat::find()->joinWith('messages',true,'RIGHT JOIN')->where(['viewId' =>$this->id])->count();
    }
      public function getVisitor_name(){
      $n='не задано';
      if($this->client_name)$n=$this->client_name;
      elseif($this->link->client_name)$n=$this->link->client_name;
          return $n;
    }
      public function getVisitor_phone(){
      $n='не задано';
      if($this->client_phone)$n=$this->client_phone;
      elseif($this->link->client_phone)$n=$this->link->client_phone;
          return $n;
    }
      public function getVisitor_email(){
      $n='не задано';
      if($this->client_email)$n=$this->client_email;
      elseif($this->link->letterId&&$this->link->letter->email_to)$n=$this->link->letter->email_to;
          return $n;
    }
    public function getInterval()
    {
    	$date = new \DateTime();
	   $date->setTimestamp($this->ended_at);
    	$date2 = new \DateTime();
	   $date2->setTimestamp($this->created_at);

    	$interval = $date->diff($date2);
		return $interval->h !== 0?$interval->format('%H:%I:%S'):$interval->format('%I:%S');
    }
     public function getDevice(){    return $this::$device_types[$this->client_device];
    }
     public function getStatus_color(){     	$curr=time();
     	if($curr-$this->created_at<60*5)$color='red';
     	elseif($curr-$this->created_at>60*5&&$curr-$this->created_at<60*60*3)$color='green';
     	else $color='grey';
    return $color;
    }
     public function getStatus_label(){
     	$curr=time();
     	if(!$this->ended_at&&$curr-$this->created_at<60*60*1)$color='<span class="label label-danger">Онлайн</span>';
     	else if($curr-$this->created_at<60*5)$color='<span class="label label-danger">Горячий</span>';
     	elseif($curr-$this->created_at>60*5&&$curr-$this->created_at<60*60*3)$color='<span class="label label-success">Тёплый</span>';
     	else $color='<span class="label label-default">Холодный</span>';
    return $color;
    }
    public static function Activity($userId,$tp=0)
    {    	$res=0;
    	if($tp==1)$res=Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['userId' => Yii::$app->user->id])->andwhere(['<','UNIX_TIMESTAMP()-views.created_at',60*60*3])->count();
    	elseif ($tp==2) $res=Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['userId' => Yii::$app->user->id])->andwhere(['>','ended_at',0])->andwhere(['<','UNIX_TIMESTAMP()-views.created_at',60*5])->count();
    	else $res=Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['userId' => Yii::$app->user->id,'isNode'=>1])->andwhere(['ended_at'=>0])->count();

    	return $res;
    }
}
