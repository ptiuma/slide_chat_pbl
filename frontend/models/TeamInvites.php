<?php

namespace frontend\models;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "{{%team_invites}}".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $teamId
 * @property integer $invite_role
 * @property string $invite_email
 * @property string $invite_code
 * @property integer $isParsed
 * @property string $comments
 * @property integer $created_at
 * @property integer $updated_at
 */
class TeamInvites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%team_invites}}';
    }
           public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),

            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invite_email','invite_role'], 'required'],
        //    [['userId', 'teamId', 'invite_role',  'created_at', 'updated_at'], 'integer'],
       //     [['comments'], 'string'],
            [['invite_email', 'invite_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userId' => Yii::t('app', 'User ID'),
            'teamId' => Yii::t('app', 'Team ID'),
            'invite_role' => Yii::t('app', 'Роль'),
            'invite_email' => Yii::t('app', 'Email'),
            'invite_code' => Yii::t('app', 'Invite Code'),
            'isParsed' => Yii::t('app', 'Is Parsed'),
            'comments' => Yii::t('app', 'Comments'),
            'created_at' => Yii::t('app', 'Создано'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
