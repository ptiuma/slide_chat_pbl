<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
 use common\models\Links;
 use frontend\models\LinksViews;
 use common\models\Letters;
/**
 * This is the model class for table "{{%addressbook}}".
 *
 * @property string $id
 * @property integer $userId
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $nickname
 * @property string $company
 * @property string $title
 * @property string $address
 * @property string $addr_long
 * @property string $addr_lat
 * @property string $addr_status
 * @property string $home
 * @property string $mobile
 * @property string $work
 * @property string $fax
 * @property string $email
 * @property string $homepage
 * @property string $address2
 * @property string $phone2
 * @property string $notes
 * @property integer $isArchive
 * @property integer $created_at
 * @property integer $updated_at
 */
class AddressBook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%addressbook}}';
    }
     public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),

            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'email'], 'required'],
            [['id', 'userId', 'isArchive'], 'integer'],
            ['email', 'email'],
             [['email'], 'unique', 'targetAttribute' => ['userId', 'email']],
            [['address', 'notes'], 'string'],
            [['name', 'phone','company'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userId' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Имя'),
            'company' => Yii::t('app', 'Компания'),
             'phone' => Yii::t('app', 'Телефон'),
            'fax' => Yii::t('app', 'Fax'),
            'email' => Yii::t('app', 'Email'),
            'notes' => Yii::t('app', 'Комментарий'),
            'isArchive' => Yii::t('app', 'Is Archive'),
            'created_at' => Yii::t('app', 'Создано'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
     public function getLetters_count()
    {
          return Letters::find()->where(['userId' => Yii::$app->user->id])->andwhere(['like','email_to',$this->email])->count();
    }
     public function getVisits_count()
    {
          $query=LinksViews::find()->joinWith('link')->where(['userId'=>Yii::$app->user->id]);
          $emailSubQuery = Letters::find()->select('id')->where(['userId'=>Yii::$app->user->id])->andwhere(['like', 'email_to', $this->email]);
        $query->andFilterWhere(['or',
        ['in', 'links.docId', $emailSubQuery],
        ['like', 'links_views.client_email', $this->email]
        ]);
          return $query->count();
    }
}
