<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%user_mail_settings}}".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $email_address
 * @property string $email_password
 * @property string $smtp_server
 * @property integer $port
 * @property integer $encryption
 * @property integer $isActive
 */
class UserMailSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_mail_settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'smtp_server'], 'required'],
            [['userId', 'port', 'encryption', 'isActive'], 'integer'],
            [['email_address', 'email_password', 'smtp_server'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userId' => Yii::t('app', 'User ID'),
            'email_address' => Yii::t('app', 'Email'),
            'email_password' => Yii::t('app', 'Пароль'),
            'smtp_server' => Yii::t('app', 'Smtp сервер'),
            'port' => Yii::t('app', 'Порт'),
            'encryption' => Yii::t('app', 'SSL/TLS'),
            'isActive' => Yii::t('app', 'Is Active'),
        ];
    }
}
