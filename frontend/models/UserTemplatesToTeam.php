<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%user_templates_to_team}}".
 *
 * @property integer $teamId
 * @property integer $templateId
 */
class UserTemplatesToTeam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_templates_to_team}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teamId', 'templateId'], 'required'],
            [['teamId', 'templateId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teamId' => 'Team ID',
            'templateId' => 'Template ID',
        ];
    }
}
