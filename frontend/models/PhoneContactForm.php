<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

class PhoneContactForm extends Model
{
    public $name;
    public $phone;
    public $viewId;

    public function rules()
    {
        return [
            [['name', 'phone','viewId' ], 'required'],
            [['viewId'], 'integer'],
            [['name','phone',], 'string', 'max' => 128],
        ];
    }
   public function attributeLabels()
    {
        return [
            'viewId' => Yii::t('app', 'View ID'),
            'name' => Yii::t('app', 'Имя'),
            'phone' => Yii::t('app', 'Телефон'),
        ];
    }
}
