<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\LinksViews;
use common\models\Links;
use common\models\Documents;
use common\models\Letters;
/**
 * LinksViewsSearch represents the model behind the search form about `\frontend\models\LinksViews`.
 */
class LinksViewsSearch extends LinksViews
{
    /**
     * @inheritdoc
     */
    public $type;
         public $keyword;
    public function rules()
    {
        return [
            [['type','id',  'linkId', 'client_device', 'isAlertSended', 'downloads', 'print', 'started_at', 'ended_at',  'updated_at'], 'integer'],
            [['keyword','docId','socketId','created_at', 'client_session', 'client_ip', 'client_browser', 'client_os', 'client_platorm', 'client_country', 'client_city', 'client_name', 'client_email', 'client_phone', 'client_company'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LinksViews::find()->joinWith('link')->where(['userId'=>Yii::$app->user->id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

              'sort' => [
            'defaultOrder' => [
                'id' => SORT_DESC
            ]
        ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            print_r($this->getErrors());
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'linkId' => $this->linkId,
            'client_device' => $this->client_device,
            'isAlertSended' => $this->isAlertSended,
            'downloads' => $this->downloads,
            'print' => $this->print,
            'started_at' => $this->started_at,
            'ended_at' => $this->ended_at,

        ]);
        if($this->type)
        {
             if($this->type==1) $query->andFilterWhere(['>', 'links.letterId', '0']);
             else  $query->andFilterWhere(['=', 'links.letterId', '0']);
         }
        if($this->docId)
        {
       $docSubQuery = Documents::find()->select('id')->where(['userId'=>Yii::$app->user->id])->andwhere(['like', 'name', $this->docId]);
        $query->andFilterWhere(['in', 'links.docId', $docSubQuery]);
         }
        if($this->client_email)
        {
       $emailSubQuery = Letters::find()->select('id')->where(['userId'=>Yii::$app->user->id])->andwhere(['like', 'email_to', $this->client_email]);
        $query->andFilterWhere(['or',
        ['in', 'links.docId', $emailSubQuery],
        ['like', 'links_views.client_email', $this->client_email]
        ]);
         }
        $query->andFilterWhere(['like', 'client_ip', $this->client_ip])
            ->andFilterWhere(['like', 'client_browser', $this->client_browser])
            ->andFilterWhere(['like', 'client_os', $this->client_os])
            ->andFilterWhere(['like', 'client_platorm', $this->client_platorm])
            ->andFilterWhere(['like', 'links_views.client_country', $this->client_country])
            ->andFilterWhere(['like', 'links_views.client_city', $this->client_city])
            ->andFilterWhere(['like', 'links_views.client_name', $this->client_name])
            ->andFilterWhere(['like', 'links_views.client_phone', $this->client_phone])
            ->andFilterWhere(['like', 'client_company', $this->client_company]);
     if(isset($this->created_at) && $this->created_at!=''){
				$date_explode = explode(":", $this->created_at);
				$date1 = strtotime(trim($date_explode[0]));
				$date2= strtotime(trim($date_explode[1]));
				$query->andFilterWhere(['between', 'links_views.created_at', $date1,$date2]);
	 }

  if ($this->keyword) {
   $emailSubQuery = Letters::find()->select('id')->where(['userId'=>Yii::$app->user->id])->andwhere(['like', 'email_to', $this->keyword]);
        $query->andFilterWhere(['or',
            ['like','links_views.client_name',$this->keyword],
            ['like','links_views.client_phone',$this->keyword],
            ['like','links.client_name',$this->keyword],
            ['like','links.client_phone',$this->keyword],
            ['in', 'links.docId', $emailSubQuery],
        	['like', 'links_views.client_email', $this->keyword]
        	]);
   }

        return $dataProvider;
    }
}
