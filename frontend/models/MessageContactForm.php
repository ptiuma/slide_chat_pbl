<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

class MessageContactForm extends Model
{
    public $name;
    public $phone;
    public $email;
    public $message;
    public $viewId;

    public function rules()
    {
        return [
            [['message','name','phone','email','viewId' ], 'required'],
            [['viewId'], 'integer'],
            [['message','name','phone','email'], 'string', 'max' => 255],
        ];
    }
   public function attributeLabels()
    {
        return [
            'viewId' => Yii::t('app', 'View ID'),
            'name' => Yii::t('app', 'Имя'),
            'phone' => Yii::t('app', 'Телефон'),
            'email' => Yii::t('app', 'Email'),
            'message' => Yii::t('app', 'Сообщение'),
        ];
    }
}
