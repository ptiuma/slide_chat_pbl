<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%user_settings}}".
 *
 * @property integer $userId
 * @property string $send_sms
 * @property string $send_email_alert
 * @property string $email_copy
 * @property string $email_crm
 *
 * @property User $user
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_settings}}';
    }
   public static $email_alert_types = [
    "0"=>"Присылать всегда",
      //    "1"=>"В начале просмотра",
  // "2"=>"После окончания просмотра",
    "3"=>"Не присылать вообще",
    ];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId'], 'required'],
            [['userId'], 'integer'],
            [['send_sms','send_telegram_alert', 'send_email_alert', 'alert_no_open_att','alert_after_day'], 'string', 'max' => 255],
            [[ 'email_copy', 'email_crm'], 'safe'],

        // [['userId'], 'exist', 'skipOnError' => true,  'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => Yii::t('app', 'User ID'),
            'send_sms' => Yii::t('app', 'Send Sms'),
            'send_email_alert' => Yii::t('app', 'Send Email Alert'),
            'send_telegram_alert' => Yii::t('app', 'Send Telegram Alert'),
            'email_copy' => Yii::t('app', 'Email Copy'),
            'email_crm' => Yii::t('app', 'Email Crm'),
            'alert_no_open_att' => Yii::t('app', 'alert_no_open_att'),
                   'alert_after_day' => Yii::t('app', 'alert_after_day'),   ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
