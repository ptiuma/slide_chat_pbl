<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\Letters;

/**
 * This is the model class for table "{{%user_templates}}".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $name
 * @property string $emai_subject
 * @property string $email_body
 * @property integer $created_at
 * @property integer $updated_at
 */
class UserTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_templates}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),

            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId','name', 'email_subject'], 'required'],
            [['userId'], 'integer'],
            [['name', 'email_subject'], 'string', 'max' => 255],
            [['email_body','AttachmentCollection'], 'string', 'max' => 12555],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userId' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Назание шаблона'),
            'email_subject' => Yii::t('app', 'Тема'),
            'email_body' => Yii::t('app', 'Email Body'),
            'AttachmentCollection' => Yii::t('app', 'AttachmentCollection'),
            'teamId' => Yii::t('app', 'Team ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
         public function getLetters_total()
    {
          return Letters::find()->where(['templateId' =>$this->id])->count();
    }
         public function getLetters_opened()
    {
          return Letters::find()->where(['templateId' =>$this->id,'isOpened'=>1])->count();
    }
         public function getLetters_opened_att()
    {
          return Letters::find()->joinWith('links')->where('links.id is not null')->where(['templateId' =>$this->id,'letters.isOpened'=>1])->count();
    }
}
