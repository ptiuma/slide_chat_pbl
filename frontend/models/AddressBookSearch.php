<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\AddressBook;

/**
 * AddressBookSearch represents the model behind the search form about `\frontend\models\AddressBook`.
 */
class AddressBookSearch extends AddressBook
{
    /**
     * @inheritdoc
     */
     public $keyword;
    public function rules()
    {
        return [
            [['id', 'userId', 'isArchive', 'created_at', 'updated_at'], 'integer'],
            [['keyword','name','company', 'phone', 'fax', 'email', 'notes'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AddressBook::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'userId' => $this->userId,
            'isArchive' => $this->isArchive,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);


        $query->andFilterWhere(['like', 'company', $this->company])

            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'notes', $this->notes]);

  if ($this->keyword) {
        $query->andFilterWhere(['or',
            ['like','name',$this->keyword],
            ['like','phone',$this->keyword],
            ['like','email',$this->keyword],
            ['like','company',$this->keyword]]);
   }

        return $dataProvider;
    }
}
