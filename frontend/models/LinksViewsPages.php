<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%links_views_pages}}".
 *
 * @property integer $id
 * @property integer $pageId
 * @property integer $linkId
 * @property string $client_session
 * @property integer $viewId
 * @property integer $started_at
 * @property integer $ended_at
 * @property integer $total_time
 * @property integer $created_at
 * @property integer $updated_at
 */
class LinksViewsPages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%links_views_pages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pageNum', 'linkId', 'client_session', 'viewId', 'started_at', 'ended_at', 'total_time', 'created_at', 'updated_at'], 'required'],
            [['pageId','pageNum', 'linkId', 'viewId', 'started_at', 'ended_at', 'total_time', 'created_at', 'updated_at'], 'integer'],
            [['client_session'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pageId' => Yii::t('app', 'Page ID'),
                        'pageNum' => 'pageNum',
            'linkId' => Yii::t('app', 'Link ID'),
            'client_session' => Yii::t('app', 'Client Session'),
            'viewId' => Yii::t('app', 'View ID'),
            'started_at' => Yii::t('app', 'Started At'),
            'ended_at' => Yii::t('app', 'Ended At'),
            'total_time' => Yii::t('app', 'Total Time'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
