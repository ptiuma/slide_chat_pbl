<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
 use common\models\Documents;
 use common\models\Links;
/**
 * This is the model class for table "{{%letters}}".
 *
 * @property integer $id
 * @property integer $linkDocId
 * @property integer $templateId
 * @property string $email_to
 * @property string $email_copy
 * @property string $subject
 * @property string $email_body
 * @property string $client_name
 * @property string $client_phone
 * @property string $sheduller
 * @property integer $type
 * @property integer $isArchive
 * @property integer $created_at
 * @property integer $updated_at
 */
class Mailing extends \yii\db\ActiveRecord
{
    public $attachments;
        public $maildb;
        public $mailing_from;

 /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%letters}}';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),

            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email_to',  'email_subject', 'email_body', 'userId','key'], 'required'],
            [[ 'type','askLinkInfo', 'isArchive','isMultiEmail','sheduller_use','templateId','alert_after_days','alert_opened_in_moment'], 'integer'],
            ['email_to', 'string'],
            [['email_copy', 'email_subject', 'client_name','client_company','sheduller', 'client_phone','key'], 'string', 'max' => 255],
            [['email_body'], 'string', 'max' => 12555],
               [['maildb'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xlsx, xls','checkExtensionByMimeType'=>false,'maxSize' => 1024 * 1024 * 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userId' => Yii::t('app', 'User ID'),
            'linkDocId' => Yii::t('app', 'Link Doc ID'),
            'templateId' => Yii::t('app', 'Шаблон'),
            'email_to' => Yii::t('app', 'Кому'),
            'email_copy' => Yii::t('app', 'Копия'),
            'email_subject' => Yii::t('app', 'Тема'),
            'email_body' => Yii::t('app', 'Собщение'),
            'client_name' => Yii::t('app', 'Клиент'),
            'client_company' => Yii::t('app', 'Компания'),
            'client_phone' => Yii::t('app', 'Телефон Клиента'),
            'sheduller' => Yii::t('app', 'Расписание'),
            'sheduller_use' => Yii::t('app', 'Use Sheduller'),
            'isMultiEmail'=>'isMultiEmail',
            'askLinkInfo' => Yii::t('app', 'запросить контактные данные у посетителя перед открытием вложения'),
            'type' => Yii::t('app', 'Type'),
            'isOpened' => Yii::t('app', 'isOpened'),
            'opened_at' => Yii::t('app', 'Opened At'),
            'key' => Yii::t('app', 'Key'),

            'alert_after_days' => Yii::t('app', 'или если не откроют в течение'),
            'alert_opened_in_moment' => Yii::t('app', 'Уведомить в момент'),
            'isArchive' => Yii::t('app', 'Is Archive'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

}
