<?php
use yii\helpers\Html;
use  yii\helpers\Url;
 use common\models\Letters;

?>

<div class="email-body">
  Здравствуйте!<br />
 Вас приглашают в команду <?=$model->name?> на сервисе Slide.chat.
 <br />
Приняв приглашение, Вы получите доступ к документам группы и станете частью команды!

</div>
<div><a href="<?=Url::to(['team/activate-invite?key='.$members->invite_code], true);?>">Присоединиться к команде</a></div>
