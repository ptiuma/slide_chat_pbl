<?php
use yii\helpers\Html;
use  yii\helpers\Url;
 use common\models\Letters;

?>
Статистика по вашей команде "<?=$team['name']?>" за прошлую неделю с <?=date('d/m/Y',$stats['start_date'])?> по <?=date('d/m/Y',$stats['end_date'])?>.
<div class="teamstats-body">
<table width="100%" cellpadding="0" cellspacing="0" style="margin-top:20px">
   <tr>
      <td width="33%" align="center">
      <img src="<?php echo Url::to(['/'],true)?>img/mail/images/envelope1-icon.png" alt="envelope"  border="0" />
      </td>
      <td width="33%" align="center">
      <img src="<?php echo Url::to(['/'],true)?>img/mail/images/envelope2-icon.png" alt="envelope-no-attach"  border="0" />
      </td>
      <td width="33%" align="center">
             <img src="<?php echo Url::to(['/'],true)?>img/mail/images/link-icon.png" alt="link"  border="0" />
      </td>
   </tr>

   <tr>
      <td width="33%" align="center">Письма с вложениями </td>
      <td width="33%" align="center">Письма без вложений</td>
      <td width="33%" align="center">Ссылки</td>
   </tr>
   <tr>
     <td width="33%" align="center"><?=$stats['letters_total_sent']?>&nbsp;&nbsp;&nbsp; &#8594; &nbsp;&nbsp;&nbsp;<?=$stats['letters_total_opened']?> &nbsp;&nbsp;&nbsp;<span style="color:#FF0000">=&nbsp;&nbsp;&nbsp; <?=$stats['letters_conversion']?></span></td>
     <td width="33%" align="center"><?=$stats['lettersnoatt_total_sent']?>&nbsp;&nbsp;&nbsp; &#8594; &nbsp;&nbsp;&nbsp;<?=$stats['lettersnoatt_total_opened']?>&nbsp;&nbsp;&nbsp; <span style="color:#FF0000"> = &nbsp;&nbsp;&nbsp;<?=$stats['lettersnoatt_conversion']?></span></td>
     <td width="33%" align="center"><?=$stats['links_total_sent']?>&nbsp;&nbsp;&nbsp; &#8594; &nbsp;&nbsp;&nbsp;<?=$stats['links_total_opened']?> &nbsp;&nbsp;&nbsp;<span style="color:#FF0000"> =&nbsp;&nbsp;&nbsp; <?=$stats['links_conversion']?></span></td>
   </tr>
     <tr style="font-size:10px">
      <td width="33%" align="center">Отправлено Открыто Конверсия</td>
      <td width="33%" align="center">Отправлено Открыто Конверсия</td>
      <td width="33%" align="center">Отправлено Открыто Конверсия</td>
   </tr>
</table>
<table width="100%" cellpadding="10" cellspacing="0" style="margin-top:20px">
   <tr>
      <td width="50%" align="center">
        <div style="background:#5FBEC4;padding:10px">
            <a class="mcnButton " title="Открыть статистику за прошлый месяц" href="<?php echo Url::to(['/'],true)?>statistics/team?id=<?php echo $team->id?>&from_date=<?php echo date('Y-m-d',strtotime("-1 month",$stats['end_date']))?>&to_date=<?php echo date('Y-m-d',$stats['end_date'])?>" target="_blank" style="background:#5FBEC4;font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;">Открыть статистику за прошлый месяц</a>
            </div>
      </td>
      <td width="50%" align="center">
       <div style="background:#E8643E;padding:10px">
            <a class="mcnButton " title="Открыть статистику за прошлую неделю" href="<?php echo Url::to(['/'],true)?>statistics/team?id=<?php echo $team->id?>&from_date=<?php echo date('Y-m-d',strtotime("-1 week",$stats['end_date']))?>&to_date=<?php echo date('Y-m-d',$stats['end_date'])?>" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;">Открыть статистику за прошлую неделю</a>
        </div>
    </td>
    </tr>
  </table>
</div>