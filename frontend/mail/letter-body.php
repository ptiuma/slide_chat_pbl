<?php
use yii\helpers\Html;
use  yii\helpers\Url;
 use common\models\Letters;

?>

<div class="email-body">
      <?=$model->email_body?>

</div>
<?php foreach($model->links as $link):
?>
  <div style="margin-top:30px;margin-right:30px;padding:0;background:#F0F0F0;float:left;width:230px">
 <div  style="margin:4px;"><span style="padding:4px;background:<?=$link->doc->noParsed==1?'#41B88E':'#FF0000'?>;"><?=$link->doc->noParsed==1?'X':'PDF'?></span>  <?=$link->doc->name?></div>
 <div>
  <table border="0" cellpadding="0" cellspacing="0" style="margin:8px;padding:0" background="#ffffff" bgcolor="#ffffff">
                                    <tbody><tr>
                                        <td>
                                <table width="210" border="0" cellpadding="0" cellspacing="0" style="margin:0;padding:0;;border:solid #666 1px;border-bottom:0;background-position:center;background-repeat:no-repeat" background="<?=Yii::$app->getUrlManager()->createAbsoluteUrl('/').$link->doc->thumb?>">
                                                <tbody><tr>
                                                    <td height="110">
                                                      &nbsp;
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </td>
                                    </tr>
                                </tbody></table>
      </div>
  <div style="padding:5px;background:#F0F0F0;border:solid #dadada 1px;"><a href="<?=Yii::$app->getUrlManager()->createAbsoluteUrl('track/click?u='.$model->key.'&lid='.$link->link_name);?>" target="_blank">Открыть документ</a></div>
</div>
<?php endforeach;?>
<div style="clear:both"><img src="<?=Yii::$app->getUrlManager()->createAbsoluteUrl('track/open?u='.$model->key.'&id='.Yii::$app->security->generateRandomString(20));?>" width="1" height="1"></div>