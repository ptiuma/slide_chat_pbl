<?php

namespace frontend\controllers;
use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use frontend\models\Settings;
use common\models\Links;
use common\models\Letters;
use frontend\models\LinksViews;
use frontend\models\TelegramToken;
use \yii\db\Query;
use yii\helpers\Json;
class IntegrationController extends \yii\web\Controller
{
    public function behaviors() {
    return [
        'access' => [
            'class' =>  \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
    ];
}
 public function actionIndex()
    {

       return $this->render('index', [
            'dataProvider' => $dataProvider,
         ]);
    }
 public function actionTelegram()
    {
         $model=TelegramToken::find()->where(['userId'=>Yii::$app->user->id])->one();
        if(!$model->id)$model = new TelegramToken;
        if(Yii::$app->request->get('code'))
        {
           if(!$model->activation_code)
        {
        	$model->activation_code=Yii::$app->user->id.':'.Yii::$app->security->generateRandomString(7);
        	$model->userId=Yii::$app->user->id;
        	$model->save();
        }
        //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //	 return ['output'=>$model->activation_code, 'message'=>'success'];


        }
       return $this->render('telegram', [
          'model'=>$model
         ]);
    }
}
