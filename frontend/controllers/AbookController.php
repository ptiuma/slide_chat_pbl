<?php

namespace frontend\controllers;

use Yii;
use frontend\models\AddressBook;
use frontend\models\AddressBookSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AbookController implements the CRUD actions for AddressBook model.
 */
class AbookController extends Controller
{
    /**
     * @inheritdoc
     */

        public function behaviors() {
    return [
         'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        'access' => [
            'class' =>  \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
    ];
}

    /**
     * Lists all AddressBook models.
     * @return mixed
     */
    public function actionIndex()
    {
        $keyword=Yii::$app->request->get('keyword');
        $searchModel = new AddressBookSearch();
        $searchModel->keyword=trim($keyword);
        $searchModel->userId=Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'keyword'=>$keyword,
        ]);
    }

    /**
     * Displays a single AddressBook model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AddressBook model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AddressBook();
        $model->userId=Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
      //  print_r( $model->getErrors());
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AddressBook model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) &&$model->userId==Yii::$app->user->id && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AddressBook model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AddressBook model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AddressBook the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AddressBook::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
        public function actionMultipleEmailDelete()
    {
         $ids=Yii::$app->request->post('row_id');
        AddressBook::deleteAll(['id'=>$ids,'userId'=>Yii::$app->user->id]);
		return;
     }
        public function actionMultipleEmail()
    {
         $ids=Yii::$app->request->post('row_id');
        $records =  AddressBook::find()->where(['id'=>$ids,'userId'=>Yii::$app->user->id])->all();
		$str=[];
		foreach($records as $record)
		{
		  $str[]=$record->email;
		}
		return implode(', ',$str);
     }

}
