<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;

use common\models\Documents;
use common\models\Links;

/**
 * View controller
 */
class ViewController extends Controller
{
   public $layout = 'viewer';
    /**
     * Displays viewer.
     *
     * @return mixed
     */
    public function actionIndex()
    {

     $key=Yii::$app->request->get('slug');

        $browser = get_browser(null, true);
        $devices_type=Yii::$app->params['devicedetect'];
        if($devices_type['isDesktop']==1)$mobile=1;
        elseif($devices_type['isMobile']==1)$mobile=2;
        elseif($devices_type['isTablet']==1)$mobile=3;

		$link=$this->findModel($key);

$phone_model = new \yii\base\DynamicModel(['phone']);
$phone_model->addRule(['phone'], 'string', ['max' => 128]);
if ($phone_model->load(Yii::$app->request->post()) && $phone_model->validate()) {
    // do what you want
}
$message_model = new \yii\base\DynamicModel(['message','email']);
$message_model->addRule(['message','email'], 'string', ['max' => 128]);
if ($message_model->load(Yii::$app->request->post()) && $message_model->validate()) {
    // do what you want
}
        return $this->render('index', [
            'model' => $link,
            'browser'=>$browser,
            'mobile'=>$mobile,
            'phone_model'=>$phone_model,
            'message_model'=>$message_model
        ]);
    }
    public function actionDownloads()
    {

        $key=Yii::$app->request->get('key');
		$model=$this->findModel($key);
		$file=$model->doc->dir.'/'.$model->doc->filename;
//        echo $key;
        if (file_exists($file)) {

		   Yii::$app->response->sendFile($file);

		  }


    }
      protected function findModel($id)
    {
        if (($model = Links::findOne(['link_name'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
