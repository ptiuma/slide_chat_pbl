<?php

namespace frontend\controllers;
use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\Letters;
use common\models\Links;
use common\models\Documents;
use yii\helpers\Json;

class TrackController extends \yii\web\Controller
{

 public function actionClick()
    {
        $key=Yii::$app->request->get('u');
        $linkKey=Yii::$app->request->get('lid');
        if($key)
        {
		        $model=Letters::find()->andWhere(['key' => $key])->one();
		        if($model->id)
		        {
                    $model->isOpened=1;
    	        	$model->save();


                  $link=Links::find()-> andWhere(['letterId' => $model->id,'link_name' => $linkKey])->one();
                  if($link->id)
                  {
                  			         return $this->redirect(['view/'.$link->link_name]);
                  }
                 }
        }




    }
 public function actionOpen()
    {
        $key=$_GET['u'];
        //common\models\Logger::addLog('track','open '.$key);
        if($key)
        {
		        $model=Letters::find()->andWhere(['key' => $key])->one();
		        if($model->id&&$model->user->settings->send_email_alert!=3) Yii::$app->slidehelper->sendviewalert($model,1);
		        if($model->id&&!$model->isOpened)
		        {
		        	$model->isOpened=1;
		        	$model->opened_at=time();
		        	$model->save();

		        }
        }
        header("Content-Type: image/png");
		$im = @imagecreate(10, 10);
		$background_color = imagecolorallocate($im, 0, 0, 0);

		imagepng($im);
		imagedestroy($im);


    }


}
