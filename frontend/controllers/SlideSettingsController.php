<?php

namespace frontend\controllers;
use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\models\Settings;
use frontend\models\UserMailSettings;
class SlideSettingsController extends \yii\web\Controller
{
    public function behaviors() {
    return [
        'access' => [
            'class' =>  \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
    ];
}
    public function actionIndex()
    {
        $model=$this->findModel(Yii::$app->user->id);
               if ($model->load(Yii::$app->request->post())) {
               // $model->email_crm=implode(',',$model->email_crm);
               //  $model->email_copy=implode(',',$model->email_copy);
                if ($model->validate() && $model->save()) {
               Yii::$app->getSession()->setFlash('success', 'Настройки сохранены!');
                 return $this->refresh();
               	}
              }

               $model->email_crm=explode(',',$model->email_crm);
                $model->email_copy=explode(',',$model->email_copy);
          return $this->render('index', [
                'model' => $model,
            ]);
    }
      protected function findModel($id)
    {
        if (($model = Settings::findOne(['userId'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

   public function actionSmtp()
    {
        $model=UserMailSettings::findOne(['userId'=>Yii::$app->user->id]);
        if(!$model->id)$model=new UserMailSettings;
        $model->isActive=1;
               if ($model->load(Yii::$app->request->post())) {
                $model->userId=Yii::$app->user->id;
                if ($model->validate() && $model->save()) {
               Yii::$app->getSession()->setFlash('success', 'Настройки сохранены!');
                 return $this->refresh();
               	}
              }

               if($model->isNewRecord)$model->email_address=Yii::$app->user->identity->email;
                return $this->render('smtp', [
                'model' => $model,
            ]);
    }
}
