<?php

namespace frontend\controllers;

use Yii;
use common\models\Letters;
use common\models\Links;
use common\models\User;
use common\models\MailLog;
use frontend\models\AddressBook;
use common\models\Documents;
use common\models\search\DocumentsSearch;
use frontend\models\UserTemplates;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

class EmailController extends \yii\web\Controller
{
    public function behaviors() {
    return [
        'access' => [
            'class' =>  \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
    ];
}
  public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionSend()
    {

        $user=User::findOne(Yii::$app->user->id);
          if(!$user->smtp->id||!$user->smtp->email_address)
          {
          	  return $this->redirect(['slide-settings/smtp']);
          }

       $send_to=Yii::$app->request->get('send_to');
       $letterId=Yii::$app->request->get('letterId');
       $docId=Yii::$app->request->get('docId');
       if($letterId&&Yii::$app->request->get('repeat')==1)$repeat_model=Letters::find()->where(['id'=>$letterId,'userId' => Yii::$app->user->id])->one();
       if($docId)$doc=Documents::find()->where(['id'=>$docId,'userId' => Yii::$app->user->id])->one();

       $model = new Letters();

		if ($model->load(Yii::$app->request->post()))
        {
          $model->userId=Yii::$app->user->id;
          $model->key = Yii::$app->security->generateRandomString(32);
          $model->templateId=(int)$model->templateId;
          $model->sheduller_use=!empty($model->sheduller)?1:0;
          $model->isMultiEmail=$this->isMultiEmail($model->email_to);
          if ($model->validate())
          {
           $posted = $_POST['Letters'];
           if($posted['attachments'])$posted['attachments']=explode(',',$posted['attachments']);

                $model->save();
                $isSend=1;
                if(is_array($posted['attachments']))
                 {

                  foreach($posted['attachments'] as $attach)
                  {


                     $link = new Links();
        			 $link->docId=$attach;
				     $link->userId = Yii::$app->user->id;
     				 $link->link_name=Yii::$app->security->generateRandomString(10);
     				 $link->letterId=$model->id;
     				 $link->client_name=$model->client_name;
     				 $link->client_company=$model->client_company;
     				 $link->client_phone=$model->client_phone;
     				 $link->isProtect=$link->denyDownloading=$link->isExpire=0;
     				 $link->askInfo=$model->askLinkInfo;
     				 $link->save();

                 }
      			}
             $abook=AddressBook::find()->where(['userId'=>Yii::$app->user->id,'email'=>$model->email_to])->one();
             if(!$abook->id)
             {
              $abook=new AddressBook;
              $abook->userId=Yii::$app->user->id;
              $abook->email=$model->email_to;
              $abook->name=$model->client_name;
     		  $abook->company=$model->client_company;
              $abook->phone=$model->client_phone;
              $abook->save();
               //print_r($abook->getErrors());
             }
            	 if($model->sheduller_use!='1')$this->SendLetter($model);
            	//  if($posted['sheduller_use']!='1')Yii::$app->slidehelper->sendletter($model);
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
              return ['success' => true];

          }

       }

		if($repeat_model->id)
		{
			$model=$repeat_model;
			$model->email_to=$repeat_model->email_to;
			$model->email_body=$repeat_model->email_body;

			$model->email_subject=$repeat_model->email_subject;
			$model->email_copy=$repeat_model->email_copy;
			$model->templateId=$repeat_model->templateId;
			$model->client_name=$repeat_model->client_name;
			$model->client_phone=$repeat_model->client_phone;
		}

                if($model->isNewRecord&&$send_to)
                $model->email_to=$send_to;
       return $this->render('send', [
                'model' => $model,
                'isSend' => $isSend,
                'doc'=>$doc

            ]);
    }


        public function actionSelect()
    {

        $dataProvider = new ActiveDataProvider([
    'query' => Documents::find()->active()->andwhere(['userId'=>Yii::$app->user->id])->orderBy('id desc'),
    'pagination' => [
        'pageSize' => 24,
    ],
]);


          return $this->renderAjax('_select', [
            'dataProvider' => $dataProvider,
                'isTempl'=>Yii::$app->request->get('isTempl')
        ]);

    }
     public function actionTest2()
	{
        $command = Letters::find()->where(['sheduller_use'=>1])
       ->andwhere(['>','sheduller',0])
       ->andwhere(['<','sheduller',date('Y-m-d h:i:s')])
        ->andwhere(['isMailing'=>null])
       //->all()
       ->createCommand();
		echo  $command->getRawSql();
		exit;
       $user=User::findOne(Yii::$app->user->id);
       Yii::$app->mailer->useFileTransport=false;
			  Yii::$app->mailer->htmlLayout='layouts/html2';
     Yii::$app->mailer->setTransport(['class' => 'Swift_SmtpTransport', 'host' => $user->smtp->smtp_server,
              'username' => $user->smtp->email_address,
              'password' => $user->smtp->email_password,
              'port' => $user->smtp->port,
              'encryption' => $user->smtp->encryption==1?'ssl':false]);
			     $mail= \Yii::$app->mailer
    ->compose('mailViewName', ['model' => $model])
     ->setFrom('ptiuma@mail.ru')
    ->setTo('ptiuma@list.ru');

    $response=$mail->send();
    var_dump($response);
    var_dump($mail);
      /*
      $mail= \Yii::$app->mailgun
    ->compose('mailViewName', ['model' => $model])
     ->setFrom('ptiuma@mail.ru')
    ->setTo('ptiuma2@gmail.com')
    ->send();
    print_r($mail);
    echo $mail->http_response_code;
    echo $mail->http_response_body->id;
    */
    }
     public function actionTest()
	{
	//mail('ptiuma@list.ru', 'the subject', 'the message', null,
   //'-f ptiuma@mail.ru');
 //  exit;
          echo Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['userId' => Yii::$app->user->id])->andwhere(['>','ended_at',0,])->andwhere(['>','UNIX_TIMESTAMP()-views.created_at',60*5])->andwhere(['<','UNIX_TIMESTAMP()-views.created_at',60*60*3])->count();
          echo "--".Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['userId' => Yii::$app->user->id])->andwhere(['>','ended_at',0])->andwhere(['<','UNIX_TIMESTAMP()-views.created_at',60*5])->count();
           echo "--".Links::find()->joinWith('views',true,'RIGHT JOIN')->where(['userId' => Yii::$app->user->id,'isNode'=>1])->andwhere(['ended_at'=>0])->count();
          exit;
	  Yii::$app->mailer->htmlLayout='layouts/html2';
         $mail= Yii::$app->mailer->compose()
                ->setTo('ptiuma@list.ru')
               ->setFrom('ptiuma@mail.ru')
                ->setSubject('my test')
               ->setHtmlBody('Hello!')
                ->setTextBody('Hello!')
            ->send();
           exit;
	  $user=User::findOne(Yii::$app->user->id);
	    try{
	if($user->telegram->chat_id)
      \Yii::$app->bot->sendMessage($user->telegram->chat_id, 'Клиент открыл ваше письмо');
	}
	 catch (\TelegramBot\Api\HttpException $e) {
       print_r($e);
	}

	}

      public function actionAttachContent()
    {
       $model=Documents::findOne(Yii::$app->request->post('id'));
       $html=$this->renderPartial('_attach', [
            'model' => $model,
            'isTempl'=>Yii::$app->request->post('isTempl')
        ]);
        echo $html;
        exit;
    }
	public function actionDeleteTemplate()
    {
       $templateId=Yii::$app->request->post('id');
       $template=UserTemplates::find()->where(['id'=>$templateId,'userId' => Yii::$app->user->id])->one();
	    $template->delete();
               Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['success' => true];
     }
	public function actionTemplateData()
    {
       $templateId=Yii::$app->request->post('id');
       $template=UserTemplates::find()->where(['id'=>$templateId,'userId' => Yii::$app->user->id])->one();
			$data['email_body']=$template->email_body;
			$data['email_subject']=$template->email_subject;
            $data['AttachmentCollection']=explode(',',$template->AttachmentCollection);
              return Json::encode($data);
     }
     	public function actionListTemplates()
    {
             $templates=UserTemplates::find()->select(['id','name'])->where(['userId' => Yii::$app->user->id])->asArray()->all();
           return Json::encode($templates);
    }
     public function actionCreateTemplate()
    {

        $model = new UserTemplates();

    if ($model->load(Yii::$app->request->post())) {
     $model->userId = Yii::$app->user->id;
    $posted = $_POST['UserTemplates'];
    $model->AttachmentCollection=is_array($posted['AttachmentCollection'])?implode(',',$posted['AttachmentCollection']):'';
        if ($model->save()) {
                // JSON response is expected in case of successful save
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['success' => true];

         }
        else
        {
        	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['success' => false];
        }
    }

    if (Yii::$app->request->isAjax) {
        return $this->renderAjax('_template', [
            'model' => $model,
        ]);
    }
    }


    private function SendLetter($model)
    {
  //  echo $this->RFCEmail($model->email_to);
   // exit;
          $user=User::findOne(Yii::$app->user->id);
          if($user->smtp->id&&$user->smtp->isActive==1)
          {
          	  $provider='smtp';
          	  Yii::$app->swiftmailer->useFileTransport=false;
			  Yii::$app->swiftmailer->htmlLayout='layouts/html2';
              Yii::$app->swiftmailer->setTransport(['class' => 'Swift_SmtpTransport', 'host' => $user->smtp->smtp_server,
              'username' => $user->smtp->email_address,
              'password' => $user->smtp->email_password,
              'port' => $user->smtp->port,
              'encryption' => $user->smtp->encryption==1?'ssl':false]);
			     $mail= \Yii::$app->swiftmailer
			    ->compose('letter-body', ['model' => $model])
			     ->setFrom($user->smtp->email_address)
			     ->setSubject($model->email_subject)
			    ->setTo($this->RFCEmail($model->email_to))
			    ->setCC($this->RFCEmail($model->email_copy));
			    $response=$mail->send();

                Yii::warning($mail);
          }
          else
          {
          $provider='mailgun';
           Yii::$app->mailer->htmlLayout='layouts/html2';
           $mail=Yii::$app->mailer->compose('letter-body',['model'=>$model])
                ->setTo($model->email_to)
                ->setFrom(Yii::$app->user->identity->email)
                ->setSubject($model->email_subject)
                ->setCc($model->email_copy)
                ->send();
         }
           $log=new MailLog;
           $log->userId=$model->userId;
           $log->letterId=$model->id;
           $log->messageId=$provider=='smtp'?'':$mail->http_response_body->id;
           $log->provider=$provider;
           $log->save();
         //  print_r($log->getErrors());

            return true;

    }
      private function isMultiEmail($email)
    {
         $emails=explode(',',$email);
           return count($emails)>1?1:0;
            return "'" . implode("','", $emails) . "'";

    }
  private function RFCEmail($email)
    {
        if(!$email) return null;
           $emails=explode(',',$email);
            return $emails;
             return implode(", ", $emails);
            return "'" . implode("','", $emails) . "'";

    }
    public function actionRegisterAlert()
    {


        return $this->renderAjax('@frontend/views/register_slide/new', [
            'model' => $model,
        ]);
    }


}
