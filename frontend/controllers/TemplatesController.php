<?php

namespace frontend\controllers;

use Yii;
use frontend\models\UserTemplates;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\Team;
/**
 * TemplatesController implements the CRUD actions for UserTemplates model.
 */
class TemplatesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserTemplates models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => UserTemplates::find(),
        ]);
        $personal_templates=UserTemplates::find()->where(['userId'=>Yii::$app->user->id,'teamId'=>0])->all();
        $teams=Team::find()->joinWith('members')->where(['memberId'=>Yii::$app->user->id])->groupBy('id')->all();
        $total_templates=count($personal_templates);
        foreach($teams as $team)
        {
			$total_templates+=count($team->templates);

        }
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'personal_templates'=>$personal_templates,
            'teams'=>$teams,
            'total_templates'=>$total_templates,
        ]);
    }

    /**
     * Displays a single UserTemplates model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserTemplates model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserTemplates();

        if ($model->load(Yii::$app->request->post())) {
        $model->userId = Yii::$app->user->id;
        $posted = $_POST['UserTemplates'];
         $model->AttachmentCollection=is_array($posted['AttachmentCollection'])?implode(',',$posted['AttachmentCollection']):'';
          if ($model->save()) {
            return $this->redirect(['index']);
            }
        }
            return $this->render('create', [
                'model' => $model,
            ]);

    }

    /**
     * Updates an existing UserTemplates model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
                     $posted = $_POST['UserTemplates'];
         $model->AttachmentCollection=is_array($posted['AttachmentCollection'])?implode(',',$posted['AttachmentCollection']):'';
         if ($model->save()) {
            return $this->redirect(['index']);
            }
        }
            return $this->render('update', [
                'model' => $model,
            ]);

    }

    /**
     * Deletes an existing UserTemplates model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserTemplates model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserTemplates the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserTemplates::findOne($id)) !== null&&$model->userId == Yii::$app->user->id) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    	public function actionTemplateData()
    {
       $templateId=Yii::$app->request->get('tplId');
       $template=UserTemplates::find()->where(['id'=>$templateId,'userId' => Yii::$app->user->id])->one();
		 if (Yii::$app->request->isAjax) {
        return $this->renderAjax('_template', [
            'template' => $template,
			        ]);
			    }
     }
    	public function actionCopyTemplate()
    {
       $templateId=Yii::$app->request->get('tplId');
       $template=UserTemplates::find()->where(['id'=>$templateId,'userId' => Yii::$app->user->id])->one();
        $teamId=Yii::$app->request->get('teamId');
       $team=Team::find()->joinWith('members')->where(['id'=>$teamId,'memberId'=>Yii::$app->user->id])->one();
       if($team->id&&$template->id)
       {         $model=new UserTemplates;
         $model->attributes = $template->attributes;
         $model->teamId=$team->id;
         $model->save();
              Yii::$app->getSession()->setFlash('success', '������ ����������!');
          return $this->redirect(['index']);
       }

        $teams=Team::find()->joinWith('members')->where(['memberId'=>Yii::$app->user->id])->groupBy('id')->all();
		 if (Yii::$app->request->isAjax) {
        return $this->renderAjax('_copy_template', [
            'template' => $template,
             'teams' => $teams,
			        ]);
			    }
     }
}
