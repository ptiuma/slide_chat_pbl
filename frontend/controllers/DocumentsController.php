<?php

namespace frontend\controllers;

use Yii;
use common\models\Documents;
use common\models\search\DocumentsSearch;
use common\models\DocumentsPages;
use common\models\Links;
use common\models\Team;
use \common\models\DocumentsToTeam;
use frontend\models\Presentations;
use frontend\models\LinksViews;
use frontend\models\LinksViewsPages;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
 use yii\data\ActiveDataProvider;
 use  yii\helpers\Url;
use yii\helpers\Json;

class DocumentsController extends Controller
{
    public function behaviors() {
    return [
        'access' => [
            'class' =>  \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
    ];
}


    /**
     * Lists all Documents models.
     * @return mixed
     */
    public function actionIndex()
    {

        $totalSize = Documents::find()->sum('size') ?: 0;
            $dataProvider = new ActiveDataProvider([
              'query' => Documents::find()
              ->active()
              ->andwhere(['userId'=>Yii::$app->user->id])
              ->orderBy('id desc'),
                         'pagination' => [
        'pageSize' => 10,
    ],
        ]);

	if (Yii::$app->request->post()) {
         $newId=$this->FileUpload();
         if($newId)
         {
         	    Yii::$app->getSession()->setFlash('success', 'Файл закачан!');
			     return $this->redirect(['documents/']);
		 }
		 else
		 {
		 	    Yii::$app->getSession()->setFlash('warning', 'Ошибка, неверный формат файла!');
		 	    return $this->redirect(['documents/']);
		 }
    }
        return $this->render('index', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            // 'totalSize' => $totalSize
        ]);
    }

    /**
     * Displays a single Documents model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        if(Yii::$app->request->post('hasEditable')&&$model->userId==Yii::$app->user->id)
        {
        	$model->name=Yii::$app->request->post('file_name');
        	$model->save();
        	 $out = Json::encode(['output'=>'', 'message'=>'']);
        	 echo $out;
        	 return;
        }
              $viewsProvider = new ActiveDataProvider([
              'query' => LinksViews::find()->joinWith('link')
              ->where(['links.docId'=>$model->id,'userId'=>Yii::$app->user->id])
             // ->andwhere(['is','letterId',null])
              ->orderBy('id desc'),
                          'pagination' => ['pageSize' => 10],
					        ]);

        return $this->render('view', [
            'model' => $model,
        'viewsProvider'=>$viewsProvider
     ]);
    }
   public function actionFileUploadSend()
    {

	if (Yii::$app->request->post()) {
         $newId= $this->FileUpload();
         if($newId)
         {
         	$result=['success'=>1,'docId'=>$newId];
              return Json::encode($result);
         }
         else
         {
         	         	$result=['success'=>0];
              return Json::encode($result);
         }
    }

    }
    private function FileUpload()
    {

           $model=new Documents;
			 $doc =UploadedFile::getInstanceByName('doc');
			 $model->docFile=$doc;
			   $model->name = $doc->name;
			   $isPDF=$doc->type=='application/pdf'?1:0;
			    $ext = end((explode(".", $doc->name)));
			    $uploadDir=Yii::$app->keyStorage->get('system.uploadDir');
			    $doc_r = Yii::$app->user->id.'_'.Yii::$app->security->generateRandomString(7);
			    $doc_n = Yii::$app->security->generateRandomString(7);
			    $path = $uploadDir . '/' . $doc_r . '/' . $doc_n.".{$ext}";
                $pdf=$isPDF==1?$path:$uploadDir . '/' . $doc_r. '/'.$doc_n.'.pdf';
                $pdf_dir=$uploadDir . '/' . $doc_r . '/';
                $emptyImgFile=$uploadDir . '/' . $doc_r. '/'.$doc_n.'.png';


			    $model->name = $doc->name;
                $model->userId = Yii::$app->user->id;
				$model->upload_ip = Yii::$app->getRequest()->getUserIP();
                $model->mimeType = $doc->type;
			    $model->size = $doc->size;
                $model->repository = $doc_r;
                $model->name = $doc->name;
                $model->original = $doc->name;
                $model->source_name = $doc_n;
                $model->filename = basename($pdf);
                $model->path = $path;


//	 print_r($model->getErrors());


			 if ($model->validate()) {


			    \yii\helpers\BaseFileHelper::createDirectory($uploadDir . '/' . $doc_r);
			    \yii\helpers\BaseFileHelper::createDirectory($uploadDir . '/' . $doc_r . '/thumbs');
			    $model->docFile->saveAs($path);
			    //$doc->saveAs($pdf);
                if($isPDF!==1)
                {
					    if(!in_array(strtolower($ext),array('xls','xlsx')))
					    {
							    $command=Yii::$app->params['pdfConverter'];
							    $command=str_replace('{filename-in}',$path,$command);
							    $command=str_replace('{filename-out}',$pdf,$command);
							    $response=exec($command,$out,$res);

							 //   echo $command.$res;
							 //   echo "--".$response;
							  //  exit;
			             }
			    if(strlen($response)>0||!file_exists($pdf))$model->noParsed=1;
			    }

			    if($model->noParsed==1)
			    {
			    	   $image = new \Imagick();
						$image->newImage(300, 400, new \ImagickPixel('#34AB4B'));
						$image->setImageFormat('png');

                    $draw = new \ImagickDraw();
					$draw->setFillColor('black');
					//$draw->setFont('Arial');
					$draw->setFontSize( 30 );
					$image->annotateImage($draw, 100, 100, 0, 'Excel ');
					    $image->writeimage(\Yii::getAlias('@webroot').'/'.$emptyImgFile);
					  $save_to_th=Yii::getAlias('@webroot').'/'.$uploadDir.'/'.$model->repository.'/thumbs/'.basename($emptyImgFile);
					    exec(Yii::$app->params['imagickPath'].' -resize '.Yii::$app->keyStorage->get('common.thumbs_width').' '.$emptyImgFile.' '.$save_to_th);
                     $model->source_name = $emptyImgFile;
                $model->filename = basename($path);
			    }
			    $model->docFile=null;
			    $model->save(false);

                if($model->noParsed!==1)
			    {
			       $im = new \Imagick();
				   $im->pingImage(Yii::getAlias('@webroot').'/'.$pdf);
				   $pages_num=$im->getNumberImages();
						   for($z=0;$z<$pages_num;$z++)
						   {
						   	    $page_img=Yii::$app->security->generateRandomString(10).".jpg";
						   	    $save_to=Yii::getAlias('@webroot').'/'.$uploadDir.'/'.$model->repository.'/'.$page_img;
						   	    $save_to_th=Yii::getAlias('@webroot').'/'.$uploadDir.'/'.$model->repository.'/thumbs/'.$page_img;


								exec(Yii::$app->params['imagickPath'].' -background white '.Yii::getAlias('@webroot').'/'.$pdf.'['.$z.']  -quality 95 '.$save_to);
		//						exec(Yii::$app->params['imagickPath'].' -resample 72 -density 175 -background white -alpha remove  '.Yii::getAlias('@webroot').'/'.$pdf.'['.$z.']  -quality 95 '.$save_to);
								exec(Yii::$app->params['imagickPath'].' -resize '.Yii::$app->keyStorage->get('common.thumbs_width').' '.$save_to.' '.$save_to_th);
								$page=new DocumentsPages;
								$page->docId=$model->id;
								$page->filename=$page_img;
								$page->pageNum=($z+1);
		                        if ($page->validate()) {
		                         $page->save();
		                        }
		                   }

				   }
			return $model->id;
             }
             else {
               return false;
             }
    }

    /**
     * Deletes an existing Documents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);

        if($model->userId==Yii::$app->user->id)
        {

        	$model->isDeleted=1;
        	$model->save();
         }

        return $this->redirect(['documents/']);
    }
     public function actionCorrectPageNum()
    {
     $docs=Documents::find()->all();
     foreach($docs as $doc)
     {
       $pages=DocumentsPages::find()->where(['docId'=>$doc->id])->orderby('id')->all();
       $cnt=1;
            foreach($pages as $page)
		    {
		        echo $page->id."--$cnt<br>";
		    	$page->pageNum=$cnt;
		    	$page->save();
		    	print_r($page->getErrors());
		    	LinksViewsPages::updateAll(['pageNum'=>$cnt], ['pageId'=>$page->id]);
		    	$cnt++;
		    }
      }
    }
     public function actionDocList()
    {
        $data=Documents::find()->active()
              ->andwhere(['userId'=>Yii::$app->user->id])
              ->andwhere(['!=','noParsed',1])
              ->orderBy('id desc')->limit(150)->all();

        return $this->renderPartial('ajax/_doc_list', [
            'data'=>$data
        ]);
    }
     public function actionPagesList()
    {
      $id=Yii::$app->request->post('id');
       $data=Documents::find()->active()
              ->andwhere(['id'=>$id])
              ->andwhere(['userId'=>Yii::$app->user->id])
              ->orderBy('id desc')->limit(150)->one();

        return $this->renderPartial('ajax/_pages_list', [
            'data'=>$data
        ]);
    }
     public function actionBlockLink()
    {
        $model=Links::findOne(Yii::$app->request->post('id'));
        $status=Yii::$app->request->post('status');
        if($model->userId==Yii::$app->user->id)
        {

        	$model->isBlocked=$status=='false'?1:0;
        	echo $model->isBlocked;
        	$model->save();
         }

    }
    /**
     * Finds the Documents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Documents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Documents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
     public function actionCreatePresentation()
    {

         $presentation=new Presentations();
        $docId=Yii::$app->request->get('docId');
        if($docId)$doc=Documents::findOne($docId);
        $current=Presentations::find()->where(['userId' => Yii::$app->user->id,'isClosed'=>0])->one();
        if($current->id)
        {
            if($doc->id)
            {
            	$current->docId=$doc->id;
				$current->pageId=1;
            	$current->save();
            }
        	return Url::to(['/'],true).'p/'.$current->id;
        }
        else
         {
        	Presentations::updateAll(['isClosed'=>1],['userId'=>Yii::$app->user->id]);
					$presentation->userId=Yii::$app->user->id;
					if($doc->userId==Yii::$app->user->id)
					{
							$presentation->docId=$doc->id;
					    	$presentation->pageId=1;
					 }
					    	$presentation->save();
		                echo Url::to(['/'],true).'p/'.$presentation->id;
		                return;

		   }
          return false;

    }
      public function actionCreateLink()
    {

        $model = new Links();
        $model->scenario = 'link';
        $docId=Yii::$app->request->get('docId');
    if ($model->load(Yii::$app->request->post())) {
     $model->userId = Yii::$app->user->id;
     $model->link_name=Yii::$app->security->generateRandomString(10);
     /*$dateTime = new DateTime($model->expired_date);
     if($model->isExpire==1)$model->expired_date=$dateTime->format('U');
     $model->expired_date=(int)$model->expired_date;
       */ if ($model->validate()&&$model->save()) {
                echo Url::to(['/'],true).Yii::$app->params['slideShowPath'].$model->link_name;
                return;

        }
        else
        {
        	//print_r($model->getErrors());
         Yii::$app->getSession()->setFlash('warning', 'Link has not been created');
        }
    }
          if ($model->isNewRecord) {
            //$model->allowDownloading = 1;
            $model->docId=$docId;
        }
        return $this->renderAjax('ajax/_links_form', [
            'model' => $model,
        ]);

    }
	public function actionUpdateLink()
	{
	        $id=Yii::$app->request->get('id');
	        if(!empty($_POST['Links']['id']))$id=$_POST['Links']['id'];
	        if(!$id)$model=new Links;
	        else $model=Links::findOne($id);
	         if ($model->isNewRecord&&!$model->docId)
    	    {
            	$model->docId=Yii::$app->request->get('docId');
	        }
	        $model->scenario = 'link';

	if (is_array($_POST['Links'])&&$model->load(Yii::$app->request->post())&&$model->validate()) {
		$model->save();
		return 'success';
	}

	 return $this->renderAjax('ajax/_update_link', [
            'model' => $model,
        ]);
	}
       public function actionVisitsStats()
    {
         return $this->renderAjax('ajax/_visits_stats', [
           // 'model' => $model,
        ]);
    }
       public function actionViewStats()
    {
         $id=Yii::$app->request->get('id');
          $model=LinksViews::findOne($id);
         return $this->renderAjax('ajax/_view_stats', [
            'model' => $model,
        ]);
    }
       public function actionDocStats()
    {
           $html= $this->renderPartial('ajax/_charts', [
           // 'model' => $model,
        ]);
          return Json::encode($html);
    }
 	public function actionDocTeam()
	{

 	  $model=new DocumentsToTeam;
	  $id=Yii::$app->request->get('id');
	    $doc=Documents::findOne($id);
	     $model->docId=$doc->id;
        if ($model->load(Yii::$app->request->post())) {
        $posted=$_POST['DocumentsToTeam'];
        DocumentsToTeam::deleteAll(['docId'=>$posted['docId']]);
        if(is_array($posted['teamList'])&&count($posted['teamList']))
        {
			        foreach($posted['teamList'] as $team)
			        {

			                 $model=new DocumentsToTeam;
					         $model->docId=$posted['docId'];
					         $model->teamId=$team;
			                 $model->validate();
					         $model->save();

					 }
		 }
		  Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		  return 'Данные обновлены';
    }

		 return $this->renderAjax('ajax/_doc_team', [
             'model' => $model,
              'doc' => $doc,
        ]);
	}
	public function actionDownloadDoc()
    {

        $id=Yii::$app->request->get('id');
	 $model=$this->findModel($id);

        if($model->userId==Yii::$app->user->id)
        {
		$file=$model->dir.'/'.$model->filename;
//        echo $key;
        if (file_exists($file)) {

		   Yii::$app->response->sendFile($file);

		  }
       }

    }
}
