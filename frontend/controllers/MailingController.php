<?php

namespace frontend\controllers;

use Yii;
use kartik\widgets\ActiveForm;
use frontend\models\Mailing;
use common\models\Letters;
use common\models\Links;
use common\models\User;
use common\models\MailLog;
use frontend\models\AddressBook;
use common\models\Documents;
use common\models\search\DocumentsSearch;
use frontend\models\UserTemplates;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class MailingController extends \yii\web\Controller
{
    public function behaviors() {
    return [
        'access' => [
            'class' =>  \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                     'roles' => ['superAdmin', 'admin', 'manager'],
                ],
            ],
        ],
    ];
}
  public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionSend()
    {


        $model = new Letters(['scenario' => 'mailing']);
		if ($model->load(Yii::$app->request->post())&&$model->validate())
        {
              $msg='';
              $body=$model->email_body;
              $emails=array_map('trim',explode(",",$model->email_to));
              $dbemails=$this->ParseFile($model);
              $emails=array_merge($this->CorrectNameInEmail($emails),$dbemails);
              Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
              if(!count($emails))$msg='Не загружено ни одного email';
              if(count($emails)>1000)$msg='Загружено слишком много адресов';
              if(count($emails)>20)$msg='Загружено большое число email<br />Используйте рассылку по расписанию для отправки большого числа адресов';
              if($model->mailing_use_smtp==1&&strpos($model->mailing_from, '@slide.chat')==false)$msg='Не правильно прописан email в поле От Кого';
              if(count($emails)>=499&&$model->mailing_use_smtp==2)$msg='Загружено большое число email<br />Выбранный метод рассылки имеет ограничяения ыдля отправки большого числа адресов';
              if($this->checkDateInPast($model->sheduller)==true)$msg='Дата и время в планиировщике некорректны.<br />Выберите дату и  время больше текущего времени ';
              //$msg=count($emails).$model->email_to.$model->email_subject;
              $posted = $_POST['Letters'];


              if(!$msg&&count($emails))
              {
			              foreach($emails as $e)
			              {
			              	    if(!empty($e[1]))
			              	    {

					              	    $model->id = null;
										$model->isNewRecord = true;
										$model->userId=Yii::$app->user->id;
							            $model->key = Yii::$app->security->generateRandomString(32);
							            $model->templateId=(int)$model->templateId;
							            $model->client_name=$e[0];
							            $model->sheduller_use=!empty($posted['sheduller'])||count($emails)>20?1:0;
										$model->email_to=$e[1];
										$model->email_body=str_replace('[NAME]',$e[0],$body);
										$model->isMailing=1;
										$model->mailing_from=$posted['mailing_from'];
                                        $model->sheduller=!empty($posted['sheduller'])?$posted['sheduller']:$model->sheduller;
										$model->save();
										$model->isMailing=1;
										$model->mailing_from=$posted['mailing_from'];
										$model->save();
                                             // print_r($model);


								            if($posted['attachments'])$posted['attachments']=explode(',',$posted['attachments']);
                                            $isSend=1;

								                if(is_array($posted['attachments']))
								                 {

													                  foreach($posted['attachments'] as $attach)
													                  {


													                     $link = new Links();
													        			 $link->docId=$attach;
																	     $link->userId = Yii::$app->user->id;
													     				 $link->link_name=Yii::$app->security->generateRandomString(10);
													     				 $link->letterId=$model->id;
													     				 $link->client_name=$model->client_name;
													     				 $link->client_company=$model->client_company;
													     				 $link->client_phone=$model->client_phone;
													     				 $link->isProtect=$link->denyDownloading=$link->isExpire=0;
													     				 $link->askInfo=$model->askLinkInfo;
													     				 $link->save();

													                 }
								      			}

										if(!$model->sheduller_use)
											Yii::$app->slidehelper->sendmailingletter($model);
                                }
			              }
              }
              return !empty($msg)?['failure' => true,'message'=>$msg]:['success' => true];



       }
        //   print_r($model->getErrors());

       return $this->render('send', [
                'model' => $model,
                'isSend' => $isSend,
                'doc'=>$doc

            ]);
    }


        private function ParseFile($model)
    {
         $emails=[];
          $doc =UploadedFile::getInstance($model,'maildb');

          if($doc)
          {

 				$ext = end((explode(".", $doc->name)));
			    $doc_r = Yii::$app->user->id.'_'.Yii::$app->security->generateRandomString(10);
			    $path = Yii::getAlias('@webroot') .'/uploads/excel/' . $doc_r .".{$ext}";
			    $doc->saveAs($path);
		        $data = \moonland\phpexcel\Excel::import($path, [
		        'setFirstRecordAsKeys' => false, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel.
		        'setIndexSheetByName' => false, // set this if your excel data with multiple worksheet, the index of array will be set with the sheet name. If this not set, the index will use numeric.
		    ]);
		    if(count($data[0]))
		    {
				       foreach($data[0] as $k=>$row)
				       {
				       	  if($k==1)continue;
				       	  $emails[]=[0=>$row['A'],1=>$row['B']];
				       }
		    }
		    unlink($path);
       //print_r($data);
       }
       return  $emails;


    }
      private function CorrectNameInEmail($data)
    {
       $emails=[];
       if(count($data))
       {
		       foreach($data as $row)
		       {
		       	  $emails[]=[0=>'',1=>$row];
		       }
       }
       return $emails;
    }
      private function checkDateInPast($sheduller)
{

    if($sheduller)
    {
			        if(strtotime($sheduller)<time())
			        {
			             return true;
			        }

    }
    return false;
  }

}
