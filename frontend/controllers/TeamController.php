<?php

namespace frontend\controllers;

use Yii;
use common\models\Team;
use common\models\User;
use common\models\Logger;
use common\models\TeamMembers;
use frontend\models\TeamInvites;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * TeamController implements the CRUD actions for Team model.
 */
class TeamController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
            'class' =>  \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Team models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Team::find()->joinWith('members',true,'RIGHT JOIN')->where(['memberId' => Yii::$app->user->id]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Team model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
          $model=$this->findModel($id);
      $members = new TeamInvites();
      $name=Yii::$app->request->post('name');
              if(Yii::$app->request->post('hasEditable')&&$model->userId==Yii::$app->user->id&&!empty($name))
        {
        	$model->name=Yii::$app->request->post('name');
        	$model->save();
        	 $out = Json::encode(['output'=>'', 'message'=>'']);
        	 echo $out;
        	 return;
        }
        if ($members->load(Yii::$app->request->post()) && $members->save()) {
                 $members->teamId=$id;
                 $members->userId=Yii::$app->user->id;
                 $members->invite_code=Yii::$app->security->generateRandomString(20);
                 $members->save();
                 $this->SendInvite($model,$members);
                 return $this->redirect(['view', 'id' => $_GET['id']]);
        }
         $leaderProvider = new ActiveDataProvider([
            'query' => User::find()->joinWith('teams',true,'RIGHT JOIN')->where(['teamId' => $id,'member_status'=>1]),
        ]);
          $managerProvider = new ActiveDataProvider([
             'query' => User::find()->joinWith('teams',true,'RIGHT JOIN')->where(['teamId' => $id,'member_status'=>2]),
        ]);
          $invitesProvider = new ActiveDataProvider([
            'query' => TeamInvites::find()->where(['teamId' => $id,'isParsed'=>0]),
        ]);
        return $this->render('view', [
            'model' => $model,
            'members'=> $members,
            'managerProvider'=>$managerProvider,
            'leaderProvider'=>$leaderProvider,
            'invitesProvider'=>$invitesProvider

        ]);
    }

    /**
     * Creates a new Team model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Team();
        $model->name=$_POST['name'];
        $model->userId=Yii::$app->user->id;

        if ($model->validate() && $model->save()) {
            $members=new TeamMembers();
                $members->memberId=Yii::$app->user->id;
                $members->teamId= $model->id;
        	     $members->member_status=1;
        	     $members->save();

           $members=new TeamMembers();
                $members->memberId=Yii::$app->user->id;
                $members->teamId= $model->id;
        	     $members->member_status=2;
        	      $members->save();
        	       Logger::addLog('team','Создана новая команда # '.$model->id);
       	 return $this->redirect(['view', 'id' => $model->id]);
        }
        else
        {
        	//print_r($model->getErrors());
        }
    }

    /**
     * Updates an existing Team model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Team model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        if($model->userId==Yii::$app->user->id)
        {
        	   Logger::addLog('team','Удалена команда # '.$id);
        	$this->findModel($id)->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Team the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Team::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


      public function actionDeleteInvite($id)
    {
        $model=TeamInvites::findOne($id);
        if($model->userId==Yii::$app->user->id)
        {
        	$model->delete();
        }
        return $this->redirect(['team/'.$model->teamId]);
    }
      public function actionDeleteMember()
    {
        $teamId=Yii::$app->request->get('teamId');
        $id=Yii::$app->request->get('id');
                $type=Yii::$app->request->get('type');
        $model=Team::findOne($teamId);
        if($model->userId==Yii::$app->user->id)
        {

           $member=TeamMembers::find()->where(['memberId'=>$id,'teamId'=>$teamId,'member_status'=>$type])->one();
        	$member->delete();
        }
        return $this->redirect(['team/'.$model->id]);
    }
      public function actionActivateInvite()
    {
     $key=Yii::$app->request->get('key');
        $invite=TeamInvites::find()->where(['invite_code'=>$key])->one();
        if($invite->id&&Yii::$app->user->id!=$invite->userId)
        {
              $members=new TeamMembers();
                $members->memberId=Yii::$app->user->id;
                $members->teamId= $invite->teamId;
        	     $members->member_status=$invite->invite_role;
        	      $members->save();
        	$invite->delete();
        	 Yii::$app->getSession()->setFlash('success', 'Вы успешно активировали приглашение в команду!');
        return $this->redirect(['activity/']);
     }
     elseif($invite->id&&Yii::$app->user->id==$invite->userId){
            throw new \yii\web\ForbiddenHttpException('Вы являетесь основателем данной группы и не можете добавить себя в команду');
           }
     else {
            throw new \yii\web\ForbiddenHttpException('Ошибка активации. Возможно ссылка устарела');
           }

    }
       private function SendInvite($model,$members)
    {
          Yii::$app->mailer->compose('invite-body',['model'=>$model,'members'=>$members])
                ->setTo($members->invite_email)
                ->setFrom(Yii::$app->user->identity->email)
                ->setSubject("Приглашение в команду ".$model->name)
                ->send();

            return true;

    }
}
