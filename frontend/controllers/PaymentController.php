<?php

namespace frontend\controllers;
use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use frontend\models\Settings;
use common\models\Links;
use common\models\Letters;
use frontend\models\LinksViews;
use \yii\db\Query;
use yii\helpers\Json;

use common\models\PaymentsPlans;
use common\models\Payment;
class PaymentController extends \yii\web\Controller
{
    public function behaviors() {
    return [
        'access' => [
            'class' =>  \yii\filters\AccessControl::className(),
          'rules' => [
                    [
                        'actions' => ['payonline-success'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

        ],
    ];
}
 public function actionTariff()
    {
      $plan=Yii::$app->request->get('plan');
          $plan1=PaymentsPlans::findOne(1);
          $plan2=PaymentsPlans::findOne(2);
          $plan3=PaymentsPlans::findOne(3);
          if($plan==1)$selected_plan=$plan1;
          elseif($plan==2)$selected_plan=$plan2;
          if($plan==3)$selected_plan=$plan3;

          if($selected_plan->id)
          {
          	$order=new Payment;
          	$order->amount=$selected_plan->total;
          	$order->months=$selected_plan->months;
          	$order->planId=$selected_plan->id;
          	$order->userId=Yii::$app->user->id;
            $order->currency='RUB';
          	$order->save();

          }
       return $this->render('tariff', [
           'plan1' => $plan1,
            'plan2' => $plan2,
             'plan3' => $plan3,
             'selected_plan'=>$selected_plan,
             'order'=>$order
         ]);
    }

  public function actionPayonlineSuccess()
    {

        $params=$_GET;
        if($params['OrderId'])
        {
            $DateTimeV       = isset($_GET['DateTime']) 	 ? 	$_GET['DateTime']  		: '';
			$TransactionIDV  = isset($_GET['TransactionID']) ? 	$_GET['TransactionID']  : '';
			$OrderIdV        = isset($_GET['OrderId']) 		 ? 	$_GET['OrderId']  		: '';
			$AmountV         = isset($_GET['Amount']) 		 ? 	$_GET['Amount']  		: '';
			$CurrencyV       = isset($_GET['Currency']) 	 ? 	$_GET['Currency']  		: '';
			$SecurityKeyV    = isset($_GET['SecurityKey']) 	 ? 	$_GET['SecurityKey']  	: '';
            $baseQuery = "DateTime=".$DateTimeV."&TransactionID=".$TransactionIDV."&OrderId=".$OrderIdV."&Amount=".$AmountV."&Currency=".$CurrencyV."&PrivateSecurityKey=".Yii::$app->payonline->PrivateSecurityKey;
			$hash = md5($baseQuery);
				if($hash==$SecurityKeyV)
				{
				$order=Payment::findOne($params['OrderId']);

				        $order->isPaid=1;
				        $order->transactionId=$params['TransactionID'];
				        $order->payment_by='payonline';
				        $order->save();
				        \common\components\payments\PaymentHelpers::calcPaidDate($order->id);
				        \common\models\Logger::addLog('payment','Новый платёж # '.$order->id.' на сумму'.$order->amount);
		        }
        }
        /*
        foreach ($_GET as $key => $value)
   			 $body .= $key . ' -> ' . $value . '<br>';
        mail('ptiuma@list.ru', 'the subject',$body, null,'-f info@slide.chat');
        */
    }
    public function actionTest()
    {
         \common\components\payments\PaymentHelpers::calcPaidDate(20);
    }
}
