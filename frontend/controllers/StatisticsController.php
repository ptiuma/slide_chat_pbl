<?php

namespace frontend\controllers;
use yii;
use yii\web\Controller;
use common\models\Links;
 use frontend\models\LinksViews;
use common\models\Letters;
use common\models\User;
use frontend\models\UserTemplates;
use common\models\Team;
use common\models\TeamMembers;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use kartik\helpers\Enum;
use common\components\TeamStats;

class StatisticsController extends \yii\web\Controller
{
      public function behaviors() {
    return [
        'access' => [
            'class' =>  \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
    ];
}
   public function actionIndex()
    {
        $from_date=Yii::$app->request->post('from_date');
        $to_date=Yii::$app->request->post('to_date');
        if(!$from_date)$from_date=date('Y-m-d',strtotime("-1 week"));
        if(!$to_date)$to_date=date('Y-m-d');
        $params_model = new \yii\base\DynamicModel([
       'teamid','from_date', 'to_date', 'manager_data'
    ]);
		$params_model->from_date=$from_date;
		$params_model->to_date=$to_date;
		$params_model->manager_data=0;
       return $this->render('index', [
            'stats' => $stats,
         'params_model'=>$params_model,
         ]);
    }



   public function actionStatData()
    {



	}
		public function actionTabsData() {
		   	  $type=Yii::$app->request->post('type');
	  $from_date=Yii::$app->request->post('from_date');
	  $to_date=Yii::$app->request->post('to_date');
	  $teamid=Yii::$app->request->post('teamid');
	  $manager_data=Yii::$app->request->post('manager_data');
	  $from_date= strtotime($from_date);
	  $to_date= strtotime($to_date);
    $datetime1 = new \DateTime(date('Y-m-d',$from_date));
$datetime2 = new \DateTime(date('Y-m-d',$to_date));
$interval = $datetime2->diff($datetime1);
$daysbetween = $interval->format('%a');
if($daysbetween>31)$daysbetween=31;
//echo $daysbetween;
//echo  date('Y-m-d',$datetime1->getTimestamp());
//echo  date('Y-m-d',$datetime2->getTimestamp());
if($teamid)$team=Team::findOne($teamid);
if($team->id)
{
	$users=ArrayHelper::map(User::find()->joinWith('teams',true,'RIGHT JOIN')->where(['teamId' => $team->id])->groupBy('user.id')->asArray()->all(),'','id');
	if(!in_array($team->userId,$users))$users[]=$team->userId;
}
else $users=[Yii::$app->user->id];


        if($type=='3')$stats['total_sent']=(int)Links::find()->andwhere(['IN','userId',$users])->andFilterWhere(['between','created_at',$datetime1->getTimestamp(),$datetime2->getTimestamp()])->count();
        else if($type=='2')
        	$stats['total_sent']=(int)Letters::find()->joinWith('links')->where('links.id is null')->andwhere(['IN','letters.userId',$users])->andFilterWhere(['between','letters.created_at',$datetime1->getTimestamp(),$datetime2->getTimestamp()])->count();
        else if($type=='1')
        	$stats['total_sent']=(int)Letters::find()->joinWith('links')->where('links.id is not null')->andwhere(['IN','letters.userId',$users])->andFilterWhere(['between','links.created_at',$datetime1->getTimestamp(),$datetime2->getTimestamp()])->count();
if($manager_data==1)
{
       $teamProvider = new ActiveDataProvider([
             'query' => User::find()->andwhere(['IN','id',$users])->groupBy('id'),
        ]);

}
else
{
	  if($type=='4')
	  {

	  	        $templs=UserTemplates::find()->andwhere(['IN','userId',$users])->all();
	  	        $stats['total_templates']=count($templs);
	  	        //	$stats['pie'][]= ['category'  => 'Без шаблона', 'column-1' => Letters::find()->where(['templateId'=>0])->andwhere(['IN','userId',$users])->count()];
	  	        foreach($templs as $templ)
	  	        {
					$stats['pie'][]= ['category'  =>$templ->name, 'column-1' => Letters::find()->where(['templateId'=>$templ->id])->count()];
					$stats['column'][]= ['category'  =>$templ->name, 'column-1' => Letters::find()->where(['templateId'=>$templ->id])->count(),
					'column-2' => Letters::find()->where(['templateId'=>$templ->id,'isOpened'=>1])->count(),
					'column-3' => Letters::find()->joinWith('links',true,'RIGHT JOIN')->where(['templateId'=>$templ->id,'links.isOpened'=>1])->groupby('letterId')->count()
					];
				}
	  }
	 else if($type=='3')
	  {
	            $stats['total_opened']=(int)Links::find()->andwhere(['IN','userId',$users])->andFilterWhere(['between','created_at',$datetime1->getTimestamp(),$datetime2->getTimestamp()])->count();
				$stats['pyramid'][]= ['title'  => 'Всего создано', 'value' =>$stats['total_sent'] ];
				$stats['pyramid'][]= ['title'  => 'Открыто ссылок', 'value' => $stats['total_opened']];
				for($z=0;$z<=intval($daysbetween);$z++)
                {
                       $date=new \DateTime(date('Y-m-d',$from_date));
                     if($z>0)
                     {
		                      $interval = new \DateInterval('P'.$z.'D');
		                     $date->add($interval);
                     }
                     $st=$date->getTimestamp();
                	 $stats['line'][]=['category'=>$date->format('Y-m-d'),
                	 'column-1'=>Links::find()->andwhere(['IN','userId',$users])->andFilterWhere(['between','created_at',$st,$st+60*60*24])->count(),
                	 'column-2'=>Links::find()->where(['letterId'=>0,'isOpened' => 1])->andwhere(['IN','userId',$users])->andFilterWhere(['between','links.created_at',$st,$st+60*60*24])->count(),
                	 ];

                }

                                $hours=Enum::timeList('hour');
                 foreach ($hours as $hour)
                {

                	 $stats['hour'][]=['category'=>$hour.':00',

                	 'column-1'=>Links::find()->joinWith('views',true,'RIGHT JOIN')->andwhere(['IN','userId',[1]])->andFilterWhere(['<=','letterId',0])
                	 //->andFilterWhere(['isOpened' => 1])
                	 ->andFilterWhere(['between','views.created_at',$datetime1->getTimestamp(),$datetime2->getTimestamp()])
                	  ->andFilterWhere(['=', new \yii\db\Expression('HOUR( FROM_UNIXTIME(views.created_at))'),(int)$hour])->count(),
                	 ];

                }
	  }
	 else if($type=='2')
    {


                for($z=0;$z<=intval($daysbetween);$z++)
                {
                     $date=new \DateTime(date('Y-m-d',$from_date));
                     if($z>0)
                     {
		                     $interval = new \DateInterval('P'.$z.'D');
		                     $date->add($interval);
                     }
                	 $st=$date->getTimestamp();
                	 $stats['line'][]=['category'=>$date->format('Y-m-d'),
                	 'column-1'=>Letters::find()->joinWith('links')->where('links.id is null')->andwhere(['IN','letters.userId',$users])->andFilterWhere(['between','letters.created_at',$st,$st+60*60*24])->count(),
                	 'column-2'=>Letters::find()->joinWith('links')->where('links.id is null')->andwhere(['IN','letters.userId',$users])->andFilterWhere(['letters.isOpened' => 1])->andFilterWhere(['between','letters.created_at',$st,$st+60*60*24])->count(),

                	 ];

                }
                $hours=Enum::timeList('hour');
                 foreach ($hours as $hour)
                {

                	 $stats['hour'][]=['category'=>$hour.':00',
                	 'column-1'=>Letters::find()->joinWith('links')->where('links.id is null')->andwhere(['IN','letters.userId',$users])
                	 ->andFilterWhere(['letters.isOpened' => 1])
 				     ->andFilterWhere(['between','letters.created_at',$datetime1->getTimestamp(),$datetime2->getTimestamp()])
                	 ->andFilterWhere(['=', new \yii\db\Expression('HOUR( FROM_UNIXTIME(letters.created_at))'),(int)$hour])->count(),

                	 ];
                }
                $stats['total_opened']=(int)Letters::find()->joinWith('links')->where('links.id is null')->andwhere(['IN','letters.userId',$users])->andFilterWhere(['between','letters.created_at',$datetime1->getTimestamp(),$datetime2->getTimestamp()])
                ->andFilterWhere(['letters.isOpened' => 1])->count();

				$stats['pyramid'][]= ['title'  => 'Всего отправлено', 'value' => $stats['total_sent']];
				$stats['pyramid'][]= ['title'  => 'Всего доставлено', 'value' => $stats['total_sent']];
				$stats['pyramid'][]= ['title'  => 'Открыто писем', 'value' => $stats['total_opened']];

    }
	      else
    {


                for($z=0;$z<=intval($daysbetween);$z++)
                {
                     $date=new \DateTime(date('Y-m-d',$from_date));
                     if($z>0)
                     {
		                     $interval = new \DateInterval('P'.$z.'D');
		                     $date->add($interval);
                     }
                	 $st=$date->getTimestamp();
                	 $stats['line'][]=['category'=>$date->format('Y-m-d'),
                	 'column-1'=>Letters::find()->andwhere(['IN','userId',$users])->andFilterWhere(['between','created_at',$st,$st+60*60*24])->count(),
                	 'column-2'=>Letters::find()->andwhere(['IN','userId',$users])->andFilterWhere(['isOpened' => 1])->andFilterWhere(['between','created_at',$st,$st+60*60*24])->count(),
                	 'column-3'=>Links::find()->andwhere(['IN','userId',$users])->andFilterWhere(['>','letterId',0])->andFilterWhere(['between','created_at',$st,$st+60*60*24])->groupBy('letterId')->count(),
                	 ];

                }
                $hours=Enum::timeList('hour');
                 foreach ($hours as $hour)
                {

                	 $stats['hour'][]=['category'=>$hour.':00',
                	 'column-1'=>Letters::find()->andwhere(['IN','userId',$users])
                	 ->andFilterWhere(['isOpened' => 1])
 				     ->andFilterWhere(['between','created_at',$datetime1->getTimestamp(),$datetime2->getTimestamp()])
                	 ->andFilterWhere(['=', new \yii\db\Expression('HOUR( FROM_UNIXTIME(created_at))'),(int)$hour])->count(),
                	 'column-2'=>Links::find()->joinWith('views',true,'RIGHT JOIN')->andwhere(['IN','userId',[1]])->andFilterWhere(['>','letterId',0])
                	 ->andFilterWhere(['between','views.created_at',$datetime1->getTimestamp(),$datetime2->getTimestamp()])
                	  ->andFilterWhere(['=', new \yii\db\Expression('HOUR( FROM_UNIXTIME(views.created_at))'),(int)$hour])->count(),
                	 ];
                }
                $stats['total_opened']=(int)Letters::find()->andwhere(['IN','userId',$users])->andFilterWhere(['between','created_at',$datetime1->getTimestamp(),$datetime2->getTimestamp()])
                ->andFilterWhere(['isOpened' => 1])->count();
                $stats['total_opened_att']=(int)Links::find()->joinWith('views',true,'RIGHT JOIN')->andwhere(['IN','userId',$users])->andFilterWhere(['>','letterId',0])
                ->andFilterWhere(['between','links.created_at',$datetime1->getTimestamp(),$datetime2->getTimestamp()])
                ->groupby('links.id')->count();
				$stats['pyramid'][]= ['title'  => 'Всего отправлено', 'value' => $stats['total_sent']];
				$stats['pyramid'][]= ['title'  => 'Всего доставлено', 'value' => $stats['total_sent']];
				$stats['pyramid'][]= ['title'  => 'Открыто писем', 'value' => $stats['total_opened']];
				$stats['pyramid'][]= ['title'  => 'Открыто вложений', 'value' => $stats['total_opened_att']];

    }
}

		  $html = $this->renderPartial($type==4?'_charts_templates':'_charts_letters',[
		    'stats'=>$stats,
		    'type'=>$type,
		    'manager_data'=>$manager_data,
		    'teamProvider'=>$teamProvider,
		    ]);

		   return Json::encode($html);
		}
     public function actionTest()
    {
        $stat=new TeamStats;
        $stat->genTeamStat(7);
			//	echo  $command->getRawSql();
	  	       exit;
     return $this->render('test', [
             ]);
    }
	      public function actionTeam()
    {
        $id=Yii::$app->request->get('id');
        $team=Team::findOne($id);
        $from_date=Yii::$app->request->post('from_date');
        $to_date=Yii::$app->request->post('to_date');
        $from_date2=Yii::$app->request->get('from_date');
        $to_date2=Yii::$app->request->get('to_date');
        if(!$from_date&&!empty($from_date2))$from_date=$from_date2;
        if(!$to_date&&!empty($to_date2))$to_date=$to_date2;
        if(!$from_date)$from_date=date('Y-m-d',$team->created_at);
        if(!$to_date)$to_date=date('Y-m-d');
          $teamProvider = new ActiveDataProvider([
             'query' => User::find()->joinWith('teams',true,'RIGHT JOIN')->where(['teamId' => $id])->groupBy('user.id'),
        ]);
       $users=ArrayHelper::getColumn(TeamMembers::find()->where(['teamId' => $id])->groupBy('memberId')->asArray()->all(),'memberId');
      // $users=memberId
       //print_r($users);
      // echo $id;
       if(Yii::$app->user->id!==$team->userId&&!in_array(Yii::$app->user->id,$users))
         return $this->redirect(['/']);


              $mostviewProvider = new ActiveDataProvider([
             'query' => Links::find()->joinWith('views',true,'RIGHT JOIN')
             ->where(['IN','userId',$users]),
        ]);

$params_model = new \yii\base\DynamicModel([
     'teamid','from_date', 'to_date', 'manager_data'
    ]);
$params_model->from_date=$from_date;
$params_model->to_date=$to_date;
$params_model->manager_data=0;
$params_model->teamid=$team->id;
     return $this->render('team', [
     'team' => $team,
     'stats' => $stats,
    'params_model'=>$params_model,
             ]);
    }
}
