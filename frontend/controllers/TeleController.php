<?php

namespace frontend\controllers;
use yii;
use \yii\db\Query;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use frontend\models\Settings;
use common\models\Links;
use common\models\Letters;
use frontend\models\LinksViews;
use frontend\models\TelegramToken;
use ptiuma\chat\models\Chat;
use ptiuma\chat\models\ChatMessages;
		 use ElephantIO\Client;
		 use ElephantIO\Engine\SocketIO\Version1X as Version1X;
class TeleController extends \yii\web\Controller
{
     public function beforeAction($action) {
        $this->enableCsrfValidation = ($action->id !== "hook");
        return parent::beforeAction($action);
    }
 public function actionHook()
    {

	 $bot = new \TelegramBot\Api\Client(Yii::$app->params['telegramBot']);

          //  exit;
		    $bot->command('start', function ($message) use ($bot) {
				$msg='Добро пожаловать в Slide.Chat';
		        $bot->sendMessage($message->getChat()->getId(), $msg);
		    });
		    $bot->command('help', function ($message) use ($bot) {
				$msg='Все доступные опции Вы можете глянуть  на сайте "Slide.Chat" в разделе "Интеграция"'."\n";
				$msg.="/help - помощь\n";
				$msg.="/activity - кол-во горячих и тёплых клиентов\n";
		        $bot->sendMessage($message->getChat()->getId(), $msg);
		    });
		    $bot->command('activity', function ($message) use ($bot) {

                TeleController::setActivity($message,$bot);

		    });
		    $bot->command('chat', function ($message) use ($bot) {

                TeleController::saveAnswer($message,$bot);

		    });
		      $bot->command('myinfo', function ($message) use ($bot) {

                $answer=explode(' ', $message->getText(true), 3);
                $chat=Chat::findOne($answer[1]);
                if(!$chat->id||$chat->user->telegram->chat_id!=$message->getChat()->getId())
                {
		                $msg='Ошибка! Уточните айди чата';
				        $bot->sendMessage($message->getChat()->getId(), $msg);
				        exit;
                }
                  $bot->sendMessage($message->getChat()->getId(), 'Информация выведена клиенту');
		          TeleController::emitChat($chat->id,'myinfo');

		    });
		      $bot->command('clientinfo', function ($message) use ($bot) {

                $answer=explode(' ', $message->getText(true), 3);
                $chat=Chat::findOne($answer[1]);
                if(!$chat->id||$chat->user->telegram->chat_id!=$message->getChat()->getId())
                {
		                $msg='Ошибка! Уточните айди';
				        $bot->sendMessage($message->getChat()->getId(), $msg);
				        exit;
                }
                  $bot->sendMessage($message->getChat()->getId(), 'Информация выведена клиенту');
		          TeleController::emitChat($chat->id,'myinfo');

		    });

		    $bot->command('sendactivationcode', function ($message) use ($bot) {
				$code=explode(' ',$message->getText(true));
				$code=$code[1];
      $model=TelegramToken::find()->where(['activation_code'=>$code])->one();
      if($code&&$model->id)
      {
      	 $model->chat_id=$message->getChat()->getId();
      	 $model->isActive=1;
      	 $model->save();
      	 $msg="Ваш код успешно активирован! \n Теперь Вы сможете моментально отслеживать активность ваших клиентов";

      }
      else
      {
       $msg="Произошла ошибка активации\nУточните код активации на сайте 'Slide.Chat' в разделе 'Интеграция'";
      }
		        $bot->sendMessage($message->getChat()->getId(), $msg);
		    });
		    $bot->command('ping', function ($message) use ($bot) {
		      TeleController::pong();
		     $bot->sendMessage($message->getChat()->getId(), $message->getText(true));
		        $bot->sendMessage($message->getChat()->getId(), 'pong!');
		       // $this->checkJSON($chatID,$message);
		    });

		    $bot->run();


    }
     public function actionTest()
    {

       TeleController::pong();
     	//;$bot = new \TelegramBot\Api\Client(Yii::$app->params['telegramBot']);
      	// $bot->sendMessage('194823463', 'pong!');

    }
 public function actionEmit($action='myinfo')
    {


		$client = new Client(new Version1X(Yii::$app->params['socketServer'].':3000'));

		$client->initialize();
		$client->emit($action, ['sid' => 'CLJZcGCFPgl5SpCEAAAA']);
		$client->close();
    }
    public static function setActivity($message,$bot)
    {

    			$model=TelegramToken::find()->where(['chat_id'=>$message->getChat()->getId()])->one();
    			if($model->id)
    			{
		    			$msg="Клиенты онлайн: ".LinksViews::Activity($model->userId,0)."\n";
		    			$msg.="Горячие клиенты: ".LinksViews::Activity($model->userId,2)."\n";
		    			$msg.="Тёплые клиенты: ".LinksViews::Activity($model->userId,1)."\n";
				        $bot->sendMessage($message->getChat()->getId(), $msg);
		        }
    }
    public static function saveAnswer($message,$bot)
    {
                $model=new ChatMessages;
                $answer=explode(' ', $message->getText(true), 3);
                $chat=Chat::findOne($answer[1]);
                if(!$chat->id||$chat->user->telegram->chat_id!=$message->getChat()->getId())
                {
		                $msg='Ошибка! Уточните айди чата';
				        $bot->sendMessage($message->getChat()->getId(), $msg);
                }
                else
                {
		                $model->chatId=$chat->id;
		                $model->message=$answer[2];
		                $model->replyTo=$answer[1];
		                $model->isTelegram=1;
		                $model->isManager=1;
		                $model->msgTime=time();
		                if($model->save())
		                {

		                                  $msg='Ответ доставлен';
				        	$bot->sendMessage($message->getChat()->getId(), $msg);
				          	self::emitChat($chat->id);
		                }
		                else
		                {
			                $msg='Ошибка! ответ не доставлен'.$answer[2];
					        $bot->sendMessage($message->getChat()->getId(), $msg);
				        }
		        }
    }
 public static function emitChat($sid,$action='refreshchat')
    {

        if($sid)
        {
				$client = new Client(new Version1X(Yii::$app->params['socketServer'].':3000'));

				$client->initialize();
				$client->emit($action, ['sid' => $sid]);
				$client->close();
		}
    }
	public static function checkJSON($chatID,$update){

	    $myFile = "frontend/controllers/log.txt";
	    $updateArray = print_r($update,TRUE);
	    $fh = fopen($myFile, 'a') or die("can't open file");
	    fwrite($fh, $chatID ."nn");
	    fwrite($fh, $updateArray."nn");
	    fclose($fh);
	}
		public static function pong(){
       echo 'yyyyy';
	}
}
