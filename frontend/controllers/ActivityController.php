<?php

namespace frontend\controllers;
use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use frontend\models\Settings;
use common\models\Links;
use common\models\Letters;
use frontend\models\LinksViews;
use \yii\db\Query;
use yii\helpers\Json;

use frontend\models\LinksViewsSearch;

class ActivityController extends \yii\web\Controller
{
    public function behaviors() {
    return [
        'access' => [
            'class' =>  \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
    ];
}
 public function actionIndex()
    {

             $viewsProvider = new ActiveDataProvider([

              'query' => LinksViews::find()->joinWith('link')
              ->where(['userId'=>Yii::$app->user->id])
              ->orderBy('id desc')->limit(5),
                        'pagination' => false,
        ]);

$letters = Letters::find()->active()->select([new \yii\db\Expression('1 as type'),'id','userId','created_at']);
$links = Links::find()->active()->noletter()->select([new \yii\db\Expression('2 as type'),'id','userId','created_at']);
$letters->union($links, true)->orderBy(['created_at' => SORT_ASC]);


$unionQuery = (new \yii\db\Query())
	->select('*')
    ->from(['u_name' =>$letters])
    ->where(['userId'=>Yii::$app->user->id])
    ->orderBy(['created_at' => SORT_ASC]);

$unionProvider = new ActiveDataProvider([
    'query' => $unionQuery->orderBy('created_at desc')->limit(5),
    'pagination' => false,
]);



       return $this->render('index', [
           // 'searchModel' => $searchModel,
            'viewsProvider' => $viewsProvider,
            'linksProvider' => $linksProvider,
            'lettersProvider' => $unionProvider,
         ]);
    }
 public function actionVisits()
    {

                $searchModel = new LinksViewsSearch;
        $email=Yii::$app->request->get('email');
         if(Yii::$app->request->get('check')==1)$searchModel->client_email=str_replace('%40','@',$email);
          $keyword=Yii::$app->request->get('keyword');
        $searchModel->keyword=$keyword;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->render('visits', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);

    }
public function actionClient()
{
    $id=Yii::$app->request->get('id');
    $model=Links::findOne($id);
	if(!$model->id||$model->userId!==Yii::$app->user->id)
	{
		throw new NotFoundHttpException('Вы не имеете доступа к этим данным');
	}
	 return $this->render('client', [
           // 'searchModel' => $searchModel,
            'model' => $model,
             ]);
}

public function actionTaskArchive()
{
    $id=Yii::$app->request->get('id');
    $type=Yii::$app->request->get('type');

    if($type==2)$model=Links::findOne($id);
	else if($type==1)$model=Letters::findOne($id);
	if($model->userId==Yii::$app->user->id)
	{
		$model->isArchive=1;
		$model->save();
	}
	  Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['success' => true];
}

    public function actionTest()
    {
    	   $links = Links::find()->joinWith('views',true,'RIGHT JOIN')->all();
    	   foreach($links as $link)
    	   {
    	  // print_r($link);
    	  // exit;
    	    echo $link->link_name."--".$link->id."--".$link->isOpened."--".count($link->views)."-- ".$link->views_count."<br>";
    	   	 if($link->views_count>0)
    	   	 {
    	   	 $link->isOpened=1;
             $link->save();
              print_r($link->getErrors());
             }
    	   }
    }

//history
 public function actionHistory()
    {

$letters = Letters::find()->select([new \yii\db\Expression('1 as type'),'id','userId','created_at','isArchive','isOpened']);
$links = Links::find()->select([new \yii\db\Expression('2 as type'),'id','userId','created_at','isArchive','isOpened'])->andFilterWhere(['letterId' => 0]);


$letters->union($links, true)->andFilterWhere(['userId' => Yii::$app->user->id])->orderBy(['created_at' => SORT_DESC]);


$counters['total_noactive']=(new \yii\db\Query())->from(['u_name' =>$letters])->where(['userId'=>Yii::$app->user->id])->andFilterWhere(['isArchive' => 0,'isOpened' => 0])->count();
$counters['total_archive']=(new \yii\db\Query())->from(['u_name' =>$letters])->where(['userId'=>Yii::$app->user->id])->andFilterWhere(['isArchive' => 1])->count();
$counters['total_active']=(new \yii\db\Query())->from(['u_name' =>$letters])->where(['userId'=>Yii::$app->user->id])->andFilterWhere(['isArchive' => 0,'isOpened' => 1])->count();
$counters['total_all']=$counters['total_active']+$counters['total_noactive'];
$counters['total_shed']=Letters::find()->where(['userId'=>Yii::$app->user->id])->andwhere(['>','sheduller',0])->andwhere(['=','sheduller_use',1])->count();
$unionQuery = (new \yii\db\Query())
	->select('*')
    ->from(['u_name' =>$letters])
    ->where(['userId'=>Yii::$app->user->id])
    ->orderBy(['created_at' => SORT_DESC]);

$isArchive=0;
$tab=!$_REQUEST['tab']?'active':$_REQUEST['tab'];
if($_REQUEST['tab']=='noactive')
{

 $unionQuery->andFilterWhere(['isArchive' => 0,'isOpened' => 0]);
    $label='Не  открытые';

}
else if($_REQUEST['tab']=='active')
{
    $unionQuery->andFilterWhere(['isArchive' => 0,'isOpened' => 1]);
    $label='Открытые';
}
else if($_REQUEST['tab']=='all')
{
        $unionQuery->andFilterWhere(['isArchive' => 0]);
	 $label='Все записи';
}
else if($_REQUEST['tab']=='archive')
{
        $unionQuery->andFilterWhere(['isArchive' => 1]);
    $label='Архив';
$isArchive=1;
}
else
{
	    $unionQuery->andFilterWhere(['isArchive' => 0,'isOpened' => 1]);
}




$unionProvider = new ActiveDataProvider([
    'query' => $unionQuery,
    'pagination' => [
        'pageSize' => 5,
    ],
]);


       if (Yii::$app->request->isAjax) {
       	      $html = $this->renderPartial('_tabs_content', [
            'dataProvider' => $unionProvider,
            'label'=>$label,
            'tab'=>$tab,
            'isArchive'=>$isArchive
        ]);
         return Json::encode($html);
       }
       return $this->render('history', [
            'dataProvider' => $unionProvider,
            'tab'=>$tab,
            'counters'=>$counters
         ]);
    }

    public function actionChatArchive()
{
    $id=Yii::$app->request->get('id');


    $model=LinksViews::findOne($id);
 	if (Yii::$app->request->isAjax) {
		            return $this->renderAjax('chat', [
		                'model' => $model,
		            ]);
    }
}
    public function actionTest2()
    {
    	$links = Links::find()->andFilterWhere(['letterId' => 0])->all();
    	foreach($links as $link)
    	{
    		if($link->views_count>0)
    		{
    			$link->isOpened=1;
    			$link->save();
    		}
    		echo $link->views_count." = ".$link->isOpened."<br>";
    	}
    }


    public function actionExportLetters()
    {

				        \moonland\phpexcel\Excel::export([
				    'models' => Letters::find()->andwhere(['userId'=>Yii::$app->user->id])->orderby('id desc')->limit(3000)->all(),
				     'mode' => 'export',
				        'columns' => [
				            'email_to',
				             'email_subject',
                             'client_name',
                              'client_phone',
                             'sheduller',
                        //     'isOpened',
                             [
                    'attribute' => 'opened_at',
                    'header' => 'Открыто',
                         'value' => function($model) {
                        return !empty($model->opened_at)?date('Y-m-d H:i', $model->opened_at):'';
                    },
            ],
                       //    'opened_at:datetime',
                             'created_at:datetime'
				        ],

				]);

    }
             public function actionShedullerLetters()
    {

        $dataProvider = new ActiveDataProvider([
    'query' => Letters::find()
    ->andwhere(['userId'=>Yii::$app->user->id])
    ->andwhere(['>','sheduller',0])
    ->andwhere(['=','sheduller_use',1])
    ->orderBy('id desc'),
    'pagination' => [
        'pageSize' => 24,
    ],
]);


          return $this->render('sheduller-letters', [
            'dataProvider' => $dataProvider,

        ]);

    }
         public function actionMultipleEmailDelete()
    {
         $ids=Yii::$app->request->post('row_id');
        Letters::deleteAll(['id'=>$ids,'userId'=>Yii::$app->user->id]);
		return;
     }
}
