<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;

use common\models\Documents;
use common\models\DocumentsPages;
use common\models\Links;
use frontend\models\LinksViews;
use common\models\User;
use frontend\models\Presentations;
use frontend\models\VisitorsInfo;
use frontend\models\AddressBook;
use kartik\helpers\Enum;

use frontend\models\PhoneContactForm;
use frontend\models\MessageContactForm;
/**
 * View controller
 */
class ViewController extends Controller
{
   public $layout = 'viewer';
    /**
     * Displays viewer.
     *
     * @return mixed
     */
    public function actionIndex()
    {

     $key=Yii::$app->request->get('slug');

        $browser = Enum::getBrowser();
        $devices_type=Yii::$app->params['devicedetect'];
        if($devices_type['isDesktop']==1)$mobile=1;
        elseif($devices_type['isMobile']==1)$mobile=2;
        elseif($devices_type['isTablet']==1)$mobile=3;

		$link=$this->findModel($key);

$phone_model = new PhoneContactForm;
$message_model = new MessageContactForm;
$session = Yii::$app->session;
$visitor_info = $session->get('visitor_info');
$visitor_info_model = new \yii\base\DynamicModel(['name','email','phone']);
$visitor_info_model->addRule(['name','email','phone'], 'string', ['max' => 28]);
$visitor_info_model->addRule(['name','email','phone'], 'required');
$visitor_info_model=new VisitorsInfo;
if($link->isArchive==1||$link->isBlocked==1||($link->isExpire==1&&(strtotime($link->expire_date)<time())))
{
  return $this->render('_blocked', [
            'model' => $link,

        ]);
}
else if(($link->askInfo=='1'||$link->letter->isMultiEmail)&&empty($visitor_info))
{
		if ($visitor_info_model->load(Yii::$app->request->post())) {
					if ($visitor_info_model->validate()) {
			          $visitor_info['name']=$visitor_info_model->name;
			          $visitor_info['phone']=$visitor_info_model->phone;
			          $visitor_info['email']=$visitor_info_model->email;

			              $abook=new AddressBook;
			              $abook->userId=$link->userId;
			              $abook->email=$visitor_info_model->email;
			              $abook->name=$visitor_info_model->name;
			              $abook->phone=$visitor_info_model->phone;
			              $abook->save();
			              //print_r($abook->getErrors());

			            // exit;
			          $session->set('visitor_info', $visitor_info);


			          $this->refresh();
					} else {
					    $errors = $visitor_info_model->errors;

					}
		}



							  return $this->render('_visitor_info', [
						            'model' => $link,
						             'visitor_info_model'=>$visitor_info_model
						        ]);
}
else if($link->isProtect=='1'&&!empty($link->protect_code))
{
if($_POST['DynamicModel']['protect_code'])$protect_code=$_POST['DynamicModel']['protect_code'];
		if(isset($protect_code)&&$protect_code==$link->protect_code){}
		else
		{
				$protect_model = new \yii\base\DynamicModel(['protect_code']);
				$protect_model->addRule(['protect_code'], 'string', ['max' => 128]);
						$protect_model->addRule(['protect_code'], 'required');
					  return $this->render('_protect_code', [
				            'model' => $link,
				             'protect_model'=>$protect_model
				        ]);
		}
}
           $view=new LinksViews;
           $view->linkId=$link->id;
           $view->client_session=Yii::$app->security->generateRandomString(7);
           $view->client_browser=$browser['name'];
           $view->client_os=$browser['platform'];
           $view->client_device=$mobile;
  		   $view->client_name=$visitor_info['name'];
		   $view->client_phone=$visitor_info['phone'];
		   $view->client_email=$visitor_info['email'];
		   $view->client_ip=Yii::$app->getRequest()->getUserIP();
           $view->downloads=1;
           $view->created_at=$view->started_at=$view->updated_at=time();
			$view->ended_at=0;
           $ip=Yii::$app->getRequest()->getUserIP();
           if($ip!='127.0.0.1'&&$link->doc->noParsed==1)
           {
           	 $geo=yii::$app->geolocation->getInfo();
              $geo=json_decode($geo);
           $view->client_country=$geo->country_name?$geo->country_name:'';
           $view->client_city=$geo->city?$geo->city:'';
           $view->client_ip=Yii::$app->getRequest()->getUserIP();
           }
           $view->save();
if($link->user->settings->send_email_alert!=3) Yii::$app->slidehelper->sendviewalert($link);
if($link->doc->noParsed==1)
{
	$file=$link->doc->dir.'/'.$link->doc->filename;
//        echo $key;
        if (file_exists($file)) {
           $view->ended_at=time();
           $view->save();

           $link->isOpened=1;
           $link->save();

		   Yii::$app->response->sendFile($file,$link->doc->name);

		  }
}

if ($message_model->load(Yii::$app->request->post()) && $message_model->validate()) {
    // do what you want
}
		$chat=new \ptiuma\chat\models\Chat;
		$chat->viewId=$view->id;$chat->userId = $view->link->userId;
		$chat->save();
		if($link->user->telegram->chat_id)
		{
		      $msg='';
		      $msg.='Имя клиента: '.$view->visitor_name."\r\n";
		      $msg.="Вы можете:\r\n";
		      $msg.="1.отправить сообщение пользователю в чат\r\n /chat ".$chat->id." ваш_ответ";
		      $msg.="\r\n2.вывести клиенту свою визитную карточку \r\n /myinfo ".$chat->id."";
		      \Yii::$app->bot->sendMessage($link->user->telegram->chat_id, $msg);
		}
$phone_model->viewId=$view->id;
$message_model->viewId=$view->id;
           $link->isOpened=1;
           $link->save();
        return $this->render('index', [
            'model' => $link,
            'view'=>$view,
            'chat'=>$chat,
            'phone_model'=>$phone_model,
            'message_model'=>$message_model
        ]);
    }

   public function actionPdf(){
        $this->layout = 'viewer_pdf';
   $key=Yii::$app->request->get('slug');
   $link=$this->findModel($key);
   $phone_model = new PhoneContactForm;
$message_model = new MessageContactForm;
 $view=new LinksViews;
           $view->linkId=$link->id;
           $view->client_session=Yii::$app->security->generateRandomString(7);
           $view->client_browser=$browser['name'];
           $view->client_os=$browser['platform'];
           $view->client_device=$mobile;
  		   $view->client_name=$visitor_info['name'];
		   $view->client_phone=$visitor_info['phone'];
		   $view->client_email=$visitor_info['email'];
		   $view->client_ip=Yii::$app->getRequest()->getUserIP();
           $view->downloads=1;
           $view->created_at=$view->started_at=$view->updated_at=time();
			$view->ended_at=0;
           $ip=Yii::$app->getRequest()->getUserIP();
           if($ip!='127.0.0.1'&&$link->doc->noParsed==1)
           {
           	 $geo=yii::$app->geolocation->getInfo();
              $geo=json_decode($geo);
           $view->client_country=$geo->country_name?$geo->country_name:'';
           $view->client_city=$geo->city?$geo->city:'';
           $view->client_ip=Yii::$app->getRequest()->getUserIP();
           }
           $view->save();
          return $this->render('pdf', [
            'model' => $link,
            'view'=>$view,
            'chat'=>$chat,
            'phone_model'=>$phone_model,
            'message_model'=>$message_model
        ]);
}

  //presentation
      public function actionPresentation()
    {
     $key=Yii::$app->request->get('id');
    $presentation=Presentations::find()->where(['id'=>$key,'isClosed'=>0])->one();
    $user=User::find()->where(['id'=>$presentation->userId])->one();
    if(!$presentation->id)
    {

    	  return $this->render('_blocked', [
            'model' => $link,

        ]);
    }
    $room=$user->id;
    $isManager=$room==Yii::$app->user->id?1:0;

    $doc=Documents::findOne($presentation->docId);
    if(!$presentation->chat->id)
    {
    			$chat=new \ptiuma\chat\models\Chat;
		$chat->presentationId=$presentation->id;$chat->userId = $presentation->userId;
		$chat->save();
    }
    else $chat=$presentation->chat;
     return $this->render('presentation', [
            'doc' => $doc,
            'presentation'=>$presentation,
            'chat'=>$chat,
            'isManager'=>$isManager
        ]);
    }
    public function actionDownloads()
    {

        $key=Yii::$app->request->get('key');
		$model=$this->findModel($key);
		$file=$model->doc->dir.'/'.$model->doc->filename;
//        echo $key;
        if (file_exists($file)) {

		   Yii::$app->response->sendFile($file);

		  }


    }
    public function actionPhoneContact()
    {

        $post=Yii::$app->request->post('phone');
		$phone_model = new PhoneContactForm;
		if ($phone_model->load(Yii::$app->request->post()) && $phone_model->validate()) {
		$view=LinksViews::findOne($phone_model->viewId);
		     Yii::$app->mailer->compose('phone-contact',['model'=>$view,'model_message'=>$phone_model])
                ->setTo($view->link->user->email)
                ->setFrom([Yii::$app->params['adminEmail']=> Yii::$app->params['name']])
                ->setSubject('Запрос на телефонный звонок')
                ->send();
        if($view->link->user->settings->send_telegram_alert!=2 && $view->link->user->telegram->chat_id)
		{
		      $msg="Клиент запрашивает телефонный звонок.\r\n";
		      $msg.="Документ: ".$view->link->doc->name."\r\n";
		      $msg.='Имя: '.$phone_model->name."\r\n";
		      $msg.="Телефон: ".$phone_model->phone."";
		      \Yii::$app->bot->sendMessage($view->link->user->telegram->chat_id, $msg);
		}
                 echo success;
		}else {
        	exit;
        }

    }
    public function actionMessageContact()
    {

$message_model = new MessageContactForm;
		if ($message_model->load(Yii::$app->request->post()) && $message_model->validate()) {
		     $view=LinksViews::findOne($message_model->viewId);
		     Yii::$app->mailer->compose('message-contact',['model'=>$view,'model_message'=>$message_model])
                ->setTo($view->link->user->email)
                ->setFrom([Yii::$app->params['adminEmail']=> Yii::$app->params['name']])
                ->setSubject('Сообщение от клиента')
                ->send();
                 echo success;
        if($view->link->user->settings->send_telegram_alert!=2 && $view->link->user->telegram->chat_id)
		{
		      $msg="Клиент отправил сообщение через контактную форму.\r\n";
		      $msg.="Документ: ".$view->link->doc->name."\r\n";
		      $msg.='Имя: '.$message_model->name."\r\n";
		      $msg.='Телефон: '.$message_model->phone."\r\n";
		      $msg.="Email: ".$message_model->email."\r\n";
		      $msg.="Сообщение: ".$message_model->message."\r\n";
		      \Yii::$app->bot->sendMessage($view->link->user->telegram->chat_id, $msg);
		}

		}
		else {
        	exit;
        }


    }
      protected function findModel($id)
    {
        if (($model = Links::findOne(['link_name'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

       public function actionPagePresentation()
    {
        $page=DocumentsPages::findOne(Yii::$app->request->post('pageId'));
         $presentation=Presentations::findOne(Yii::$app->request->post('presId'));

        if($presentation->userId==Yii::$app->user->id)
        {

        	$presentation->pageId=$page->id;
        	$presentation->save();
         }

    }
       public function actionClosePresentation()
    {
		Presentations::updateAll(['isClosed' => 1], ['=', 'userId', Yii::$app->user->id]);
    }
       public function actionSendChat()
    {
		if (!empty($_POST)) {
            //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
         		    echo \ptiuma\chat\ChatRoom::sendChat($_POST);
	    }
        exit;
      // return;
    }
   	public function actionPreviewDoc()
    {

        $id=Yii::$app->request->get('id');
	 $model=Documents::findOne($id);
            $viewid=Yii::$app->request->get('viewid');
      $view=LinksViews::findOne($viewid);
      $chat=$view->chat;
        if($model->userId!==Yii::$app->user->id)
        return $this->redirect(['/activity/']);



if($model->noParsed==1)
{
	$file=$model->dir.'/'.$model->filename;
//        echo $key;
        if (file_exists($file)) {


		   Yii::$app->response->sendFile($file,$model->name);

		  }
}

         return $this->render('preview', [
            'model' => $model,
           'view'=>$view,
           'chat'=>$chat,
           'runSocket'=>(!empty($chat->id)?1:0),
        ]);
    }
 	public function actionDocLink()
    {

        $id=Yii::$app->request->get('id');
	 $model=Documents::findOne($id);

         return $model->fulldoc;
    }
 	public function actionSaveDocPresentation()
    {

        $presId=Yii::$app->request->post('presId');
                $docId=Yii::$app->request->post('docId');
         $pres=Presentations::find()->where(['userId' => Yii::$app->user->id,'id'=>$presId])->one();
         if($pres->id)
         {
		         $pres->docId=$docId;
		         $pres->pageId=1;
		         $pres->save();
         }
         return true;
    }
}
