<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/frontend/assets';
    public $css = [
        'css/site.css',
        'css/plans.css',
              'plugins/offline/offline-theme-chrome.css',
            'plugins/offline/offline-language-english.css',
                'plugins/offline/offline-language-english-indicator.css',
        //'css/checkboxes.css',
    ];
    public $js = [
    'js/clipboard.min.js',
    'js/main.js',
       'plugins/offline/offline.min.js',
     ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
