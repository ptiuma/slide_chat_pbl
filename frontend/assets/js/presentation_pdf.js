var ymap_geo; var ymaps;
var pointTime;
var sessionid;
var update_stats= presentation_config.isTranslation==1||presentation_config.preview==1?0:1


$(document).ready(function () {

if(RunPresentation==1)
{
   socket = io.connect(SOCKET_SERVER+':3000/');
		    socket.on('connect', function () {
		    if(presentation_config.isView!=1)
		    {
			    socket.emit('create room',presentation_config);
			    if(update_stats==1)
		   		 {
			    socket.emit('linkstat',presentation_config);
		        socket.emit('pagestat',presentation_config);
		        }
             }
		    });
         socket.on('refreshchat', function (data) {
					$("#chatPop").show();

				    reloadchat('', false);
								});
		   socket.on('myinfo', function (data) {
					$("#main-chat").show();
								});
           socket.on('endpresent', function (data) {
								    $("#endPresent").modal('show');
					});
					   socket.on('changedoc', function (data) {

						       UpdateDoc(data)
						});
								socket.on('changepage', function (data) {

						        if(data!=PDFViewerApplication.pdfViewer.currentPageNumber)
						        {
						        	PDFViewerApplication.pdfViewer.currentPageNumber=data;
						        }
						});
								socket.on('closepointer', function (data) {
						        $("#dragpointer").hide(3000)
						});
						socket.on('updatepointer', function (data) {
							data.xPos=presentation_config.isManager==1?data.xPos:data.xPos-0;
						         $("#dragpointer").css({'z-index':'9999','position': 'absolute','top': data.yPos, 'left' : data.xPos})
           						 $("#dragpointer").show()
						});


								socket.on('updateusers', function (data) {
								if(presentation_config.isTranslation==1) $("#users-num").html(data);
								else if(presentation_config.preview==1)
								{
  								   if(data<='0')
								   {
								     $("#isClientOnline").addClass('btn-danger')
								    $("#isClientOnlineSt").html('Клиент оффлайн');
								   }
									 }
								});

$(window).on('beforeunload', function(){
    socket.close();
});
             if(ymaps)
		ymaps.ready(ymap_geo = setInterval(setGeo, 1000));
}
 });

$("#chatPopBtn").bind('click', function() {
         $("#chatPop").toggle();
});
function setGeo() {
    var geolocation = ymaps.geolocation;
	if (geolocation&&geolocation.country) {
	console.log(geolocation.country)
	if(presentation_config.preview==0)socket.emit('setgeo',{'linkid':presentation_config.linkid,'country':geolocation.country,'city':geolocation.city});

	}
	clearInterval(ymap_geo)
}

 function getActivePageId() {
    var activeSlide = $('.active');

   page= activeSlide.first().find('img').attr('id');
        console.log(page);
   return page;
}

$("#phoneform").submit(function(e)
{
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax(
    {
        url : BASE+'view/phone-contact',
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          if(data=='success'){

        	 $("#phone").modal("hide");
   		 	 $("#successmessage").modal("show");
   		 }else{
   		 	alert('Заполните все поля');
   		 }

        },
    });
    e.preventDefault();
});
$("#messageform").submit(function(e)
{
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax(
    {
        url : BASE+'view/message-contact',
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {

         if(data=='success'){
    $("#message").modal("hide");
    $("#successmessage").modal("show");
   		 }else{
   		 	alert('Заполните все поля');
   		 }
        },
    });
    e.preventDefault();
});
$("#displayInfoBtn").bind('click', function() {
          socket.emit('myinfo',{sid:presentation_config.room});
});
$( "#endBtn" ).click(function() {
  socket.emit('end room',presentation_config);
  ClosePresentation()
});
$( ".exchangeDoc").bind('click', function() {
    $.ajax({
       type: "POST",
       url: BASE+"documents/doc-list",
       cache: false
    }).done(function(data) {
        $("#selectDoc").modal('show');
       $("#selectDoc").find('.modal-body').html(data);
    });

});
$( "#managerChat").bind('click', function() {
     $("#chatPop").toggle();

});
function SelectDoc(id)
{
      socket.emit('changedoc',id);

}



//$('#viewer').find('*').unbind('click');
//pointer fncs
$('#viewer').bind( "click",   function(e)  {
    e.stopImmediatePropagation()
    e.preventDefault()
  var elm = $(this);
    var xPos = e.pageX - elm.offset().left;
    var yPos = e.pageY - elm.offset().top;
    console.log(e.offsetX,e.offsetY);
    console.log(xPos, yPos);
        if ($("#enablepointer").hasClass('active-point')) {


           $("#dragpointer").css({'z-index':'9999','position': 'absolute','top': e.clientY+10, 'left' : e.clientX+5})
           $("#dragpointer").show()
           changePointer()
        }

})
$( "#enablepointer" ).click(function() {
  var $this = $(this);
        if (!$this.hasClass('active-point')) {
            $this.addClass('active-point');
        }
        else
        {
        	  $this.removeClass('active-point');
        	  $("#dragpointer").hide()
        	  closePointer()
        }

});
function changePointer()
{
    console.log('change pointer');
    clearInterval(pointTime)
    var elm = $('#dragpointer');
    var xPos =elm.offset().left;
    var yPos =elm.offset().top;
    var pointer={'yPos':yPos,'xPos':xPos}
   if(presentation_config.isManager==1)socket.emit('set pointer',pointer);
   pointTime = setInterval(clearPointer, 5000)
}
function clearPointer()
{
    clearInterval(pointTime)
    socket.emit('close pointer');
}
function closePointer()
{
    socket.emit('close pointer');
}



document.addEventListener('pagechange', function(e) {
  if (e.pageNumber !== e.previousPageNumber) {
    console.log('page changed from ' + e.previousPageNumber + ' to ' + e.pageNumber);
    presentation_config.pageid=e.pageNumber;
    if(update_stats==1)socket.emit('pagestat',presentation_config);
      socket.emit('changepage',presentation_config);

  }
});


function ClosePresentation()
{
    $.ajax({
       type: "POST",
       url: BASE+"/view/close-presentation"
    });

}
function UpdateDoc(id)
{
     $.ajax({
       type: "POST",
       url: BASE+"view/doc-link?id="+id,
       cache: false
    }).done(function(data) {
     $("#selectDoc").modal('hide');
     update_stats=0;
        PDFViewerApplication.open(data)
        PDFViewerApplication.pdfViewer.currentPageNumber=1;
        $('#exchangeDocEmpty').hide()
         if(presentation_config.isTranslation==1&&presentation_config.isManager==1) SaveDoc(id);
    });

}
function SaveDoc(id)
{

               $.ajax({
       type: "POST",
       url: BASE+"/view/save-doc-presentation",
       data: {docId : id,presId:PresentationId},
       cache: false
    });

}