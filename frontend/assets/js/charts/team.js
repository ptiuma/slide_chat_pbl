
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  runTab()
});
$('#statTbs').on('tabsX.error', function (event, data, status, jqXHR) {
    console.log('tabsX.beforeSend event');
});
function runTab()
{	      var activeTabIdx = $('#statTbs li.active').index();
    $('#statTbs-tab'+activeTabIdx).html('<i class=\"fa fa-spinner fa-spin\"></i>')
    var from_date=$('[name=from_date]').val()
	var to_date=$('[name=to_date]').val()
    $.ajax({
       type: "POST",
         dataType: 'json',
       url: BASE+"statistics/tabs-data",
       data: {type : activeTabIdx+1,from_date:from_date,to_date:to_date},
       cache: false
    }).done(function(data) {
         $('#statTbs-tab'+activeTabIdx).html(data)
    });
}
function displayCharts(type,data)
{

        if(type=='3')
        {        PieChart(type,data.pie)
        ColumnChart(type,data.column)
        }
        else
        {
        PyramidChart(type,data.pyramid)
        type==2?LineChartLinks(type,data.line):LineChart(type,data.line)
        }

}
function ColumnChart(type,data)
{

			AmCharts.makeChart("chartdivcolumn"+type,
				{
					"type": "serial",
					"colors": [
						"#7DC5DF",
						"#EB7272",

					],
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] c шабл. [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Отослано",
							"type": "column",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[title]] с шабл. [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-2",
							"title": "Открыто",
							"type": "column",
							"valueField": "column-2"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Кол-во"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Соотношение отосланных и открытых писем в привязке к шаблонам"
						}
					],
					"dataProvider": data
				}
			);
}
function PieChart(type,data)
{

	var pie=AmCharts.makeChart("chartdivpie"+type,
				{
				"type": "pie",
					"angle": 12,
					"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"depth3D": 15,
					"titleField": "category",
					"valueField": "column-1",
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"align": "center",
						"markerType": "circle"
					},
					"titles": [],
					"dataProvider": data
				}
			);
}
function PyramidChart(type,data)
{	var column=AmCharts.makeChart("pyramidchartdiv"+type,{
					"type": "funnel",
					"balloonText": "[[title]]:<b>[[value]]</b>",
					"labelFunction":formatValue,
					"labelPosition": "right",
					"colors": [
		"#E8815E",
		"#73D2C9",
		"#A36CD9",
		"#FCD202"
	],
					//"rotate": true,
					  "depth3D":100,
 					 "angle":40,
  					"outlineAlpha":1,
					"marginLeft": 15,
					"marginRight": 160,
					"titleField": "title",
					"valueField": "value",
					"allLabels": [],
					"balloon": {},
					"titles": [],
					"dataProvider": data

				});
}
function formatValue(value, formattedValue, valueAxis){


 percent=percentCalculation(total_sent, value.value);
        return formattedValue+"\n "+percent+"%";

}
function percentCalculation(a, b){
  var c = b*100/a;
  return c.toFixed(2);
}
function LineChart(type,data)
{
		   AmCharts.makeChart("chartdiv"+type, {
	"type": "serial",
	"categoryField": "category",
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start"
	},
	"trendLines": [],
	    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "AmGraph-1",
        "scrollbarHeight": 40
    },
	"graphs": [
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "round",
			"id": "AmGraph-1",
			"title": "Отправлено писем",
			"valueField": "column-1"
		},
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "square",
			"id": "AmGraph-2",
			"title": "Открыто писем",
			"valueField": "column-2"
		},
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "square",
			"id": "AmGraph-3",
			"title": "Открыто вложений",
			"valueField": "column-3"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"title": "Кол-во"
		}
	],
	"allLabels": [],
	"balloon": {},
	"legend": {
		"enabled": true,
		"useGraphSettings": true
	},
	"titles": [
		{
			"id": "Title-1",
			"size": 15,
			"text": "Динамика отправки и открываемости"
		}
	],
	"dataProvider": data
});
}
function LineChartLinks(type,data)
{
		   AmCharts.makeChart("chartdiv"+type, {
	"type": "serial",
	"categoryField": "category",
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start"
	},
	"trendLines": [],
	    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "AmGraph-1",
        "scrollbarHeight": 40
    },
	"graphs": [
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "round",
			"id": "AmGraph-1",
			"title": "Отправлено ссылок",
			"valueField": "column-1"
		},
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "square",
			"id": "AmGraph-2",
			"title": "Открыто ссылок",
			"valueField": "column-2"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"title": "Axis title"
		}
	],
	"allLabels": [],
	"balloon": {},
	"legend": {
		"enabled": true,
		"useGraphSettings": true
	},
	"titles": [
		{
			"id": "Title-1",
			"size": 15,
			"text": "Динамика отправки и открываемости"
		}
	],
	"dataProvider": data
});
}
