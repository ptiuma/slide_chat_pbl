$('#statTbs').on('tabsX.click', function (event, jqXHR, status, message) {
   //var activeTabIdx = $('#statTbs li.active').index();
   // alert(JSON.stringify(event));
     //    alert(this.id);
    //runTab()


});
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  runTab()
});
$('#statTbs').on('tabsX.error', function (event, data, status, jqXHR) {
    console.log('tabsX.beforeSend event');
});
function runTab()
{	      var activeTabIdx = $('#statTbs li.active').index();
    $('#statTbs-tab'+activeTabIdx).html('<i class=\"fa fa-spinner fa-spin\"></i>')
    var from_date=$('#dynamicmodel-from_date').val();
	var to_date=$('#dynamicmodel-to_date').val();
	var teamid=$('#dynamicmodel-teamid').val();
	var manager_data=$('input[type=radio]:checked').val();
    $.ajax({
       type: "POST",
         dataType: 'json',
       url: BASE+"statistics/tabs-data",
       data: {type : activeTabIdx+1,from_date:from_date,to_date:to_date,teamid:teamid,manager_data:manager_data},
       cache: false
    }).done(function(data) {
         $('#statTbs-tab'+activeTabIdx).html(data)
    });
}
function displayCharts(type,data)
{

        if(type=='4')
        {        PieChart(type,data.pie)
        ColumnChart(type,data.column)
        }
        else
        {
        //PyramidChart(type,data.pyramid)
        if(type==3)LineChartLinks(type,data.line)
        else LineChart(type,data.line)
        //if(type==1)
        LineChartHour(type,data.hour)
        }

}
function runCharts(type)
{

var from_date=$('[name=from_date]').val()
var to_date=$('[name=to_date]').val()
$.ajax({
       type: "POST",
         dataType: 'json',
       url: "statistics/stat-data",
       data: {type : type,from_date:from_date,to_date:to_date},
       cache: false
    }).done(function(data) {
   //alert(data.line)
        PyramidChart(type,data.pyramid)
        if(type==2)LineChartLinks(type,data.line)
        else LineChart(type,data.line)
        LineChartHour(type,data.hour)
    });
}
function ColumnChart(type,data)
{

			AmCharts.makeChart("chartdivcolumn"+type,
				{
					"type": "serial",
					//"colors": [
					//	"#7DC5DF",
					//	"#EB7272",
					//],
					"categoryField": "category",
										"angle": 30,
					"depth3D": 30,
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] c шабл. [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Отослано",
							"type": "column",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[title]] с шабл. [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-2",
							"title": "Открыто писем",
							"type": "column",
							"valueField": "column-2"
						},
						{
							"balloonText": "[[title]] с шабл. [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-3",
							"title": "Открыто вложений",
							"type": "column",
							"valueField": "column-3"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Кол-во"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Соотношение отосланных и открытых писем и вложений в привязке к шаблонам"
						}
					],
					"dataProvider": data
				}
			);
}
function PieChart(type,data)
{

	var pie=AmCharts.makeChart("chartdivpie"+type,
				{
				"type": "pie",
					"angle": 12,
					"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"depth3D": 15,
					"titleField": "category",
					"valueField": "column-1",
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"align": "center",
						"markerType": "circle"
					},
					"titles": [],
						"theme": "light",
					"dataProvider": data
				}
			);
}
function PyramidChart(type,data)
{	var column=AmCharts.makeChart("pyramidchartdiv"+type,{
					"type": "funnel",
					"balloonText": "[[title]]:<b>[[value]]</b>",
					"labelFunction":formatValue,
					"labelPosition": "right",
					"colors": [
		"#ED7D31",
		"#B091ED",
		"#F0EE03",
		"#A9D18E"
	],
					//"rotate": true,
					//  "depth3D":100,
 					 "angle":40,
  					"outlineAlpha":1,
					"marginLeft": 15,
					"marginRight": 160,
					"titleField": "title",
					"valueField": "value",
					"allLabels": [],
					"balloon": {},
					"titles": [],
						"fontSize": 13,
						"outlineThickness": 7,
						"valueRepresents": "area",
						"theme": "light",
					"dataProvider": data

				});
}
function formatValue(value, formattedValue, valueAxis){

 //alert(value.value)
 //console.debug(value);
 percent=percentCalculation(total_sent, value.value);
        return formattedValue+"\n "+percent+"%";

}
function percentCalculation(a, b){
  var c = b*100/a;
  return c.toFixed(2);
}
function LineChart(type,data)
{
		   AmCharts.makeChart("chartdiv"+type, {
	"type": "serial",
	"categoryField": "category",
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start"
	},
	"trendLines": [],
	    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "AmGraph-1",
        "scrollbarHeight": 40
    },
	"graphs": type==2?[
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "round",
			"id": "AmGraph-1",
			"title": "Отправлено писем",
			"valueField": "column-1"
		},
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "square",
			"id": "AmGraph-2",
			"title": "Открыто писем",
			"valueField": "column-2"
		}
	]:[
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "round",
			"id": "AmGraph-1",
			"title": "Отправлено писем",
			"valueField": "column-1"
		},
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "square",
			"id": "AmGraph-2",
			"title": "Открыто писем",
			"valueField": "column-2"
		},
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "square",
			"id": "AmGraph-3",
			"title": "Открыто вложений",
			"valueField": "column-3"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"title": "Кол-во"
		}
	],
	"allLabels": [],
	"balloon": {},
	"legend": {
		"enabled": true,
		"useGraphSettings": true
	},
	"titles": [
		{
			"id": "Title-1",
			"size": 15,
			"text": "Динамика отправки и открываемости"
		}
	],
	"dataProvider": data
});
}
function LineChartHour(type,data)
{
	   AmCharts.makeChart("chartdivhour"+type, {
	"type": "serial",
	"categoryField": "category",
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start"
	},
	"trendLines": [],
	    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "AmGraph-1",
        "scrollbarHeight": 40
    },
	"graphs": type==3?[
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "square",
			"id": "AmGraph-2",
			"title": "Открыто ссылок",
			"valueField": "column-1"
		},
	]:type==2?[
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "square",
			"id": "AmGraph-2",
			"title": "Открыто писем",
			"valueField": "column-1"
		}

	]:[
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "square",
			"id": "AmGraph-2",
			"title": "Открыто писем",
			"valueField": "column-1"
		},
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "square",
			"id": "AmGraph-3",
			"title": "Открыто вложений",
			"valueField": "column-2"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"title": "Кол-во"
		}
	],
	"allLabels": [],
	"balloon": {},
	"legend": {
		"enabled": true,
		"useGraphSettings": true
	},
	"titles": [
		{
			"id": "Title-1",
			"size": 15,
			"text": type==2?"Статистика по времени открытия ссылок":"Зависимость просмотра вложений относительно времени открытия письма"
		}
	],
	"dataProvider": data
});
}
function LineChartLinks(type,data)
{
		   AmCharts.makeChart("chartdiv"+type, {
	"type": "serial",
	"categoryField": "category",
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start"
	},
	"trendLines": [],
	    "chartScrollbar": {
        "autoGridCount": true,
        "graph": "AmGraph-1",
        "scrollbarHeight": 40
    },
	"graphs": [
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "round",
			"id": "AmGraph-1",
			"title": "Отправлено ссылок",
			"valueField": "column-1"
		},
		{
			"balloonText": "[[title]]  [[category]]:[[value]]",
			"bullet": "square",
			"id": "AmGraph-2",
			"title": "Открыто ссылок",
			"valueField": "column-2"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"title": "Axis title"
		}
	],
	"allLabels": [],
	"balloon": {},
	"legend": {
		"enabled": true,
		"useGraphSettings": true
	},
	"titles": [
		{
			"id": "Title-1",
			"size": 15,
			"text": "Динамика отправки и открываемости"
		}
	],
	"dataProvider": data
});
}
$('input[type=radio]').on('change', function() {	   runTab()
});
