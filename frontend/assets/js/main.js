var clipboard = new Clipboard('.btn-copy');
//$('#btn-copy').tooltip();

clipboard.on('success', function(e) {
 	$(e.trigger).attr('title', 'Скопировано').tooltip('fixTitle').tooltip('show');
});


$('body').on('click',".task-archive", function(e) {
 e.preventDefault();
url=$(this).attr('href');
 $.ajax({
       type: "GET",
       url: url
    }).done(function(data) {
	   isHistory=='1'?window.location.reload():$.pjax({container:"#tasksListGrid"});
    });

});
$('body').on('click',".view-letter-stat", function(e) {
 e.preventDefault();
	   var button = $(this)
        var modal = $('#letterStat')
        modal.modal('show')
        var id = button.data('viewid')
        if(!id)var id = button.attr('href')
        var href =BASE+'documents/view-stats?id='+id
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data);
                alert(amchartConf)
            });
});
$('.create-presentation-link').on( "click", function(e) {
 e.preventDefault();
	   var button = $(this)
        var modal = $('#createPresentation')
        modal.modal('show')
          var docId = button.data('docid')
        if(docId)var href =BASE+'documents/create-presentation?docId='+docId
        else var href =BASE+'documents/create-presentation'
        $.post(href)
            .done(function( data ) {
                $('#presname').val(data);
                $('#preslink').attr("href",data)
            });
});
      $(document).on('click', '.showModalButton', function(e){
 		e.preventDefault();
        if ($('#modal').data('bs.modal').isShown) {
            $('#modal').find('#modalContent')
                    .load($(this).attr('href'));
            document.getElementById('modalHeader').innerHTML = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><b>' + $(this).attr('title') + '</b>';
        } else {
                    $('#modal').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('href'));
            document.getElementById('modalHeader').innerHTML = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><b>' + $(this).attr('title') + '</b>';
        }
    });