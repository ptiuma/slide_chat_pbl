var attachList=[];
var attachListT=[];
$(document).ready(function () {


 $(document).on('pjax:complete', function() {
      //$('#success').modal('show')
});

$('#createTemplate').on('hidden.bs.modal', function () {
    console.log('d')
     //tinymce.('usertemplates-email_body').remove();
     if(typeof(tinyMCE) !== 'undefined') {
  var length = tinyMCE.editors.length;
      tinyMCE.editors[length-1].remove();
}
})
$('body').on('submit','#emailForm', function( event ) {
  event.preventDefault();
  var formData = $('#emailForm').serialize() ;
 // alert(formData)
        $.ajax({
            type: "POST",
            url: "send",
            data: formData,
            success: function(data){
		            if(data.failure==true)
		            {            	         $.notify("<p>"+data.message+"</p>",{'body':'','type':'danger'});
		            }
		            else
		            {
						$('#success').modal('show')
						event.preventDefault();
		            }
            },
            error: function(){
            //alert("failure");
            }
        });
    return false;
});

$('body').on('click', '.create-template',function() {	 $('#success').modal('hide')
	   var button = $(this)
        var modal = $('#createTemplate')
        var id = $("#letters-templateid" ).val()
        var href =BASE+'email/create-template?id='+id
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data);
                attachListT=[]
                setTemplate(id)

            });
});
$('body').on('click', '.select-cloud', function () {
	   var button = $(this)
        var modal = $('#cloud')
        var isTempl = button.data('istempl')
        var href =BASE+'email/select?isTempl='+isTempl
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data);
            });
});

  $('.modal').on('submit', '#editTemplateForm', function( event ) {
  event.preventDefault();
      var formData = $('#editTemplateForm').serialize() ;
       $.ajax({
            type: "POST",
            url: BASE+"email/create-template",
            data: formData,
            success: function(msg){
               $('#createTemplate').modal('hide')
               $('.modal-backdrop').remove();
                updateListTemplates()
            $.notify("<p>Шаблон обновлён</p>",{'body':'','type':'success'});
             //$.pjax.reload({container:'#emailForm'});
            },

        });
    return false;
});


 });

$('body').on('click', '.delete-template',function() {
     $.ajax({
       type: "POST",
               dataType: "json",
       url: BASE+"email/delete-template",
       data: {id : $("#letters-templateid" ).val()},
       cache: false
    }).done(function(data) {
                    $.notify("<p>Шаблон удалён</p>",{'message':'','type':'success'});
                    $.pjax.reload({container:'#emailForm'});
		});
});

$( "#letters-templateid" ).change(function() {
     $.ajax({
       type: "POST",
        dataType: "json",
       url: BASE+"email/template-data",
       data: {id : $("#letters-templateid" ).val()},
       cache: false
    }).done(function(data) {
  //  alert(JSON.stringify(data));
            $("#tplR").show();
   			$("#letters-email_subject" ).val(data.email_subject);
              tinyMCE.get('letters-email_body').setContent(data.email_body);
            //$('#letters-email_body').html( data.email_body);
            $.each(data.AttachmentCollection, function( key, value ) {
                if(value)insertAttachment(value)
			});

    });
});
$('#doc').on('fileuploaded', function(event, data) {
    var response = data.response;
        if(response.success==1)
        {         insertAttachment(response.docId)
        }
        else
        {//			  alert(JSON.stringify(data));
			alert('Ошибка при загрузке файла.\nВозможно, некорректный формат');
        }
});
$('#docTemplate').on('fileuploaded', function(event, data) {
        var response = data.response;
        if(response.success==1)
        {
         insertAttachment(response.docId,1)
        }
        else
        {
			alert('Ошибка при загрузке файла.\nВозможно, некорректный формат');
        }
});


 function insertAttachment(id,isTempl)
 {   removeAttachment(id,isTempl)
   $.ajax({
       type: "POST",
       url: BASE+"email/attach-content",
       data: {id : id,isTempl:isTempl},
       cache: false
    }).done(function(data) {
       if(isTempl==1)
       {
             $(data).hide().appendTo('#attachesT').slideDown(400); attachListT.push(id);$("#usertemplates-attachmentcollection").val(attachListT);
       }else
       {               console.log(attachList)
               $(data).hide().appendTo('#attaches').slideDown(400); attachList.push(id);$("#letters-attachments").val(attachList);
                 console.log(attachList)
       }
    });

 }
 function removeAttachment(id,isTempl)
 {
     if(isTempl==1)
     {
        attachListT=removeFromArray(id,attachListT);     	$('#attachT_'+id).remove();$("#usertemplates-attachmentcollection").val(attachList);
     }
     else
     {
        attachList=removeFromArray(id,attachList);
     	$('#attach_'+id).remove();$("#letters-attachments").val(attachList);
     }

 }
 function setTemplate(id)
 {         $("#usertemplates-email_subject" ).val($("#letters-email_subject" ).val());
            tinyMCE.get('usertemplates-email_body').setContent(tinyMCE.get('letters-email_body').getContent());
            var at=attachList;
            $.each(at, function( key, value ) {
                insertAttachment(value,1)
			});

 }
 function removeFromArray(id, array){
 array = $.grep(array , function(value) {
                      return value != id;
                });
   return array;
}

function updateListTemplates()
{    $.ajax({
        type: "POST",
        url: BASE+"email/list-templates",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            $("#letters-templateid").empty();

            $.each(data, function (i, item) {
              // alert(item.id)
               $("#letters-templateid").append($("<option></option>").val(item.id).html(item.name));
            });
        }
    });
}


$('#successRegister').on('shown.bs.modal', function() {
         $.post('register-alert')
            .done(function( data ) {
                $('#successRegister').find('.modal-body').html(data);
            });
});


$('#doc').on('filebatchselected', function(event) {
$('#doc').fileinput('upload');
    console.log("change");
});
$('#docTemplate').on('filebatchselected', function(event) {
$('#docTemplate').fileinput('upload');

});

$('body').on('submit','#mailingForm', function( event ) {
  event.preventDefault();
  var formData = new FormData($('form')[0]);
  $("#spinner").show();
 // alert(formData)
        $.ajax({
            type: "POST",
            url: "send",
            data: formData,
                   cache: false,
        contentType: false,
        processData: false,
            success: function(data){
              $("#spinner").hide();
		            if(data.failure==true)
		            {
            	         $.notify("<p>"+data.message+"</p>",{'body':'','type':'danger'});
		            }
		            else
		            {
						$('#success').modal('show')
						event.preventDefault();
		            }
            },
            error: function(){
            //alert("failure");
            }
        });
    return false;
});
$( "#letters-mailing_use_smtp").change(function() {
   if($("#letters-mailing_use_smtp").val()=='1') $("#letters-mailing_from").prop("readonly", false);
   else  $("#letters-mailing_from").prop("readonly", true);
});

