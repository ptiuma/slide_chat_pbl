var pointTime;
var sessionid;
$(document).ready(function () {
			if(RunPresentation==1)
			{
					    socket = io.connect(SOCKET_SERVER+':3000/');

					    socket.on('connect', function () {
					        socket.emit('create room',presentation_config);
					    });
						socket.on('refreshchat', function (data) {
							$("#chatPop").show();
       						reloadchat('', false);
						});
		   				socket.on('myinfo', function (data) {
								$("#main-chat").show();
								});
						socket.on('updateslide', function (data) {
						        $("#activePage").attr('src',data);
						});

						socket.on('closepointer', function (data) {
						        $("#dragpointer").hide(4000)
						});
						socket.on('updatepointer', function (data) {
						data.xPos=presentation_config.isManager==1?data.xPos:data.xPos-120;
						         $("#dragpointer").css({'z-index':'9999','position': 'absolute','top': data.yPos, 'left' : data.xPos})
           						 $("#dragpointer").show()
						});

						if(presentation_config.isManager==1)
						{
								socket.on('updateusers', function (data) {
								    $("#users-num").html(data);
								});
						}


    						  socket.on('endpresent', function (data) {
								    $("#endPresent").modal('show');
								    if(presentation_config.isManager==1)
								    {								    	 $("#activePage").attr('src','');
								    	 $("#pages-block").html('');
								    }
								});



			}
 });

$( "#endBtn" ).click(function() {
  socket.emit('end room',presentation_config);
  ClosePresentation()
});
$( ".exchangeDoc" ).click(function() {
    $.ajax({
       type: "POST",
       url: BASE+"/documents/doc-list",
       cache: false
    }).done(function(data) {
        $("#selectDoc").modal('show');
       $("#selectDoc").find('.modal-body').html(data);
    });

});
$( "#enablepointer" ).click(function() {
  var $this = $(this);
        if (!$this.hasClass('active-point')) {
            $this.addClass('active-point');
        }
        else
        {        	  $this.removeClass('active-point');
        	  $("#dragpointer").hide()
        	  closePointer()
        }
});
$( ".item" ).click(function(e) {

  var elm = $(this);
    var xPos = e.pageX - elm.offset().left;
    var yPos = e.pageY - elm.offset().top;

  //  console.log(xPos, yPos);
        if ($("#enablepointer").hasClass('active-point')) {

           $("#dragpointer").css({'z-index':'9999','position': 'absolute','top': yPos+30, 'left' : xPos+270})
           $("#dragpointer").show()
           changePointer()
        }
        else
        {
            $("#dragpointer").hide(4000);
            closePointer()
        }
});
function SelectDoc(id)
{
    $.ajax({
       type: "POST",
       url: BASE+"/documents/pages-list",
       data: {id : id},
       cache: false
    }).done(function(data) {
        $("#pages-block").html(data);
        $("#selectDoc").modal("hide");
        getFirstPage()
    });

}

function getFirstPage()
{	el=$(".page-item:first");
	page=el.data("pageid");
	changePage(page)
}
function changePage(id)
{    img=$("#page"+id).data("imgsrc");
    presentation_config.activeImg=img
    socket.emit('set slide',presentation_config);
    SavePage(id);
}
function changePointer()
{
    	clearInterval(pointTime)
    var elm = $('#dragpointer');
    var xPos =elm.offset().left;
    var yPos =elm.offset().top;
    var pointer={'yPos':yPos,'xPos':xPos}
   socket.emit('set pointer',pointer);
   pointTime = setInterval(clearPointer, 5000)
}
function clearPointer()
{
        clearInterval(pointTime)
    socket.emit('close pointer');
}
function closePointer()
{
    socket.emit('close pointer');
}
function SavePage(id)
{
    $.ajax({
       type: "POST",
       url: BASE+"/view/page-presentation",
       data: {pageId : id,presId:PresentationId},
       cache: false
    });

}
function ClosePresentation()
{
    $.ajax({
       type: "POST",
       url: BASE+"/view/close-presentation"
    });

}


$("#chatPopBtn").bind('click', function() {
         $("#chatPop").toggle();
});