var ymap_geo;
var pointTime;
var sessionid;
$(document).ready(function () {
      $(".item img").panzoom({
          $zoomIn: $(".zoom-in"),
          $zoomOut: $(".zoom-out"),
          $zoomRange: $(".zoom-range"),
          $reset: $(".reset")
      });


if(RunPresentation==1)
{

   socket = io.connect(SOCKET_SERVER+':3000/');
		    socket.on('connect', function () {
			    socket.emit('create room',presentation_config);
			    if(presentation_config.preview==0)socket.emit('linkstat',presentation_config);
		        presentation_config.pageid=getActivePageId();
		        if(presentation_config.preview==0)socket.emit('pagestat',presentation_config);
			  //  sessionid=socket.io.engine.id;
			  //  console.log(sessionid);

		    });
         socket.on('refreshchat', function (data) {
					$("#chatPop").show();

				    reloadchat('', false);
								});
		   socket.on('myinfo', function (data) {
					$("#main-chat").show();
								});

		$( ".downloads" ).click(function() {
		   if(presentation_config.preview==0)socket.emit('downloads',presentation_config);
		});
		$( ".print" ).click(function() {
		   if(presentation_config.preview==0)socket.emit('print',presentation_config);
		   window.print()
		   //w=window.open();
			//w.document.write($('.active').html());
			//w.print();
			//w.close();

			return false;
		});
								socket.on('changepage', function (data) {

						        if(data!=getActivePageId())
						        {						        	// alert(data+' ='+getActivePageId());
						        }
						});
								socket.on('closepointer', function (data) {
						        $("#dragpointer").hide(3000)
						});
						socket.on('updatepointer', function (data) {
							data.xPos=presentation_config.isManager==1?data.xPos:data.xPos-0;
						         $("#dragpointer").css({'z-index':'9999','position': 'absolute','top': data.yPos, 'left' : data.xPos})
           						 $("#dragpointer").show()
						});


								socket.on('updateusers', function (data) {
								if(presentation_config.preview==1)
								{
  								   if(data<='0')
								   {
								    $("#isClientOnline").addClass('btn-danger')
								    $("#isClientOnlineSt").html('Клиент оффлайн');
								   }
								 }
								});


		$('#viewer').bind('slide.bs.carousel', function (e) {

		    presentation_config.pageid=getActivePageId();
		     socket.emit('changepage',presentation_config);
		     if(presentation_config.preview==0)socket.emit('pagestat',presentation_config);

		});
$(window).on('beforeunload', function(){
    socket.close();
});

		ymaps.ready(ymap_geo = setInterval(setGeo, 1000));
}
 });

$("#chatPopBtn").bind('click', function() {
         $("#chatPop").toggle();
});
function setGeo() {
    var geolocation = ymaps.geolocation;
	if (geolocation&&geolocation.country) {
	console.log(geolocation.country)
	if(presentation_config.preview==0)socket.emit('setgeo',{'linkid':presentation_config.linkid,'country':geolocation.country,'city':geolocation.city});

	}
	clearInterval(ymap_geo)
}

 function getActivePageId() {
    var activeSlide = $('.active');

   page= activeSlide.first().find('img').attr('id');
        console.log(page);
   return page;
}

$("#phoneform").submit(function(e)
{
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax(
    {
        url : 'phone-contact',
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {
          if(data=='success'){
        	 $("#phone").modal("hide");
   		 	 $("#successmessage").modal("show");
   		 }else{   		 	alert('Заполните все поля');
   		 }

        },
    });
    e.preventDefault();
});
$("#messageform").submit(function(e)
{
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax(
    {
        url : 'message-contact',
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR)
        {

         if(data=='success'){
    $("#message").modal("hide");
    $("#successmessage").modal("show");
   		 }else{
   		 	alert('Заполните все поля');
   		 }
        },
    });
    e.preventDefault();
});
$("#displayInfoBtn").bind('click', function() {
          socket.emit('myinfo',{sid:presentation_config.room});
});
//pointer fncs
$( ".item" ).click(function(e) {

  var elm = $(this);
    var xPos = e.pageX - elm.offset().left;
    var yPos = e.pageY - elm.offset().top;

  //  console.log(xPos, yPos);
        if ($("#enablepointer").hasClass('active-point')) {

           $("#dragpointer").css({'z-index':'9999','position': 'absolute','top': yPos+30, 'left' : xPos+360})
           $("#dragpointer").show()
           changePointer()
        }
        else
        {
            $("#dragpointer").hide(4000);
            closePointer()
        }
});
$( "#enablepointer" ).click(function() {
  var $this = $(this);
        if (!$this.hasClass('active-point')) {
            $this.addClass('active-point');
        }
        else
        {
        	  $this.removeClass('active-point');
        	  $("#dragpointer").hide()
        	  closePointer()
        }
});
function changePointer()
{
    	clearInterval(pointTime)
    var elm = $('#dragpointer');
    var xPos =elm.offset().left;
    var yPos =elm.offset().top;
    var pointer={'yPos':yPos,'xPos':xPos}
   socket.emit('set pointer',pointer);
   pointTime = setInterval(clearPointer, 5000)
}
function clearPointer()
{
    clearInterval(pointTime)
    socket.emit('close pointer');
}
function closePointer()
{
    socket.emit('close pointer');
}
