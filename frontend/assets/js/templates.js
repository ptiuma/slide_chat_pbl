var attachList=[];
var attachListT=[];
$(document).ready(function () {
   $("#search-field").keyup( function () {
      //alert('fg');
      $('.dataTable').DataTable().search(this.value).draw()
    } );

     $('.dataTable tbody').on( 'click', 'tr', function () {
         console.log( $(this).attr('id') );
         var id=$(this).attr('id');
         if(!$(this).hasClass( "selected" ))
         {
            $('#tpl-content').hide();         	$('#tpl-preview').show();
         }
         else
         {
            var href =BASE+'/templates/template-data?tplId='+id
		           $.post(href)
		            .done(function( data ) {
		            $('#tpl-content').html(data);
		                 $('#tpl-content').show();
         				 $('#tpl-preview').hide();
		            });
		 }
    } );
     //click sidenav
     $('ul.kv-sidenav li ').click(function(e)
    {
     // alert($(this).data("id"));
     id=$(this).data("id");
      if(id=='all')
      {          	$('.tpl-table').show();
      }
      else
      {
      	$('.tpl-table').hide();
       $('#table-tpl-'+id).show();
      }
    })
       $(document).on('click', '.showModalButton', function(e){
 		e.preventDefault();
                    $('#modal').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('href'));
            document.getElementById('modalHeader').innerHTML = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><b>' + $(this).attr('title') + '</b>';

    });
});


$('body').on('click', '.select-cloud', function () {
	   var button = $(this)
        var modal = $('#cloud')
        var isTempl = button.data('istempl')
        var href =BASE+'/email/select?isTempl='+isTempl
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data);
            });
});


$('#docTemplate').on('fileuploaded', function(event, data) {
        var response = data.response;
        if(response.success==1)
        {
         insertAttachment(response.docId)
        }
        else
        {
			alert('Ошибка при загрузке файла.\nВозможно, некорректный формат');
        }
});


 function insertAttachment(id)
 {
  isTempl=1;
   removeAttachment(id)
   $.ajax({
       type: "POST",
       url: BASE+"/email/attach-content",
       data: {id : id,isTempl:isTempl},
       cache: false
    }).done(function(data) {

             $(data).hide().appendTo('#attachesT').slideDown(400); attachListT.push(id);$("#usertemplates-attachmentcollection").val(attachListT);

    });

 }
 function removeAttachment(id)
 {

        attachListT=removeFromArray(id,attachListT);
     	$('#attachT_'+id).remove();$("#usertemplates-attachmentcollection").val(attachList);

 }
 function removeFromArray(id, array){
 array = $.grep(array , function(value) {
                      return value != id;
                });
   return array;
}
