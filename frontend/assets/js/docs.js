$(document).ready(function () {



$('#successLink').on('hidden.bs.modal', function (e) {
  window.location.reload();
});
  $('#editLink').on( "submit", function( event ) {
  event.preventDefault();
  var formData = $('#editLinkForm').serialize() ;
       $.ajax({
            type: "POST",
            url: "update-link",
            data: formData,
            success: function(msg){
               $('#editLink').modal('hide')
               $('.modal-backdrop').remove();
                $.pjax.reload({container:'#docPanel'});
            },
            error: function(){
            //alert("failure");
            }
        });
    return true;
});

  $('#createLink').on( "submit", function( event ) {
  event.preventDefault();
  var formData = $('#createLinkForm').serialize() ;
       $.ajax({
            type: "POST",
            url: BASE+'documents/create-link',
            data: formData,
            success: function(data){

               $('#createLink').modal('hide')
               $('.modal-backdrop').remove();
               $('#copy-input').val(data);
		        $('#successLink').modal('show');
		         $.pjax.reload({container:'#docPanel'});
            },
            error: function(){
            //alert("failure");
            }
        });
    return true;
});
$('.activity-view-link').click(function() {
	   var button = $(this)
        var modal = $('#editLink')
        var id = button.data('id')
        var href ='update-link?id='+id
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data)
            });
});

$('.create-view-link').on( "click", function() {
	   var button = $(this)
        var modal = $('#createLink')
        var docId = button.data('docid')
        var href =BASE+'documents/create-link?docId='+docId
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>')
        $.post(href)
            .done(function( data ) {
                  modal.find('.modal-body').html(data)
            });
});

$('.team-settings').on( "click", function() {
	   var button = $(this)
        var modal = $('#docTeam')
        var docId = button.data('docid')
        var href ='doc-team?id='+docId
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data)
            });
});
$('body').on('submit', '#docTeamForm', function( event ) {
  event.preventDefault();
  var formData = $('#docTeamForm').serialize() ;
       $.ajax({
            type: "POST",
            url: "doc-team",
            data: formData,
            success: function(data){
               $('#docTeam').modal('hide')
            },
            error: function(){

            }
        });
    return true;
});

});
$('input[type="checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {

});
function blockLink(id,status)
{
    console.log(status)	 $.ajax({
       type: "POST",
       url: "block-link",
       data: {id : id,status:status},
       cache: false
    }).done(function(data) {

    });
}


$('#doc').on('filebatchselected', function(event, files) {
$('#doc').fileinput('upload');
    console.log('File batch selected triggered');
});
$('#doc').on('fileuploaded', function(event, data) {
    var response = data.response;
        if(response.success==1)
        {
           $.notify("<p>Файл успешно закачан</p>",{'message':'','type':'success'});
           $('#doc').fileinput('clear').fileinput('enable');
            $.pjax.reload({container:'#docL'});
        }
        else
        {
			 $.notify("<p>Ошибка при загрузке файла.\nВозможно, некорректный формат файла</p>",{'message':'','type':'warning'});
        }
});

