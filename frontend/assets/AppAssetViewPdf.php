<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 2.0
 */
class AppAssetViewPdf extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/frontend/assets';
    public $css = [
        'css/viewer_pdf.css',
       // 'css/simple-sidebar.css',
        //'js/pdfviewer/web/viewer.css'

    ];
    public $js = [

   		 'js/clipboard.min.js',
        'js/socket/socket.io.js',
        //'js/pdfviewer/web/compatibility.js',
        //'js/pdfviewer/web/l10n.js',
          //  'js/pdfviewer/build/pdf.js',
            //        'js/pdfviewer/web/debugger.js',
              //              'js/pdfviewer/web/viewer.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

