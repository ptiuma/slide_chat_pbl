<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Page',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="page-update">
<div class="page-header">
    </div>
          <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div >Редактировать страницу</div>
                                </div>
                                <div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
                                </div>
                       </div>
</div>
