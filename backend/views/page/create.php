<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = Yii::t('backend', 'Создать страницу', [
    'modelClass' => 'Page',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-create">

<div class="page-header">
    </div>
          <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div >Создать страницу</div>
                                </div>
                                <div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
                                </div>
                       </div>
</div>
