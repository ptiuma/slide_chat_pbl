<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Тарифные планы';
$this->params['breadcrumbs'][] = $this->title;
?>
<p>&nbsp;</p>
<div class="payment-form row">

 <div class="col-md-4">
   <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">План №1</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php $form = ActiveForm::begin([
                'id' => 'plan1'
                ]); ?>

    <?= $form->field($plan1, 'id')->hiddenInput()->label(false) ?>

    <?= $form->field($plan1, 'months')->textInput(['maxlength' => true]) ?>

    <?= $form->field($plan1, 'amount')->textInput(['maxlength' => true]) ?>
    <?= $form->field($plan1, 'top_title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($plan1, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($plan1, 'comments')->textArea(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


            </div>
            <!-- /.box-body -->
          </div>
	</div>



 <div class="col-md-4">
   <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">План №2</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php $form = ActiveForm::begin([
                'id' => 'plan2'
                ]); ?>

    <?= $form->field($plan2, 'id')->hiddenInput()->label(false) ?>

    <?= $form->field($plan2, 'months')->textInput(['maxlength' => true]) ?>

    <?= $form->field($plan2, 'amount')->textInput(['maxlength' => true]) ?>
    <?= $form->field($plan2, 'top_title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($plan2, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($plan2, 'comments')->textArea(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


            </div>
            <!-- /.box-body -->
          </div>
	</div>




 <div class="col-md-4">
   <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">План №3</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php $form = ActiveForm::begin([
                'id' => 'plan3'
                ]); ?>

    <?= $form->field($plan3, 'id')->hiddenInput()->label(false) ?>

    <?= $form->field($plan3, 'months')->textInput(['maxlength' => true]) ?>

    <?= $form->field($plan3, 'amount')->textInput(['maxlength' => true]) ?>
    <?= $form->field($plan3, 'top_title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($plan3, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($plan3, 'comments')->textArea(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


            </div>
            <!-- /.box-body -->
          </div>
	</div>


</div>
