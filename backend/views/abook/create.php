<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var frontend\models\AddressBook $model
 */

$this->title = 'Create Address Book';
$this->params['breadcrumbs'][] = ['label' => 'Address Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-book-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
