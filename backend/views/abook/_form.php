<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var frontend\models\AddressBook $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="address-book-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter ID...', 'maxlength'=>9]],

            'userId'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User ID...']],

            'firstname'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Firstname...', 'maxlength'=>255]],

            'middlename'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Middlename...', 'maxlength'=>255]],

            'lastname'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Lastname...', 'maxlength'=>255]],

            'nickname'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Nickname...', 'maxlength'=>255]],

            'company'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Company...', 'maxlength'=>255]],

            'title'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Title...', 'maxlength'=>255]],

            'address'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Address...','rows'=> 6]],

            'home'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Home...','rows'=> 6]],

            'mobile'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Mobile...','rows'=> 6]],

            'work'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Work...','rows'=> 6]],

            'fax'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Fax...','rows'=> 6]],

            'email'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Email...','rows'=> 6]],

            'homepage'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Homepage...','rows'=> 6]],

            'address2'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Address2...','rows'=> 6]],

            'phone2'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Phone2...','rows'=> 6]],

            'notes'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Notes...','rows'=> 6]],

            'created_at'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Created At...']],

            'updated_at'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Updated At...']],

            'isArchive'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Is Archive...']],

            'addr_long'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Addr Long...','rows'=> 6]],

            'addr_lat'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Addr Lat...','rows'=> 6]],

            'addr_status'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Addr Status...','rows'=> 6]],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
