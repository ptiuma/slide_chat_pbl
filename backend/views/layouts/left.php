<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree','data-widget'=>"tree"],
                'items' => [
                    ['label' => 'Навигация', 'options' => ['class' => 'header']],
                  ['label' => 'Главная панель', 'icon' => 'fa fa-dashboard', 'url' => ['/']],
                  ['label' => 'Пользователи', 'icon' => 'fa fa-file-code-o', 'url' => ['/user/admin/']],
                  ['label' => 'Статические страницы', 'icon' => 'fa fa-edit', 'url' => ['/page']],

                  ['label' => 'Команды', 'icon' => 'fa fa-file-code-o', 'url' => ['/team']],
                [
                        'label' => 'Оплата',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                    'options' => ['class' => 'treeview'],
                        'items' => [
                            ['label' => 'Транзакции', 'icon' => 'fa fa-circle-o text-red', 'url' => ['/payments'],],
                            ['label' => 'Тарифные планы', 'icon' => 'fa fa-circle-o text-light-blue', 'url' => ['/payments/plans'],],

                        ],
                    ],
                ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],'visible'=>YII_DEBUG],
                    ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],'visible'=>YII_DEBUG],

                   [
                        'label' => 'Контроль',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                       'options' => ['class' => 'treeview'],
                        'items' => [
                            ['label' => 'Настройки', 'icon' => 'fa fa-file-code-o', 'url' => ['/key-storage'],],
                            ['label' => 'Просмотр логов', 'icon' => 'fa fa-dashboard', 'url' => ['/site/logger'],],

                        ],
                    ],


                ],
            ]
        ) ?>

    </section>

</aside>
