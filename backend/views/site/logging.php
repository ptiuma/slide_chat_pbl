<?php
use kartik\grid\GridView;
use kartik\widgets\DatePicker;
use yii\helpers\Html;
use common\models\Logger;
/* @var $this yii\web\View */
$this->title = 'Лог событий';
$users=yii\helpers\ArrayHelper::map(\dektrium\user\models\User::find()->asArray()->orderBy('username')->all(),'id','username');

$this->params['breadcrumbs'][] = $this->title;

?>
 <?php
$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],

           ['attribute'=>'id','width'=>'80px'],
             [
                'attribute'=>'category',
                	'format'=>'raw',
                'value'=>function($data){
                   return $data->category;
                },
             /*
             'filterType'=>GridView::FILTER_SELECT2,
   'filter' => $events,
    'filterWidgetOptions'=>[
        'pluginOptions' => ['allowClear' => true],
    ],
    'filterInputOptions' => ['placeholder' => 'Все события'],
    */
    	],
         /*  [
                'attribute'=>'objectType',
                'value'=>function($model){
                    return Logger::getType($model->objectType);
                },

            ],
            [
                'attribute'=>'objectID',
                'value'=>function($model){
                    return $model->objectID;
                },

            ],
           */
            'message',

     [
'attribute' => 'log_time',
 'value'=>function($data){
                   return date('d M Y h:i',$data->log_time);
                },
 'filterType'=> \kartik\grid\GridView::FILTER_DATE_RANGE,
'filterWidgetOptions' => [
'presetDropdown' => true,
'pluginOptions' => [
'format' => 'YYYY-MM-DD',
'locale'=>[
'separator' => ' : ',
            'format'=>'YYYY-MM-DD'
        ],

'opens'=>'left',
] ,
'pluginEvents' => [
"apply.daterangepicker" => "function() { apply_filter('date') }",
]
],
],
        ];

    ?>


    <?php
      echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'pjax' => true, // pjax is set to always true for this demo



    // parameters from the demo form
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    //'showPageSummary' => $pageSummary,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => 'Просмотр событий',

    ],

]);
/*  echo '<label class="control-label">Anniversary</label>';
echo DatePicker::widget([
    'name' => 'anniversary',
    'value' => '08/10/2004',
    'readonly' => true,
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'dd.mm.yyyy'
    ]
]);
*/
  ?>





