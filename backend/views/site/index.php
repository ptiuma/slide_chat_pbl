  <?php
use yii\helpers\Html;
use yii\grid\GridView;
  use common\models\User;
  use common\models\Documents;
    use common\models\Letters;
      use common\models\Links;
        use common\models\LinksViews;
      use common\models\Payment;
/* @var $this yii\web\View */
$this->title = 'Администраторская панель';
 $today=mktime(0, 0, 0, date("m"), date("d"), date("Y"));
 $last_week_start=mktime(0, 0, 0, date("m"), date("d")-7, date("Y"));
?>
<div class="site-index">
    <div class="row">
             <div class="col-md-4">

                                    <table class="table table-condensed">
                                          <tr>
                                            <td>1.</td>
                                            <td>Пользователей</td>
                                            <td><span class="badge bg-red"><?php echo User::find()->count();?></span></td>
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>Всего документов</td>
                                            <td><span class="badge bg-yellow"><?php echo Documents::find()->active()->count();?></span></td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>Писем</td>
                                            <td><span class="badge bg-light-blue"><?php echo Letters::find()->count();?></span></td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>Ссылок</td>
                                            <td><span class="badge bg-light-grey"><?php echo Links::find()->count();?></span></td>
                                        </tr>
                                    </table>

                   </div>
   <div class="col-md-2" style="text-align:center">

   </div>

<div class="col-md-3">
                      <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div >Статистика за неделю</div>
                                </div>
                                <div class="bootstrap-admin-panel-content" style="margin:20px">
                                     <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  Новые пользователи
                  <span class="pull-right badge bg-light-blue"> <?php echo User::find()->where(['between', 'created_at', $last_week_start,$today])->count();?></span>
                </li>
                <li class="list-group-item">
                  Кол-во транзакций
                  <span class="pull-right badge bg-yellow"> <?php echo Payment::find()->where(['isPaid'=>1])->andwhere(['between', 'created_at', $last_week_start,$today])->count();?></span>
                </li>
                <li class="list-group-item">
                  Общая сумма
                  <span class="pull-right badge bg-red"> <?php echo (int)Payment::find()->where(['isPaid'=>1])->andwhere(['between', 'created_at', $last_week_start,$today])->sum('amount');?></span>
                </li>
                </ul>

                </div>
            </div>

</div>
  <div class="col-md-3">
                      <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div >Статистика за сегодня</div>
                                </div>
                                <div class="bootstrap-admin-panel-content" style="margin:20px">
                                       <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  Новые пользователи
                  <span class="pull-right badge bg-light-blue"> <?php echo User::find()->where(['>','created_at',$today])->count();?></span>
                </li>
                <li class="list-group-item">
                  Кол-во транзакций
                  <span class="pull-right badge bg-yellow"> <?php echo Payment::find()->where(['isPaid'=>1])->andwhere(['>','created_at',$today])->count();?></span>
                </li>
                <li class="list-group-item">
                  Общая сумма
                  <span class="pull-right badge bg-red"> <?php echo (int)Payment::find()->where(['isPaid'=>1])->andwhere(['>','created_at',$today])->sum('amount');?></span>
                </li>
                </ul>

                </div>
            </div>

</div>
    </div>               <div class="row">
                        <div class="col-md-6">

           <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Последние пользователи</div>
                                </div>
                                <div class="bootstrap-admin-panel-content">
                                     <?= GridView::widget([
        'dataProvider' => $userProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

         'username',
         'email',
       'created_at:datetime',
        ],
    ]); ?>

                                </div>
                            </div>
                      </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title">Последние события</div>
                                </div>
                                <div class="bootstrap-admin-panel-content">
                                     <?= GridView::widget([
        'dataProvider' => $logProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
         'category',
            'message',
       'log_time:datetime',
        ],
    ]); ?>

                                </div>
                            </div>
                        </div>
                    </div>
        </div>

</div>
