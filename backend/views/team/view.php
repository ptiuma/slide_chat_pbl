<?php

use kartik\grid\GridView;
use yii\widgets\Pjax;

use kartik\helpers\Html;
use kartik\helpers\Enum;
use kartik\popover\PopoverX;
use kartik\widgets\ActiveForm;
use  yii\helpers\Url;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $model common\models\Team */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-view">

    <h2><?php
         echo Editable::widget([
    'name'=>'name',
        'format' => Editable::FORMAT_BUTTON,
    'asPopover' => true,
    'value' => $this->title,
    'header' => 'Название команды',
    'size'=>'md',
    'options' => ['class'=>'form-control', 'placeholder'=>'Enter  name...']
]);?></h2>
      <div class="row">
            <div class="col-md-6">
        <div class="text-right">


        </div>
	&nbsp;
	<div class="box box-success box-solid">
            			<div class="box-header with-border">
              					<h3 class="box-title">Руководители</h3>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">

 <?= GridView::widget([
        'dataProvider' =>$leaderProvider,
           'pjax' => true,
           'bordered' => false,
            'layout' => '{items}',
            'showPageSummary' => false,
               // 'showCaption' => false,
         //  'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
         'id',
         'username',
         'email',
        /*	[
    'attribute' => 'phone',
    'format' => 'html',
    'value' => function($data) { return $data->profile->phone; },
],
         */
 [
        'class' => '\kartik\grid\ActionColumn',
         'template' => '{my_button}',
         'buttons' => [
    'my_button' => function ($url, $model, $key) {
        return $model->id==Yii::$app->user->id?'<small class="label label-info">Основатель</small>':Html::a('Удалить', ['delete-member', 'id'=>$model->id],['class'=>'btn btn-danger']);
    },
]

    ]
],


    ]); ?>

		            </div>
        		  </div>
            </div>

        <div class="col-md-6">
        <div class="text-right">


        </div>
&nbsp;
		<div class="box box-info box-solid">
            			<div class="box-header with-border">
              					<h3 class="box-title">Менеджеры</h3>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">

 <?= GridView::widget([
        'dataProvider' =>$managerProvider,
           'pjax' => true,
           'bordered' => false,
            'layout' => '{items}',
            'showPageSummary' => false,
               // 'showCaption' => false,
         //  'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
        'id',
         'username',
         'email',
        /*	[
    'attribute' => 'phone',
    'format' => 'html',
    'value' => function($data) { return $data->profile->phone; },
],
         */
 [
        'class' => '\kartik\grid\ActionColumn',
         'template' => '{my_button}',
         'buttons' => [
    'my_button' => function ($url, $model, $key) {
        return $model->id==Yii::$app->user->id?'<small class="label label-info">Основатель</small>':Html::a('Удалить', ['delete-member', 'id'=>$model->id],['class'=>'btn btn-danger']);
    },
]

    ]
],


    ]); ?>

		            </div>
        		  </div>
            </div>

            </div>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div class="row">
     <div class="col-md-12">
		<div class="box box-default box-solid">
            			<div class="box-header with-border">
              					<h3 class="box-title">Ожидают приглашения</h3>

		              			<div class="box-tools pull-right">
		                			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		              			</div>
            			</div>

		            <div class="box-body" style="display: block;">

 <?= GridView::widget([
        'dataProvider' =>$invitesProvider,
           'pjax' => true,
           'bordered' => false,
            'layout' => '{items}',
            'showPageSummary' => false,
               // 'showCaption' => false,
         //  'showHeader'=>false,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => false,
         'columns' =>
         [
        'id',

         'invite_email',

              	[
    'attribute' => 'invite_role',
    'format' => 'html',
    'value' => function($data) { return $data->invite_role==1?'Руководитель':'Менеджер'; },
],
             'created_at:date',
 [
        'class' => '\kartik\grid\ActionColumn',
         'template' => '{my_button}{delete}',
         'buttons' => [
    'my_button' => function ($url, $model, $key) {
        return '';
    },
],
      'urlCreator' => function ($action, $model, $key, $index) {
            if ($action == 'delete') {
             return Url::toRoute(['team/delete-invite', 'id' => $key]);
        }
      }

    ]
],


    ]); ?>

		            </div>
        		  </div>
            </div>

            </div>

