<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('app', 'Команды');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">
    <div class="page-header">

    </div>

    <p>
        <?php /* echo Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Team',
]), ['create'], ['class' => 'btn btn-success'])*/  ?>
    </p>

    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
          'created_at:date',
    	[
	 		'attribute' => 'userId',
	 		  'label'=>'Кем созана',
	 		'format'=>'raw',
		       'value'=>function ($data) {
		        return !$data->user->profile->name?$data->user->email:$data->user->profile->name;
		    },
      ],
     	[
	 		'attribute' => 'numof',
	 		  'label'=>'Кол-во участников',
	 		'format'=>'raw',
		       'value'=>function ($data) {
		        return $data->Members_count;
		    },
      ],
     [
            'class' => '\kartik\grid\ActionColumn',
            'template' => '{view} {delete}',


        ],

        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>true,




        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',
        //   'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>
