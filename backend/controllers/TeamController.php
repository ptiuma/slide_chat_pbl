<?php

namespace backend\controllers;

use Yii;
use common\models\Team;
use common\models\User;
use common\models\TeamMembers;
use frontend\models\TeamInvites;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TeamController implements the CRUD actions for Team model.
 */
class TeamController extends Controller
{
    public function behaviors()
    {
        return [
         'access' => [
            'class' =>  \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Team models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Team::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Team model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
          $model=$this->findModel($id);
      $members = new TeamInvites();
      $name=Yii::$app->request->post('name');
              if(Yii::$app->request->post('hasEditable')&&!empty($name))
        {
        	$model->name=Yii::$app->request->post('name');
        	$model->save();
        	 $out = Json::encode(['output'=>'', 'message'=>'']);
        	 echo $out;
        	 return;
        }
        if ($members->load(Yii::$app->request->post()) && $members->save()) {
                 $members->teamId=$id;
                 $members->userId=Yii::$app->user->id;
                 $members->invite_code=Yii::$app->security->generateRandomString(20);
                 $members->save();
                 $this->SendInvite($model,$members);
                 return $this->redirect(['view', 'id' => $_GET['id']]);
        }
         $leaderProvider = new ActiveDataProvider([
            'query' => User::find()->joinWith('teams',true,'RIGHT JOIN')->where(['teamId' => $id,'member_status'=>1]),
        ]);
          $managerProvider = new ActiveDataProvider([
             'query' => User::find()->joinWith('teams',true,'RIGHT JOIN')->where(['teamId' => $id,'member_status'=>2]),
        ]);
          $invitesProvider = new ActiveDataProvider([
            'query' => TeamInvites::find()->where(['teamId' => $id,'isParsed'=>0]),
        ]);
        return $this->render('view', [
            'model' => $model,
            'members'=> $members,
            'managerProvider'=>$managerProvider,
            'leaderProvider'=>$leaderProvider,
            'invitesProvider'=>$invitesProvider

        ]);
    }


    /**
     * Creates a new Team model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Team;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Team model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Team model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Team the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Team::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

      public function actionDeleteInvite($id)
    {
        $model=TeamInvites::findOne($id);

        	$model->delete();

        return $this->redirect(['team/'.$model->teamId]);
    }


}
