<?php

namespace backend\controllers;

use Yii;
use common\models\Payment;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\PaymentsPlans;
use common\models\PaymentSearch;

/**
 * PaymentsController implements the CRUD actions for Payment model.
 */
class PaymentsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
         'access' => [
            'class' =>  \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Payment model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Payment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Payment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Payment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionPlans()
    {
          $plan1=PaymentsPlans::findOne(1);
          $plan2=PaymentsPlans::findOne(2);
          $plan3=PaymentsPlans::findOne(3);
         // print_r($_POST);
 		if (isset($_POST)&&$_POST['PaymentsPlans']['id']==1&&$plan1->load(Yii::$app->request->post())&&$plan1->save()) {
          Yii::$app->getSession()->setFlash('success', 'Тарифный план обновлён!');
             return $this->redirect(['plans']);
        }
 		elseif (isset($_POST)&&$_POST['PaymentsPlans']['id']==2&&$plan2->load(Yii::$app->request->post())&&$plan2->save()) {
          Yii::$app->getSession()->setFlash('success', 'Тарифный план обновлён!');
             return $this->redirect(['plans']);
        }
 		elseif (isset($_POST)&&$_POST['PaymentsPlans']['id']==3&&$plan3->load(Yii::$app->request->post())&&$plan3->save()) {
          Yii::$app->getSession()->setFlash('success', 'Тарифный план обновлён!');
             return $this->redirect(['plans']);
        }
        return $this->render('plans', [
            'plan1' => $plan1,
            'plan2' => $plan2,
             'plan3' => $plan3,
        ]);
    }
}
